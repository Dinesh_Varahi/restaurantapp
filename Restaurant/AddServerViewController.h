//
//  AddServerViewController.h
//  Restaurant
//
//  Created by HN on 17/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerDetailViewController.h"
#import "NewServer.h"
#import "MVPlaceSearchTextField.h"
#import "Servers.h"
#import "DEMODataSource.h"
#import "Restaurants.h"
#import "HomeViewController.h"
#import "AutocompletionTableView.h"


@protocol AutocompletionTableViewDelegate;

@interface AddServerViewController : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate,PlaceSearchTextFieldDelegate,UITextFieldDelegate,UIAlertViewDelegate,UIGestureRecognizerDelegate, MLPAutoCompleteTextFieldDelegate>
{
    
    __weak IBOutlet UITextField *txtServerName;
    
    NSString *restraurantID;
    
    DEMODataSource *objDemoDataSource;
    
    __weak IBOutlet MVPlaceSearchTextField *txLocation;
    
    RLMResults<Restaurants  *> *allRestaurants;
    
    NSString *restoID;
    
    NSDictionary *serverInfo;
    NSMutableArray *arrNames;
    NSMutableArray *arrCity;
    NSMutableArray *arrIDs;
    
    //__weak IBOutlet UITextField *txtRestaurantName;
    
    __weak IBOutlet UITextField *txtServerDesignation;
    
    __weak IBOutlet UIImageView *profilePic;
}


@property (nonatomic, strong) AutocompletionTableView *autoCompleter;


- (IBAction)addServerButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet MLPAutoCompleteTextField *txtRestaurantName;

- (IBAction)addPhotoPressed:(id)sender;

- (IBAction)selectDesignation:(id)sender;




@end
