//
//  AddServerViewController.m
//  Restaurant
//
//  Created by HN on 17/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "AddServerViewController.h"


@interface AddServerViewController ()
{
    CALayer *border;
    CALayer *locationBorder;
    CGFloat borderWidth ;
    
    NSString *designation;
    NSString *userImageString;
    
    NSInteger serverID;
    
    CLLocationCoordinate2D locationCoord;
    NSString *location;
}
@end

@implementation AddServerViewController

@synthesize autoCompleter = _autoCompleter;


- (AutocompletionTableView *)autoCompleter
{
    if (!_autoCompleter)
    {
        NSMutableDictionary *options = [NSMutableDictionary dictionaryWithCapacity:2];
        [options setValue:[NSNumber numberWithBool:YES] forKey:ACOCaseSensitive];
        [options setValue:nil forKey:ACOUseSourceFont];
        
        _autoCompleter = [[AutocompletionTableView alloc] initWithTextField: _txtRestaurantName inViewController:self withOptions:options];
        _autoCompleter.autoCompleteDelegate = self;
        NSMutableArray *arrResto = [[NSMutableArray alloc]init];
        for (int i =0; i < [allRestaurants count]; i++)
        {
            [arrResto addObject:[[allRestaurants objectAtIndex:i] valueForKey:@"name"]];
        }
        _autoCompleter.suggestionsDictionary = arrResto;//[NSArray arrayWithObjects:@"hostel",@"caret",@"carrot",@"house",@"horse", nil];
    }
    return _autoCompleter;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavigationBar];
    
    allRestaurants = [Restaurants allObjects];
    arrNames = [[NSMutableArray alloc] init];
    arrIDs = [[NSMutableArray alloc] init];
    
    restoID = @"-1";
    objDemoDataSource = [[DEMODataSource alloc]init];
    //[txtRestaurantName addTarget:self.autoCompleter action:@selector(textFieldValueChanged:) forControlEvents:UIControlEventEditingChanged];
    
    UITapGestureRecognizer *tapSelectedImage = [[UITapGestureRecognizer alloc]initWithTarget:self
                                                                                      action:@selector(tappedToSelectImage:)];
    tapSelectedImage.delegate = self;
    [tapSelectedImage setNumberOfTapsRequired:1];
    [tapSelectedImage setNumberOfTouchesRequired:1];
    [profilePic addGestureRecognizer:tapSelectedImage];
    [profilePic setUserInteractionEnabled:YES];
    
//    self.txtRestaurantName.autoCompleteDelegate = self;
//    self.txtRestaurantName.delegate = self;
    
    CALayer *border1 = [CALayer layer];
    CGFloat borderWidth1 = 1.5;
    border1.borderColor = [UIColor blackColor].CGColor;
    border1.frame = CGRectMake(0,self.txtRestaurantName.frame.size.height - borderWidth1 , self.txtRestaurantName.frame.size.width, self.txtRestaurantName.frame.size.height);
    border1.borderWidth = borderWidth1;
    [self.txtRestaurantName.layer addSublayer:border1];
    self.txtRestaurantName.layer.masksToBounds = YES;

    self.txtRestaurantName.autoCompleteTableCellBackgroundColor = [UIColor whiteColor];
    self.txtRestaurantName.autoCompleteShouldHideOnSelection = YES;
    self.txtRestaurantName.autoCompleteShouldHideClosingKeyboard = YES;
    self.txtRestaurantName.borderStyle = UITextGranularityLine;
    self.txtRestaurantName.backgroundColor = [UIColor clearColor];
    
    txtServerName.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"serverName"];
    
    txLocation.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"serverLocation"];
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"serverLocation"] isEqualToString:@""]) {
    txLocation.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"location"];
        
    }
    
    location = txLocation.text;
    
    [self setNeedsStatusBarAppearanceUpdate];
    self.title = @"ADD SERVER";
    
    txLocation.delegate = self;
    txtServerName.delegate = self;
    _txtRestaurantName.delegate = self;
    txtServerDesignation.delegate = self;
    
    
    [self customTextField];
    [self setChangeLocationButton];
    
    [_txtRestaurantName addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    profilePic.layer.cornerRadius = profilePic.frame.size.width/2;
    profilePic.layer.masksToBounds = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(assignDict:)
                                                 name:@"serverDictFetched"
                                               object:nil];
    
}

-(void)textDidChange:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setValue:_txtRestaurantName.text forKey:@"NameOfRestro"];
    
}


//-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
////    textField.textColor = [UIColor whiteColor];
//    if (textField.text.length == 0)
//    {
//        
//    }
//    
//    if (textField == _txtRestaurantName)
//    {
//        [[NSUserDefaults standardUserDefaults] setValue:_txtRestaurantName.text forKey:@"NameOfRestro"];
//    }
//    return YES;
//}

-(void)assignDict:(NSNotification *)notification
{
    if ([notification.name isEqualToString:@"serverDictFetched"])
    {
        serverInfo = notification.userInfo;
        arrNames = [serverInfo valueForKey:@"name"];
        arrCity = [serverInfo valueForKey:@"city"];
        arrIDs = [serverInfo valueForKey:@"_id"];
    }
    
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self setNavigationBar];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [self reSetNavigationBar];
}
- (void)viewDidDisappear:(BOOL)animated {
    [self reSetNavigationBar];
}
- (void)setNavigationBar{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    
}
- (void)reSetNavigationBar{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBarTintColor:[UIColor clearColor]];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    
    
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}
- (void)customTextField {
    
    txLocation.borderStyle = UITextBorderStyleNone;
    
    
    border = [CALayer layer];
    locationBorder = [CALayer layer];
    
    borderWidth = 1.5;
    
    border.borderColor = [UIColor grayColor].CGColor;
    locationBorder.borderColor = [UIColor grayColor].CGColor;
    
    border.frame = CGRectMake(0,txLocation.frame.size.height - borderWidth , txLocation.frame.size.width, txLocation.frame.size.height);
    
    border.borderWidth = borderWidth;
    [txLocation.layer addSublayer:border];
    
    
    txLocation.layer.masksToBounds = YES;
}
- (void)setChangeLocationButton {
    UIButton* overlayButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [overlayButton setTitle:@"Change City" forState:UIControlStateNormal];
    [overlayButton addTarget:self action:@selector(changeLocationPressed:)
            forControlEvents:UIControlEventTouchUpInside];
    [overlayButton setFrame:CGRectMake(0, 0, txLocation.frame.size.width/2.5, 28)];
    overlayButton.titleLabel.font = [UIFont systemFontOfSize:14];
    overlayButton.titleLabel.textColor = [UIColor colorWithRed:179/255 green:179/255 blue:179/255 alpha:1];
    [overlayButton setTitleColor:[UIColor colorWithRed:179/255 green:179/255 blue:179/255 alpha:1] forState:UIControlStateNormal];
    overlayButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    
    // Assign the overlay button to a stored text field
    txLocation.rightView = overlayButton;
    txLocation.rightViewMode = UITextFieldViewModeAlways;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    
    if (textField == txLocation) {
        NSLog(@"City:%@",txLocation.text);
//        NSString *location;
//        
//        if (txLocation.text.length > 0) {
//            location = [[AddressResolver sharedInstance] validateAddress:txLocation.text];
//            
//            txLocation.text = location;
//            NSLog(@"Selected City:%@",txLocation.text);
//            
//        }
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [[self view] endEditing:YES];
    
}
- (void)changeLocationPressed:(id)sender {
    
    txLocation.text=@"";
      
}

- (void)dissmissView:(id)sender {
    
    [self dismissViewControllerAnimated:NO completion:nil];
}
- (IBAction)addServerButtonPressed:(id)sender {
    
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [APP huddie];
    
    NSLog(@"Add New Server");
    
    
    if ((location.length <= 0) && (![txLocation.text isEqualToString:@""]))
    {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [APP.hud setHidden:YES];
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@""
                                           message:@"Please Enter Valid Location"
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:NO completion:nil];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:NO completion:nil];
                
            });
            return ;
        });
        
    }
    
    if (([location isEqualToString:@""]) || ([txtServerDesignation.text isEqualToString:@""]) ||
        ([_txtRestaurantName.text isEqualToString:@""]) || ([txtServerName.text isEqualToString:@""]))
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [APP.hud setHidden:YES];
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Please Enter All Information"
                                           message:@"Information can not be Empty"                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:NO completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:NO completion:nil];
                
            });
            
            
            
        });
    }
    else
    {
        if ((location.length <= 0) && (![txLocation.text isEqualToString:@""]))
        {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [APP.hud setHidden:YES];
                UIAlertController * alert2=   [UIAlertController
                                               alertControllerWithTitle:@""
                                               message:@"Please Enter Valid Location"
                                               preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert2 animated:NO completion:nil];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [alert2 dismissViewControllerAnimated:NO completion:nil];
                    
                });
                return ;
            });
            
        }
        
        NSMutableDictionary *dataDict = [NSMutableDictionary new];
        
        [dataDict setValue:[NSString stringWithFormat:@"%@",restoID] forKey:@"restaurantId"];
        NSString *strServerName = [NSString upperCase:txtServerName.text];
        [dataDict setValue:strServerName forKey:@"serverName"];
        [dataDict setValue:_txtRestaurantName.text forKey:@"restaurantName"];
        [dataDict setValue:location forKey:@"city"];
        [dataDict setValue:designation forKey:@"designation"];
        
        
        if (userImageString.length > 0)
        {
            [dataDict setValue:userImageString forKey:@"image"];
        }
        
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDict options:0 error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@",SITE_URL,ADD_NEW_SERVER_URL] parameters:nil error:nil];
        
        [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
        
        [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse *response, id data, NSError *error) {
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            
            NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
            
            if ([data isKindOfClass:[NSDictionary class]]){
                
                NSDictionary *dataDic = (NSDictionary *)data;
                NSLog(@"Data:%@",dataDic);
                
                NSInteger status = [[dataDic valueForKey:@"status"] integerValue];
                if(status == 202)
                {
                    
                    Servers *server = [[Servers alloc] init];
                    
                    [realm beginWriteTransaction];
                    
                    NSDictionary  *serverData;
                    for (serverData in [dataDic valueForKey:@"data"]) {
                        
                        NSPredicate *predicate;
                        
                        server.serverID = [[serverData valueForKey:@"_id"] integerValue];
                        serverID = [[serverData valueForKey:@"_id"] integerValue];
                        server.workingRestroID = [[serverData valueForKey:@"_idWorksInRestaurant"] integerValue];
                        server.avgRating = [[serverData valueForKey:@"avgRating"] floatValue];
                        server.imageURL = [serverData valueForKey:@"image"];
                        server.serverName = [serverData valueForKey:@"name"];
                        server.email = [serverData valueForKey:@"email"];
                        server.toalRatings = [[serverData valueForKey:@"totalRatings"] integerValue];
                        server.city = [serverData valueForKey:@"city"];
                        
                        predicate = [NSPredicate predicateWithFormat:@"restroID = %ld",[[serverData valueForKey:@"_idWorksInRestaurant"] integerValue]];
                        Restaurants *restaurant =  [[Restaurants objectsWithPredicate:predicate] firstObject];
                        server.restaurantName = restaurant.name;
                        server.toalRatings = [[serverData valueForKey:@"totalRatings"] integerValue];
                        server.avgRating = [[serverData valueForKey:@"avgRating"] floatValue];
                        server.restaurantName = [serverData valueForKey:@"restaurantName"];
                        
                    }
                    
                    
                    [realm addObject:server];
                    [realm commitWriteTransaction];
                    
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [APP.hud setHidden:YES];
                        UIAlertController * alert2=   [UIAlertController
                                                       alertControllerWithTitle:@"Server Added"
                                                       message:@"New Server added Successfully"
                                                       preferredStyle:UIAlertControllerStyleAlert];
                        
                        [self presentViewController:alert2 animated:NO completion:nil];
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                            [alert2 dismissViewControllerAnimated:NO completion:nil];
                            //                            NSArray *array = [self.navigationController viewControllers];
                            //                            [self.navigationController popToViewController:[array objectAtIndex:3] animated:YES];
//                            for (UIViewController *controller in self.navigationController.viewControllers) {
                           
                                //Do not forget to import AnOldViewController.h
//                                if ([controller isKindOfClass:[HomeViewController class]]) {
//                                    
//                                    [self.navigationController popToViewController:controller
//                                                                          animated:YES];
//                                    break;
//                                }
//                            }

                            
                            
                            ServerDetailViewController *serverDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ServerDetailViewController"];
                            serverDetailVC.serverID = [NSString stringWithFormat:@"%ld",(long)serverID];
                            
                            [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%ld",(long)serverID] forKey:@"ServerID"];

                            NSLog(@"ID:%@",[NSString stringWithFormat:@"%ld",(long)serverID]);
                            [self.navigationController pushViewController:serverDetailVC animated:NO];
                            
                        });
                    });
                    
                    
                    
                    
                }
                else if(status == 400)
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [APP.hud setHidden:YES];
                        UIAlertController * alert2=   [UIAlertController
                                                       alertControllerWithTitle:@"Sorry"
                                                       message:[dataDic valueForKey:@"result"]
                                                       preferredStyle:UIAlertControllerStyleAlert];
                        
                        [self presentViewController:alert2 animated:NO completion:nil];
                        
                        
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                            [alert2 dismissViewControllerAnimated:NO completion:nil];
                        });
                    });
                    
                }
                
            }
            
            
        }] resume];
        
    }
    
}


- (IBAction)selectDesignation:(id)sender {
    {
        
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            [self dismissViewControllerAnimated:NO completion:nil];
            
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Bartender" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            txtServerDesignation.text = @"Bartender";
            
            designation = @"bartender";
            
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Server" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            txtServerDesignation.text = @"Server";
            designation = @"server";
            
        }]];
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Busboy" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            txtServerDesignation.text = @"Busboy";
            designation = @"busboy";
            
        }]];
        
        [self.view endEditing:YES];
        [self presentViewController:actionSheet animated:NO completion:nil];
    }
}
- (IBAction)addPhotoPressed:(id)sender {
    
    
}
- (IBAction)tappedToSelectImage:(UITapGestureRecognizer *)tapRecognizer {
    
    
    if (tapRecognizer.state == UIGestureRecognizerStateEnded)
    {
        CGFloat frameHeight = profilePic.frame.size.height;
        CGRect imageViewFrame = CGRectInset(profilePic.bounds, 0.0, (CGRectGetHeight(profilePic.frame) - frameHeight) / 2.0 );
        BOOL userTappedOnimageView = (CGRectContainsPoint(imageViewFrame, [tapRecognizer locationInView:profilePic]));
        if (userTappedOnimageView)
        {
            [self selectPhotos];
            
        }
    }
    
}
- (void)selectPhotos {
    
    
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Photos" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
        picker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
        
        [self presentViewController:picker animated:NO completion:nil];
        
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
        
        [self presentViewController:picker animated:NO completion:nil];
        
    }]];
    
    [self presentViewController:actionSheet animated:NO completion:nil];
    
    
    
    
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *originalImage =  info[UIImagePickerControllerOriginalImage];
    profilePic.image = originalImage;
    UIImage *tmpImage = [self resizeImage:originalImage];
    //    NSString *imageString = [self encodeToBase64String:originalImage];
    userImageString = [self encodeToBase64String:tmpImage];
    
    //    NewServer *newServer = [[NewServer alloc] init];
    //    RLMRealm *realm = [RLMRealm defaultRealm];
    //
    //    newServer.profilePic = imageString;
    //
    //    [realm transactionWithBlock:^{
    //        [realm addObject:newServer];
    //    }];
    //
    
    [self dismissViewControllerAnimated:NO completion:nil];
}
- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}


- (void)setupAutoCompleteTextField {
    
    txLocation.placeSearchDelegate = self;
    
    txLocation.strApiKey = kGoogleAPIKey;
    
    txLocation.superViewOfList = self.view;
    
    txLocation.autoCompleteShouldHideOnSelection = YES;
    
    txLocation.maximumNumberOfAutoCompleteRows  = 10;
    
    
}

- (void)placeSearch:(MVPlaceSearchTextField*)textField ResponseForSelectedPlace:(GMSPlace*)responseDict {
    [self.view endEditing:YES];
    
    NSLog(@"SELECTED ADDRESS :%@",responseDict);
    NSLog(@"City Name:%@",responseDict.types);
    
    locationCoord.latitude = responseDict.coordinate.latitude;
    locationCoord.longitude = responseDict.coordinate.longitude;
    NSDictionary *locationInfo = [[NSDictionary alloc] init];
    
    if (responseDict)
    {
        locationInfo = [[NSDictionary alloc] init];
        locationInfo = [[AddressResolver sharedInstance] getAddressDetails:locationCoord];
        location = [locationInfo valueForKey:@"city"];
        
        NSLog(@"Address Details:City %@",location);
    }

    
}
- (void)placeSearchWillShowResult:(MVPlaceSearchTextField*)textField {
    
}
- (void)placeSearchWillHideResult:(MVPlaceSearchTextField*)textField {
    
}
- (void)placeSearch:(MVPlaceSearchTextField*)textField ResultCell:(UITableViewCell*)cell withPlaceObject:(PlaceObject*)placeObject atIndex:(NSInteger)index {
        cell.contentView.backgroundColor = [UIColor whiteColor];
    
}


#pragma mark - MLPAutoCompleteTextField Delegate

- (BOOL)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
shouldStyleAutoCompleteTableView:(UITableView *)autoCompleteTableView
               forBorderStyle:(UITextBorderStyle)borderStyle;
{
    
    
    
    return YES;
}


- (BOOL)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
          shouldConfigureCell:(UITableViewCell *)cell
       withAutoCompleteString:(NSString *)autocompleteString
         withAttributedString:(NSAttributedString *)boldedString
        forAutoCompleteObject:(id<MLPAutoCompletionObject>)autocompleteObject
            forRowAtIndexPath:(NSIndexPath *)indexPath;
{
    //This is your chance to customize an autocomplete tableview cell before it appears in the autocomplete tableview
    cell.backgroundColor = [UIColor lightGrayColor];
    
    return YES;
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
  didSelectAutoCompleteString:(NSString *)selectedString
       withAutoCompleteObject:(id<MLPAutoCompletionObject>)selectedObject
            forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(selectedObject){
        NSLog(@"selected object from autocomplete menu %@ with string %@", selectedObject, [selectedObject autocompleteString]);
    } else {
        NSLog(@"selected string '%@' from autocomplete menu", selectedString);
        
        
        restraurantID = [objDemoDataSource.dict valueForKey:@"_id"];
        
        NSInteger index = [arrNames indexOfObject:selectedString];
        restoID = [arrIDs objectAtIndex:index];
        self.txtRestaurantName.text = selectedString;
        
    }
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField willHideAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view will be removed from the view hierarchy");
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField willShowAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view will be added to the view hierarchy");
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField didHideAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view ws removed from the view hierarchy");
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField didShowAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view was added to the view hierarchy");
}

- (NSString*)validateAddress:(NSString*)addressString {
    
    
    @try {
        
        
        NSCharacterSet *set = [NSCharacterSet URLHostAllowedCharacterSet];
        NSString *resultString = [addressString stringByAddingPercentEncodingWithAllowedCharacters:set];
        
        NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", resultString];
        NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
        
        
        NSData *objectData = [result dataUsingEncoding:NSUTF8StringEncoding];
        
        NSDictionary *jsonArray=[NSJSONSerialization JSONObjectWithData:objectData options:-1 error:nil];
        
        NSArray *addressArray=[[jsonArray valueForKeyPath:@"results.address_components"] objectAtIndex:0];
        
        
        if([[jsonArray valueForKey:@"status"] isEqualToString:@"OK"])
        {
            //        NSString *state;
            NSString *city;
            
            for (NSDictionary *dictAddress in addressArray)
            {
                //            if ([[[dictAddress objectForKey:@"types"]objectAtIndex:0] isEqualToString:@"administrative_area_level_1"])
                //                {
                //                    state = [dictAddress objectForKey:@"long_name"];
                //                     NSLog(@"state :%@",state);
                //
                //                }
                
                city = [dictAddress objectForKey:@"long_name"];
                
                return city;
                
            }
        }
        
        return @"New York";
    } @catch (NSException *exception) {
        NSLog(@"%@",exception);
        return nil;
    }
    
}


- (UIImage *)resizeImage:(UIImage *)image {
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 300.0;
    float maxWidth = 400.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
    
}
@end
