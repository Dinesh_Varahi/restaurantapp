//
//  AddressResolver.h
//  Restaurant
//
//  Created by HN on 22/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface AddressResolver : NSObject


+ (instancetype)sharedInstance;

- (NSString*)validateAddress:(NSString*)addressString;
- (NSDictionary*)getAddressDetails:(CLLocationCoordinate2D)aLocation;
@end
