//
//  AddressResolver.m
//  Restaurant
//
//  Created by HN on 22/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "AddressResolver.h"

@implementation AddressResolver
// https://maps.googleapis.com/maps/api/geocode/json?latlng=52.3182742,4.7288558

+ (instancetype)sharedInstance {
    static id _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[[self class] alloc] init];
    });
    
    return _sharedInstance;
}


- (NSString*)validateAddress:(NSString*)addressString {
    
    
    @try
    {

        NSCharacterSet *set = [NSCharacterSet URLHostAllowedCharacterSet];
        NSString *resultString = [addressString stringByAddingPercentEncodingWithAllowedCharacters:set];
        
        NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", resultString];
        NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
        
        
        NSData *objectData = [result dataUsingEncoding:NSUTF8StringEncoding];
        
        NSDictionary *jsonArray=[NSJSONSerialization JSONObjectWithData:objectData options:-1 error:nil];
        
        
        NSArray *addressArray=[[jsonArray valueForKeyPath:@"results.address_components"] objectAtIndex:0];
        NSLog(@"Address Array:%@",addressArray);
        
        if([[jsonArray valueForKey:@"status"] isEqualToString:@"OK"])
        {
            //        NSString *state;
            NSString *city;
            
            for (NSDictionary *dictAddress in addressArray){
                
                
                // Locality i.e. City
                if ([[[dictAddress objectForKey:@"types"]objectAtIndex:0] isEqualToString:@"locality"]) {
                    city = [dictAddress objectForKey:@"long_name"];
                     NSLog(@"City Name : %@",city);
                }
                
            }
            
            return city;
            
            if (city.length ==0 ) {
                return @"New York";
            }
            
        }
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Address Resolver Exception:%@",exception);
        return nil;
    }
    
}

- (NSDictionary*)getAddressDetails:(CLLocationCoordinate2D)aLocation {

    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    
    @try
    {

        NSString *locationString = [NSString stringWithFormat:@"%f,%f",aLocation.latitude,aLocation.longitude];
        
        
        NSString *req = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?latlng=%@", locationString];
        NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
        
        
        NSData *objectData = [result dataUsingEncoding:NSUTF8StringEncoding];
        
        NSDictionary *jsonArray=[NSJSONSerialization JSONObjectWithData:objectData options:-1 error:nil];
        
        
        NSArray *addressArray=[[jsonArray valueForKeyPath:@"results.address_components"] objectAtIndex:0];
        NSLog(@"Address Array:%@",addressArray);
        
        if([[jsonArray valueForKey:@"status"] isEqualToString:@"OK"])
        {
            //        NSString *state;
            
//            if (city.length ==0 ) {
//                return nil;
//            }
            NSString *city;
            NSString *zipCode;
            
            for (NSDictionary *dictAddress in addressArray){
                
                
                
                // Locality i.e. City
                if ([[[dictAddress objectForKey:@"types"]objectAtIndex:0] isEqualToString:@"locality"]) {
                    NSLog(@"Data:%@",dictAddress);
                    
                    city = [dictAddress objectForKey:@"long_name"];
                    [dataDict setValue:city forKey:@"city"];
                    NSLog(@"City Name : %@",city);
                }
                if ([[[dictAddress objectForKey:@"types"]objectAtIndex:0] isEqualToString:@"postal_code"]) {
                    NSLog(@"Data:%@",dictAddress);
                    
                    zipCode = [dictAddress objectForKey:@"long_name"];
                    [dataDict setValue:zipCode forKey:@"zipcode"];
                    NSLog(@"zip code : %@",zipCode);
                }
                
            }

        }
        
        
        NSLog(@"Data Dictionary:%@",dataDict);
        return dataDict;
        
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Address Resolver Exception:%@",exception);
        return nil;
    }
    
    
    
    
    
}
@end
