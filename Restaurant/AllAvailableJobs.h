//
//  AllAvailableJobs.h
//  Restaurant
//
//  Created by Parth Pandya on 02/12/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <Realm/Realm.h>

@interface AllAvailableJobs : RLMObject

@property NSInteger ID;
@property NSString *latitude;
@property NSString *longitude;
@property NSString *name;
@property NSString *zipcode;
@property NSString *city;
@property NSString *updatedAt;
@property NSString *createdAt;
@property NSString *jobDescription;
@property NSString *status;
@property NSString *profileType;
@property NSInteger RestaurantID;
//@property NSString *_v;

@end
