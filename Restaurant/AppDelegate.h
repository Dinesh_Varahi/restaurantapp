//
//  AppDelegate.h
//  Restaurant
//
//  Created by HN on 03/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "User+CoreDataClass.h"
#import "LocalData.h"
#import "AppLocationManager.h"
#import <Google/SignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>
#import "MBProgressHUD.h"
#import <UserNotifications/UserNotifications.h>
#import "Connectivity.h"



@class LocalData;

@interface AppDelegate : UIResponder <UIApplicationDelegate,GIDSignInDelegate,UNUserNotificationCenterDelegate>
{
    AppLocationManager *locationController;
}

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

@property (strong) NSManagedObjectContext *managedObjectContext;
@property (strong,nonatomic,readonly) LocalData *localData;


- (void)saveContext;

@property (nonatomic, strong) MBProgressHUD *hud;

-(void)huddie;

@property (strong) UNUserNotificationCenter *center;

@property (nonatomic) Connectivity *hostReachability;
@property (nonatomic) Connectivity *internetReachability;
@property (nonatomic) Connectivity *wifiReachability;


@end

