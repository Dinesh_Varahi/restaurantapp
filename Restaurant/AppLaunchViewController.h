//
//  AppLaunchViewController.h
//  Restaurant
//
//  Created by HN on 12/12/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HomeViewController.h"
#import "AppLocationManager.h"
#import "ServerLandingViewController.h"
#import "AppLocationManager.h"
#import "Restaurants.h"
#import "RestaurantDetailsViewController.h"
#import "OwnerDashboard.h"

@interface AppLaunchViewController : UIViewController<AppLocationDelegate>
{
    AppLocationManager *locationController;

}

@end
