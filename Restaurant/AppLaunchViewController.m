//
//  AppLaunchViewController.m
//  Restaurant
//
//  Created by HN on 12/12/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "AppLaunchViewController.h"

@interface AppLaunchViewController ()
@property (nonatomic,strong)RLMResults *restaurantsArray;
@end

@implementation AppLaunchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [NSThread sleepForTimeInterval:3.0];
    if ([self isNetworkAvailable])
    {
//        [self initLocationManager];
        
      //  [super viewWillAppear:YES];
        
        if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"isuserloggedin"] isEqualToString:@"yes"] && [[[NSUserDefaults standardUserDefaults]valueForKey:@"role"]isEqualToString:@"customer"]) {
            
            [self initLocationManager];
            
            [[LocalData sharedInstance] FetchAllRestaurants:^(BOOL result) {
                if (result)
                {
                    HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                    [self.navigationController pushViewController:home animated:NO];
                }
                else{
                    HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                    [self.navigationController pushViewController:home animated:NO];
                }
                
                
                
            }];
            
        }
        else if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"isuserloggedin"]isEqualToString:@"yes"] && [[[NSUserDefaults standardUserDefaults]valueForKey:@"role"]isEqualToString:@"server"]){
            
            [self fetchDataFromServer];
            
            
        }
        else if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"isuserloggedin"]isEqualToString:@"yes"] && [[[NSUserDefaults standardUserDefaults]valueForKey:@"role"]isEqualToString:@"owner"]){
            
            
//            [self goToOwnerDashboard];
            
            [self checkIfUserVerified];
            
        
        }
        else if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"isuserloggedin"]isEqualToString:@"no"] || ![[NSUserDefaults standardUserDefaults]valueForKey:@"isuserloggedin"])
        {
            [self initLocationManager];
            
            [[LocalData sharedInstance] FetchAllRestaurants:^(BOOL result) {
                if (result)
                {
                    HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                    [self.navigationController pushViewController:home animated:NO];
                }
                else{
                    HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                    [self.navigationController pushViewController:home animated:NO];
                }
                
                
                
            }];
            
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Please Check Internet Connectivity"
                                           message:@""
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:NO completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                
                [alert2 dismissViewControllerAnimated:NO completion:nil];
                return ;
                
            });
            
        });
    }
    
}
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
}

#pragma mark - Fetch Data From Server
- (void)fetchDataFromServer
{
    
    if ([self isNetworkAvailable]) {
        
        AppUser *user = [[AppUser allObjects] firstObject];
        
        
        [[LocalData sharedInstance] FetchRestaurantsList:^(BOOL result)
         {
             
             if (result)
             {
                 NSLog(@"Login: Resto List Fetched");
                 
                 
             }
             else{
                 NSLog(@"Login: Error while fetching Resto List");
                 
             }
             
             if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"role"]isEqualToString:@"server"]) {
                 
                 
                 ServerLandingViewController *serverLanding = [self.storyboard instantiateViewControllerWithIdentifier:@"ServerLandingViewController"];
                 serverLanding.navigationItem.leftBarButtonItem = nil;
                 serverLanding.navigationItem.hidesBackButton = YES;
                 [self.navigationController pushViewController:serverLanding animated:NO];
                 
             }
             
             
             
         }];
        
        [[LocalData sharedInstance] GetAllAvailableJobs:user.token];
        
        [[LocalData sharedInstance] FetchRatingsAndComments:[NSString stringWithFormat:@"%ld",(long)user.profileID] token:user.token completion:^(BOOL result)
         {
             
             if (result)
             {
                 NSLog(@"Login: Ratings and Comments Downloaded");
                 
                 
             }
             else{
                 NSLog(@"Login: Error while fetching Ratings and Comments");
                 
             }
             
         }];
        
        [[LocalData sharedInstance] FetchServerProfile:user.token completion:^(BOOL result)
         {
             
             if (result)
             {
                 NSLog(@"Login: Server Profile Fetched");
                 
                 
             }
             else
             {
                 NSLog(@"Login: Error while fetching Server Profile");
                 
             }
             
             
         }];
        
        
        
        
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Please Check Internet Connection"
                                           message:@""
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:NO completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                
                [alert2 dismissViewControllerAnimated:NO completion:nil];
                return ;
                
            });
            
        });
    }
    
}
- (bool)isNetworkAvailable
{
    SCNetworkReachabilityFlags flags;
    SCNetworkReachabilityRef address;
    address = SCNetworkReachabilityCreateWithName(NULL, "www.apple.com" );
    Boolean success = SCNetworkReachabilityGetFlags(address, &flags);
    CFRelease(address);
    
    bool canReach = success
    && !(flags & kSCNetworkReachabilityFlagsConnectionRequired)
    && (flags & kSCNetworkReachabilityFlagsReachable);
    
    return canReach;
}



#pragma mark - Init location Manager
- (void)initLocationManager
{
    
    locationController = [[AppLocationManager alloc] init];
    locationController.delegate = self;
    locationController.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    //    [locationController.locationManager startUpdatingLocation];
    if ([locationController.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [locationController.locationManager requestWhenInUseAuthorization];
    }
    
    if (![CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        UIAlertController * alert2=   [UIAlertController
                                       alertControllerWithTitle:@"Location Services Disabled!"
                                       message:@"Please enable Location Based Services for better results!"
                                       preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *settings = [UIAlertAction actionWithTitle:@"Go To Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
            
        }];
        [alert2 addAction:settings];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [alert2 dismissViewControllerAnimated:NO completion:nil];
        }];
        [alert2 addAction:cancel];
        
        [self presentViewController:alert2 animated:NO completion:nil];
        
    }
    else
    {
        //Location Services Enabled, let's start location updates
        [locationController.locationManager startUpdatingLocation];
    }
}

- (void)locationError:(NSError *)error
{
    
    NSLog(@"Occured Error:%@",error.description);
    
}

- (void)locationUpdate:(CLLocation *)location
{
    //    NSLog(@"Location:%@",location);
}


#pragma mark - Show Restaurant Details By Location

- (void)goToCustomerHomeView {
    [RLMRealm defaultRealm];
    self.restaurantsArray  = [Restaurants allObjects];
    NSString *lat;
    NSString *longi;
    lat = @"18.5152592";
    longi = @"73.7114462";
    
    if (_restaurantsArray.count > 0)
    {
        Restaurants *object;
        for (object in self.restaurantsArray) {
            
            NSLog(@"Object:%@",object);
            
            if ([object.latitude isEqualToString:lat] && [object.longitude isEqualToString:longi]) {
                RestaurantDetailsViewController *restoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RestaurantDetailsViewController"];
                restoVC.restaurantID = [NSString stringWithFormat:@"%ld",(long)object.restroID];
                
                [self.navigationController presentViewController:restoVC animated:NO completion:nil];
            }
            
        }
        
        
        
    }
    else
    {
        HomeViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
        [self.navigationController pushViewController:homeVC animated:NO];
    }
    
    
}

- (void)goToOwnerDashboard {
    
    
    if ([self isNetworkAvailable]) {
        
        AppUser *user = [[AppUser allObjects] firstObject];
        
        [APP huddie];
        
        [[LocalData sharedInstance] FetchOwnerProfile:user.token completion:^(BOOL success) {
            if (success)
            {
                NSLog(@"Applaunch: Owner Profile Fetched");
                                 [APP.hud setHidden:YES];
                OwnerDashboard *ownerLanding = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerDashboard"];
//                ownerLanding.navigationItem.leftBarButtonItem = nil;
                ownerLanding.navigationItem.hidesBackButton = YES;
                [self.navigationController pushViewController:ownerLanding animated:NO];
            }
            else
            {
                NSLog(@"Applaunch:  while fetching Owner Profile");
                                 [APP.hud setHidden:YES];
                OwnerDashboard *ownerLanding = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerDashboard"];
//                ownerLanding.navigationItem.leftBarButtonItem = nil;
                ownerLanding.navigationItem.hidesBackButton = YES;
                [self.navigationController pushViewController:ownerLanding animated:NO];
            }
            
        }];
        
        
        
        
       
        
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Error while connecting to Server"
                                           message:@""
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:NO completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                
                [alert2 dismissViewControllerAnimated:NO completion:nil];
                return ;
                
            });
            
        });
    }
    
    
}

- (void) checkIfUserVerified
{
    AppUser *user = [[AppUser allObjects] firstObject];
    
    if (user.isVerified == 1) {
        
        if (user.isOwnerAccountUpdated == 0) {
            
            [APP.hud setHidden:YES];
            OwnerProfileStep1ViewController *ownerProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerProfileStep1ViewController"];
            
            ownerProfile.navigationItem.leftBarButtonItem = nil;
            ownerProfile.navigationItem.hidesBackButton = YES;
            [self.navigationController pushViewController:ownerProfile animated:NO];
        }
        else{
            [APP.hud setHidden:YES];
            
            if ([self isNetworkAvailable]) {
                
                [[LocalData sharedInstance] FetchOwnerProfile:user.token completion:^(BOOL success) {
                    if (success)
                    {
                        NSLog(@"Login: Owner Profile Fetched");
                        
                        OwnerDashboard *ownerLanding = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerDashboard"];
//                            ownerLanding.navigationItem.leftBarButtonItem = nil;
                            ownerLanding.navigationItem.hidesBackButton = YES;
                        [self.navigationController pushViewController:ownerLanding animated:NO];
                        
                    }
                    else
                    {
                        OwnerDashboard *ownerLanding = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerDashboard"];
//                            ownerLanding.navigationItem.leftBarButtonItem = nil;
                            ownerLanding.navigationItem.hidesBackButton = YES;
                        [self.navigationController pushViewController:ownerLanding animated:NO];
                        
                        NSLog(@"Login:  while fetching Owner Profile");
                        
                    }
                    
                }];
                
                
                
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController * alert2=   [UIAlertController
                                                   alertControllerWithTitle:@"Error while connecting to Server"
                                                   message:@""
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert2 animated:NO completion:nil];
                    
                    
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        
                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                        return ;
                        
                    });
                    
                });
            }
            
            
            
        }
        
    }
    else
    {
         [[NSUserDefaults standardUserDefaults] setValue:@"no" forKey:@"isuserloggedin"];
        HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
        [self.navigationController pushViewController:home animated:NO];
        
//        NSLog(@"Go to Email Verification");
//        
//        OwnerVerifyOTPViewController *verifyOTPVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerVerifyOTPViewController"];
//        verifyOTPVC.navigationItem.leftBarButtonItem = nil;
//        verifyOTPVC.navigationItem.hidesBackButton = YES;
//        [self.navigationController pushViewController:verifyOTPVC animated:NO];
        
    }
}

@end
