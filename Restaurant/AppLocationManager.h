//
//  AppLocationManager.h
//  Restaurant
//
//  Created by HN on 29/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol AppLocationDelegate<NSObject>
@required
- (void)locationUpdate:(CLLocation *)location;
- (void)locationError:(NSError *)error;
@end



@interface AppLocationManager : NSObject <CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
    
}   
@property (nonatomic, strong) CLLocation *bestEffortAtLocation;
@property(strong, nonatomic) NSString *aLatitude;
@property(strong, nonatomic) NSString *aLongitude;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic,weak) id<AppLocationDelegate> delegate;


- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation;

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error;
@end
