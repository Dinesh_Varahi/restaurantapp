//
//  AppLocationManager.m
//  Restaurant
//
//  Created by HN on 29/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "AppLocationManager.h"
#import "AppDelegate.h"


@implementation AppLocationManager

NSString *address;

@synthesize locationManager;
@synthesize delegate;

- (id) init {
    self = [super init];
//    [self RestaurantDetailsFromLocation];
    self.locationManager = [[CLLocationManager alloc] init] ;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    self.locationManager.delegate = self; // send loc updates to myself
    return self;
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    
    
    NSTimeInterval locationAge = -[newLocation.timestamp timeIntervalSinceNow];
    if (locationAge > 30.0) {
        return;
    }
    
    if (newLocation.horizontalAccuracy < 5) {
        return;
    }
    
    if (self.bestEffortAtLocation == nil || self.bestEffortAtLocation.horizontalAccuracy > newLocation.horizontalAccuracy) {
        
        _bestEffortAtLocation = newLocation;
        
        if (newLocation.horizontalAccuracy <= self.locationManager.desiredAccuracy)
        {
            [locationManager stopUpdatingLocation];
         
            @try {
            
            NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
            
            _aLatitude = [NSString stringWithFormat:@"%f", _bestEffortAtLocation.coordinate.latitude];
            _aLongitude = [NSString stringWithFormat:@"%f", _bestEffortAtLocation.coordinate.longitude];
            NSLog(@"Latitude:%@",_aLatitude);
            NSLog(@"Longitude:%@",_aLongitude);
            
            [defaults setValue:_aLatitude forKey:@"Latitude"];
            [defaults setValue:_aLongitude forKey:@"Longitude"];
            
            [self RestaurantDetailsFromLocation];
            
            NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=false&",_bestEffortAtLocation.coordinate.latitude,_bestEffortAtLocation.coordinate.longitude]];
            
            NSURLSession *session = [NSURLSession sharedSession];
            [[session dataTaskWithURL:url
                    completionHandler:^(NSData *data,
                                        NSURLResponse *response,
                                        NSError *error) {
                        // handle response
                        if (data.length > 0 && error == nil)
                        {
                            
                            NSDictionary *jsonArray=[NSJSONSerialization JSONObjectWithData:data options:-1 error:nil];
                            NSArray *addressArray=[[jsonArray valueForKeyPath:@"results.address_components"] objectAtIndex:0];
                            if([[jsonArray valueForKey:@"status"] isEqualToString:@"OK"])
                            {
                                
                                address=[jsonArray valueForKeyPath:@"results.formatted_address"][0];
                                // NSLog(@"Address After Resolve: %@",newAddress);
                                [defaults setValue:address forKey:@"FullAddress"];
                                
                                for (NSDictionary *dictAddress in addressArray)
                                {
                                    if ([[dictAddress objectForKey:@"types"] count]>0)
                                    {
                                        // Street Number
                                        if ([[[dictAddress objectForKey:@"types"]objectAtIndex:0] isEqualToString:@"street_number"]) {
                                            
                                            // NSLog(@"Street Number: %@",streetNumber);
                                            //                                    [defaults setValue:obj.streetNumber forKey:@"streetnumber"];
                                        }
                                        // Route
                                        if ([[[dictAddress objectForKey:@"types"]objectAtIndex:0] isEqualToString:@"route"]) {
                                            // NSLog(@"Street Name: %@",route);
                                            //                                        [defaults setValue:obj.streetName forKey:@"route"];
                                            
                                        }
                                        // Locality i.e. City
                                        if ([[[dictAddress objectForKey:@"types"]objectAtIndex:0] isEqualToString:@"locality"]) {
                                            
                                            NSString *locality = [dictAddress objectForKey:@"long_name"];
                                            [self RestaurantDetailsFromLocation];
                                             NSLog(@"City : %@",locality);
                                            [[NSUserDefaults standardUserDefaults] setValue:locality forKey:@"location"];
                                            
                                        }
                                        // Dependent City
                                        if ([[[dictAddress objectForKey:@"types"]objectAtIndex:0] isEqualToString:@"sublocality"]) {
                                            NSString * subLocality = [dictAddress objectForKey:@"long_name"];

                                             NSLog(@"City : %@",subLocality);
                                            [defaults setValue:subLocality forKey:@"sublocality"];
                                        }
                                        // State
                                        if ([[[dictAddress objectForKey:@"types"]objectAtIndex:0] isEqualToString:@"administrative_area_level_1"]) {
                                            
                                            // NSLog(@"City : %@",state);
                                            
                                        }
                                        // Country
                                        if ([[[dictAddress objectForKey:@"types"]objectAtIndex:0] isEqualToString:@"country"]) {
                                            
                                            // NSLog(@"Country :%@",country);
                                            
                                        }
                                        // Postal Code
                                        if ([[[dictAddress objectForKey:@"types"]objectAtIndex:0] isEqualToString:@"postal_code"]) {
                                            
                                            // NSLog(@"Postal Code: %@",postalCode);
                                            
                                        }
                                    }
                                }
                            }
                            
                            else{
                                NSLog(@"Error occurred while getting location");
                                
                                
                            }
                        }
                        
                    }] resume];
        }
            @catch (NSException *exception) {
            NSLog(@"Update Location Exception:%@",exception);
            
        }
        
        }
        
    }
    
    
}

-(void)RestaurantDetailsFromLocation
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL;
    URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?latitude=%@&longitude=%@",SITE_URL,GET_NEARER_RESTAURANTS,[[NSUserDefaults standardUserDefaults] valueForKey:@"Latitude"],[[NSUserDefaults standardUserDefaults] valueForKey:@"Longitude"]]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error.description);
            dispatch_async(dispatch_get_main_queue(), ^{[APP.hud setHidden:YES];});
            
        }
        else {
            //            NSLog(@"%@ %@", response, responseObject);
            
            if ([responseObject isKindOfClass:[NSArray class]])
            {
                NSArray *jsonArray = (NSArray *)responseObject  ;
                NSLog(@"Json:%@",jsonArray);
            }
            
            if ([responseObject isKindOfClass:[NSDictionary class]])
            {
        
                NSDictionary *dataDic = (NSDictionary *)responseObject;
                NSDictionary *data = [dataDic valueForKey:@"data"];
                
                if (data.count > 0)
                {   double delayInSeconds = 3.0;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
                    [nc postNotificationName:@"NearerRestaurantAvailable" object:self userInfo:data];
                    });
                }
                
            }
        }
        
    }];
    [dataTask resume];

}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
        NSLog(@"Error: %@", [error description]);
//    [self.delegate locationError:error];
    
}

@end
