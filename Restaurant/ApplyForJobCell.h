//
//  ApplyForJobCell.h
//  Restaurant
//
//  Created by Parth Pandya on 06/12/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ApplyForJobCell : UITableViewCell

- (IBAction)btnApplyForJobPressed:(id)sender;


@end
