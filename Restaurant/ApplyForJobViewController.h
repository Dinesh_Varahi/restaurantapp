//
//  ApplyForJobViewController.h
//  Restaurant
//
//  Created by HN on 16/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IntroductoryOfferCell.h"
#import "AllAvailableJobs.h"
#import "AppDelegate.h"
#import "ApplyJobCell.h"
#import "ServerJobApplied.h"

@interface ApplyForJobViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    UIButton *btnApplyJob;
    UIView *buttonBackgroundView;
    //RLMResults<AllAvailableJobs  *> *allAvailableJobs;
    RLMResults<ServerJobApplied  *> *appliedJobs;
    
    UILabel *lblJobCount;
    
    NSDictionary *dataDic;
    
    NSMutableArray *arrCheckBoxStatus;
    NSMutableArray *arrExperiance;
    NSMutableArray *arrButtonStatus;
    NSMutableArray *arrJobIDs;
    NSMutableArray *arrAlreadyApplied;
    NSMutableArray *arrIDs;
    BOOL isClosed;
    BOOL toggle;
    
    NSMutableArray *arrRestaurantName;
    NSMutableArray *arrProfileType;
    NSMutableArray *arrCity;
    NSMutableArray *arrUpdatedAt;
    NSMutableArray *arrIdRestaurant;
    NSMutableArray *allJobsArray;
    NSMutableArray *arrLocations;
    
    int pageIndex;
    

}
@property (strong, nonatomic) IBOutlet UITextField *txtWorkProfile;
@property (strong, nonatomic) IBOutlet UITextField *txtCity;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) CGFloat lastContentOffset;

- (void)setNavigationBar;

@end
