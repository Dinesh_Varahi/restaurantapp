//
//  ApplyForJobViewController.m
//  Restaurant
//
//  Created by HN on 16/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "ApplyForJobViewController.h"
#import "ShowMapViewController.h"
#import "JobPostSuccessAlertViewController.h"

@interface ApplyForJobViewController ()
{
    BOOL hideOfferCell;
}
@property (nonatomic, strong) RLMResults *filteredJobsArray;
@property  BOOL isFiltered;
@end

@implementation ApplyForJobViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.title = @"Apply for Job";
    
    allJobsArray = [[NSMutableArray alloc] init];
    arrRestaurantName = [[NSMutableArray alloc] init];
    arrUpdatedAt = [[NSMutableArray alloc] init];
    arrProfileType = [[NSMutableArray alloc] init];
    arrCity = [[NSMutableArray alloc] init];
    arrExperiance = [[NSMutableArray alloc] init];
    arrIdRestaurant = [[NSMutableArray alloc] init];
    arrIDs = [[NSMutableArray alloc] init];
    arrLocations = [[NSMutableArray alloc] init];
    
    [_txtWorkProfile setDelegate:self];
    [_txtCity setDelegate:self];
    pageIndex = 0;
    
    
    arrCheckBoxStatus = [[NSMutableArray alloc]init];
    toggle = NO;
    _txtWorkProfile.textColor = [UIColor whiteColor];
    _txtCity.textColor = [UIColor whiteColor];
    
    arrJobIDs = [[NSMutableArray alloc]init];
    arrButtonStatus = [[NSMutableArray alloc]init];
        [self setValues];
    //NSString *ID = [[allAvailableJobs objectAtIndex:0] valueForKey:@"ID"];
    
        [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(OpenMaps)
                                                 name:@"btnCityPressed"//reloadTableData
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(OpenSuccessAlert)
                                                 name:@"JobAppliedSuccesfully"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(btnYesPressed)
                                                 name:@"reloadTableData"
                                               object:nil];

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(OpenAlert)
                                                 name:@"JobAppliedError"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(popView)
                                                 name:@"btnNoPressed"
                                               object:nil];
    
    buttonBackgroundView = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height - 50, self.view.frame.size.width, 50)];
    buttonBackgroundView.hidden = YES;
    buttonBackgroundView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:buttonBackgroundView];
    
    
    btnApplyJob = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/4.26, 7, self.view.frame.size.width/1.875, 36)];
    [btnApplyJob setBackgroundColor:[UIColor redColor]];
    [btnApplyJob addTarget:self action:@selector(btnApplyJobPressed:) forControlEvents:UIControlEventTouchUpInside];
    btnApplyJob.clipsToBounds = YES;
    btnApplyJob.layer.cornerRadius = btnApplyJob.frame.size.height / 2;
    [btnApplyJob setTitle:@"APPLY FOR JOB" forState:UIControlStateNormal];
    btnApplyJob.titleLabel.font = [UIFont systemFontOfSize:13.0f];
    [btnApplyJob setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnApplyJob.layer.borderWidth = 2.0f;
    [buttonBackgroundView addSubview:btnApplyJob];
    
    [_txtWorkProfile addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_txtCity addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    
}

-(void)btnYesPressed {
    pageIndex = 0;
    
    
    [arrButtonStatus removeAllObjects];
    [arrRestaurantName removeAllObjects];
    [arrUpdatedAt removeAllObjects];
    [arrProfileType removeAllObjects];
    [arrCity removeAllObjects];
    [arrIdRestaurant removeAllObjects];
    [arrIDs removeAllObjects];
    
    [self GetAvailableJobs];
}
-(void)GetAvailableJobs {
    
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc]
                                             initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activityView.center=self.view.center;
    [activityView startAnimating];
    [activityView hidesWhenStopped];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    [self.view addSubview:activityView];

    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    AppUser *user = [[AppUser allObjects] firstObject];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"bearer %@",user.token] forHTTPHeaderField:@"Authorization"];
    
    
    NSString *urlString =[[NSString stringWithFormat:@"%@%@?page=%d&workProfile=%@&city=%@",SITE_URL,GET_ALL_JOB_OPENING_URL,pageIndex,_txtWorkProfile.text,_txtCity.text] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSURL *URL = [NSURL URLWithString:urlString];
    
    [manager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        if ([responseObject isKindOfClass:[NSArray class]])
        {
            NSArray *jsonArray = (NSArray *)responseObject  ;
            NSLog(@"Json:%@",jsonArray);
            [activityView stopAnimating];
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
            });
        }
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            
            dataDic = (NSDictionary *)responseObject;
            
            allJobsArray = [dataDic valueForKey:@"data"];
            
           
            for (int i = 0; i < [allJobsArray count]; i++)
            {
                
                NSDictionary *dict = [allJobsArray objectAtIndex:i];
                
                if ([dict objectForKey:@"isAlreadyApplied"])
                {
                    [arrButtonStatus addObject:@"YES"];
                }
                else
                {
                    [arrButtonStatus addObject:@"NO"];
                }
                
                [arrLocations addObject:[dict valueForKey:@"location"]];
                [arrExperiance addObject:[dict valueForKey:@"experience"]];
                [arrCity addObject:[dict valueForKey:@"city"]];
                [arrRestaurantName addObject:[dict valueForKey:@"name"]];
                [arrProfileType addObject:[dict valueForKey:@"profileType"]];
                [arrUpdatedAt addObject:[dict valueForKey:@"updatedAt"]];
                [arrIdRestaurant addObject:[dict valueForKey:@"_idRestaurant"]];
                [arrIDs addObject:[dict valueForKey:@"_id"]];
                
                
//                arrExperiance = [dict valueForKey:@"experience"];
//                arrCity = [dict valueForKey:@"city"];
//                arrRestaurantName = [dict valueForKey:@"name"];
//                arrProfileType = [dict valueForKey:@"profileType"];
//                arrUpdatedAt = [dict valueForKey:@"ipdatedAt"];
//                arrIdRestaurant = [dict valueForKey:@"_idRestaurant"];
//                arrIDs = [dict valueForKey:@"_id"];
                [self setValues];
                
            }
        }
        double delayInSeconds = 1.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        });
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"jobListingAvailble" object:allJobsArray];
        });
        [activityView stopAnimating];
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        });
        
        [activityView stopAnimating];
        NSLog(@"Error: %@", error);
    }];

}
-(void)setValues {
    //allAvailableJobs = [AllAvailableJobs allObjects];
    appliedJobs = [ServerJobApplied allObjects];
    [arrCheckBoxStatus removeAllObjects];
    for (int i =0 ; i < [arrExperiance count]; i++)
    {
        [arrCheckBoxStatus addObject:@"0"];
    }
    
    [self.tableView reloadData];

}

- (void)viewWillAppear:(BOOL)animated {
    
//    allAvailableJobs = [AllAvailableJobs allObjects];
//    NSLog(@"All Jobs List:%@",allAvailableJobs);
    
    [self setValues];
    [super viewWillAppear:YES];
    [self setTitle:@"Apply for Job"];
    [self GetAvailableJobs];
    self.tabBarController.navigationItem.title = @"Jobs";
    [self.tableView reloadData];
    
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setNavigation3" object:self];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"rsetDashboardNavigation1" object:self];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setNavigation3" object:self];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setJobsNavigation" object:self];
    //[self reSetNavigationBar];
    
}

- (void)reSetNavigationBar {
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
//                                                  forBarMetrics:UIBarMetricsDefault];
//    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.view.backgroundColor = [UIColor appMainColor];
    [self.navigationController.navigationBar setBarTintColor:[UIColor clearColor]];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    
    
}
-(void)popView {
    
//    ServerLandingViewController *landingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"navLanding"];
//    [self presentViewController:landingVC animated:NO completion:nil];
    [self.navigationController popViewControllerAnimated:NO];
}
-(void)OpenAlert {
    
}
-(void)OpenMaps {
    ShowMapViewController *ShowMapViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ShowMapViewController"];
    
    //    [self.navigationController presentViewController:homeVC animated:YES completion:nil];
    
    UINavigationController *navHome = [[UINavigationController alloc] initWithRootViewController:ShowMapViewController];
    
    [self presentViewController:navHome animated:NO completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setNavigationBar {
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.backgroundColor = [UIColor appMainColor];
    self.navigationController.view.backgroundColor = [UIColor appMainColor];
    [self.navigationController.navigationBar setBarTintColor:[UIColor appMainColor]];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor appMainColor]];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];

}
- (void)textDidChange:(id)sender {
    
    pageIndex = 0;
    
    [arrRestaurantName removeAllObjects];
    [arrUpdatedAt removeAllObjects];
    [arrProfileType removeAllObjects];
    [arrCity removeAllObjects];
    [arrIdRestaurant removeAllObjects];
    [arrIDs removeAllObjects];

    [self setValues];
    [self GetAvailableJobs];
}

#pragma mark - TableView delegate Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //allAvailableJobs = [AllAvailableJobs allObjects];
    if (section == 0)
    {
        if(hideOfferCell) {
            hideOfferCell = YES;
            return 0;
        }
        else {
            return 1;
        }
    }
//    if (allAvailableJobs.count == 0 && self.filteredJobsArray.count == 0) {
//        return 0;
//    }
//    else if(self.isFiltered)
//    {
//        return self.filteredJobsArray.count;
//    }
    return arrRestaurantName.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath  {
    cell.backgroundColor = [UIColor clearColor];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 124;
}
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    //ApplyJobCell *Cell = [[ApplyJobCell alloc] init];
    
    if (indexPath.section == 0)
    {
        IntroductoryOfferCell *introCell = [[IntroductoryOfferCell alloc]init];
        introCell = [tableView dequeueReusableCellWithIdentifier:@"IntroductoryOfferCell" forIndexPath:indexPath];
        [introCell.btnClosePressed addTarget:self action:@selector(hideOfferSection:) forControlEvents:UIControlEventTouchUpInside];
        return introCell;
    }
    else
    {
//        AllAvailableJobs *aJob = [[AllAvailableJobs alloc] init];
//        
//        if (self.isFiltered) {
//            aJob = self.filteredJobsArray[indexPath.row];
//        }
//        else {
//            aJob = allAvailableJobs[indexPath.row];
//        }
        
        ApplyJobCell *Cell = (ApplyJobCell*)[tableView dequeueReusableCellWithIdentifier:@"applyCell"];
        //Cell = [tableView dequeueReusableCellWithIdentifier:@"applyCell" forIndexPath:indexPath];
        
        
        
        Cell.btnCheckbox.tag = indexPath.row;
        
        if ([[arrCheckBoxStatus objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
            [Cell.btnCheckbox setImage:[UIImage imageNamed:@"blank-square"] forState:UIControlStateNormal];
            [Cell.btnCheckbox setHidden:NO];
            [Cell.btnApplyJob setTitle:@"APPLY FOR JOB" forState:UIControlStateNormal];
            Cell.btnApplyJob.userInteractionEnabled = YES;
            [Cell.btnApplyJob setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            Cell.btnApplyJob.layer.borderColor = [UIColor redColor].CGColor;
            }
        else if ([[arrCheckBoxStatus objectAtIndex:indexPath.row] isEqualToString:@"1"])
        {
            [Cell.btnCheckbox setHidden:NO];
            [Cell.btnCheckbox setImage:[UIImage imageNamed:@"checked-box"] forState:UIControlStateNormal];
            [Cell.btnApplyJob setTitle:@"SELECTED" forState:UIControlStateNormal];
            [Cell.btnApplyJob setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            Cell.btnApplyJob.layer.borderColor = [UIColor darkGrayColor].CGColor;
            Cell.btnApplyJob.userInteractionEnabled = NO;
        }
        
        if ([[arrButtonStatus objectAtIndex:indexPath.row] isEqualToString:@"YES"])
        {
            [Cell.btnCheckbox setHidden:YES];
            [Cell.btnApplyJob setTitle:@"APPLIED" forState:UIControlStateNormal];
            [Cell.btnApplyJob setUserInteractionEnabled:NO];
            [Cell.btnApplyJob setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            Cell.btnApplyJob.layer.borderColor = [UIColor grayColor].CGColor;
        }
        
        [Cell layoutIfNeeded];
        
        [Cell.btnCheckbox addTarget:self action:@selector(btnChecks:) forControlEvents:UIControlEventTouchUpInside];
        [Cell.btnApplyJob addTarget:self action:@selector(applyForSingleJob:) forControlEvents:UIControlEventTouchUpInside];
        Cell.btnCity.tag = indexPath.row;
        Cell.btnApplyJob.tag = indexPath.row;
        Cell.lblExperiance.text = [arrExperiance objectAtIndex:indexPath.row];
        Cell.lblRestaurantName.text = [NSString upperCase:[arrRestaurantName objectAtIndex:indexPath.row]];;
        
        Cell.lblWorkProfile.text = [[arrProfileType objectAtIndex:indexPath.row] capitalizedString];
        
        NSString *strDate = [NSString getFormattedDate:[arrUpdatedAt objectAtIndex:indexPath.row]];
        Cell.lblDate.text = [NSString stringWithFormat:@"Posted on: %@",strDate];
        
        Cell.lblCity.text = [arrCity objectAtIndex:indexPath.row];
        
//        [Cell.btnCity setTitle:aJob.city forState:UIControlStateNormal];
        
        return Cell;
    }
    
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0)
    {
        return @"";
    }
    if(self.isFiltered && self.filteredJobsArray.count == 0)
    {
        return @"No Jobs Found";
    }

    return @"Recommended Jobs for you";
}


-(void)OpenSuccessAlert {
    
    JobPostSuccessAlertViewController *jbpsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"JobPostSuccessAlertViewController"];
    
    jbpsVC.providesPresentationContextTransitionStyle = YES;
    jbpsVC.definesPresentationContext = YES;
    [jbpsVC setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    jbpsVC.view.backgroundColor = [UIColor clearColor];

    jbpsVC.transitioningDelegate = (id)self;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        AppUser *user = [[AppUser allObjects] firstObject];
        [[LocalData sharedInstance] FetchServerProfile:user.token completion:^(BOOL result) {
            
            if (result)
            {
                NSLog(@"Login: Server Profile Fetched");
                
            }
            else{
                NSLog(@"Login: Error while fetching Server Profile");
                
            }
            
        }];
    });
    
    [self.navigationController presentViewController:jbpsVC animated:NO completion:nil];

}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    //Get the page
    if (self.lastContentOffset < scrollView.contentOffset.y)
    {
        NSLog(@"Scrolling Down");
        //pageIndex = scrollView.contentOffset.x / scrollView.bounds.size.width;
        
        pageIndex ++;
        [self GetAvailableJobs];
        
    }
    
    
    self.lastContentOffset = scrollView.contentOffset.y;
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        //Get the page
        if (self.lastContentOffset < scrollView.contentOffset.y)
        {
            NSLog(@"Scrolling Down");
            //pageIndex = scrollView.contentOffset.x / scrollView.bounds.size.width;
            
            pageIndex ++;
            [self GetAvailableJobs];
            
        }
        
        self.lastContentOffset = scrollView.contentOffset.y;
        
        
    }
}



- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)hideOfferSection:(id)sender {
    hideOfferCell = YES;
    [self.tableView reloadData];
    
}

- (IBAction)applyForSingleJob:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    NSError *error;
    
    
//    RLMResults <AllAvailableJobs*> *tempJobsArray;
//    
//    if (self.isFiltered) {
//        tempJobsArray = self.filteredJobsArray;
//    }
//    else {
//        tempJobsArray = allAvailableJobs;
//    }
//    
//    allAvailableJobs = [AllAvailableJobs allObjects];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    
    NSString *urlString =[[NSString stringWithFormat:@"%@%@",SITE_URL,APPLY_TO_JOB_OPENING_URL] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:urlString parameters:nil error:nil];
    
    AppUser *user = [[AppUser allObjects] firstObject];
    NSString *token = user.token;
    
    NSLog(@"Token:%@",token);
    
    [req addValue:[NSString stringWithFormat:@"bearer %@",token] forHTTPHeaderField:@"Authorization"];
    [req addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [req addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSMutableDictionary *dataDict = [NSMutableDictionary new];
    NSMutableArray *array = [[NSMutableArray alloc]init];
    
    [dataDict setValue:[arrIDs objectAtIndex:btn.tag] forKey:@"_idJobOpening"];
    
    [array addObject:dataDict];
    
    
    NSMutableDictionary *Value = [[NSMutableDictionary alloc]init];
    [Value setObject:array forKey:@"_openingList"];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:Value options:0 error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse *response, id data, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        //            double status = (long)[httpResponse statusCode];
        NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
        
        if ([data isKindOfClass:[NSDictionary class]]){
            
            NSDictionary *dataDic = (NSDictionary *)data;
            NSLog(@"Data:%@",dataDic);
            
            NSInteger status = [[dataDic valueForKey:@"status"] integerValue];
            if(status == 202)
            {
                
                [self OpenSuccessAlert];
                
                
            }
            else if(status == 400)
            {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController * alert2=   [UIAlertController
                                                   alertControllerWithTitle:@"Error"
                                                   message:[dataDic valueForKey:@"result"]
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert2 animated:NO completion:nil];
                    
                    
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                    });
                });
                
            }
            
        }
        
        
    }] resume];
    
}

-(IBAction)btnChecks:(id)sender {
    
    
    UIButton *btn = (UIButton *)sender;
    
    
    if (![arrJobIDs containsObject:[NSString stringWithFormat:@"%@",[arrIDs objectAtIndex:btn.tag] ]])    {
        [arrJobIDs addObject:[NSString stringWithFormat:@"%@",[arrIDs objectAtIndex:btn.tag] ]];
        [arrCheckBoxStatus replaceObjectAtIndex:btn.tag withObject:@"1"];
        [self.tableView reloadData];
    }
    else //if([arrJobIDs containsObject:[[tempJobsArray objectAtIndex:btn.tag] valueForKey:@"ID"]])
    {
        NSString *integer = [NSString stringWithFormat:@"%@",[arrIDs objectAtIndex:btn.tag]  ];
        NSLog(@"%@",integer);
        
        long index = [arrJobIDs indexOfObject:integer];
        [arrCheckBoxStatus replaceObjectAtIndex:btn.tag withObject:@"0"];
        [self.tableView reloadData];
        [arrJobIDs removeObjectAtIndex:index];
    }
    
    [btnApplyJob setTitle:[NSString stringWithFormat:@"APPLY SELECTED(%lu)",(unsigned long)arrJobIDs.count] forState:UIControlStateNormal];
    if (arrJobIDs.count>0)
    {
        buttonBackgroundView.hidden = NO;
    }
    else if(arrJobIDs.count==0)
    {
        buttonBackgroundView.hidden = YES;
    }
}

-(IBAction)btnApplyJobPressed:(id)sender {
    //RLMRealm *realm = [RLMRealm defaultRealm];
    NSError *error;
    
    [APP huddie];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    
    NSString *urlString =[[NSString stringWithFormat:@"%@%@",SITE_URL,APPLY_TO_JOB_OPENING_URL] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:urlString parameters:nil error:nil];
    
    AppUser *user = [[AppUser allObjects] firstObject];
    NSString *token = user.token;
    
    //    NSData *data = [NSJSONSerialization dataWithJSONObject:arrJobIDs options:0 error:nil];
    //    NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Token:%@",token);
    
    [req addValue:[NSString stringWithFormat:@"bearer %@",token] forHTTPHeaderField:@"Authorization"];
    [req addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [req addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSMutableArray *array = [[NSMutableArray alloc]init];
    for (int i = 0; i < [arrJobIDs count]; i++)
    {
        NSMutableDictionary *dataDict = [NSMutableDictionary new];
        [dataDict setValue:[arrJobIDs objectAtIndex:i] forKey:@"_idJobOpening"];
        //id value = [dataDict valueForKey:@"_idJobOpening"];
        [array addObject:dataDict];
    }
    NSMutableDictionary *Value = [[NSMutableDictionary alloc]init];
    [Value setObject:array forKey:@"_openingList"];
    //    NSData *data = [NSJSONSerialization dataWithJSONObject:array options:0 error:nil];
    //        NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:Value options:0 error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse *response, id data, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        //            double status = (long)[httpResponse statusCode];
        NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
        
        if ([data isKindOfClass:[NSDictionary class]]){
            
            NSDictionary *dataDic = (NSDictionary *)data;
            NSLog(@"Data:%@",dataDic);
            
            NSInteger status = [[dataDic valueForKey:@"status"] integerValue];
            if(status == 202)
            {
                [APP.hud setHidden:YES];
                [arrJobIDs removeAllObjects];
                [buttonBackgroundView setHidden:YES];
                [self OpenSuccessAlert];
                
            }
            else if(status == 400)
            {
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [APP.hud setHidden:YES];
                    UIAlertController * alert2=   [UIAlertController
                                                   alertControllerWithTitle:@"Sorry"
                                                   message:[dataDic valueForKey:@"result"]
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert2 animated:NO completion:nil];
                    
                    
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                    });
                });
                
            }
            
        }
        
        
    }] resume];
    
}



@end
