//
//  ApplyJobCell.h
//  Restaurant
//
//  Created by HN on 16/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppUser.h"
#import "AllAvailableJobs.h"
#import "JobPostSuccessAlertViewController.h"
#import "AppDelegate.h"

@interface ApplyJobCell : UITableViewCell
{
    BOOL toggle;
    RLMResults<AllAvailableJobs  *> *allAvailableJobs;
    NSMutableArray *arrLocations;
    NSMutableArray *arrLatitude;
    NSMutableArray *arrLongitude;
    NSMutableArray *allJobsArray;
    NSMutableArray *arrRestroName;
}
@property (weak, nonatomic) IBOutlet UIButton *btnCheckbox;
@property (weak, nonatomic) IBOutlet UILabel *lblRestaurantName;
@property (weak, nonatomic) IBOutlet UILabel *lblWorkProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UIButton *btnCity;
@property (weak, nonatomic) IBOutlet UILabel *lblExperiance;


@property (weak, nonatomic) IBOutlet UIButton *btnApplyJob;
@property (weak, nonatomic) IBOutlet UIView *background;
@property (weak, nonatomic) IBOutlet UILabel *lblCity;


- (IBAction)btnCheckBoxPressed:(id)sender;
- (IBAction)btnCityPressed:(id)sender;
- (IBAction)btnApplyJobPressed:(id)sender;



@end
