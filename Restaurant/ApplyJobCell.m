//
//  ApplyJobCell.m
//  Restaurant
//
//  Created by HN on 16/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "ApplyJobCell.h"

@implementation ApplyJobCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    toggle = YES;
    self.background.layer.cornerRadius = 5;
    arrLongitude = [[NSMutableArray alloc] init];
    arrLatitude = [[NSMutableArray alloc] init];
    arrRestroName = [[NSMutableArray alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(JobsAvailble:)
                                                 name:@"jobListingAvailble"
                                               object:nil];

    
}

-(void)JobsAvailble:(NSNotification *)notification

{
    if ([notification.name isEqualToString:@"jobListingAvailble"])
    {
        
        allJobsArray = notification.object;
        
        for (int i = 0; i < [allJobsArray count]; i++)
        {
            
            NSDictionary *dict = [allJobsArray objectAtIndex:i];
            
            [arrRestroName addObject:[dict valueForKey:@"name"]];
            [arrLocations addObject:[dict valueForKey:@"location"]];
            arrLatitude = [arrLocations valueForKey:@"lat"];
            arrLongitude = [arrLocations valueForKey:@"long"];

            
            
        }
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btnCheckBoxPressed:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    NSLog(@"%ld",(long)btn.tag);
    toggle = !toggle;
    [_btnCheckbox setImage:[UIImage imageNamed:toggle ? @"blank-square.png" :@"checked-box.png"] forState:UIControlStateNormal];
    }

- (IBAction)btnCityPressed:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    [[NSUserDefaults standardUserDefaults] setValue:[arrLatitude objectAtIndex:btn.tag] forKey:@"restaurantLatitude"];
    
    [[NSUserDefaults standardUserDefaults] setValue:[arrLongitude objectAtIndex:btn.tag] forKey:@"restaurantLongitude"];
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@",[arrRestroName objectAtIndex:btn.tag]] forKey:@"buttontag"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"btnCityPressed" object:self];
}

- (IBAction)btnApplyJobPressed:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    NSError *error;
    [APP huddie];
    allAvailableJobs = [AllAvailableJobs allObjects];
    //    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDict options:0 error:&error];
    //    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSString *urlString =[[NSString stringWithFormat:@"%@%@",SITE_URL,APPLY_TO_JOB_OPENING_URL] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:urlString parameters:nil error:nil];
    
    AppUser *user = [[AppUser allObjects] firstObject];
    NSString *token = user.token;
    
    //    NSData *data = [NSJSONSerialization dataWithJSONObject:arrJobIDs options:0 error:nil];
    //    NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Token:%@",token);
    
    [req addValue:[NSString stringWithFormat:@"bearer %@",token] forHTTPHeaderField:@"Authorization"];
    [req addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [req addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSMutableDictionary *dataDict = [NSMutableDictionary new];
    NSMutableArray *array = [[NSMutableArray alloc]init];
    //for (int i = 0; i < [arrJobIDs count]; i++)
    {
        [dataDict setValue:[[allAvailableJobs objectAtIndex:btn.tag] valueForKey:@"ID"] forKey:@"_idJobOpening"];
        //id value = [dataDict valueForKey:@"_idJobOpening"];
        [array addObject:dataDict];
    }
    NSMutableDictionary *Value = [[NSMutableDictionary alloc]init];
    [Value setObject:array forKey:@"_openingList"];
    //    NSData *data = [NSJSONSerialization dataWithJSONObject:array options:0 error:nil];
    //        NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:Value options:0 error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse *response, id data, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        //            double status = (long)[httpResponse statusCode];
        NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
        
        if ([data isKindOfClass:[NSDictionary class]]){
            
            NSDictionary *dataDic = (NSDictionary *)data;
            NSLog(@"Data:%@",dataDic);
            
            NSInteger status = [[dataDic valueForKey:@"status"] integerValue];
            if(status == 202)
            {
                
             [APP.hud setHidden:YES];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"JobAppliedSuccesfully" object:self];
                
                
            }
            else if(status == 400)
            {
                [APP.hud setHidden:YES];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"JobAppliedError" object:self];
                                
            }
            
        }
        
        
    }] resume];

}
@end
