//
//  BackgroundView.m
//  Restaurant
//
//  Created by HN on 19/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "BackgroundView.h"


@implementation BackgroundView

- (id)initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
    
    if (self) {
        
        self.layer.cornerRadius = 5;
        
        self.layer.masksToBounds = NO;
        
        self.layer.shadowColor = [UIColor grayColor].CGColor;
        self.layer.shadowOpacity = 0.6;
        self.layer.shadowRadius = 5;
        self.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
 
    }
    
    return self;
    
}
@end
