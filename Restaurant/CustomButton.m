//
//  CustomButton.m
//  Restaurant
//
//  Created by HN on 03/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "CustomButton.h"
#import <QuartzCore/QuartzCore.h>

@implementation CustomButton


- (id)initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
    
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        
//        [self setBackgroundImage:[UIImage imageNamed:@"bordered_button.png"] forState:UIControlStateNormal];
        //        [self setTitle:self.titleLabel.text.uppercaseString forState:UIControlStateNormal];
        
        [self.titleLabel setFont:[UIFont systemFontOfSize:13 weight:UIFontWeightMedium]];
        
        self.clipsToBounds = YES;
        self.layer.cornerRadius = self.frame.size.height / 2;
        self.layer.borderColor = [UIColor whiteColor].CGColor;
        self.layer.borderWidth = 2.0f;
    }
    
    return self;
    
}

@end
