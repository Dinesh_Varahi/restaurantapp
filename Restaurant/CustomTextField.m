//
//  CustomTextField.m
//  Restaurant
//
//  Created by HN on 03/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "CustomTextField.h"

@implementation CustomTextField


- (id)initWithCoder:(NSCoder*)coder {
   
    self = [super initWithCoder:coder];
    
    if (self) {
       
        self.borderStyle = UITextBorderStyleNone;
        
        CALayer *border = [CALayer layer];
        CGFloat borderWidth = 1.5;
        border.borderColor = [UIColor textFieldBorderColor].CGColor;
        border.frame = CGRectMake(0,self.frame.size.height - borderWidth , self.frame.size.width, self.frame.size.height);
        border.borderWidth = borderWidth;
        [self.layer addSublayer:border];
        self.layer.masksToBounds = YES;
        
    
    }
    
    return self;
}

@end
