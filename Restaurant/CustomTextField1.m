//
//  CustomTextField1.m
//  Restaurant
//
//  Created by HN on 03/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "CustomTextField1.h"

@implementation CustomTextField1


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    self.borderStyle = UITextBorderStyleNone;
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1.5;
    border.borderColor = [UIColor darkGrayColor].CGColor;
    border.frame = CGRectMake(0,self.frame.size.height - borderWidth , self.frame.size.width, self.frame.size.height);
    border.borderWidth = borderWidth;
    [self.layer addSublayer:border];
    self.layer.masksToBounds = YES;
    
    
}

/*
- (id)initWithCoder:(NSCoder*)coder {
    
    self = [super initWithCoder:coder];
    
    if (self) {
        
        
        
     
    }
    
    return self;
    
}
 */

@end
