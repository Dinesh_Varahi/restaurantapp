//
//  CustomTextView.h
//  Restaurant
//
//  Created by HN on 04/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTextView : UITextView
@property (nonatomic, strong) UILabel *placeHolderLabel;
@end
