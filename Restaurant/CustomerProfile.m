//
//  CustomerProfile.m
//  Restaurant
//
//  Created by HN on 01/12/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "CustomerProfile.h"

@implementation CustomerProfile

// Specify default values for properties

//+ (NSDictionary *)defaultPropertyValues
//{
//    return @{};
//}

// Specify properties to ignore (Realm won't persist these)

//+ (NSArray *)ignoredProperties
//{
//    return @[];
//}

@end
