//
//  CustomerProfileCell.h
//  Restaurant
//
//  Created by Parth Pandya on 09/02/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerProfileCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *lblName;

@end
