//
//  DashboardViewController.h
//  Restaurant
//
//  Created by HN on 16/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DashboardTableViewCell.h"
#import "ApplyForJobViewController.h"
#import "Servers.h"
#import "AppDelegate.h"
#import "LoginView.h"
#import "HCSStarRatingView.h"
#import "JobsAppliedViewController.h"
#import "RatingViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface DashboardViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>
{
    
    __weak IBOutlet UIImageView *imgProfilePic;
    __weak IBOutlet UILabel *lblName;
    IBOutletCollection(UIImageView) NSArray *rateStar;
    
    __weak IBOutlet UIProgressView *pvRateOne;
    __weak IBOutlet UIProgressView *pvRateTwo;
    __weak IBOutlet UIProgressView *pvRateThree;
    __weak IBOutlet UIProgressView *pvRateFour;
    __weak IBOutlet UIProgressView *pvRateFive;
    
    __weak IBOutlet UILabel *lblRateOne;
    __weak IBOutlet UILabel *lblRateTwo;
    __weak IBOutlet UILabel *lblRateThree;
    __weak IBOutlet UILabel *lblRateFour;
    __weak IBOutlet UILabel *lblRateFive;
    
    
    __weak IBOutlet UITableView *tabelView;
 
    
}


- (IBAction)btnLogoutPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *tableView;


- (IBAction)btnApplyForJobPressed:(id)sender;
//@property (weak, nonatomic) IBOutlet UIView *starRatings;

@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRatings;


@end
