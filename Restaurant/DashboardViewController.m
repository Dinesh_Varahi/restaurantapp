//
//  DashboardViewController.m
//  Restaurant
//
//  Created by HN on 16/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "DashboardViewController.h"
#import "ServerRatingsVC.h"

@interface DashboardViewController ()
{
    
}

@end

@implementation DashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    imgProfilePic.layer.cornerRadius = imgProfilePic.frame.size.width / 2;
    imgProfilePic.layer.masksToBounds = YES;
    
    [[NSUserDefaults standardUserDefaults] setValue:@"server" forKey:@"Sender"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setServerInfo) name:@"profileFetched" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reSetNavigationBar)
                                                 name:@"rsetDashboardNavigation"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reSetNavigationBar)
                                                 name:@"rsetDashboardNavigation1"
                                               object:nil];
    

    [self setNaigationBar];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [RLMRealm defaultRealm];
       [self setServerInfo];
    [self setNaigationBar];
}

-(void)setNaigationBar {
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.view.backgroundColor = [UIColor appMainColor];
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
}

- (void)reSetNavigationBar{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBarTintColor:[UIColor clearColor]];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    
    
}
- (void)setServerInfo {
    
    
    [APP.hud setHidden:YES];
    
    //Enable Comment Button
    dispatch_after(2, dispatch_get_main_queue(), ^(void){
    
    Servers *newServer = [[Servers allObjects] firstObject];
    
   NSLog(@"Server info:%@",newServer);
    
    // Assigning Name
    if (newServer.serverName.length > 0)
    {
       lblName.text = [NSString upperCase:newServer.serverName];
    }
    
    //Assigning Total Ratings to that Server
    
    _starRatings.value = newServer.avgRating;
    lblRateOne.text = [NSString stringWithFormat:@"%ld",(long)newServer.oneRatings];
    lblRateTwo.text = [NSString stringWithFormat:@"%ld",(long)newServer.twoRatings];
    lblRateThree.text = [NSString stringWithFormat:@"%ld",(long)newServer.threeRatings];
    lblRateFour.text = [NSString stringWithFormat:@"%ld",(long)newServer.fourRatings];
    lblRateFive.text = [NSString stringWithFormat:@"%ld",(long)newServer.fiveRatings];
    float TotalRatings = newServer.toalRatings;
    
    pvRateOne.progress = newServer.oneRatings/TotalRatings;
    pvRateTwo.progress = newServer.twoRatings/TotalRatings;
    pvRateThree.progress = newServer.threeRatings/TotalRatings;
    pvRateFour.progress = newServer.fourRatings/TotalRatings;
    pvRateFive.progress = newServer.fiveRatings/TotalRatings;
    
    [rateStar enumerateObjectsUsingBlock:^(UIImageView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj setImage:(idx < (NSInteger)newServer.avgRating ? [UIImage imageNamed:@"star_blank.png"] : [UIImage imageNamed:@"star_filled.png"])];
    }];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        // Assigning Image
        
        [imgProfilePic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,newServer.imageURL]]
                         placeholderImage:[UIImage imageNamed:@"ic_user_b.png"]];
        
    });
    
        
    });
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView delegate Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([tableView isEqual:self.tableView])
    {
        return 1;
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath  {
    cell.backgroundColor = [UIColor clearColor];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 65;
}
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Servers *newServer = [[Servers allObjects] firstObject];
    
    DashboardTableViewCell *Cell = [[DashboardTableViewCell alloc] init];
    Cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    if (indexPath.row == 0) {
//        NSLog(@"Jobs Applied");
    
        Cell.lblTitle.text = @"Jobs Applied";
     
        Cell.lblDescription.text = [NSString stringWithFormat:@"You applied %ld Jobs in last 30 days",(long)newServer.jobsInPastThirtyDays];

        Cell.lblCount.text = [NSString stringWithFormat:@"%ld",(long)newServer.totalJobsApplied];
        
    }
    if (indexPath.row == 1) {
//        NSLog(@"Total Ratings");
        
        Cell.lblTitle.text = @"Total Ratings";
        //Cell.lblDescription.text = @"Last rating you got 2 days ago";
        if (newServer.toalRatings == 0)
        {
            Cell.lblDescription.text = @"No Rating's";
        }
//        else if (newServer.lastRating == 0)
//        {
//            Cell.lblDescription.text = @"Last rating you got today";
//        }
        else
        {
            Cell.lblDescription.text = newServer.lastRating;//[NSString stringWithFormat:@"Last rating you got %ld days ago",(long)newServer.lastRating];
        }

        Cell.lblCount.text = [NSString stringWithFormat:@"%ld",(long)newServer.toalRatings];
        
    }
    if (indexPath.row == 2) {
//        NSLog(@"Total Comments");
        Cell.lblTitle.text = @"Total Comments";
        if (newServer.totalComments == 0)
        {
            Cell.lblDescription.text = @"No Comment's";
        }
//        else if (newServer.lastComment == 0)
//        {
//            Cell.lblDescription.text = @"Last comment you got today";
//        }
        else
        {
            Cell.lblDescription.text = newServer.lastComment;//[NSString stringWithFormat:@"Last comment you got %ld days ago",(long)newServer.lastComment];
        }

        Cell.lblCount.text = [NSString stringWithFormat:@"%ld",(long)newServer.totalComments];
    }
    if (indexPath.row == 3) {
//        NSLog(@"Sharing Rewards");
        Cell.lblTitle.text = @"Sharing Reward";
        Cell.lblDescription.text = @"Get $5 for every new Bartender/Server registration from your link.";
        Cell.lblCount.text = [NSString stringWithFormat:@"$%ld",(long)newServer.reward];
    }

    return Cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
//        NSLog(@"Jobs Applied");
       
        
        JobsAppliedViewController *jobVC = [self.storyboard instantiateViewControllerWithIdentifier:@"JobsAppliedViewController"];
        [self.navigationController pushViewController:jobVC animated:NO];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"setNavigation2" object:self];
        
    }
    if (indexPath.row == 1) {
//        NSLog(@"Total Ratings");
        
        ServerRatingsVC *ratingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ServerRatingsVC"];
        
        [self.navigationController pushViewController:ratingVC animated:NO];
        
    }
    if (indexPath.row == 2) {
//        NSLog(@"Total Comments");
        RatingViewController *ratingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RatingViewController"];
        
        [self.navigationController pushViewController:ratingVC animated:NO];
       
    }
    if (indexPath.row == 3) {
//        NSLog(@"Sharing Rewards");
        [RLMRealm defaultRealm];
        Servers *aServer = [[Servers allObjects] firstObject];
        NSString *message = [NSString stringWithFormat:@"I'm using Tapped App. It's simply an awesome app. Give it a Try with Referral Code : \"%@\"",aServer.referralCode];
        //this is your text string to share
        
        NSArray *activityItems = @[message];
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        activityVC.excludedActivityTypes = @[UIActivityTypePostToWeibo,
                                             UIActivityTypePrint,
                                             UIActivityTypeCopyToPasteboard,
                                             UIActivityTypeAssignToContact,
                                             UIActivityTypeSaveToCameraRoll,
                                             UIActivityTypeAddToReadingList,
                                             UIActivityTypePostToFlickr,
                                             UIActivityTypePostToVimeo,
                                             UIActivityTypePostToTencentWeibo,
                                             UIActivityTypeAirDrop];
        [self presentViewController:activityVC animated:false completion:nil];
       
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}
- (IBAction)btnApplyForJobPressed:(id)sender {
    
    ApplyForJobViewController *applyJob = [self.storyboard instantiateViewControllerWithIdentifier:@"ApplyForJobViewController"];
    
    [self.navigationController pushViewController:applyJob animated:NO];
    
    
}


- (IBAction)btnLogoutPressed:(id)sender {
    
//    [[NSUserDefaults standardUserDefaults] setValue:@"no" forKey:@"isuserloggedin"];
//    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"rememberMe"];
//    LoginView *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
//    [self.navigationController pushViewController:login animated:NO];
    
    
}
@end
