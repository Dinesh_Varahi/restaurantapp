//
//  ExperianceCell.h
//  Restaurant
//
//  Created by Parth Pandya on 05/12/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerWorkExperience.h"

@interface ExperianceCell : UITableViewCell
{
    RLMResults<ServerWorkExperience  *> *arrServerWorkExperience;
}
@property (weak, nonatomic) IBOutlet UILabel *lblRestaurantName;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblWorkProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblFromToDate;

@property (weak, nonatomic) IBOutlet UIButton *btnEdit;

- (IBAction)btnEditPressed:(id)sender;

@end
