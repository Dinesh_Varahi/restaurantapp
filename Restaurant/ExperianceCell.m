//
//  ExperianceCell.m
//  Restaurant
//
//  Created by Parth Pandya on 05/12/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "ExperianceCell.h"

@implementation ExperianceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btnEditPressed:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%ld",(long)btn.tag] forKey:@"editTag"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"EditPressed" object:self];
}



@end
