//
//  ForgotPasswordViewController.h
//  Restaurant
//
//  Created by HN on 09/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking.h>
#import "AppDelegate.h"

@interface ForgotPasswordViewController : UIViewController<UITextFieldDelegate>


@property (weak, nonatomic) IBOutlet UITextField *txtEmailAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;


- (IBAction)btnSubmit:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtOTP;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;



@end
