//
//  ForgotPasswordViewController.m
//  Restaurant
//
//  Created by HN on 09/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "ForgotPasswordViewController.h"

@interface ForgotPasswordViewController ()

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _txtOTP.delegate = self;
    _txtEmailAddress.delegate = self;
    _txtNewPassword.delegate = self;
    _txtConfirmPassword.delegate = self;
    
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)btnSubmit:(id)sender {
    
    if ([_btnSubmit.titleLabel.text isEqualToString:@"Submit"])//![[NSUserDefaults standardUserDefaults]valueForKey:@"Process"] || [[[NSUserDefaults standardUserDefaults]valueForKey:@"Process"] isEqualToString:@"ForgotPassword"])
    {
        
        if (_txtEmailAddress.text.length > 0) {
     
            [APP huddie];        
            
            NSMutableDictionary *dataDict = [NSMutableDictionary new];
            
            [dataDict setValue:_txtEmailAddress.text  forKey:@"email"];
            
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDict options:0 error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            
            NSString *urlString =[[NSString stringWithFormat:@"%@%@",SITE_URL,FORGOT_PASSWORD_URL]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            
            NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:urlString parameters:nil error:nil];
            
            [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
            
            [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse *response, id data, NSError *error) {
                
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                //            double status = (long)[httpResponse statusCode];
                NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
                
                if ([data isKindOfClass:[NSDictionary class]]){
                    
                    NSDictionary *dataDic = (NSDictionary *)data;
                    NSLog(@"Data:%@",dataDic);
                    
                    NSInteger status = [[dataDic valueForKey:@"status"] integerValue];
                    if(status == 202)
                    {
                        
                        NSDictionary  *serverData = [[NSDictionary alloc] init]; ;
                        serverData = [dataDic valueForKey:@"data"];
                        
                        NSLog(@"Received Data:%@",serverData);
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"Email Sent"
                                                           message:@"One Time Password (OTP) sent successfully to your mail id"
                                                           preferredStyle:UIAlertControllerStyleAlert];
                            
                            [APP.hud setHidden:YES];
                            
                            [self presentViewController:alert2 animated:NO completion:nil];
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                                self.btnSubmit.titleLabel.text = @"Update Password";
                                
                                [self.btnSubmit setTitle:@"Update Password" forState:UIControlStateNormal];
                                //[[NSUserDefaults standardUserDefaults]setObject:@"ResetPassword" forKey:@"Process"];
                                _txtOTP.hidden = NO;
                                _txtNewPassword.hidden = NO;
                                _txtConfirmPassword.hidden = NO;
                                _txtEmailAddress.userInteractionEnabled = NO;
                                _txtEmailAddress.textColor = [UIColor lightGrayColor];
                                _lblMessage.text = @"Please enter One Time Password (OTP) received on your registered Email ID";
                                
                            });
                        });
                        
                    }
                    else //if(status == 400)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"Sorry"
                                                           message:[dataDic valueForKey:@"result"]
                                                           preferredStyle:UIAlertControllerStyleAlert];
                            [APP.hud setHidden:YES];
                            [self presentViewController:alert2 animated:NO completion:nil];
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                            });
                        });
                        
                    }
                    
                }
                
            }] resume];
            
        }
        else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertController * alert2=   [UIAlertController
                                               alertControllerWithTitle:@""
                                               message:@"Please enter Email Address"
                                               preferredStyle:UIAlertControllerStyleAlert];
                [APP.hud setHidden:YES];
                [self presentViewController:alert2 animated:NO completion:nil];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [alert2 dismissViewControllerAnimated:NO completion:nil];
                });
            });
            return;
        }
    }
    else
    {
        if ([_txtNewPassword.text isEqualToString:_txtConfirmPassword.text]) {
            
            [APP huddie];
            
            NSMutableDictionary *dataDict = [NSMutableDictionary new];
            
            [dataDict setValue:_txtEmailAddress.text  forKey:@"email"];
            [dataDict setValue:_txtOTP.text forKey:@"otp"];
            [dataDict setValue:_txtNewPassword.text forKey:@"password"];
            
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDict options:0 error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            
            NSString *urlString =[[NSString stringWithFormat:@"%@%@",SITE_URL,RESET_PASSWORD_URL] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

            
            NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:urlString parameters:nil error:nil];
            
            [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
            
            [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse *response, id data, NSError *error) {
                
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                //            double status = (long)[httpResponse statusCode];
                NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
                
                if ([data isKindOfClass:[NSDictionary class]]){
                    
                    NSDictionary *dataDic = (NSDictionary *)data;
                    NSLog(@"Data:%@",dataDic);
                    
                    NSInteger status = [[dataDic valueForKey:@"status"] integerValue];
                    if(status == 202)
                    {
                        
                        [[NSUserDefaults standardUserDefaults]setObject:@"ForgotPassword" forKey:@"Process"];
                        NSDictionary  *serverData = [[NSDictionary alloc] init]; ;
                        serverData = [dataDic valueForKey:@"data"];//ForgotPassword
                        
                        NSLog(@"Received Data:%@",serverData);
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"Password Reset Successful"
                                                           message:@"Your new password has been set successfully, Please logIn again with new password."
                                                           preferredStyle:UIAlertControllerStyleAlert];
                            
                            [APP.hud setHidden:YES];
                            [self presentViewController:alert2 animated:NO completion:nil];
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                                [self.navigationController popViewControllerAnimated:YES];
                                
                            });
                        });
                        
                    }
                    else if(status == 400)
                    {
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"Sorry"
                                                           message:[dataDic valueForKey:@"result"]
                                                           preferredStyle:UIAlertControllerStyleAlert];
                            
                            [APP.hud setHidden:YES];
                            [self presentViewController:alert2 animated:NO completion:nil];
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                            });
                        });
                        
                    }
                    
                }
            }] resume];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertController * alert2=   [UIAlertController
                                               alertControllerWithTitle:@""
                                               message:@"Password Does Not Matches"
                                               preferredStyle:UIAlertControllerStyleAlert];
                [APP.hud setHidden:YES];
                [self presentViewController:alert2 animated:NO completion:nil];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [alert2 dismissViewControllerAnimated:NO completion:nil];
                });
            });
            
        }
        
    }
    
}

@end
