//
//  HeaderFieldsCell.h
//  Restaurant
//
//  Created by Parth Pandya on 25/12/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MVPlaceSearchTextField.h"
#import <QuartzCore/QuartzCore.h>
#import "MLPAutoCompleteTextFieldDelegate.h"
#import "MLPAutoCompleteTextFieldDataSource.h"
#import "DEMODataSource.h"
#import "AddressResolver.h"
#import "CustomButton.h"


@class AddressResolver;
@class HeaderFieldsCell;
@class DEMODataSource;
@class MLPAutoCompleteTextField;

@protocol HeaderViewDelegate <NSObject>

-(void)headerView:(HeaderFieldsCell*)view didEndEditingServerNameTextField:(UITextField*)serverName;
-(void)headerView:(HeaderFieldsCell*)view didEndEditingCityNameTextField:(UITextField*)cityName;
-(void)headerView:(HeaderFieldsCell*)view didEndEditingRestaurantNameTextField:(UITextField*)restaurantName;
-(void)headerView:(HeaderFieldsCell*)view resolvedAddressSelected:(NSString*)location;
@end


@interface HeaderFieldsCell : UITableViewCell<UITextFieldDelegate,PlaceSearchTextFieldDelegate, MLPAutoCompleteTextFieldDataSource, MLPAutoCompleteTextFieldDelegate>
{
    id <HeaderViewDelegate> delegate;
    NSMutableArray *allRestaurants;
    NSDictionary *serverDict;
}
@property (strong, nonatomic) IBOutlet UIButton *btnSearch;
@property (strong, nonatomic) IBOutlet UITextField *txtServerName;
@property (strong, nonatomic) IBOutlet MVPlaceSearchTextField *txtLocation;
//@property (strong, nonatomic) IBOutlet UITextField *txtRestaurantName;
@property (strong, nonatomic) IBOutlet UIButton *overlayButton;

@property (assign) BOOL testWithAutoCompleteObjectsInsteadOfStrings;
@property (assign) BOOL simulateLatency;
@property (strong, nonatomic) IBOutlet DEMODataSource *autocompleteDataSource; 
@property (strong,nonatomic) NSDictionary *locationInfo;
@property (weak, nonatomic) IBOutlet MLPAutoCompleteTextField *txtRestaurantName;
@property (weak, nonatomic) NSString* strCity;
@property (nonatomic, weak) id <HeaderViewDelegate> delegate;

//- (IBAction)btnSubmitPressed:(id)sender;
@end
