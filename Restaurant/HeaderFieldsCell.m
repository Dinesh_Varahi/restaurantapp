//
//  HeaderFieldsCell.m
//  Restaurant
//
//  Created by Parth Pandya on 25/12/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "HeaderFieldsCell.h"
#import "DEMOCustomAutoCompleteCell.h"
#import "AppDelegate.h"

@implementation HeaderFieldsCell
{
    CLLocationCoordinate2D locationCoord;
}


@synthesize delegate = _delegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.txtServerName.delegate = self;
    self.txtLocation.delegate = self;
    self.txtRestaurantName.delegate = self;
    
    
    
    self.txtLocation.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"location"];
    
    self.strCity = self.txtLocation.text;
    
    [self.btnSearch addTarget:self action:@selector(hideKeyboard:) forControlEvents:UIControlEventTouchUpInside];
    
    self.txtRestaurantName.autoCompleteDelegate = self;
    self.txtRestaurantName.delegate = self;
    
    CALayer *border1 = [CALayer layer];
    CGFloat borderWidth1 = 1.5;
    border1.borderColor = [UIColor textFieldBorderColor].CGColor;
    border1.frame = CGRectMake(0,self.txtRestaurantName.frame.size.height - borderWidth1 , self.txtRestaurantName.frame.size.width, self.txtRestaurantName.frame.size.height);
    border1.borderWidth = borderWidth1;
    [self.txtRestaurantName.layer addSublayer:border1];
    self.txtRestaurantName.layer.masksToBounds = YES;

    [_txtRestaurantName addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    self.txtRestaurantName.autoCompleteTableCellBackgroundColor = [UIColor whiteColor];
    self.txtRestaurantName.autoCompleteShouldHideOnSelection = YES;
    self.txtRestaurantName.autoCompleteShouldHideClosingKeyboard = YES;
//    self.txtRestaurentName.showAutoCompleteTableWhenEditingBegins = YES;
    
    
    
    [self.txtRestaurantName addTarget:self
                               action:@selector(typeDidChange:)
                     forControlEvents:UIControlEventValueChanged];
    
    
    if ([[[UIDevice currentDevice] systemVersion] compare:@"6.0" options:NSNumericSearch] != NSOrderedAscending) {
        [self.txtRestaurantName registerAutoCompleteCellClass:[DEMOCustomAutoCompleteCell class]
                                       forCellReuseIdentifier:@"CustomCellId"];
    }
    else{
        //Turn off bold effects on iOS 5.0 as they are not supported and will result in an exception
        self.txtRestaurantName.applyBoldEffectToAutoCompleteSuggestions = NO;
    }

    self.txtRestaurantName.borderStyle = UITextGranularityLine;
    self.txtRestaurantName.backgroundColor = [UIColor clearColor];
    
    [self customTextField];
    [self setupAutoCompleteTextField];
    [self setChangeLocationButton];

}

-(void)textDidChange:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setValue:_txtRestaurantName.text forKey:@"NameOfRestro"];
    
}

- (void)typeDidChange:(UISegmentedControl *)sender
{
    if(sender.selectedSegmentIndex == 0){
        [self.txtRestaurantName setAutoCompleteTableAppearsAsKeyboardAccessory:NO];
    } else {
        [self.txtRestaurantName setAutoCompleteTableAppearsAsKeyboardAccessory:YES];
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    //    self.view.frame  = CGRectMake(0, -30, self.view.frame.size.width,self.view.frame.size.height)    ;
    
    
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    textField.textColor = [UIColor whiteColor];
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField {
    
    //    self.view.frame  = CGRectMake(0, 0, self.view.frame.size.width,self.view.frame.size.height)    ;
    
    if (textField == _txtServerName) {
        NSLog(@"Server:%@",_txtServerName.text);
        [self.delegate headerView:self didEndEditingServerNameTextField:textField];
        
    }
    if (textField == _txtLocation) {
        NSLog(@"City:%@",_txtLocation.text);
        
        
//        if (_txtLocation.text.length > 0) {
//            location = [[AddressResolver sharedInstance] validateAddress:_txtLocation.text];
//            
//            _txtLocation.text = location;
//        }
    }
    if (textField == _txtRestaurantName) {
        NSLog(@"Restaurant:%@",_txtRestaurantName.text);
        
        [self.delegate headerView:self didEndEditingRestaurantNameTextField:textField];
    }
    
    
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.superview endEditing:YES];
}




#pragma mark - MLPAutoCompleteTextField Delegate

- (BOOL)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
shouldStyleAutoCompleteTableView:(UITableView *)autoCompleteTableView
               forBorderStyle:(UITextBorderStyle)borderStyle;
{
    
    
    
    return YES;
}


- (BOOL)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
          shouldConfigureCell:(UITableViewCell *)cell
       withAutoCompleteString:(NSString *)autocompleteString
         withAttributedString:(NSAttributedString *)boldedString
        forAutoCompleteObject:(id<MLPAutoCompletionObject>)autocompleteObject
            forRowAtIndexPath:(NSIndexPath *)indexPath;
{
    //This is your chance to customize an autocomplete tableview cell before it appears in the autocomplete tableview
    cell.backgroundColor = [UIColor lightGrayColor];
    
    return YES;
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
  didSelectAutoCompleteString:(NSString *)selectedString
       withAutoCompleteObject:(id<MLPAutoCompletionObject>)selectedObject
            forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(selectedObject){
        NSLog(@"selected object from autocomplete menu %@ with string %@", selectedObject, [selectedObject autocompleteString]);
    } else {
        NSLog(@"selected string '%@' from autocomplete menu", selectedString);
        
        self.txtRestaurantName.text = selectedString;
        
    }
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField willHideAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view will be removed from the view hierarchy");
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField willShowAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view will be added to the view hierarchy");
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField didHideAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view ws removed from the view hierarchy");
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField didShowAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view was added to the view hierarchy");
}


- (void) customTextField {
    
    CALayer *border;
    CGFloat borderWidth ;
    
    _txtLocation.borderStyle = UITextBorderStyleNone;
    
    border = [CALayer layer];
    borderWidth = 1.5;
    border.borderColor = [UIColor textFieldBorderColor].CGColor;
    border.frame = CGRectMake(0,_txtLocation.frame.size.height - borderWidth , _txtLocation.frame.size.width, _txtLocation.frame.size.height);
    border.borderWidth = borderWidth;
    [_txtLocation.layer addSublayer:border];
    _txtLocation.layer.masksToBounds = YES;
}


#pragma mark - Place search Textfield Delegates

-(void)setupAutoCompleteTextField {
    
    _txtLocation.placeSearchDelegate = self;
    
    _txtLocation.strApiKey = kGoogleAPIKey;
    
    _txtLocation.superViewOfList = self.superview;
    
    _txtLocation.autoCompleteShouldHideOnSelection = YES;
    
    _txtLocation.maximumNumberOfAutoCompleteRows  = 2;
    
    _txtLocation.partOfAutoCompleteRowHeightToCut = 0.2;
    
    
}

-(void)placeSearch:(MVPlaceSearchTextField*)textField ResponseForSelectedPlace:(GMSPlace*)responseDict {
    [self.superview endEditing:YES];
    [_txtLocation resignFirstResponder];
    
    NSLog(@"SELECTED ADDRESS :%@",responseDict);
    NSLog(@"City Name:%@",responseDict.types);
    
    locationCoord.latitude = responseDict.coordinate.latitude;
    locationCoord.longitude = responseDict.coordinate.longitude;
    
    if (responseDict)
    {
        _locationInfo = [[NSDictionary alloc] init];
        _locationInfo = [[AddressResolver sharedInstance] getAddressDetails:locationCoord];
        self.txtLocation.text = [_locationInfo valueForKey:@"city"];
        self.strCity = [_locationInfo valueForKey:@"city"];
        NSLog(@"Address Details:%@",self.strCity);
    }
    
    
}
-(void)placeSearchWillShowResult:(MVPlaceSearchTextField*)textField {
    
}

//- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
// possibleCompletionsForString:(NSString *)string
//            completionHandler:(void (^)(NSArray *))handler
//{
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
//    dispatch_async(queue, ^{
//        if(self.simulateLatency){
//            CGFloat seconds = arc4random_uniform(4)+arc4random_uniform(4); //normal distribution
//            NSLog(@"sleeping fetch of completions for %f", seconds);
//            sleep(seconds);
//        }
//        
//        NSArray *completions;
//        if(self.testWithAutoCompleteObjectsInsteadOfStrings){
//            completions = [self allCountries];
//        } else {
//            completions = [self allCountries];
//        }
//        
//        handler(completions);
//    });
//}

- (NSArray *)allCountries
{
 
    allRestaurants = [[NSMutableArray alloc]init];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSString *urlString =[[NSString stringWithFormat:@"%@%@?restaurant=%@",SITE_URL,@"/restaurant/getRestaurants",self.txtRestaurantName.text] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

    
    NSURL *URL = [NSURL URLWithString:urlString];
    NSLog(@"%@",URL);
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    
    
    
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        
        
        if (error) {
            NSLog(@"Error: %@", error.description);
            dispatch_async(dispatch_get_main_queue(), ^{[APP.hud setHidden:YES];});
            
        }
        else {
            //            NSLog(@"%@ %@", response, responseObject);
            
            if ([responseObject isKindOfClass:[NSArray class]])
            {
//                NSArray *jsonArray = (NSArray *)responseObject  ;
                //                NSLog(@"Json:%@",jsonArray);
            }
            
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                
                
                serverDict = (NSDictionary *)responseObject;
                //[arrServers addObjectsFromArray:[serverDict valueForKey:@"data"]];
                //arrServers = [serverDict valueForKey:@"data"];
                
            }
        }
    }];
    [dataTask resume];

    return allRestaurants;
}

-(void)placeSearchWillHideResult:(MVPlaceSearchTextField*)textField {
    
}
-(void)placeSearch:(MVPlaceSearchTextField*)textField ResultCell:(UITableViewCell*)cell withPlaceObject:(PlaceObject*)placeObject atIndex:(NSInteger)index {
    
    cell.contentView.backgroundColor = [UIColor whiteColor];
}

- (void) setChangeLocationButton {
    _overlayButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_overlayButton setTitle:@"Change City" forState:UIControlStateNormal];
    [_overlayButton addTarget:self action:@selector(changeLocationPressed:)
             forControlEvents:UIControlEventTouchUpInside];
    [_overlayButton setFrame:CGRectMake(0, 0, _txtLocation.frame.size.width/2.5, 28)];
    _overlayButton.titleLabel.font = [UIFont systemFontOfSize:14];
    
    _overlayButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    
    // Assign the overlay button to a stored text field
    self.txtLocation.rightView = _overlayButton;
    self.txtLocation.rightViewMode = UITextFieldViewModeAlways;
}

- (void) changeLocationPressed:(id)sender {
    NSLog(@"Change Location");
    _txtLocation.text = @"";
    
    
    
}

- (void) hideKeyboard:(id)sender {
    
    
    [self.superview endEditing:YES];
    
    NSMutableDictionary *tmpData = [[NSMutableDictionary alloc] init];
//    [tmpData setValue:self.strCity forKey:@"location"];
    [tmpData setValue:self.txtLocation.text forKey:@"location"];    
    [tmpData setValue:_txtServerName.text forKey:@"serverName"];
    [tmpData setValue:_txtRestaurantName.text forKey:@"restaurantName"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"searchServerPressed" object:tmpData];
    
}


- (IBAction)btnSubmitPressed:(id)sender {
    
    
}
@end
