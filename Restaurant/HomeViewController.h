//
//  HomeViewController.h
//  Restaurant
//
//  Created by HN on 03/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MapKit/MapKit.h>
#import <GoogleMaps/GoogleMaps.h>

#import "AppDelegate.h"
#import "LoginView.h"
#import "AddServerViewController.h"
#import "ServerSearchViewController.h"
#import "ServerDetailViewController.h"
#import "MVPlaceSearchTextField/MVPlaceSearchTextField.h"
#import "AddressResolver.h"
#import "CustomButton.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "HeaderFieldsCell.h"
#import "MenuVC.h"
#import "PrivcayPoliciesViewController.h"

@class AddressResolver;


@interface HomeViewController : UIViewController <UITextFieldDelegate,UICollectionViewDataSource,PlaceSearchTextFieldDelegate,CLLocationManagerDelegate,GMSMapViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,AppLocationDelegate,HeaderViewDelegate>
{
    BOOL *loggedInStatus;
    
    NSString *aServerName;
    NSString *alocation;
    NSString *aRestaurantName;
    AppLocationManager *locationController;
}
@property (weak, nonatomic) IBOutlet UITableView *tblDashboard;
@property (weak, nonatomic) IBOutlet UITextField *txtServerName;
@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *txtLocation;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnMenu;


@property (weak, nonatomic) IBOutlet UITextField *txtRestaurantName;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnLogin;
@property (weak, nonatomic) IBOutlet UICollectionView *serverListCollection;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

@property (weak,nonatomic) NSString *aServerName;
@property (weak,nonatomic) NSString *alocation;
@property (weak,nonatomic) NSString *aRestaurantName;


- (IBAction)searchButtonPressed:(id)sender;
- (IBAction)btnMenuPressed:(id)sender;


@end
