//
//  HomeViewController.m
//  Restaurant
//
//  Created by HN on 03/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "HomeViewController.h"
#import "ServerListCell.h"
#import "TopServerListCell.h"
#import "HeaderFieldsCell.h"
#import "RestaurantDetailsViewController.h"
#import "TopRestaurantListCell.h"
#import "NearerRestaurantList.h"
#import "ServerListCustomFlowLayout.h"




@interface HomeViewController ()
{
    RLMResults *collectionViewRestaurantArray;
    RLMResults *collectionViewRestaurantImageArray;
    RLMResults *collectionViewServerArray;
    
    Restaurants *restaurant;
    RestaurantImages *restaurantImage;
    Servers *server;
    CLLocationCoordinate2D fromLocationCoord;
    CLLocationCoordinate2D toLocationCoord;
    NSString *fromLocationString;
    NSString *toLocationString;
    
    CALayer *border;
    CGFloat borderWidth ;
    
    
}


@property (strong, nonatomic) NSArray<UIImageView *> *ratingStars;
@property (nonatomic) NSInteger rating;

@property (nonatomic, strong) RLMResults *ServerArray;
@property (nonatomic, strong) RLMResults *RestaurantArray;



#define kHeaderIdentifier @"header"

@property(nonatomic, copy) NSAttributedString *attributedPlaceholder;

@end

@implementation HomeViewController

@synthesize aServerName = _aServerName;
@synthesize alocation = _alocation;
@synthesize aRestaurantName = _aRestaurantName;

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.collectionView registerNib:[UINib nibWithNibName:@"ServerListCell" bundle:[NSBundle mainBundle]]
          forCellWithReuseIdentifier:@"ServerListCell"];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverDetail)
                                                 name:@"serverSelected"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(searchServerPressed:)
                                                 name:@"searchServerPressed"
                                               object:nil];//ApplyForJobPressed//restoaurantSelected
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(restaurantDetails)
                                                 name:@"restaurantSelected"
                                               object:nil];//NearerRestaurantAvailable
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(OpenRestaurantList:)
                                                 name:@"NearerRestaurantAvailable"
                                               object:nil];
    
    [self updateData];//serverSelected
    
    
    // Go to Privacy view
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"goToPrivacyView"]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"goToPrivacyView"];
        
        NSLog(@"Go To Policies View");
        PrivcayPoliciesViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivcayPoliciesViewController"];
        
        [self.navigationController pushViewController:profileVC animated:NO];
    }

    [self.tblDashboard reloadData];
    
    
}

-(void)OpenRestaurantList:(NSNotification *)notification {
    if ([notification.name isEqualToString:@"NearerRestaurantAvailable"])
    {
        NSDictionary *Dict = notification.userInfo;
        NearerRestaurantList *list = [self.storyboard instantiateViewControllerWithIdentifier:@"NearerRestaurantList"];
        [self.navigationController pushViewController:list animated:YES];
        
        double delayInSeconds = 2.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"ShowRestaurantList" object:self userInfo:Dict];
        });
    }
    
}

-(void)restaurantDetails{
    
    RestaurantDetailsViewController *serverDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RestaurantDetailsViewController"];
    
    [self.navigationController pushViewController:serverDetailVC animated:NO];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"restaurantSelected"
                                                  object:nil];
}

-(void)serverDetail{
    
    ServerDetailViewController *serverDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ServerDetailViewController"];
    
    [self.navigationController pushViewController:serverDetailVC animated:NO];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"serverSelected"
                                                  object:nil];
    
    
}

- (void) viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault]; //UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.shadowImage = [UIImage new];////UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    
    self.navigationItem.hidesBackButton = YES;
    
    [_txtServerName setDelegate:self];
    [_txtLocation  setDelegate:self];
    [_txtRestaurantName setDelegate:self];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self.collectionView setDelegate:self];
    [self.collectionView setDataSource:self];
    
    self.collectionView.collectionViewLayout = [[ServerListCustomFlowLayout alloc] init];
    
    NSLog(@"Server Data:%@",collectionViewServerArray);
    
    //    NSLog(@"Server Count: %lu",(unsigned long)collectionViewServerArray.count);
    
    AppUser *user = [[AppUser alloc] init];
    
    NSLog(@"Token:%@",user.token);
    
    [self.view setNeedsLayout];
    
    
    //    [self setChangeLocationButton];
    //    [self setupAutoCompleteTextField];
    //    [self customTextField];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [APP.hud setHidden:YES];
        
    });
}

- (void) customTextField {
    
    _txtLocation.borderStyle = UITextBorderStyleNone;
    
    border = [CALayer layer];
    borderWidth = 1.5;
    border.borderColor = [UIColor textFieldBorderColor].CGColor;
    border.frame = CGRectMake(0,_txtLocation.frame.size.height - borderWidth , _txtLocation.frame.size.width, _txtLocation.frame.size.height);
    border.borderWidth = borderWidth;
    [_txtLocation.layer addSublayer:border];
    _txtLocation.layer.masksToBounds = YES;
}

#pragma mark - Set up additional data and ui
- (void) setChangeLocationButton {
    UIButton* overlayButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [overlayButton setTitle:@"Change Location" forState:UIControlStateNormal];
    [overlayButton addTarget:self action:@selector(changeLocationPressed:)
            forControlEvents:UIControlEventTouchUpInside];
    [overlayButton setFrame:CGRectMake(0, 0, _txtLocation.frame.size.width/2.5, 28)];
    overlayButton.titleLabel.font = [UIFont systemFontOfSize:14];
    
    overlayButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    
    // Assign the overlay button to a stored text field
    self.txtLocation.rightView = overlayButton;
    self.txtLocation.rightViewMode = UITextFieldViewModeAlways;
}
- (void) updateData {
    
    collectionViewRestaurantArray = [Restaurants allObjects];
    collectionViewRestaurantImageArray = [RestaurantImages allObjects];
    collectionViewServerArray = [Servers allObjects];
    self.ServerArray = [[Servers allObjects]sortedResultsUsingProperty:@"toalRatings" ascending:NO];
    
    NSLog(@"Servers:%@",_ServerArray);
    AppUser *user;
    
    NSLog(@"Token:%@",user.token);
    [self.collectionView reloadData];
    
    
    //    [self fetchDataFromServer];
    [self setLoggedInUser];
    
}
- (void) setLoggedInUser {
    
    NSString *flag = [[NSUserDefaults standardUserDefaults] valueForKey:@"isuserloggedin"];
    NSLog(@"flag:%@",flag);
    if([flag isEqualToString:@"yes"] ){
        
        //        NSString *title = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
        //        NSLog(@"Title:%@",title);
        [self.btnLogin setTitle:@"LOGOUT"];
        
    }
    else
    {
        [self.btnLogin setTitle:@"LOGIN"];
        
    }
}
- (void) changeLocationPressed:(id)sender {
    NSLog(@"Change Location");
    _txtLocation.text = @"";
    [_txtLocation becomeFirstResponder];
    
    [self initLocationManager];
    
    
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}
- (void) changePlaceHolderColor {
    if ([self.attributedPlaceholder length]) {
        // Extract attributes
        NSDictionary * attributes = (NSMutableDictionary *)[ (NSAttributedString *)self.attributedPlaceholder attributesAtIndex:0 effectiveRange:NULL];
        
        NSMutableDictionary * newAttributes = [[NSMutableDictionary alloc] initWithDictionary:attributes];
        
        [newAttributes setObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
        
        // Set new text with extracted attributes
        self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[self.attributedPlaceholder string] attributes:newAttributes];
        
    }
    
    
}
- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    textField.textColor = [UIColor whiteColor];
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    self.view.frame  = CGRectMake(0, -30, self.view.frame.size.width,self.view.frame.size.height)
    ;
    
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    self.view.frame  = CGRectMake(0, 0, self.view.frame.size.width,self.view.frame.size.height)
    ;
}

#pragma mark - Fetch Data From Server
- (void) fetchDataFromServer {
    
    
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc]
                                             initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activityView.center=self.view.center;
    [activityView startAnimating];
    [activityView hidesWhenStopped];
    [self.view addSubview:activityView];
    
    if ([self isNetworkAvailable]) {
        
        [[LocalData sharedInstance] FetchAllRestaurants:^(BOOL result) {
            if (result)
            {
                NSLog(@"Restaurant Data Downloaded");
                //                [self initLocationManager];
                
                collectionViewRestaurantArray = [Restaurants allObjects];
                collectionViewRestaurantImageArray = [RestaurantImages allObjects];
                collectionViewServerArray = [Servers allObjects];
                self.ServerArray = [[Servers allObjects]sortedResultsUsingProperty:@"toalRatings" ascending:NO];
                
                NSLog(@"Servers:%@",_ServerArray);
                AppUser *user;
                
                NSLog(@"Token:%@",user.token);
                [self.collectionView reloadData];
                
                [activityView stopAnimating];
            }
            else{
                [activityView stopAnimating];
            }
        }];
        
        //        [self initLocationManager];
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Please Check Internet Connectivity"
                                           message:@""
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:NO completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                
                [alert2 dismissViewControllerAnimated:NO completion:nil];
                return ;
                
            });
            
        });
    }
    
}

#pragma  TableView Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath  {
    cell.backgroundColor = [UIColor clearColor];
}
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (indexPath.section == 0)
    {
        HeaderFieldsCell *Cell = [[HeaderFieldsCell alloc]init];
        Cell.delegate = self;
        static NSString *simpleTableIdentifier = @"HeaderFieldsCell";
        Cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        return Cell;
    }
    else if(indexPath.section == 1){
        TopServerListCell *Cell = [[TopServerListCell alloc]init];
        static NSString *simpleTableIdentifier = @"TopServerListCell";
        Cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        return Cell;
    }
    else
    {
        TopRestaurantListCell *Cell = [[TopRestaurantListCell alloc]init];
        static NSString *simpleTableIdentifier = @"TopRestaurantListCell";
        Cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        return Cell;
    }
    
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 1)
    {
        return @"Top Bartender/Server List";
    }
    else if (section == 2)
    {
        return @"Top Restaurant List";
    }
    else
    {
        return @"";
    }
    
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    [[self view] endEditing:YES];
}

#pragma mark Search Funtion Action and Implementation
- (void)searchServerPressed:(NSNotification*)notification{
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    
    if ([notification.name isEqualToString:@"searchServerPressed"]) {
        
        data = (NSMutableDictionary*)notification.object;
        NSLog(@"Search Data:%@",data);
        
        alocation = [data valueForKey:@"location"];
        aServerName = [data valueForKey:@"serverName"];
        aRestaurantName = [data valueForKey:@"restaurantName"];
        
        if ((aServerName.length <= 0) && (aRestaurantName.length <= 0) )
        {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertController * alert2=   [UIAlertController
                                               alertControllerWithTitle:@"Please Enter Search Information"
                                               message:@"Server Name or Restaurant Name required for search result "
                                               preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert2 animated:NO completion:nil];
                
                
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [alert2 dismissViewControllerAnimated:NO completion:nil];
                    
                });
                return ;
            });
            
            
        }
        else if (alocation.length <=0 || [alocation isEqualToString:@""])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertController * alert2=   [UIAlertController
                                               alertControllerWithTitle:@"Please Enter Location Information"
                                               message:@"City is required"
                                               preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert2 animated:NO completion:nil];
                
                
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [alert2 dismissViewControllerAnimated:NO completion:nil];
                    
                });
                return ;
            });
            
        }
        
        else
        {
            
            ServerSearchViewController *searchServer = [self.storyboard instantiateViewControllerWithIdentifier:@"ServerSearchViewController"];
            searchServer.serverName = aServerName;
            searchServer.location = alocation;
            searchServer.restaurantName = aRestaurantName;
            
            [self.navigationController pushViewController:searchServer animated:NO];
            
            [[NSNotificationCenter defaultCenter] removeObserver:self
                                                            name:@"searchServerPressed"
                                                          object:nil];
            
        }
        
    }
    
    aServerName = @"";
    alocation = @"";
    aRestaurantName = @"";
    
    
}
- (void)clearTextfileds {
    [self.view endEditing:YES];
    
    _txtServerName.text = @"";
    _txtLocation.text = @"";
    _txtRestaurantName.text = @"";
    
}

#pragma mark - Place search Textfield Delegates

-(void)setupAutoCompleteTextField {
    
    _txtLocation.placeSearchDelegate = self;
    
    _txtLocation.strApiKey = kGoogleAPIKey;
    
    _txtLocation.superViewOfList = self.view;
    
    _txtLocation.autoCompleteShouldHideOnSelection = YES;
    
    _txtLocation.maximumNumberOfAutoCompleteRows  = 10;
    
    
}

-(void)placeSearch:(MVPlaceSearchTextField*)textField ResponseForSelectedPlace:(GMSPlace*)responseDict {
    [self.view endEditing:YES];
    [_txtLocation resignFirstResponder];
    
    NSLog(@"SELECTED ADDRESS :%@",responseDict);
    //    NSLog(@"Co-ordinate:(%f,%f)",responseDict.coordinate.latitude,responseDict.coordinate.longitude);
    //    NSLog(@"Address:%@",responseDict.formattedAddress);
    
    
    if (textField == _txtLocation) {
        fromLocationCoord.latitude = responseDict.coordinate.latitude;
        fromLocationCoord.longitude = responseDict.coordinate.longitude;
        //        fromLocationString = responseDict.formattedAddress;
        
        
    }
    
}
-(void)placeSearchWillShowResult:(MVPlaceSearchTextField*)textField {
    
}
-(void)placeSearchWillHideResult:(MVPlaceSearchTextField*)textField {
    
}
-(void)placeSearch:(MVPlaceSearchTextField*)textField ResultCell:(UITableViewCell*)cell withPlaceObject:(PlaceObject*)placeObject atIndex:(NSInteger)index {
    if(index%2==0){
        cell.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    }else{
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
}

#pragma mark - Get Current Location Delegates
- (void)initLocationManager {
    
    locationController = [[AppLocationManager alloc] init];
    locationController.delegate = self;
//    locationController.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    //    [locationController.locationManager startUpdatingLocation];
    if ([locationController.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [locationController.locationManager requestWhenInUseAuthorization];
    }
    
    if (![CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        UIAlertController * alert2=   [UIAlertController
                                       alertControllerWithTitle:@"Location Services Disabled!"
                                       message:@"Please enable Location Based Services for better results!"
                                       preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *settings = [UIAlertAction actionWithTitle:@"Go To Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
            
        }];
        [alert2 addAction:settings];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [alert2 dismissViewControllerAnimated:NO completion:nil];
        }];
        [alert2 addAction:cancel];
        
        [self presentViewController:alert2 animated:NO completion:nil];
        
    }
    else
    {
        //Location Services Enabled, let's start location updates
        [locationController.locationManager startUpdatingLocation];
    }
}
- (void)locationUpdate:(CLLocation *)location {
    
    NSLog(@"Location:%@",[location description]);
    
    
}

- (void)locationError:(NSError *)error {
    
    NSLog(@"Error:%@",[error description]);
    
}
- (bool)isNetworkAvailable {
    SCNetworkReachabilityFlags flags;
    SCNetworkReachabilityRef address;
    address = SCNetworkReachabilityCreateWithName(NULL, "www.apple.com" );
    Boolean success = SCNetworkReachabilityGetFlags(address, &flags);
    CFRelease(address);
    
    bool canReach = success
    && !(flags & kSCNetworkReachabilityFlagsConnectionRequired)
    && (flags & kSCNetworkReachabilityFlagsReachable);
    
    return canReach;
}



- (IBAction)searchButtonPressed:(id)sender {
    
    /*
     
     NSString *location;
     if (![_txtLocation.text isEqualToString:@""]) {
     
     location = [[AddressResolver sharedInstance] validateAddress:_txtLocation.text];
     
     
     }
     NSLog(@"Filtered Location:%@",location);
     if ((location.length <= 0) && (![_txtLocation.text isEqualToString:@""]))
     {
     
     dispatch_async(dispatch_get_main_queue(), ^{
     
     UIAlertController * alert2=   [UIAlertController
     alertControllerWithTitle:@""
     message:@"Please Enter Valid Location"
     preferredStyle:UIAlertControllerStyleAlert];
     
     [self presentViewController:alert2 animated:YES completion:nil];
     
     
     
     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
     [alert2 dismissViewControllerAnimated:YES completion:nil];
     
     });
     return ;
     });
     
     }
     else if ((_txtServerName.text.length <= 0) && (_txtRestaurantName.text.length <= 0)) {
     
     dispatch_async(dispatch_get_main_queue(), ^{
     
     UIAlertController * alert2=   [UIAlertController
     alertControllerWithTitle:@"Please Enter Information"
     message:@"Server Name or Restaurant Name required for search result "
     preferredStyle:UIAlertControllerStyleAlert];
     
     [self presentViewController:alert2 animated:YES completion:nil];
     
     
     
     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
     [alert2 dismissViewControllerAnimated:YES completion:nil];
     
     });
     return ;
     });
     
     
     }
     else {
     
     if (![_txtLocation.text isEqualToString:@""])
     {
     
     }
     ServerSearchViewController *searchServer = [self.storyboard instantiateViewControllerWithIdentifier:@"ServerSearchViewController"];
     searchServer.serverName = _txtServerName.text;
     searchServer.location = location;
     searchServer.restaurantName = _txtRestaurantName.text;
     
     [self.navigationController pushViewController:searchServer animated:YES];
     
     }
     
     location = @"";
     */
}

- (IBAction)btnMenuPressed:(id)sender {
    
    MenuVC *menuVC = [[self storyboard] instantiateViewControllerWithIdentifier:@"MenuVC"];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"toggleMenu"]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"toggleMenu"];
        
        UINavigationController *navSecondView = [[UINavigationController alloc] initWithRootViewController:menuVC];
        [self presentViewController:navSecondView animated:YES completion:nil];
        
    }

    
}

- (IBAction)loginButtonPressed:(id)sender {
    
    
    [self clearTextfileds];
    
    if ([self.btnLogin.title isEqualToString:@"LOGOUT"])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [APP huddie];
            
        });

        NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,SIGN_OUT_URL]];
        
        AppUser *user = [[AppUser allObjects] firstObject];
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"bearer %@",user.token] forHTTPHeaderField:@"Authorization"];
        
        
        [manager POST:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            
            if ([responseObject isKindOfClass:[NSArray class]])
            {
                NSArray *jsonArray = (NSArray *)responseObject  ;
                NSLog(@"Json:%@",jsonArray);
            }
            
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *dataDic = (NSDictionary *)responseObject;
//                NSDictionary *Dict = [dataDic valueForKey:@"data"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController * alert2=   [UIAlertController
                                                   alertControllerWithTitle:[dataDic valueForKey:@"result"]
                                                   message:@""
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    [APP.hud setHidden:YES];
                    [self presentViewController:alert2 animated:NO completion:nil];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        
                        RLMRealm *realm = [RLMRealm defaultRealm];
                        [realm beginWriteTransaction];
                        
                        [realm deleteObjects:[AppUser allObjectsInRealm:realm]];
                        
                        [realm commitWriteTransaction];
                        
                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                    });
                });
                
                
                GIDSignIn*sigNIn=[GIDSignIn sharedInstance];
                [sigNIn signOut];
                
                FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
                [login logOut];
                [FBSDKAccessToken setCurrentAccessToken:nil];
                [FBSDKProfile setCurrentProfile:nil];
                
                
                [self.btnLogin setTitle:@"LOGIN"];
                
                [[NSUserDefaults standardUserDefaults] setValue:@"no" forKey:@"isuserloggedin"];
                [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"commentText"];
                
                return;

                
            }
            
            
            
        }
             failure:^(NSURLSessionTask *operation, NSError *error) {
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [APP.hud setHidden:YES];
                 });
                 
                 NSLog(@"Error: %@", error);
                 
             }];

    }
    else
    {
    
    LoginView *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.navigationController pushViewController:login animated:NO];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"Home" forKey:@"Sender"];
    }
    
    
}


@end
