//
//  IntroductoryOfferCell.h
//  Restaurant
//
//  Created by HN on 16/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroductoryOfferCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnClosePressed;
@end
