//
//  JobPostSuccessAlertViewController.h
//  Restaurant
//
//  Created by HN on 04/12/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApplyForJobViewController.h"
#import "ServerLandingViewController.h"

@interface JobPostSuccessAlertViewController : UIViewController

- (IBAction)btnClosePressed:(id)sender;

- (IBAction)btnYesPressed:(id)sender;
- (IBAction)btnNoPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *alertBackground;

@end
