//
//  JobPostSuccessAlertViewController.m
//  Restaurant
//
//  Created by HN on 04/12/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "JobPostSuccessAlertViewController.h"

@interface JobPostSuccessAlertViewController ()

@end

@implementation JobPostSuccessAlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btnClosePressed:(id)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTableData" object:self];

    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)btnYesPressed:(id)sender {
//    ApplyForJobViewController *AFJVC = [[ApplyForJobViewController alloc]init];
//    [AFJVC.tableView reloadData];
    
     [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTableData" object:self];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)btnNoPressed:(id)sender {
    
    [self dismissViewControllerAnimated:NO completion:^{
        
    //[self.navigationController presentViewController:landingVC animated:YES completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"btnNoPressed" object:self];
    }];
}
@end
