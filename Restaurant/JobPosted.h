//
//  JobPosted.h
//  Restaurant
//
//  Created by Parth Pandya on 20/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <Realm/Realm.h>

@interface JobPosted : RLMObject

@property NSString *workProfile;
@property NSString *serverName;
@property NSString *imgURL;
@property NSInteger jobID;
@property NSString *date;
@property NSString *applicationStatus;
@property NSInteger count;
@property NSString *jobStatus;
@property float avgRating;
@property NSInteger totalRating;
@property NSInteger serverID;


@end

RLM_ARRAY_TYPE(JobPosted)
