//
//  JobsAppliedTableViewCell.h
//  Restaurant
//
//  Created by HN on 29/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobsAppliedTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblRestaurantName;
@property (weak, nonatomic) IBOutlet UILabel *lblWorkProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblID;
@property (weak, nonatomic) IBOutlet UIButton *btnJobStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblZIPCode;
@property (weak, nonatomic) IBOutlet UIButton *btnCity;


- (IBAction)btnCityPressed:(id)sender;

@end
