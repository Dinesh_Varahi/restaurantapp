//
//  JobsAppliedTableViewCell.m
//  Restaurant
//
//  Created by HN on 29/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "JobsAppliedTableViewCell.h"

@implementation JobsAppliedTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btnCityPressed:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%ld",(long)btn.tag] forKey:@"buttontag"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenMaps" object:self];

}
@end
