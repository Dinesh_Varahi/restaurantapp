//
//  JobsAppliedViewController.h
//  Restaurant
//
//  Created by HN on 29/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JobsAppliedTableViewCell.h"
#import "ServerJobApplied.h"
#import "ShowMapViewController.h"
#import "ApplyForJobViewController.h"

@interface JobsAppliedViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    RLMResults<AllAvailableJobs  *> *appliedJobs;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalJobsApplied;

- (IBAction)btnApplyForJob:(id)sender;


@end
