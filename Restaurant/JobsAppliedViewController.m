//
//  JobsAppliedViewController.m
//  Restaurant
//
//  Created by HN on 29/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "JobsAppliedViewController.h"

@interface JobsAppliedViewController ()

@end

@implementation JobsAppliedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.estimatedRowHeight = 96.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    
    self.tabBarController.navigationItem.leftBarButtonItem = nil;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(viewWillAppear:)
                                                 name:@"serverJobFetched"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(OpenMaps)
                                                 name:@"OpenMaps"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setNavigationBar)
                                                 name:@"setNavigation"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setNavigationBar)
                                                 name:@"setNavigation2"
                                               object:nil];


    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reSetNavigationBar)
                                                 name:@"setNavigation3"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setNavigationBar)
                                                 name:@"setJobsNavigation"
                                               object:nil];
    
    
}


- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    
    [RLMRealm defaultRealm];
    appliedJobs = [ServerJobApplied allObjects];
    
    NSLog(@"Applied Jobs:%@",appliedJobs);
    
    _lblTotalJobsApplied.text = [NSString stringWithFormat:@"%lu Jobs applied",(unsigned long)appliedJobs.count];
    //[super viewWillAppear:YES];
   self.title = @"Jobs Applied";
    //self.navigationController.navigationBar.backgroundColor = [UIColor appMainColor];
    self.tabBarController.navigationItem.title = @"Jobs Applied";
    [self.tableView reloadData];
    
}


-(void)OpenMaps {
    ShowMapViewController *ShowMapViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ShowMapViewController"];
    
    UINavigationController *navHome = [[UINavigationController alloc] initWithRootViewController:ShowMapViewController];
    
    [self presentViewController:navHome animated:NO completion:nil];
}

- (void)setNavigationBar{
    
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBarTintColor:[UIColor clearColor]];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
}
- (void)setNavigationBar1{
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.backgroundColor = [UIColor appMainColor];
    self.navigationController.view.backgroundColor = [UIColor appMainColor];
    [self.navigationController.navigationBar setBarTintColor:[UIColor appMainColor]];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor appMainColor]];
}
- (void)reSetNavigationBar{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBarTintColor:[UIColor clearColor]];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    
}


- (IBAction)btnApplyForJob:(id)sender {
    
    ApplyForJobViewController *applyJob = [self.storyboard instantiateViewControllerWithIdentifier:@"ApplyForJobViewController"];
    [applyJob setNavigationBar];
    [self.navigationController pushViewController:applyJob animated:NO];
    
    
}

#pragma mark - TableView delegate Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return appliedJobs.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 96;
}
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    JobsAppliedTableViewCell *Cell = [[JobsAppliedTableViewCell alloc] init];
    
    
    Cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    Cell.lblRestaurantName.text = [[[appliedJobs objectAtIndex:indexPath.row]valueForKey:@"restoName"] capitalizedString];
    Cell.lblZIPCode.text = @"";
    Cell.lblWorkProfile.text = [[[appliedJobs objectAtIndex:indexPath.row] valueForKey:@"workProfile"] capitalizedString];
    Cell.lblID.text = [NSString stringWithFormat:@"ID : %@",[[appliedJobs objectAtIndex:indexPath.row] valueForKey:@"jobID"]];
    //    Cell.lblZIPCode.text = [[allAvailableJobs objectAtIndex:indexPath.row] valueForKey:@"zipCode"];
    [Cell.btnCity setTitle:[[appliedJobs objectAtIndex:indexPath.row] valueForKey:@"city"] forState:UIControlStateNormal];
    Cell.btnCity.tag = indexPath.row;
    [Cell.btnJobStatus setTitle:[[appliedJobs objectAtIndex:indexPath.row]valueForKey:@"jobStatus"] forState:UIControlStateNormal];
    
    Cell.btnJobStatus.clipsToBounds = YES;
    Cell.btnJobStatus.layer.cornerRadius = Cell.btnJobStatus.frame.size.height / 2;
    if ([[[appliedJobs objectAtIndex:indexPath.row] valueForKey:@"jobStatus"] isEqualToString:@"OPEN"])
    {
        [Cell.btnJobStatus setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        Cell.btnJobStatus.layer.borderColor = [UIColor grayColor].CGColor;
    }
    else if ([[[appliedJobs objectAtIndex:indexPath.row] valueForKey:@"jobStatus"] isEqualToString:@"CLOSED"])
    {
        [Cell.btnJobStatus setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        Cell.btnJobStatus.layer.borderColor = [UIColor darkGrayColor].CGColor;
    }
    else if ([[[appliedJobs objectAtIndex:indexPath.row] valueForKey:@"jobStatus"] isEqualToString:@"PROCESSING"])
    {
        [Cell.btnJobStatus setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        Cell.btnJobStatus.layer.borderColor = [UIColor grayColor].CGColor;
    }
    else if ([[[appliedJobs objectAtIndex:indexPath.row] valueForKey:@"jobStatus"] isEqualToString:@"PENDING"])
    {
        [Cell.btnJobStatus setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        Cell.btnJobStatus.layer.borderColor = [UIColor redColor].CGColor;
    }
    Cell.btnJobStatus.layer.borderWidth = 2.0f;
    
    return Cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [NSString stringWithFormat:@"%lu Jobs applied",(unsigned long)appliedJobs.count];
}

@end
