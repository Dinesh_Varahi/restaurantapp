//
//  LocalData.h
//  Restaurant
//
//  Created by HN on 17/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "User+CoreDataClass.h"
#import <AFNetworking.h>
#import "Restaurants.h"
#import "RestaurantImages.h"
#import "Servers.h"
#import "AllAvailableJobs.h"
#import "AppUser.h"
#import "OwnerProfile.h"


@interface LocalData : NSObject
@property (strong,nonatomic) NSManagedObjectContext *managedObjectContext;

- (BOOL)loggedInStatus;


+ (instancetype)sharedInstance;



- (void)FetchAllRestaurants:(void (^)(BOOL result))aResult;
- (void)rateServer:(NSString *)serverID
            rating:(NSInteger )aRate
        completion:(void (^)(BOOL success))completionBlock;
- (void)commentServer:(NSString *)serverID
              comment:(NSString * )aComment
           completion:(void (^)(BOOL success))completionBlock;
-(void)GetAllAvailableJobs:(NSString *)aToken;

- (void)FetchRatingsAndComments:(NSString *)serverID
                          token:(NSString * )aToken
                     completion:(void (^)(BOOL success))completionBlock;

- (void)FetchServerProfile:(NSString *)aToken completion:(void (^)(BOOL success))completionBlock;
- (void)FetchRestaurantsList:(void (^)(BOOL result))aResult;

- (void)FetchOwnerProfile:(NSString *)aToken completion:(void (^)(BOOL success))completionBlock;

@end
