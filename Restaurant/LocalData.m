 //
//  LocalData.m
//  Restaurant
//
//  Created by HN on 17/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "LocalData.h"

@implementation LocalData
@synthesize managedObjectContext = _managedObjectContext;



+ (instancetype)sharedInstance {
    static id _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[[self class] alloc] init];
    });
    
    return _sharedInstance;
}

- (BOOL)loggedInStatus {
    
    _managedObjectContext = ((AppDelegate*)([UIApplication sharedApplication].delegate)).persistentContainer.viewContext;
    
    NSError *error = nil ;
    User *aUser = nil;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    [request setEntity:[NSEntityDescription entityForName:@"User" inManagedObjectContext:_managedObjectContext]];
    
    
    /*------------ For Inserting New Object -----------------*/
    //    aUser = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:_managedObjectContext];
    //    aUser.username = @"user1";
    //    aUser.isLoggenIn = NO;
    //
    /*-------------------------------------------------------*/
    
    //    NSArray *userArray = [_managedObjectContext executeFetchRequest:request error:&error];
    
    
    /*------------ For Updating existing Object -----------------*/
    aUser = [[_managedObjectContext executeFetchRequest:request error:&error] lastObject];
    /*-------------------------------------------------------*/
    //    NSLog(@"Username: %@",aUser.username);
    
    
    
    
    if(error) {
        //        NSLog(@"Unable to Save");
    }
    
    if (!aUser) {
        //        NSLog(@"No Entity");
    }
    
    //    aUser.isLoggenIn = YES;
    //    aUser.username = @"abc";
    //    aUser.password = @"123";
    //
    //    error = nil;
    //    if (![_managedObjectContext save:&error]) {
    //        NSLog(@"Error occured while saving data..");
    //    }
    //    else {
    //        NSLog(@"Data Save Successfully !!");
    //    }
    
    return aUser.isLoggenIn;
}


#pragma -
#pragma mark Customer Role Web Service Calls

- (void)FetchAllRestaurants:(void (^)(BOOL result))aResult {
    
    dispatch_async(dispatch_get_main_queue(), ^{
//    [APP huddie];
    });
    
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"location"]);
    NSString *city = [[NSUserDefaults standardUserDefaults] valueForKey:@"location"];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    NSURL *URL;
    if (city.length > 0)
    {
        NSString *urlString =[[NSString stringWithFormat:@"%@%@?city=%@",SITE_URL,GET_TOP_TEN_LIST,city] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        URL = [NSURL URLWithString:urlString];
    }
    else
    {
        
        URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?city=%@",SITE_URL,GET_TOP_TEN_LIST,@""]];
    }
    
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    
    
    
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        
        
        if (error) {
            NSLog(@"Error: %@", error.description);
            dispatch_async(dispatch_get_main_queue(), ^{[APP.hud setHidden:YES];});
            
            
            aResult(NO);
        }
        else {
            //            NSLog(@"%@ %@", response, responseObject);
            
            if ([responseObject isKindOfClass:[NSArray class]])
            {
                NSArray *jsonArray = (NSArray *)responseObject  ;
                NSLog(@"Json:%@",jsonArray);
            }
            
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                
                
                NSDictionary *dataDic = (NSDictionary *)responseObject;
                NSDictionary *data = [dataDic valueForKey:@"data"];
             
                
                NSArray *arrRestaurants = [data valueForKey:@"restaurants"];
                
                RLMRealm *realm = [RLMRealm defaultRealm];
                [realm beginWriteTransaction];
                [realm deleteObjects:[Servers allObjectsInRealm:realm]];
                [realm deleteObjects:[Restaurants allObjectsInRealm:realm]];
                [realm deleteObjects:[RestaurantImages allObjectsInRealm:realm]];
                [realm commitWriteTransaction];
                
                NSInteger count = 0;
                
                for (int i = 0; i < [arrRestaurants count]; i++) {
                    Restaurants *newRestaurant = [[Restaurants  alloc] init];
                    
//                                       NSLog(@"Restaurnat:%@",restaurantsDict);
                    
                    count ++;
                    
                    NSDictionary *restaurantsDict = [arrRestaurants objectAtIndex:i];
                    
                    [realm beginWriteTransaction];
                    
                    NSInteger idRest =[[restaurantsDict valueForKey:@"_id"] integerValue];
                    
                    NSLog(@"%ld",(long)idRest);
                    newRestaurant.restroID = idRest;
                    newRestaurant.city = [restaurantsDict valueForKey:@"city"];
                    newRestaurant.zipCode = [restaurantsDict valueForKey:@"zipCode"];
                    newRestaurant.name = [restaurantsDict valueForKey:@"name"];
                    newRestaurant.ownerName = [restaurantsDict valueForKey:@"ownerName"];
                    newRestaurant.avgRating = [[restaurantsDict valueForKey:@"avgRating"] integerValue];
                    newRestaurant.ownerEmail = [restaurantsDict valueForKey:@"ownerEmail"];
                    NSArray *arrImages = [restaurantsDict valueForKey:@"image"];
                    if (arrImages.count > 0)
                    {
                        newRestaurant.imageURL = [arrImages objectAtIndex:0];
                    }
                    
                    
                    
                    
                    NSDictionary *locations = [restaurantsDict valueForKey:@"location"];
                    
                    //                    NSLog(@"Locations:%@",locations);
                    
                    newRestaurant.latitude = [locations valueForKey:@"lat"];
                    newRestaurant.longitude = [locations valueForKey:@"long"];
                    
                    
                    @try {
                        [realm addObject:newRestaurant];
                    } @catch (NSException *exception) {
                        NSLog(@"(Customer Role) Fetch Restaurant List Exception:%@",exception.description);
                    }
                    
                    
                    [realm commitWriteTransaction];
                    
                    
                }
                
                
                //                NSLog(@"%ld Reconrds Saved",count);
                
                NSArray *arrServers = [data valueForKey:@"servers"];
                for (int j = 0; j < [arrServers count]; j++) {
                    
                    NSDictionary *serverData = [arrServers objectAtIndex:j];
                    if (serverData.count >0)
                    {
                        [realm beginWriteTransaction];
                        Servers *newServer = [[Servers alloc] init];
                        newServer.serverID = [[serverData valueForKey:@"_idUserProfile"] integerValue];
                        newServer.workingRestroID = [[serverData valueForKey:@"_idWorksInRestaurant"] integerValue];
                        newServer.avgRating = [[serverData valueForKey:@"avgRating"] floatValue];
                        newServer.imageURL = [serverData valueForKey:@"image"];
                        newServer.serverName = [serverData valueForKey:@"name"];
                        newServer.email = [serverData valueForKey:@"email"];
                        newServer.toalRatings = [[serverData valueForKey:@"totalRatings"] integerValue];
                        newServer.totalComments = [[serverData valueForKey:@"totalComments"] integerValue];
                        newServer.oneRatings = [[serverData valueForKey:@"oneRatings"] floatValue];
                        newServer.twoRatings = [[serverData valueForKey:@"twoRatings"] floatValue];
                        newServer.threeRatings = [[serverData valueForKey:@"threeRatings"] floatValue];
                        newServer.fourRatings = [[serverData valueForKey:@"fourRatings"] floatValue];
                        newServer.fiveRatings = [[serverData valueForKey:@"fiveRatings"] floatValue];
                        
                        if (!([serverData valueForKey:@"experience"] == (id)[NSNull null]))
                        {
                            NSMutableArray *expArray = [[NSMutableArray alloc] init];
                            
                            [expArray addObjectsFromArray:[serverData valueForKey:@"experience"]];
                        for (int i = 0; i < [expArray count]; i++)
                        {
                            if ([[[expArray objectAtIndex:i] valueForKey:@"isCurrentJob"] isEqualToString:@"YES"])
                            {
                                newServer.restaurantName = [[expArray objectAtIndex:i] valueForKey:@"restaurantName"];
                                newServer.city = [[expArray objectAtIndex:i]  valueForKey:@"city"];
                            }
                            
                        }
                        }
                        
                        [realm addObject:newServer];
                        [realm commitWriteTransaction];
                    }
                }

                dispatch_async(dispatch_get_main_queue(), ^{
                    [APP.hud setHidden:YES];
                
                });
                
                
                aResult(YES);
                
                
                
                
                
                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [APP.hud setHidden:YES];
                
            });
        }
        
        
    }];
    [dataTask resume];
    
}

#pragma -
#pragma mark Server Role Web Service Calls

- (void)FetchServerProfile:(NSString *)aToken completion:(void (^)(BOOL success))completionBlock {
    
    //    [APP huddie];
    
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,GET_SERVER_PROFILE_INFO]];
    //    NSLog(@"Get Profile Info URL:%@",URL);
    
    //    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"bearer %@",aToken] forHTTPHeaderField:@"Authorization"];
    
    
    [manager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        if ([responseObject isKindOfClass:[NSArray class]])
        {
            NSArray *jsonArray = (NSArray *)responseObject  ;
            NSLog(@"Json:%@",jsonArray);
        }
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            
            //NSInteger count = 0;
            RLMRealm *realm = [RLMRealm defaultRealm];
            Servers *server = [[Servers allObjects] firstObject];
            ServerWorkExperience *Experiance = [[ServerWorkExperience allObjects] firstObject];
                        @try {
                if (server.invalidated == false)
                {
                    [realm beginWriteTransaction];

                    [realm deleteObjects:[Servers allObjectsInRealm:realm]];
                    
                    //[realm deleteObjects:[ServerRating allObjectsInRealm:realm]];
                    
                    ServerJobApplied *jobs = [[ServerJobApplied allObjects] firstObject];
                    if (jobs.invalidated == false)
                    {
                        [realm deleteObjects:[ServerJobApplied allObjectsInRealm:realm]];
                    }
                    
                    if (Experiance.invalidated == false)
                    {
                        [realm deleteObjects:[ServerWorkExperience allObjectsInRealm:realm]];
                    }
                    
                    [realm commitWriteTransaction];
                }
                
                
            } @catch (NSException *exception) {
                NSLog(@"%@ LocalData",exception);
            }
            
            RLMResults<Servers *> *aServer;
            
            NSLog(@"Servers:%@",aServer);
            
            
            NSDictionary *dataDic = (NSDictionary *)responseObject;
            
            
            Servers *newServer = [[Servers allObjects] firstObject];
            
            if (newServer == nil) {
                newServer = [[Servers alloc] init];
            }
            
            
            [realm beginWriteTransaction];
            
            NSDictionary *serverProfileInfo = [dataDic valueForKey:@"data"];
            
            NSLog(@"Server Profile Info:%@",serverProfileInfo);
            
            [[NSUserDefaults standardUserDefaults] setValue:[serverProfileInfo valueForKey:@"_id"] forKey:@"ServerID"];
            
            newServer.serverID = [[serverProfileInfo valueForKey:@"_id"] integerValue];
            newServer.reward = [[serverProfileInfo valueForKey:@"rewardsInDoller"] integerValue];
            newServer.email = [serverProfileInfo valueForKey:@"email"];
            newServer.role = [serverProfileInfo valueForKey:@"role"];
            newServer.city = [serverProfileInfo valueForKey:@"city"];
            newServer.zipCode = [serverProfileInfo valueForKey:@"zip"];
            newServer.imageURL = [serverProfileInfo valueForKey:@"image"];
            newServer.serverName = [serverProfileInfo valueForKey:@"username"];
            newServer.firstName = [serverProfileInfo valueForKey:@"fName"];
            newServer.middleName = [serverProfileInfo valueForKey:@"mName"];
            newServer.lastName = [serverProfileInfo valueForKey:@"lName"];
            newServer.qualification = [serverProfileInfo valueForKey:@"qualification"];
            newServer.ssNumber = [serverProfileInfo valueForKey:@"securityNumber"];
            newServer.nickName = [serverProfileInfo valueForKey:@"nickName"];
            newServer.avgRating = [[serverProfileInfo valueForKey:@"avgRating"] floatValue];
            newServer.toalRatings = [[serverProfileInfo valueForKey:@"totalRatings"] integerValue];
            newServer.totalComments = [[serverProfileInfo valueForKey:@"totalComments"] integerValue];
            newServer.totalJobsApplied = [[serverProfileInfo valueForKey:@"totalJobsApplied"] integerValue];
            newServer.oneRatings = [[serverProfileInfo valueForKey:@"oneRatings"] floatValue];
            newServer.twoRatings = [[serverProfileInfo valueForKey:@"twoRatings"] floatValue];
            newServer.threeRatings = [[serverProfileInfo valueForKey:@"threeRatings"] floatValue];
            newServer.fourRatings = [[serverProfileInfo valueForKey:@"fourRatings"] floatValue];
            newServer.fiveRatings = [[serverProfileInfo valueForKey:@"fiveRatings"] floatValue];
            newServer.qualification = [serverProfileInfo valueForKey:@"qualification"];
            newServer.jobsInPastThirtyDays = [[serverProfileInfo valueForKey:@"jobsInPastThirtyDays"] integerValue];
            newServer.lastRating = [serverProfileInfo valueForKey:@"lastRating"] ;
            NSLog(@"%@",[serverProfileInfo valueForKey:@"lastRating"]);
            newServer.lastComment = [serverProfileInfo valueForKey:@"lastComment"] ;
            NSLog(@"%@",[serverProfileInfo valueForKey:@"lastComment"]);
            newServer.ssNumber = [serverProfileInfo valueForKey:@"securityNumber"];
            newServer.mobileNumber = [serverProfileInfo valueForKey:@"mobileNo"];
            newServer.referralCode = [serverProfileInfo valueForKey:@"referalCode"];
            
            
            //            NSLog(@"%@",[serverProfileInfo valueForKey:@"experience"]);
            
            
            
            if (!([serverProfileInfo valueForKey:@"experience"] == (id)[NSNull null]))
            {
                NSMutableArray *expArray = [[NSMutableArray alloc] init];
                
                [expArray addObjectsFromArray:[serverProfileInfo valueForKey:@"experience"]];
                
                
                for (int i = 0; i < [expArray count]; i++) {
                    ServerWorkExperience *serverExp = [[ServerWorkExperience alloc] init];
                    NSDictionary *dict = [expArray objectAtIndex:i];
                    serverExp.workingRestroID = [[dict valueForKey:@"_idWorksInRestaurant"] integerValue];
                    serverExp.city = [dict valueForKey:@"city"];
                    serverExp.fromDate = [dict valueForKey:@"fromDate"];
                    serverExp.toDate = [dict valueForKey:@"toDate"];
                    serverExp.isCurrentJob = [dict valueForKey:@"isCurrentJob"];
                    serverExp.restoName = [dict valueForKey:@"restaurantName"];
                    serverExp.restoType = [dict valueForKey:@"restaurantType"];
                    serverExp.workProfile = [dict valueForKey:@"workProfile"];
                    serverExp.zipCode = [dict valueForKey:@"zip"];
                    [newServer.workExperience addObject:serverExp];
                }
            }
            
            if (!([serverProfileInfo valueForKey:@"jobsApplied"] == (id)[NSNull null]))
            {
                NSMutableArray *jobsAppliedArray = [[NSMutableArray alloc]init] ;
                [jobsAppliedArray addObjectsFromArray:[serverProfileInfo valueForKey:@"jobsApplied"]];
                
                
                for (int i = 0; i < [jobsAppliedArray count]; i++) {
                    
                    ServerJobApplied *aJobApplied = [[ServerJobApplied  alloc] init];
                    NSDictionary *dict = [jobsAppliedArray  objectAtIndex:i];
                    
                    aJobApplied.restoName = [dict valueForKey:@"restaurantName"];
                    aJobApplied.jobID = [[dict valueForKey:@"jobOpeningId"] integerValue];
                    aJobApplied.workProfile = [dict valueForKey:@"profileType"];
                    aJobApplied.city = [dict valueForKey:@"restaurantCity"];
                    aJobApplied.jobStatus = [dict valueForKey:@"status"];
                    @try {
                        [realm addObject:aJobApplied];
                    }
                    @catch (NSException *exception){
                        NSLog(@"Server Job Applied Exception:%@",exception.description);
                    }
                    
                }
                
            }
            @try {
                [realm addObject:newServer];
                [realm commitWriteTransaction];
            } @catch (NSException *exception) {
                NSLog(@"Fetch Server Profile Exception:%@",exception.description);
            }
            dispatch_after(2, dispatch_get_main_queue(), ^(void){
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"profileFetched" object:self];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"profileFetched1" object:self];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"serverJobFetched" object:self];
            });
        }
        completionBlock(YES);
        
        
        
        
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        
        NSLog(@"Error: %@", error);
        completionBlock(NO);
        
        
        
        
        
        
        
    }];
    
}

-(void)GetAllAvailableJobs:(NSString * )aToken {
    //    [APP huddie];
    
//    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
//    [manager.requestSerializer setValue:[NSString stringWithFormat:@"bearer %@",aToken] forHTTPHeaderField:@"Authorization"];
//    
//    
//    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:[NSString stringWithFormat:@"%@%@",SITE_URL,GET_ALL_JOB_OPENING_URL] parameters:nil error:nil];
//    
//    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,GET_ALL_JOB_OPENING_URL]];
//    //NSURLRequest *request = [NSURLRequest requestWithURL:URL];
//    [request setValue:[NSString stringWithFormat:@"bearer %@",aToken] forHTTPHeaderField:@"authorization"];
//    
//    
//    [manager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
//        
//        if ([responseObject isKindOfClass:[NSArray class]])
//        {
//            NSArray *jsonArray = (NSArray *)responseObject  ;
//            NSLog(@"Json:%@",jsonArray);
//        }
//        
//        if ([responseObject isKindOfClass:[NSDictionary class]]) {
//            
//            NSDictionary *dataDic = (NSDictionary *)responseObject;
//            //             NSLog(@"All Available Jobs:%@",dataDic);
//            
//            RLMRealm *realm = [RLMRealm defaultRealm];
//            [realm beginWriteTransaction];
//            [realm deleteObjects:[AllAvailableJobs allObjectsInRealm:realm]];
//            [realm commitWriteTransaction];
//            
//            
//            NSInteger count = 0;
//            
//            NSMutableArray *allJobsArray = [[NSMutableArray alloc] init];
//            
//            [allJobsArray addObjectsFromArray:[dataDic valueForKey:@"data"]];
//            
//            for (int i = 0; i < [allJobsArray count]; i++)
//            {
//                
//                AllAvailableJobs *AvailableJobs = [[AllAvailableJobs  alloc] init];
//                
//                NSDictionary *JobsDict = [allJobsArray objectAtIndex:i];
//                
//                if ([JobsDict valueForKey:@"name"]) {
//                    
//                    count ++;
//                    [realm beginWriteTransaction];
//                    
//                    AvailableJobs.ID = [[JobsDict valueForKey:@"_id"] integerValue];
//                    AvailableJobs.city = [JobsDict valueForKey:@"city"];
//                    AvailableJobs.name = [JobsDict valueForKey:@"name"];
//                    AvailableJobs.zipcode = [JobsDict valueForKey:@"zipcode"];
//                    AvailableJobs.updatedAt = [JobsDict valueForKey:@"updatedAt"];
//                    AvailableJobs.createdAt = [JobsDict valueForKey:@"createdAt"];
//                    AvailableJobs.jobDescription = [JobsDict valueForKey:@"description"];
//                    AvailableJobs.status = [JobsDict valueForKey:@"status"];
//                    AvailableJobs.profileType = [JobsDict valueForKey:@"profileType"];
//                    AvailableJobs.RestaurantID = [[JobsDict valueForKey:@"_idRestaurant"] integerValue] ;
//                    //AvailableJobs._v = [JobsDict valueForKey:@"_v"] ;
//                    
//                    NSDictionary *locations = [JobsDict valueForKey:@"location"];
//                    
//                    AvailableJobs.latitude = [locations valueForKey:@"lat"] ;
//                    AvailableJobs.longitude = [locations valueForKey:@"long"] ;
//                    
//                    @try {
//                        [realm addObject:AvailableJobs];
//                        [realm commitWriteTransaction];
//                    } @catch (NSException *exception) {
//                        NSLog(@"(Server Role) Fetch All Avaliable Jobs Exception:%@",exception.description);
//                    }
//                    
//                    
//                }
//                
//            }
//            
//            //            NSLog(@"%ld Reconrds Saved",(long)count);
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"allJobsAvailable" object:self];
//            
//            
//            
//        }
//        
//        [APP.hud setHidden:YES];
//        
//        
//        
//    } failure:^(NSURLSessionTask *operation, NSError *error) {
//        
//        
//        
//        [APP.hud setHidden:YES];
//        
//        
//        NSLog(@"Error: %@", error);
//    }];
    
}


- (void)FetchRatingsAndComments:(NSString *)serverID token:(NSString * )aToken
                     completion:(void (^)(BOOL success))completionBlock {
    
    
    dispatch_async(dispatch_get_main_queue(), ^{[APP huddie];});
    
    
//    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",SITE_URL,RATAINGS_AND_COMMENTS,serverID]];
//    //    NSLog(@"Rating URL:%@",URL);
//    
//    
//    
//    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
//    [manager.requestSerializer setValue:[NSString stringWithFormat:@"bearer %@",aToken] forHTTPHeaderField:@"Authorization"];
//    
//    
//    [manager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
//        
//        if ([responseObject isKindOfClass:[NSArray class]])
//        {
//            NSArray *jsonArray = (NSArray *)responseObject  ;
//            NSLog(@"Json:%@",jsonArray);
//        }
//        
//        if ([responseObject isKindOfClass:[NSDictionary class]]) {
//            
//            NSDictionary *dataDic = (NSDictionary *)responseObject;
//            
//            
//            NSInteger count = 0;
//            RLMRealm *realm = [RLMRealm defaultRealm];
//            [realm beginWriteTransaction];
//            [realm deleteObjects:[ServerRating allObjectsInRealm:realm]];
//            [realm commitWriteTransaction];
//            
//            
//            
//            
//            
//            for (NSDictionary *serverExperienceDict in [dataDic valueForKey:@"data"]) {
//                ServerRating *newRating = [[ServerRating alloc] init];
//                [realm beginWriteTransaction];
//                newRating.date = [serverExperienceDict valueForKey:@"updatedAt"];
//                NSLog(@"%@",newRating.date);
//                newRating.rating = [[serverExperienceDict valueForKey:@"rating"] integerValue];
//                NSLog(@"%ld",(long)newRating.rating);
//                newRating.comment = [serverExperienceDict valueForKey:@"comment"];
//                NSLog(@"%@",newRating.comment);
//                newRating.customerName = [serverExperienceDict valueForKey:@"name"];
//                NSLog(@"%@",newRating.customerName);
////                @try {
//                    [realm addObject:newRating];
//                    [realm commitWriteTransaction];
////                } @catch (NSException *exception) {
////                    NSLog(@"(Server Role) Fetch Ratings And Comments Exception:%@",exception.description);
////                }
//                
//                //count ++;
//            }
//            
//            
//            //@try {
//                
//            
//                
//                RLMResults<ServerRating  *> *ratingData;
//                ratingData = [ServerRating allObjects];
//                dispatch_after(2, dispatch_get_main_queue(), ^(void){
//                    
//                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ratingFetched" object:self];
//                    
//                    
//                });
////            } @catch (NSException *exception) {
////                NSLog(@"(Server Role) Fetch Ratings And Comments Exception:%@",exception.description);
////            }
//            
//            
//            //            NSLog(@"%ld Rating Records Saved",(long)count);
//        }
//        
//        
//        
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//        [APP.hud setHidden:YES];
//        });
//        
//        completionBlock(YES);
//        
//        
//    } failure:^(NSURLSessionTask *operation, NSError *error) {
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [APP.hud setHidden:YES];
//        });
//        
//        NSLog(@"Error: %@", error);
//        completionBlock(NO);
//        
//    }];
}

- (void)FetchRestaurantsList:(void (^)(BOOL result))aResult {
    
    //    [APP huddie];
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,GET_ALL_RESTAURANTS_URL]];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        
        if (error) {
            
            NSLog(@"Error: %@", error.description);
            
            
            [APP.hud setHidden:YES];
            
            aResult(NO);
        }
        else {
            //            NSLog(@"%@ %@", response, responseObject);
            
            if ([responseObject isKindOfClass:[NSArray class]])
            {
//                NSArray *jsonArray = (NSArray *)responseObject  ;
                //                NSLog(@"Json:%@",jsonArray);
            }
            
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *dataDic = (NSDictionary *)responseObject;
                //                NSLog(@"JsonDict:%@",dataDic);
                
                RLMRealm *realm = [RLMRealm defaultRealm];
                [realm beginWriteTransaction];
                [realm deleteObjects:[Restaurants allObjectsInRealm:realm]];
                [realm commitWriteTransaction];
                
                NSInteger count = 0;
                
                for (NSDictionary *restaurantsDict in [dataDic valueForKey:@"data"]) {
                    Restaurants *newRestaurant = [[Restaurants  alloc] init];
                    
                    count ++;
                    
                    [realm beginWriteTransaction];
                    
                    newRestaurant.restroID = [[restaurantsDict valueForKey:@"_id"] integerValue];
                    newRestaurant.city = [restaurantsDict valueForKey:@"city"];
                    newRestaurant.name = [restaurantsDict valueForKey:@"name"];
                    newRestaurant.ownerName = [restaurantsDict valueForKey:@"ownerName"];
                    newRestaurant.ownerEmail = [restaurantsDict valueForKey:@"ownerEmail"];
                    //newRestaurant.imageURL = [restaurantsDict valueForKey:@"image"];
                    
                    NSDictionary *locations = [restaurantsDict valueForKey:@"location"];
                    
                    //                    NSLog(@"Locations:%@",locations);
                    
                    newRestaurant.latitude = [locations valueForKey:@"lat"];
                    newRestaurant.longitude = [locations valueForKey:@"long"];
                    
                    
                    @try {
                        [realm addObject:newRestaurant];
                        [realm commitWriteTransaction];
                    } @catch (NSException *exception) {
                        NSLog(@"(Server Flow)Fetch Restaurant List Exception:%@",exception.description);
                        
                    }
                    
                    
                    
                }
                [APP.hud setHidden:YES];
                //                NSLog(@"%ld Reconrds Saved",count);
                aResult(YES);
                
                
                
                
                
                
            }
            
        }
    }];
    [dataTask resume];
    
    
}


- (void)clearPersistedData {
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    //    [realm deleteObjects:[ServerRating allObjectsInRealm:realm]];
    [realm deleteObjects:[Restaurants allObjectsInRealm:realm]];
    [realm deleteObjects:[RestaurantImages allObjectsInRealm:realm]];
    
    [realm deleteObjects:[ServerWorkExperience allObjectsInRealm:realm]];
    [realm commitWriteTransaction];
    
    
}


#pragma mark - Owner Role Web service calls

- (void)FetchOwnerProfile:(NSString *)aToken completion:(void (^)(BOOL success))completionBlock {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,GET_OWNER_PROFILE_INFO]];
    //    NSLog(@"Get Profile Info URL:%@",URL);
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"bearer %@",aToken] forHTTPHeaderField:@"Authorization"];
    
    
    [manager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        if ([responseObject isKindOfClass:[NSArray class]])
        {
            NSArray *jsonArray = (NSArray *)responseObject  ;
            NSLog(@"Json:%@",jsonArray);
          
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
           
        }
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            
            //NSInteger count = 0;
            RLMRealm *realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction];
            @try {
                [realm deleteObjects:[OwnerProfile allObjectsInRealm:realm]];
                
                [realm deleteObjects:[Restaurants allObjectsInRealm:realm]];
                [realm deleteObjects:[RestaurantImages allObjectsInRealm:realm]];
                
                [realm commitWriteTransaction];
                
            } @catch (NSException *exception) {
                NSLog(@"%@ LocalData",exception);
            }
            
            NSDictionary *dataDic = (NSDictionary *)responseObject;
            
            OwnerProfile *newProfile = [[OwnerProfile allObjects] firstObject];
            
            if (newProfile == nil) {
                newProfile = [[OwnerProfile alloc] init];
            }
            
            
            [realm beginWriteTransaction];
            
            NSDictionary *ownerProfileInfo = [dataDic valueForKey:@"data"];
            
            NSLog(@"Owner Profile Info From Web Server:%@",ownerProfileInfo);
            
            newProfile.profileID = [[ownerProfileInfo valueForKey:@"_id"] integerValue];
            newProfile.city = [ownerProfileInfo valueForKey:@"city"];
            newProfile.email = [ownerProfileInfo valueForKey:@"email"];
            newProfile.profilePic = [ownerProfileInfo valueForKey:@"image"];
            newProfile.mobileNumber = [ownerProfileInfo valueForKey:@"mobileNo"];
            newProfile.fName = [ownerProfileInfo valueForKey:@"name"];
            newProfile.zip = [ownerProfileInfo valueForKey:@"zip"];
            
            
            if (!([ownerProfileInfo valueForKey:@"restaurantInfo"] == (id)[NSNull null]))
            {
                
                NSDictionary *restoInfoData = [ownerProfileInfo valueForKey:@"restaurantInfo"];
                
                    Restaurants *ownerResto = [[Restaurants allObjects] firstObject];
                
                if (ownerResto == nil) {
                    ownerResto = [[Restaurants alloc] init];
                }
               
                    ownerResto.restroID = [[restoInfoData valueForKey:@"_id"] integerValue];
                    ownerResto.zipCode = [restoInfoData valueForKey:@"zipCode"];
                    ownerResto.city = [restoInfoData valueForKey:@"city"];
                ownerResto.landmark = [restoInfoData valueForKey:@"landmark"];
                    ownerResto.restoType = [restoInfoData valueForKey:@"restaurantType"];
                    ownerResto.name = [restoInfoData valueForKey:@"name"];
                    
                    if (!([restoInfoData valueForKey:@"image"] == (id)[NSNull null]))
                    {
                        NSMutableArray *restoImageArray = [[NSMutableArray alloc]init] ;
                        [restoImageArray addObjectsFromArray:[restoInfoData valueForKey:@"image"]];
                        
                        
                        for (int i = 0; i < [restoImageArray count]; i++) {
                            
                            RestaurantImages *restoImage = [[RestaurantImages  alloc] init];
                            
                            restoImage.imageURL = [restoImageArray  objectAtIndex:i];
                            
                            
                            @try {
                                [newProfile.restoImageArray addObject:restoImage];
                            }
                            @catch (NSException *exception){
                                NSLog(@"Server Job Applied Exception:%@",exception.description);
                            }
                            
                        }
                        
                    }
                    
                    [newProfile.ownerRestaurants addObject:ownerResto];
                
            }
            
            
            @try {
                [realm addObject:newProfile];
                [realm commitWriteTransaction];
                
               OwnerProfile *newProfile = [[OwnerProfile allObjects] firstObject];
                
                NSLog(@"Fetched owner Data:%@",newProfile);
                
                
            } @catch (NSException *exception) {
                NSLog(@"Fetch Server Profile Exception:%@",exception.description);
            }
           
        }
        completionBlock(YES);
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"profileFetchedForStep1VC" object:self];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [APP.hud setHidden:YES];
                     [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                });
        
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        
        NSLog(@"Error: %@", error);
        dispatch_async(dispatch_get_main_queue(), ^{
            [APP.hud setHidden:YES];
             [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        });

        completionBlock(NO);
        
        
        
        
    }];
    
}


@end
