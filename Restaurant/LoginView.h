//
//  LoginView.h
//  Restaurant
//
//  Created by HN on 13/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <TwitterKit/TwitterKit.h>
#import "AppUser.h"
#import "ForgotPasswordViewController.h"
#import "OwnerDashboard.h"
#import "OwnerVerifyOTPViewController.h"


@interface LoginView : UIViewController <UITextFieldDelegate,NSURLSessionDelegate,GIDSignInUIDelegate,GIDSignInDelegate>
{
    NSString *refreshToken;
    NSString *accessToken;
    NSString *FBAccessToken;
}
- (IBAction)forgotPasswordButton:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;
@property (weak, nonatomic) IBOutlet UITextField *usernameText;
@property (weak, nonatomic) IBOutlet UITextField *registerNowButton;
- (IBAction)registerButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnRemeberMe;

@property (weak, nonatomic) IBOutlet GIDSignInButton *btnGooglePlus;
@property (weak, nonatomic) IBOutlet FBSDKLoginButton *btnFaceBook;
@property (weak, nonatomic) IBOutlet TWTRLogInButton *btnTwiter;

@property (weak, nonatomic) IBOutlet UIButton *btnTwitter;
@property (weak, nonatomic) IBOutlet UIButton *btnFacebook;
@property (strong,nonatomic) UIBarButtonItem *btnBack;

- (IBAction)googleSignIn:(id)sender;
- (IBAction)facebookSignIn:(id)sender;
- (IBAction)twitterSignIn:(id)sender;


@end

