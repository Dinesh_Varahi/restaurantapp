//
//  LoginView.m
//  Restaurant
//
//  Created by HN on 13/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "LoginView.h"
#import "SignUpViewController.h"
#import "ServerLandingViewController.h"
#import "HomeViewController.h"
#import "ServerDetailViewController.h"

@interface LoginView ()
{
    BOOL toggle;
    
}

@end

@implementation LoginView



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault]; //UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.shadowImage = [UIImage new];////UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    [_usernameText setDelegate:self];
    [_passwordText setDelegate:self];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(popView)
                                                 name:@"dismissToComment"
                                               object:nil];
    
    
    
    //    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"isLoggedIn"] isEqualToString:@"Yes"])
    //    {
    //        toggle = YES;
    //        NSLog(@"%d",toggle);
    //    }
    //    else{
    //        toggle = NO;
    //        NSLog(@"%d",toggle);
    //    }
    
    toggle= YES;
    
    _btnGooglePlus.layer.cornerRadius = _btnGooglePlus.frame.size.width/2;
    _btnTwitter.layer.cornerRadius = _btnGooglePlus.frame.size.width/2;
    _btnFacebook.layer.cornerRadius = _btnGooglePlus.frame.size.width/2;
    
    
    [GIDSignIn sharedInstance].delegate = self;
    
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
    
    _btnFaceBook.readPermissions =
    @[@"public_profile", @"email", @"user_friends"];
    
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Sender"] isEqualToString:@"Comment"])
    {
        UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *backBtnImage = [UIImage imageNamed:@"left.png"]  ;
        [backBtn setBackgroundImage:backBtnImage forState:UIControlStateNormal];
        [backBtn addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        backBtn.frame = CGRectMake(0, 0, 24, 24);
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn] ;
        self.navigationItem.leftBarButtonItem = backButton;
        
        
        //        self.btnBack = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        //        self.btnBack = [[UIBarButtonItem alloc]
        //                        initWithTitle:@"LOGOUT"
        //                        style:UIBarButtonItemStylePlain
        //                        target:self
        //                        action:@selector(backButtonPressed:)];
        //        self.navigationItem.leftBarButtonItem = self.btnBack;
        //
        //        [self.btnBack setTitleTextAttributes:@{
        //                                               NSFontAttributeName: [UIFont systemFontOfSize:13.0 weight:UIFontWeightMedium],
        //                                               NSForegroundColorAttributeName: [UIColor whiteColor]
        //                                               } forState:UIControlStateNormal];
        //
    }
    
    
}
-(void)popView {
    //    [self.navigationController popViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)backButtonPressed:(id)sender{
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    textField.textColor = [UIColor whiteColor];
    return YES;
}


- (IBAction)rememberMePressed:(id)sender {
    
    if (toggle) {
        [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"rememberMe"];
        NSLog(@"%d",toggle);
    }
    else{
        [[NSUserDefaults standardUserDefaults] setValue:@"no" forKey:@"rememberMe"];
        NSLog(@"%d",toggle);
    }
    toggle = !toggle;
    
    [_btnRemeberMe setImage:[UIImage imageNamed:toggle ? @"un-selected.png" :@"selected.png"] forState:UIControlStateNormal];
    
}

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    
    if(user == nil)
    {
        // Perform any operations on signed in user here.
        NSString *userId = user.userID;                  // For client-side use only!
        NSString *idToken = user.authentication.idToken;// Safe to send to the server
        refreshToken = user.authentication.refreshToken;
        accessToken = user.authentication.accessToken;
        NSString *fullName = user.profile.name;
        NSString *givenName = user.profile.givenName;
        NSString *familyName = user.profile.familyName;
        NSString *email = user.profile.email;
        
        NSLog(@"ID:%@",userId);
        NSLog(@"RefreshToken:%@",refreshToken);
        NSLog(@"accessToken:%@",accessToken);
        NSLog(@"Token:%@",idToken);
        NSLog(@"fullName:%@",fullName);
        NSLog(@"givenName:%@",givenName);
        NSLog(@"familyName:%@",familyName);
        NSLog(@"email:%@",email);
        NSLog(@"ID:%@",userId);
        
        
        [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"isuserloggedin"];
        
        
        
        
        // ...
    }
    else{
        NSLog(@"User Already logged in..");
        
        NSString *userId = user.userID;                  // For client-side use only!
        NSString *idToken = user.authentication.idToken; // Safe to send to the server
        refreshToken = user.authentication.refreshToken;
        accessToken = user.authentication.accessToken;
        NSString *fullName = user.profile.name;
        NSString *givenName = user.profile.givenName;
        NSString *familyName = user.profile.familyName;
        NSString *email = user.profile.email;
        
        NSLog(@"ID:%@",userId);
        NSLog(@"Token:%@",idToken);
        NSLog(@"RefreshToken:%@",refreshToken);
        NSLog(@"accessToken:%@",accessToken);
        NSLog(@"fullName:%@",fullName);
        NSLog(@"givenName:%@",givenName);
        NSLog(@"familyName:%@",familyName);
        NSLog(@"email:%@",email);
        NSLog(@"ID:%@",userId);
        
        [[NSUserDefaults standardUserDefaults] setValue:email forKey:@"userEmailFromSocialMedia"];
        [self GoogleRequestToServer];
        
        //        if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Sender"] isEqualToString:@"Home"]){
        
        //
        
        //            dispatch_async(dispatch_get_main_queue(), ^{
        
        //
        
        //                UIAlertController * alert2=   [UIAlertController
        
        //                                               alertControllerWithTitle:@"User Already Logged In"
        
        //                                               message:@""
        
        //                                               preferredStyle:UIAlertControllerStyleAlert];
        
        //
        
        //                [self presentViewController:alert2 animated:YES completion:nil];
        
        //
        
        //
        
        //
        
        //                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        //                    [alert2 dismissViewControllerAnimated:YES completion:nil];
        
        //                    [self.navigationController popViewControllerAnimated:YES];
        
        //                    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
        
        //                });
        
        //
        
        //            });
        
        //
        
        //
        
        //        }
        
        //        [[NSUserDefaults standardUserDefaults] setValue:@"no" forKey:@"isuserloggedin"];
        
        
        
    }
    
}

-(void)GoogleRequestToServer
{
    
    NSError *error;
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,GOOGLE_LOGIN]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                    
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                    
                                                       timeoutInterval:60.0];
    
    
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    [request setHTTPMethod:@"POST"];
    
    
    
    NSMutableDictionary *dataDict = [NSMutableDictionary new];
    
    
    
    [dataDict setValue:accessToken forKey:@"access_token"];
    
    [dataDict setValue:refreshToken forKey:@"refresh_token"];
    
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dataDict
                        
                                                       options:NSJSONWritingPrettyPrinted
                        
                                                         error:&error];
    
    
    
    NSString *postStr;
    
    if (! postData) {
        
        NSLog(@"Got an error: %@", error);
        
        
        
    } else {
        
        NSString *jsonString = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
        
        
        
        postStr = [NSString stringWithFormat:@"[%@]",jsonString];
        
        
        
    }
    
    
    
    [request setHTTPBody:postData];
    
    
    
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"Response: %@",dataString);
        
        double status = (long)[httpResponse statusCode];
        
        NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if (status == 202){
            
            
            RLMRealm *realm = [RLMRealm defaultRealm];
            
            
            
            [realm beginWriteTransaction];
            
            //                        [realm deleteObjects:[AppUser allObjectsInRealm:realm]];
            
            
            
            AppUser *newUser = [[AppUser allObjects] firstObject];
            
            
            
            if (newUser == nil) {
                
                newUser = [[AppUser alloc] init];
                
            }
            
            
            
            
            
            NSDictionary *data = [jsonDict valueForKey:@"data"];
            
            
            
            NSString *userName = [data valueForKey:@"username"];
            
            newUser.name = [data valueForKey:@"username"];
            
            
            
            [[NSUserDefaults standardUserDefaults] setValue:userName forKey:@"username"];
            
            
            
            NSString *token = [data valueForKey:@"token"];
            
            newUser.token = [data valueForKey:@"token"];
            
            
            
            [[NSUserDefaults standardUserDefaults] setValue:token forKey:@"token"];
            
            
            
            NSString *role = [data valueForKey:@"role"];
            
            newUser.role = [data valueForKey:@"role"];
            
            newUser.referralCode = [data valueForKey:@"referralCode"];
            
            
            
            newUser.profileID = [[data valueForKey:@"profileID"] integerValue];
            
            
            
            [realm addObject:newUser];
            
            
            
            [realm commitWriteTransaction];
            
            
            
            AppUser *addedUser = [[AppUser allObjects] firstObject];
            
            NSLog(@"User:%@",addedUser);
            
            
            [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"isuserloggedin"];
            
            
            
            if ([role isEqualToString:@"customer"])
                
            {
                
                //                            [[NSUserDefaults standardUserDefaults]setObject:@"customer" forKey:@"role"];
                
                [[NSUserDefaults standardUserDefaults]setValue:@"customer" forKey:@"role"];
                
                
                
                if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Sender"] isEqualToString:@"Home"]){
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [APP.hud setHidden:YES];
                        UIAlertController * alert2=   [UIAlertController
                                                       
                                                       alertControllerWithTitle:@"Login Successful"
                                                       
                                                       message:@""
                                                       
                                                       preferredStyle:UIAlertControllerStyleAlert];
                        
                        
                        
                        [self presentViewController:alert2 animated:NO completion:nil];
                      
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                            
                            
                            
                            _passwordText.text=@"";
                            
                            _usernameText.text=@"";
                            
                            [self.view endEditing:YES];
                            
                            
                            
                            [alert2 dismissViewControllerAnimated:NO completion:nil];
                            
                            
                            
                            [[LocalData sharedInstance] FetchAllRestaurants:^(BOOL result) {
                                
                                if (result)
                                    
                                {
                                    
                                    HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                                    
                                    [self.navigationController pushViewController:home animated:NO];
                                    
                                }
                                
                                else{
                                    
                                    HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                                    
                                    [self.navigationController pushViewController:home animated:NO];
                                    
                                }
                                
                                
                                
                            }];
                            
                            
                            
                        });
                        
                        
                        
                    });
                    
                    
                    
                    
                    
                }
                
                
                
                else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Sender"] isEqualToString:@"Comment"]) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [APP.hud setHidden:YES];
                        
                        UIAlertController * alert2=   [UIAlertController
                                                       
                                                       alertControllerWithTitle:@"Login Sucessful"
                                                       
                                                       message:@""
                                                       
                                                       preferredStyle:UIAlertControllerStyleAlert];
                        
                        
                        
                        [self presentViewController:alert2 animated:NO completion:nil];
                        
                        
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                            
                            
                            
                            _passwordText.text=@"";
                            
                            _usernameText.text=@"";
                            
                            [self.view endEditing:YES];
                            
                            
                            
                            
                            
                            [alert2 dismissViewControllerAnimated:NO completion:nil];
                            
                            [[NSUserDefaults standardUserDefaults] setValue:@"Login" forKey:@"Sender"];
                            
                            [self dismissViewControllerAnimated:NO completion:nil];
                            
                            
                            
                        });
                        
                        
                        
                    });
                    
                }
                
                else {
                    
                    
                    
                    [[LocalData sharedInstance] FetchAllRestaurants:^(BOOL result) {
                        
                        if (result)
                            
                        {
                            
                            HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                            
                            [self.navigationController pushViewController:home animated:NO];
                            
                        }
                        
                        else{
                            
                            HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                            
                            [self.navigationController pushViewController:home animated:NO];
                            
                        }
                        
                        
                        
                    }];
                    
                }
                
                
                
                
                
            }
            
            else if ([role isEqualToString:@"server"])
                
            {
                
                
                
                
                
                
                
                //                            [[NSUserDefaults standardUserDefaults]setObject:@"server" forKey:@"role"];
                
                [[NSUserDefaults standardUserDefaults]setValue:@"server" forKey:@"role"];
                
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    
                    
                    
                    
                    
                    UIAlertController * alert2=   [UIAlertController
                                                   
                                                   alertControllerWithTitle:@"Login Sucessful"
                                                   
                                                   message:@""
                                                   
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    
                    
                    
                    [self presentViewController:alert2 animated:NO completion:nil];
                    
                    
                    
                    
                    
                    
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        
                        
                        
                        _passwordText.text=@"";
                        
                        _usernameText.text=@"";
                        
                        [self.view endEditing:YES];
                        
                        
                        
                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                        
                        [APP.hud setHidden:YES];
                        
                        
                        
                        [self fetchDataFromServer];
                        
                    });
                    
                    
                    
                });
                
                
                
            }
            
            else {
                
                
                
                [[LocalData sharedInstance] FetchAllRestaurants:^(BOOL result) {
                    
                    if (result)
                        
                    {
                        
                        HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                        
                        [self.navigationController pushViewController:home animated:NO];
                        
                    }
                    
                    else{
                        
                        HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                        
                        [self.navigationController pushViewController:home animated:NO];
                        
                    }
                    
                    
                    
                    
                    
                    
                    
                }];
                
            }
            
        }
        
        else if (status == 400){
            
            
            
            NSLog(@"Login Failed");
            
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [APP.hud setHidden:YES];
                
                UIAlertController * alert2=   [UIAlertController
                                               
                                               alertControllerWithTitle:@"Error"
                                               
                                               message:[jsonDict valueForKey:@"result"]
                                               
                                               preferredStyle:UIAlertControllerStyleAlert];
                
                
                
                [self presentViewController:alert2 animated:NO completion:nil];
                
                
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    
                    [alert2 dismissViewControllerAnimated:NO completion:nil];
                    
                });
                
            });
            
            
            
        }
        
        else if (status == 401){
            
            
            
            NSLog(@"Login Failed");
            
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [APP.hud setHidden:YES];
                
                UIAlertController * alert2=   [UIAlertController
                                               
                                               alertControllerWithTitle:@"Info"
                                               
                                               message:@"Please Register on Tapped App"
                                               
                                               preferredStyle:UIAlertControllerStyleAlert];
                
                
                
                [self presentViewController:alert2 animated:NO completion:nil];
                
                
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    
                    [alert2 dismissViewControllerAnimated:NO completion:nil];
                    SignUpViewController *signUp = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
                    [self.navigationController pushViewController:signUp animated:NO];
                    
                });
                
            });
            
            
            
        }
        
        else {
            
            
            
            NSLog(@"Login Failed");
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [APP.hud setHidden:YES];
                
                UIAlertController * alert2=   [UIAlertController
                                               
                                               alertControllerWithTitle:@"Error"
                                               
                                               message:error.localizedDescription
                                               
                                               preferredStyle:UIAlertControllerStyleAlert];
                
                
                
                [self presentViewController:alert2 animated:NO completion:nil];
                
                
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    
                    [alert2 dismissViewControllerAnimated:NO completion:nil];
                    
                });
                
            });
            
        }
        
        
        
        
        
    }];
    
    
    
    [postDataTask resume];
    
}

- (void)postTokenToServer {
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,GOOGLE_LOGIN]];
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url
                                                cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                            timeoutInterval:30];
    // Fetch the JSON response
    NSData *urlData;
    NSURLResponse *response;
    NSError *error;
    
    urlData = [self sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
    if(urlData){
        NSLog(@"User Registered Successfully!!");
        // Construct a Dictionary around the Data from the response
        NSDictionary *aDict=[NSJSONSerialization JSONObjectWithData:urlData options:NSJSONReadingAllowFragments error:&error];
        NSLog(@"%@",aDict);
        
    }else{
        NSLog(@"User Registered unSuccessfully!!");
        
    }
    
}

- (NSData *)sendSynchronousRequest:(NSURLRequest *)request returningResponse:(NSURLResponse **)response error:(NSError **)error {
    
    NSError __block *err = NULL;
    NSData __block *data;
    BOOL __block reqProcessed = false;
    NSURLResponse __block *resp;
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable _data, NSURLResponse * _Nullable _response, NSError * _Nullable _error) {
        resp = _response;
        err = _error;
        data = _data;
        reqProcessed = true;
    }] resume];
    
    while (!reqProcessed) {
        [NSThread sleepForTimeInterval:0];
    }
    
    *response = resp;
    *error = err;
    return data;
}



- (IBAction)loginButtonPressed:(id)sender {
    
    [self.view endEditing:YES];
    
    if ([_usernameText.text isEqualToString:@""])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"User name required"
                                           message:@"Please enter user name"                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:NO completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:NO completion:nil];
            });
            
        });
    }
    
    else if (([_passwordText.text isEqualToString:@""]))
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Password required"
                                           message:@"Please enter password"                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:NO completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:NO completion:nil];
            });
            
        });
        
    }
    
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [APP huddie];
            
        });
   
        NSError *error;
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,SIGN_IN_URL]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setHTTPMethod:@"POST"];
        
        NSMutableDictionary *dataDict = [NSMutableDictionary new];
        
        NSString *strUserName = [_usernameText.text lowercaseString];
        [dataDict setValue:strUserName forKey:@"username"];
        [dataDict setValue:_passwordText.text forKey:@"password"];
        
        NSString *deviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"DeviceToken"];
        [dataDict setValue:deviceToken forKey:@"deviceTokenForNotification"];
        [dataDict setValue:@"iOS" forKey:@"deviceType"];
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:dataDict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        
        NSString *postStr;
        if (! postData) {
            NSLog(@"Got an error: %@", error);
            
        } else {
            NSString *jsonString = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
            
            postStr = [NSString stringWithFormat:@"[%@]",jsonString];
            
        }
        
        [request setHTTPBody:postData];
        
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            
            NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            NSLog(@"Response: %@",dataString);
            
            double status = (long)[httpResponse statusCode];
            NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
            if (data.length > 0) {
                
                NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                if (error != nil) {
                    NSLog(@"Error parsing JSON.");
                }
                else {
                    
                    NSLog(@"Dict: %@", jsonDict);
                    
                    if (status == 202){
                        
                        
                        
                        RLMRealm *realm = [RLMRealm defaultRealm];
                        
                        [realm beginWriteTransaction];
                        //                        [realm deleteObjects:[AppUser allObjectsInRealm:realm]];
                        
                        AppUser *newUser = [[AppUser allObjects] firstObject];
                        
                        if (newUser == nil) {
                            newUser = [[AppUser alloc] init];
                        }
                        
                        
                        NSDictionary *data = [jsonDict valueForKey:@"data"];
                        
                        NSString *userName = [data valueForKey:@"username"];
                        newUser.name = [data valueForKey:@"username"];
                        
                        [[NSUserDefaults standardUserDefaults] setValue:userName forKey:@"username"];
                        
                        NSString *token = [data valueForKey:@"token"];
                        newUser.token = [data valueForKey:@"token"];
                        
                        [[NSUserDefaults standardUserDefaults] setValue:token forKey:@"token"];
                        
                        NSString *role = [data valueForKey:@"role"];
                        newUser.role = [data valueForKey:@"role"];
                        
                        
                        newUser.profileID = [[data valueForKey:@"profileID"] integerValue];
                        newUser.rewards = [[data valueForKey:@"rewardsInDoller"] integerValue];
                        newUser.referralCode = [data valueForKey:@"referalCode"];
                        newUser.isOwnerAccountUpdated = [[data valueForKey:@"isOwnerAccountUpdated"] integerValue];
                        newUser.isServerAccountUpdated = [[data valueForKey:@"isServerAccountUpdated"] integerValue];
                        newUser.isVerified = [[data valueForKey:@"isVerified"] integerValue];
                        
                        [realm addObject:newUser];
                        
                        [realm commitWriteTransaction];
                        
                        AppUser *addedUser = [[AppUser allObjects] firstObject];
                        NSLog(@"User:%@",addedUser);
                        
                        
                        
                        
                        
                        [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"isuserloggedin"];
                        
                        if ([role isEqualToString:@"customer"])
                        {
                            //                            [[NSUserDefaults standardUserDefaults]setObject:@"customer" forKey:@"role"];
                            [[NSUserDefaults standardUserDefaults]setValue:@"customer" forKey:@"role"];
                            
                            if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Sender"] isEqualToString:@"Home"]){
                                
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    
                                    [APP.hud setHidden:YES];
                                    
                                    UIAlertController * alert2=   [UIAlertController
                                                                   alertControllerWithTitle:@"Login Successful"
                                                                   message:@""
                                                                   preferredStyle:UIAlertControllerStyleAlert];
                                    
                                    [self presentViewController:alert2 animated:NO completion:nil];
                                    
                                    
                                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                        
                                        _passwordText.text=@"";
                                        _usernameText.text=@"";
                                        [self.view endEditing:YES];
                                        
                                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                                        
                                        [[LocalData sharedInstance] FetchAllRestaurants:^(BOOL result) {
                                            if (result)
                                            {
                                                HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                                                [self.navigationController pushViewController:home animated:NO];
                                            }
                                            else{
                                                HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                                                [self.navigationController pushViewController:home animated:NO];
                                            }
                                            
                                        }];
                                        
                                    });
                                    
                                });
                                
                                
                            }
                            
                            else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Sender"] isEqualToString:@"Comment"]) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    [APP.hud setHidden:YES];
                                    UIAlertController * alert2=   [UIAlertController
                                                                   alertControllerWithTitle:@"Login Successful"
                                                                   message:@""
                                                                   preferredStyle:UIAlertControllerStyleAlert];
                                    
                                    [self presentViewController:alert2 animated:NO completion:nil];
                                    
                                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                        
                                        _passwordText.text=@"";
                                        _usernameText.text=@"";
                                        [self.view endEditing:YES];
                                        
                                        
                                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                                        [[NSUserDefaults standardUserDefaults] setValue:@"Login" forKey:@"Sender"];
                                        [self dismissViewControllerAnimated:NO completion:nil];
                                        
                                    });
                                    
                                });
                            }
                            else {
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [APP.hud setHidden:YES]; });
                                
                                [[LocalData sharedInstance] FetchAllRestaurants:^(BOOL result) {
                                    if (result)
                                    {
                                        HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                                        [self.navigationController pushViewController:home animated:NO];
                                    }
                                    else{
                                        HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                                        [self.navigationController pushViewController:home animated:NO];
                                    }
                                    
                                }];
                            }
                            
                            
                        }
                        else if ([role isEqualToString:@"server"])
                        {
                            
                            
                            
                            //                            [[NSUserDefaults standardUserDefaults]setObject:@"server" forKey:@"role"];
                            [[NSUserDefaults standardUserDefaults]setValue:@"server" forKey:@"role"];
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                
                                
                                UIAlertController * alert2=   [UIAlertController
                                                               alertControllerWithTitle:@"Login Successful"
                                                               message:@""
                                                               preferredStyle:UIAlertControllerStyleAlert];
                                
                                [self presentViewController:alert2 animated:NO completion:nil];
                                
                                
                                
                                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                    
                                    _passwordText.text=@"";
                                    _usernameText.text=@"";
                                    [self.view endEditing:YES];
                                    
                                    [alert2 dismissViewControllerAnimated:NO completion:nil];
                                    
                                    [self fetchDataFromServer];
                                });
                                
                            });
                            
                        }
                        
                        else if ([role isEqualToString:@"owner"])
                        {
                            [[NSUserDefaults standardUserDefaults]setValue:@"owner" forKey:@"role"];
                            
                            //                            [self goToOwnerDashboard];
                            
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                
                                
                                UIAlertController * alert2=   [UIAlertController
                                                               alertControllerWithTitle:@"Login Successful"
                                                               message:@""
                                                               preferredStyle:UIAlertControllerStyleAlert];
                                [APP.hud setHidden:YES];
                                [self presentViewController:alert2 animated:NO completion:nil];
                                
                                
                                
                                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                    
                                    _passwordText.text=@"";
                                    _usernameText.text=@"";
                                    [self.view endEditing:YES];
                                    
                                    [alert2 dismissViewControllerAnimated:NO completion:nil];
                                    
                                    [self checkIfUserVerified];
                                });
                                
                            });
                            
                            
                        }
                        
                        else {
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [APP.hud setHidden:YES]; });
                            
                            [[LocalData sharedInstance] FetchAllRestaurants:^(BOOL result) {
                                if (result)
                                {
                                    HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                                    [self.navigationController pushViewController:home animated:NO];
                                }
                                else{
                                    HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                                    [self.navigationController pushViewController:home animated:NO];
                                }
                                
                                
                                
                            }];
                        }
                    }
                    if (status == 400){
                        
                        NSLog(@"Login Failed");
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [APP.hud setHidden:YES];
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"Error"
                                                           message:[jsonDict valueForKey:@"result"]
                                                           preferredStyle:UIAlertControllerStyleAlert];
                            
                            [self presentViewController:alert2 animated:NO completion:nil];
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                            });
                        });
                        
                    }
                    if (status == 401){
                        
                        NSLog(@"Login Failed");
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [APP.hud setHidden:YES];
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"Error"
                                                           message:@"User Not Authenticated"
                                                           preferredStyle:UIAlertControllerStyleAlert];
                            
                            [self presentViewController:alert2 animated:NO completion:nil];
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                            });
                        });
                        
                    }
                    
                    
                }
            }
            else {
                
                NSLog(@"Login Failed");
                dispatch_async(dispatch_get_main_queue(), ^{
                    [APP.hud setHidden:YES];
                    UIAlertController * alert2=   [UIAlertController
                                                   alertControllerWithTitle:@"Error"
                                                   message:error.localizedDescription
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert2 animated:NO completion:nil];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                    });
                });
            }
            
            
        }];
        
        [postDataTask resume];
        
    }
    
}
- (IBAction)forgotPasswordButton:(id)sender {
    
    NSLog(@"Forgot Password Pressed...");
    
    ForgotPasswordViewController *FPVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordViewController"];
    
    [self.navigationController pushViewController:FPVC animated:NO];
    //
}
- (IBAction)registerButtonPressed:(id)sender {
    
    SignUpViewController *signUp = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
    [self.navigationController pushViewController:signUp animated:NO];
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:NO completion:nil];
}

- (IBAction)googleSignIn:(id)sender {
    
    @try {
        GIDSignIn*sigNIn=[GIDSignIn sharedInstance];
        [sigNIn setDelegate:self];
        [sigNIn setUiDelegate:self];
        sigNIn.shouldFetchBasicProfile = YES;
        
        //        sigNIn.scopes = @[@"https://www.googleapis.com/auth/plus.login",@"https://www.googleapis.com/auth/userinfo.email",@"https://www.googleapis.com/auth/userinfo.profile"];
        
        [sigNIn signIn];
    } @catch (NSException *exception) {
        NSLog(@"Sign In With Google Error:%@",exception.description);
    }
    
}

- (IBAction)facebookSignIn:(id)sender {
    
    
    
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile",@"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error: %@",error.description);
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
             //             [[NSUserDefaults standardUserDefaults] setValue:@"no" forKey:@"isuserloggedin"];
             
             
         } else {
             
             NSLog(@"Result:%@",result);
             
             if(result.token)
             {
                 
                 [self getFacebookProfileInfo];
             }
             
             [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"isuserloggedin"];
             
             /*
              
              if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Sender"] isEqualToString:@"Home"]){
              
              dispatch_async(dispatch_get_main_queue(), ^{
              
              UIAlertController * alert2=   [UIAlertController
              alertControllerWithTitle:@"Login Sucessful"
              message:@""
              preferredStyle:UIAlertControllerStyleAlert];
              
              [self presentViewController:alert2 animated:YES completion:nil];
              
              
              
              dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
              [alert2 dismissViewControllerAnimated:YES completion:nil];
              [self.navigationController popViewControllerAnimated:YES];
              [self.navigationController dismissViewControllerAnimated:NO completion:nil];
              });
              
              });
              
              
              }
              
              */
         }
     }];
    
}

- (void)getFacebookProfileInfo {
    
    
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@"id,name,email" forKey:@"fields"];
    
    FBSDKGraphRequest *requestMe = [[FBSDKGraphRequest alloc]initWithGraphPath:@"me" parameters:parameters];
    
    FBSDKGraphRequestConnection *connection = [[FBSDKGraphRequestConnection alloc] init];
    
    [connection addRequest:requestMe completionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        NSLog(@"FB Access Token:%@",[FBSDKAccessToken currentAccessToken].tokenString);
        FBAccessToken = [FBSDKAccessToken currentAccessToken].tokenString;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [APP huddie];
            
        });
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,FACEBOOK_LOGIN]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                        
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                        
                                                           timeoutInterval:60.0];
        
        
        
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        //[request addValue:[NSString stringWithFormat:@"bearer %@",FBAccessToken] forHTTPHeaderField:@"Authorization"];
        
        [request setHTTPMethod:@"POST"];
        NSMutableDictionary *dataDict = [NSMutableDictionary new];
        [dataDict setValue:FBAccessToken forKey:@"access_token"];
        
        //[dataDict setValue:refreshToken forKey:@"refresh_token"];
        NSData *postData = [NSJSONSerialization dataWithJSONObject:dataDict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        
        
        NSString *postStr;
        
        if (! postData) {
            
            NSLog(@"Got an error: %@", error);
            
            
            
        } else {
            
            NSString *jsonString = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
            
            
            
            postStr = [NSString stringWithFormat:@"[%@]",jsonString];
            
            
            
        }
        
        
        
        [request setHTTPBody:postData];
        
        
        
        
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            
            NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            NSLog(@"Response: %@",dataString);
            
            double status = (long)[httpResponse statusCode];
            
            NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if (data.length > 0)
            {
                
                if (status == 202){
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [APP.hud setHidden:YES];
                    });
                    
                    RLMRealm *realm = [RLMRealm defaultRealm];
                    
                    
                    
                    [realm beginWriteTransaction];
                    
                    //                        [realm deleteObjects:[AppUser allObjectsInRealm:realm]];
                    
                    
                    
                    AppUser *newUser = [[AppUser allObjects] firstObject];
                    
                    
                    
                    if (newUser == nil) {
                        
                        newUser = [[AppUser alloc] init];
                        
                    }
                    
                    NSDictionary *data = [jsonDict valueForKey:@"data"];
                    
                    NSString *userName = [data valueForKey:@"username"];
                    
                    newUser.name = [data valueForKey:@"username"];
                    
                    [[NSUserDefaults standardUserDefaults] setValue:userName forKey:@"username"];
                    NSString *token = [data valueForKey:@"token"];
                    
                    newUser.token = [data valueForKey:@"token"];
                    [[NSUserDefaults standardUserDefaults] setValue:token forKey:@"token"];
                    
                    NSString *role = [data valueForKey:@"role"];
                    
                    newUser.role = [data valueForKey:@"role"];
                    
                    newUser.referralCode = [data valueForKey:@"referralCode"];
                    newUser.profileID = [[data valueForKey:@"profileID"] integerValue];
                    [realm addObject:newUser];
                    [realm commitWriteTransaction];
                    
                    
                    
                    AppUser *addedUser = [[AppUser allObjects] firstObject];
                    
                    NSLog(@"User:%@",addedUser);
                    
                    
                    [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"isuserloggedin"];
                    
                    
                    
                    if ([role isEqualToString:@"customer"])
                        
                    {
                        
                        //                            [[NSUserDefaults standardUserDefaults]setObject:@"customer" forKey:@"role"];
                        
                        [[NSUserDefaults standardUserDefaults]setValue:@"customer" forKey:@"role"];
                        
                        
                        
                        if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Sender"] isEqualToString:@"Home"]){
                            
                            
                            
                            
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                
                                
                                //                            [APP.hud setHidden:YES];
                                
                                
                                
                                UIAlertController * alert2=   [UIAlertController
                                                               
                                                               alertControllerWithTitle:@"Login Sucessful"
                                                               
                                                               message:@""
                                                               
                                                               preferredStyle:UIAlertControllerStyleAlert];
                                
                                
                                
                                [self presentViewController:alert2 animated:NO completion:nil];
                                
                                
                                
                                
                                
                                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                    
                                    
                                    
                                    _passwordText.text=@"";
                                    
                                    _usernameText.text=@"";
                                    
                                    [self.view endEditing:YES];
                                    
                                    
                                    
                                    [alert2 dismissViewControllerAnimated:NO completion:nil];
                                    
                                    
                                    
                                    [[LocalData sharedInstance] FetchAllRestaurants:^(BOOL result) {
                                        
                                        if (result)
                                            
                                        {
                                            
                                            HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                                            
                                            [self.navigationController pushViewController:home animated:NO];
                                            
                                        }
                                        
                                        else{
                                            
                                            HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                                            
                                            [self.navigationController pushViewController:home animated:NO];
                                            
                                        }
                                        
                                        
                                        
                                    }];
                                    
                                    
                                    
                                });
                                
                                
                                
                            });
                            
                            
                            
                            
                            
                        }
                        
                        
                        
                        else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Sender"] isEqualToString:@"Comment"]) {
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [APP.hud setHidden:YES];
                                
                                UIAlertController * alert2=   [UIAlertController
                                                               
                                                               alertControllerWithTitle:@"Login Sucessful"
                                                               
                                                               message:@""
                                                               
                                                               preferredStyle:UIAlertControllerStyleAlert];
                                
                                
                                
                                [self presentViewController:alert2 animated:NO completion:nil];
                                
                                
                                
                                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                    
                                    
                                    
                                    _passwordText.text=@"";
                                    
                                    _usernameText.text=@"";
                                    
                                    [self.view endEditing:YES];
                                    
                                    
                                    
                                    
                                    
                                    [alert2 dismissViewControllerAnimated:NO completion:nil];
                                    
                                    [[NSUserDefaults standardUserDefaults] setValue:@"Login" forKey:@"Sender"];
                                    
                                    [self dismissViewControllerAnimated:NO completion:nil];
                                    
                                    
                                    
                                });
                                
                                
                                
                            });
                            
                        }
                        
                        else {
                            
                            
                            
                            [[LocalData sharedInstance] FetchAllRestaurants:^(BOOL result) {
                                
                                if (result)
                                    
                                {
                                    
                                    HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                                    
                                    [self.navigationController pushViewController:home animated:NO];
                                    
                                }
                                
                                else{
                                    
                                    HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                                    
                                    [self.navigationController pushViewController:home animated:NO];
                                    
                                }
                                
                                
                                
                            }];
                            
                        }
                        
                        
                        
                        
                        
                    }
                    
                    else if ([role isEqualToString:@"server"])
                        
                    {
                        
                        
                        
                        
                        
                        
                        
                        //                            [[NSUserDefaults standardUserDefaults]setObject:@"server" forKey:@"role"];
                        
                        [[NSUserDefaults standardUserDefaults]setValue:@"server" forKey:@"role"];
                        
                        
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            
                            
                            
                            
                            
                            
                            UIAlertController * alert2=   [UIAlertController
                                                           
                                                           alertControllerWithTitle:@"Login Sucessful"
                                                           
                                                           message:@""
                                                           
                                                           preferredStyle:UIAlertControllerStyleAlert];
                            
                            
                            
                            [self presentViewController:alert2 animated:NO completion:nil];
                            
                            
                            
                            
                            
                            
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                
                                
                                
                                _passwordText.text=@"";
                                
                                _usernameText.text=@"";
                                
                                [self.view endEditing:YES];
                                
                                
                                
                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                                
                                [APP.hud setHidden:YES];
                                
                                
                                
                                [self fetchDataFromServer];
                                
                            });
                            
                            
                            
                        });
                        
                        
                        
                    }
                    
                    else {
                        
                        
                        
                        [[LocalData sharedInstance] FetchAllRestaurants:^(BOOL result) {
                            
                            if (result)
                                
                            {
                                
                                HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                                
                                [self.navigationController pushViewController:home animated:NO];
                                
                            }
                            
                            else{
                                
                                HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                                
                                [self.navigationController pushViewController:home animated:NO];
                                
                            }
                            
                            
                            
                            
                            
                            
                            
                        }];
                        
                    }
                    
                }
                
                else if (status == 400){
                    
                    
                    
                    NSLog(@"Login Failed");
                    
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [APP.hud setHidden:YES];
                        
                        UIAlertController * alert2=   [UIAlertController
                                                       
                                                       alertControllerWithTitle:@"Error"
                                                       
                                                       message:[jsonDict valueForKey:@"result"]
                                                       
                                                       preferredStyle:UIAlertControllerStyleAlert];
                        
                        
                        
                        [self presentViewController:alert2 animated:NO completion:nil];
                        
                        
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                            
                            [alert2 dismissViewControllerAnimated:NO completion:nil];
                            
                        });
                        
                    });
                    
                    
                    
                }
                
                else if (status == 401){
                    
                    
                    
                    NSLog(@"Login Failed");
                    
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [APP.hud setHidden:YES];
                        
                        UIAlertController * alert2=   [UIAlertController
                                                       
                                                       alertControllerWithTitle:@"Info"
                                                       
                                                       message:@"Please Register on Tapped App"
                                                       
                                                       preferredStyle:UIAlertControllerStyleAlert];
                        
                        
                        
                        [self presentViewController:alert2 animated:NO completion:nil];
                        
                        
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                            
                            [alert2 dismissViewControllerAnimated:NO completion:nil];
                            SignUpViewController *signUp = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
                            [self.navigationController pushViewController:signUp animated:NO];
                            
                        });
                        
                    });
                    
                    
                    
                }
                
                else {
                    
                    
                    
                    NSLog(@"Login Failed");
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [APP.hud setHidden:YES];
                        
                        UIAlertController * alert2=   [UIAlertController
                                                       
                                                       alertControllerWithTitle:@"Error"
                                                       
                                                       message:error.localizedDescription
                                                       
                                                       preferredStyle:UIAlertControllerStyleAlert];
                        
                        
                        
                        [self presentViewController:alert2 animated:NO completion:nil];
                        
                        
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                            
                            [alert2 dismissViewControllerAnimated:NO completion:nil];
                            
                        });
                        
                    });
                    
                }
            }
            
            
            
            
            
        }];
        
        
        
        [postDataTask resume];
        
        
        
        [FBSDKAccessToken refreshCurrentAccessToken:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            
        }];
        
        
        NSLog(@"Facebook User Info:%@",result);
        if(result)
        {
            if ([result objectForKey:@"email"]) {
                
                NSLog(@"Email: %@",[result objectForKey:@"email"]);
                [[NSUserDefaults standardUserDefaults] setValue:[result objectForKey:@"email"] forKey:@"userEmailFromSocialMedia"];
                
            }
            if ([result objectForKey:@"first_name"]) {
                
                NSLog(@"First Name : %@",[result objectForKey:@"first_name"]);
                
            }
            if ([result objectForKey:@"id"]) {
                
                NSLog(@"User id : %@",[result objectForKey:@"id"]);
                
            }
            
        }
        
    }];
    
    [connection start];
    
}

- (IBAction)twitterSignIn:(id)sender {
    
    
    [[Twitter sharedInstance] logInWithMethods:TWTRLoginMethodWebBased completion:^(TWTRSession *session, NSError *error) {
        
        if (session) {
            NSLog(@"signed in as %@", session);
            NSLog(@"signed in as %@", [session userName]);
            NSLog(@"signed in ID %@", [session userID]);
            
            [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"isuserloggedin"];
            
            
            TWTRAPIClient *client = [TWTRAPIClient clientWithCurrentUser];
            NSURLRequest *request = [client URLRequestWithMethod:@"GET"
                                                             URL:@"https://api.twitter.com/1.1/account/verify_credentials.json"
                                                      parameters:@{@"include_email": @"1", @"skip_status": @"1",@"include_entities": @"0", @"user_id": session.userID }
                                                           error:nil];
            
            [client sendTwitterRequest:request completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                
                //                NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                NSError* error;
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                     options:kNilOptions
                                                                       error:&error];
                
                NSLog(@"Twitter Login Data:%@",json);
                if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Sender"] isEqualToString:@"Home"])
                {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        UIAlertController * alert2=   [UIAlertController
                                                       alertControllerWithTitle:@"Login Sucessful"
                                                       message:@""
                                                       preferredStyle:UIAlertControllerStyleAlert];
                        
                        [self presentViewController:alert2 animated:NO completion:nil];
                        
                        
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                            [alert2 dismissViewControllerAnimated:NO completion:nil];
                            [self.navigationController popViewControllerAnimated:NO];
                            [self.navigationController dismissViewControllerAnimated:NO completion:nil];
                        });
                        
                    });
                    
                    
                }
                
            }];
            
        }
        else {
            
            
            NSLog(@"error: %@", [error localizedDescription]);
            //            [[NSUserDefaults standardUserDefaults] setValue:@"no" forKey:@"isuserloggedin"];
            
        }
        
    }];
    
}

- (void)addUserInfoToRealm:(NSDictionary*)data {
    
    @try {
        
        
    } @catch (NSException *exception) {
        NSLog(@"Saving User Info Error:%@, Description:%@",exception,exception.description);
    } @finally {
        
    }
    
}

#pragma mark - Fetch Data From Server
- (void)fetchDataFromServer {
    
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    @try {
        [realm deleteObjects:[Servers allObjectsInRealm:realm]];
        [realm deleteObjects:[ServerRating allObjectsInRealm:realm]];
        [realm deleteObjects:[ServerJobApplied allObjectsInRealm:realm]];
        [realm deleteObjects:[ServerWorkExperience allObjectsInRealm:realm]];
        [realm commitWriteTransaction];
        
    } @catch (NSException *exception) {
        NSLog(@"%@ LocalData",exception);
    }
    
    
    if ([self isNetworkAvailable]) {
        
        AppUser *user = [[AppUser allObjects] firstObject];
        
        
        [[LocalData sharedInstance] FetchRestaurantsList:^(BOOL result)
         {
             
             if (result)
             {
                 NSLog(@"Login: Resto List Fetched");
                 
                 
             }
             else{
                 NSLog(@"Login: Error while fetching Resto List");
                 
             }
             
         }];
        
        [[LocalData sharedInstance] GetAllAvailableJobs:user.token];
        
        [[LocalData sharedInstance] FetchRatingsAndComments:[NSString stringWithFormat:@"%ld",(long)user.profileID] token:user.token completion:^(BOOL result)
         {
             
             if (result)
             {
                 NSLog(@"Login: Ratings and Comments Downloaded");
                 
                 
             }
             else{
                 NSLog(@"Login: Error while fetching Ratings and Comments");
                 
             }
             
         }];
        
        [[LocalData sharedInstance] FetchServerProfile:user.token completion:^(BOOL result)
         {
             
             if (result)
             {
                 NSLog(@"Login: Server Profile Fetched");
                 
                 
             }
             else
             {
                 NSLog(@"Login: Error while fetching Server Profile");
                 
             }
             
             
         }];
        
        
        
        if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"role"]isEqualToString:@"server"]) {
            
            if (user.isServerAccountUpdated == 0) {
                
                
                ProfileStep1ViewController *serverProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileStep1ViewController"];
                serverProfile.aSender = @"Register";
                serverProfile.navigationItem.leftBarButtonItem = nil;
                serverProfile.navigationItem.hidesBackButton = YES;
                [self.navigationController pushViewController:serverProfile animated:NO];
            }
            else
            {
                
                ServerLandingViewController *serverLanding = [self.storyboard instantiateViewControllerWithIdentifier:@"ServerLandingViewController"];
                serverLanding.navigationItem.leftBarButtonItem = nil;
                serverLanding.navigationItem.hidesBackButton = YES;
                [self.navigationController pushViewController:serverLanding animated:NO];
            }
            
            
        }
        
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Error while connecting to Server"
                                           message:@""
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:NO completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                
                [alert2 dismissViewControllerAnimated:NO completion:nil];
                return ;
                
            });
            
        });
    }
    
}
- (bool)isNetworkAvailable {
    SCNetworkReachabilityFlags flags;
    SCNetworkReachabilityRef address;
    address = SCNetworkReachabilityCreateWithName(NULL, "www.apple.com" );
    Boolean success = SCNetworkReachabilityGetFlags(address, &flags);
    CFRelease(address);
    
    bool canReach = success
    && !(flags & kSCNetworkReachabilityFlagsConnectionRequired)
    && (flags & kSCNetworkReachabilityFlagsReachable);
    
    return canReach;
}

- (void)goToOwnerDashboard {
    
    AppUser *user = [[AppUser allObjects] firstObject];
    
    if ([self isNetworkAvailable]) {
        
        [[LocalData sharedInstance] FetchOwnerProfile:user.token completion:^(BOOL success) {
            if (success)
            {
                NSLog(@"Login: Owner Profile Fetched");
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                });
                
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                });
                NSLog(@"Login:  while fetching Owner Profile");
                
            }
            
        }];
        
        
        
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Error while connecting to Server"
                                           message:@""
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:NO completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                
                [alert2 dismissViewControllerAnimated:NO completion:nil];
                return ;
                
            });
            
        });
    }
    
    
}

- (void) checkIfUserVerified
{
    AppUser *user = [[AppUser allObjects] firstObject];
    NSLog(@"User Info:%@",user);
    
    if (user.isVerified == 1) {
        
        if (user.isOwnerAccountUpdated == 0) {
            
            [APP.hud setHidden:YES];
            OwnerProfileStep1ViewController *ownerProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerProfileStep1ViewController"];
            
            ownerProfile.navigationItem.leftBarButtonItem = nil;
            ownerProfile.navigationItem.hidesBackButton = YES;
            [self.navigationController pushViewController:ownerProfile animated:NO];
        }
        else{
            [APP.hud setHidden:YES];
            
            if ([self isNetworkAvailable]) {
                
                [[LocalData sharedInstance] FetchOwnerProfile:user.token completion:^(BOOL success) {
                    if (success)
                    {
                        NSLog(@"Login: Owner Profile Fetched");
                        
                        OwnerDashboard *ownerLanding = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerDashboard"];
                        //                            ownerLanding.navigationItem.leftBarButtonItem = nil;
                        ownerLanding.navigationItem.hidesBackButton = YES;
                        [self.navigationController pushViewController:ownerLanding animated:NO];
                        
                    }
                    else
                    {
                        OwnerDashboard *ownerLanding = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerDashboard"];
                        //    ownerLanding.navigationItem.leftBarButtonItem = nil;
                        ownerLanding.navigationItem.hidesBackButton = YES;
                        [self.navigationController pushViewController:ownerLanding animated:NO];
                        
                        NSLog(@"Login:  while fetching Owner Profile");
                        
                    }
                    
                }];
                
                
                
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController * alert2=   [UIAlertController
                                                   alertControllerWithTitle:@"Error while connecting to Server"
                                                   message:@""
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert2 animated:NO completion:nil];
                    
                    
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        
                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                        return ;
                        
                    });
                    
                });
            }
            
            
            
        }
        
    }
    else
    {
        NSLog(@"Go to Email Verification");
        [APP.hud setHidden:YES];
        OwnerVerifyOTPViewController *verifyOTPVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerVerifyOTPViewController"];
        verifyOTPVC.navigationItem.leftBarButtonItem = nil;
        verifyOTPVC.navigationItem.hidesBackButton = YES;
        [self.navigationController pushViewController:verifyOTPVC animated:NO];
        
    }
}
@end


