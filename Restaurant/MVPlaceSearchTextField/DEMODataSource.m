//
//  DEMODataSource.m
//  MLPAutoCompleteDemo
//
//  Created by Eddy Borja on 5/28/14.
//  Copyright (c) 2014 Mainloop. All rights reserved.
//

#import "DEMODataSource.h"
#import "AppDelegate.h"
#import "DEMOCustomAutoCompleteObject.h"

@interface DEMODataSource ()

@property (strong, nonatomic) NSArray *countryObjects;

@end


@implementation DEMODataSource
@synthesize dict;


- (id) init
{
    if (self = [super init])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
           allRestaurants = [[Restaurants allObjects]sortedResultsUsingProperty:@"name" ascending:YES ];
            
            Restaurantsall = [[NSMutableArray alloc]init];
            _arrIDs = [[NSMutableArray alloc]init];
        });
        
    
    }
    return self;
}

#pragma mark - MLPAutoCompleteTextField DataSource


//example of asynchronous fetch:
- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
 possibleCompletionsForString:(NSString *)string
            completionHandler:(void (^)(NSArray *))handler
{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_async(queue, ^{
        if(self.simulateLatency){
            CGFloat seconds = arc4random_uniform(4)+arc4random_uniform(4); //normal distribution
            NSLog(@"sleeping fetch of completions for %f", seconds);
            sleep(seconds);
        }
        
        
//         NSArray __block *completions;
        
//        if(self.testWithAutoCompleteObjectsInsteadOfStrings){
//            completions = [self allCountryObjects];
//        } else {
//            
//            
//            [self FetchRestaurantsList:^(NSMutableArray *result)
//            {
//                    completions = result;
//            }];
////            completions = [self allCountries];
//
//            
//            
//        }
        [self FetchRestaurantsList:^(NSMutableArray *result)
         {
        handler(result);
        }];
    });
}

/*
 - (NSArray *)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
 possibleCompletionsForString:(NSString *)string
 {
 
 if(self.simulateLatency){
 CGFloat seconds = arc4random_uniform(4)+arc4random_uniform(4); //normal distribution
 NSLog(@"sleeping fetch of completions for %f", seconds);
 sleep(seconds);
 }
 
 NSArray *completions;
 if(self.testWithAutoCompleteObjectsInsteadOfStrings){
 completions = [self allCountryObjects];
 } else {
 completions = [self allCountries];
 }
 
 return completions;
 }
 */

- (NSArray *)allCountryObjects
{
    if(!self.countryObjects){
        NSArray *countryNames = [self allCountries];
        NSMutableArray *mutableCountries = [NSMutableArray new];
        for(NSString *countryName in countryNames){
            DEMOCustomAutoCompleteObject *country = [[DEMOCustomAutoCompleteObject alloc] initWithCountry:countryName];
            [mutableCountries addObject:country];
        }
        
        [self setCountryObjects:[NSArray arrayWithArray:mutableCountries]];
    }
    
    return self.countryObjects;
}
typedef void(^myCompletion)(NSMutableArray *);

- (void)FetchRestaurantsList:(myCompletion) compblock {
    //use your statement or call method here
           //if([[[NSUserDefaults standardUserDefaults] valueForKey:@"NameOfRestro"] length] > 1)
        {
            
            NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
            AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
            
            NSString *urlString =[[NSString stringWithFormat:@"%@%@?restaurant=%@&page=0",SITE_URL,@"/restaurant/getRestaurants",[[NSUserDefaults standardUserDefaults] valueForKey:@"NameOfRestro"]]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            
            NSURL *URL = [NSURL URLWithString:urlString];
            NSLog(@"%@",URL);
            NSURLRequest *request = [NSURLRequest requestWithURL:URL];
            
            NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                
                
                if (error) {
                    NSLog(@"Error: %@", error.description);
                    dispatch_async(dispatch_get_main_queue(), ^{[APP.hud setHidden:YES];});
                    
                }
                else {
                    //            NSLog(@"%@ %@", response, responseObject);
                    
                    if ([responseObject isKindOfClass:[NSArray class]])
                    {
                        NSArray *jsonArray = (NSArray *)responseObject  ;
                        NSLog(@"Json:%@",jsonArray);
                    }
                    
                    if ([responseObject isKindOfClass:[NSDictionary class]]) {
                        
                        //                if (Restaurantsall.count > 0)
                        //                {
                        Restaurantsall = [[NSMutableArray alloc]init];
                        //                }
                        serverDict = (NSDictionary *)responseObject;
                        dict = [serverDict valueForKey:@"data"];
                        Restaurantsall = [dict valueForKey:@"name"];
                        NSLog(@"%@",Restaurantsall);
                        _arrIDs = [dict valueForKey:@"_id"];
                        //                [arrRestaurants addObjectsFromArray:[serverDict valueForKey:@"data"]];
                        
                        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
                        [nc postNotificationName:@"serverDictFetched" object:self userInfo:dict];
                        //                return Restaurantsall;
                        //arrServers = [serverDict valueForKey:@"data"];
                        
                        compblock(Restaurantsall);
                    }
                }
            }];
            [dataTask resume];
        }
}


- (NSArray *)allCountries
{
    NSMutableArray *arrResto = [[NSMutableArray alloc]init];
//    dispatch_async(dispatch_get_main_queue(), ^{
//        
//        for (int i =0; i < [allRestaurants count]; i++)
//        {
//            [arrResto addObject:[[allRestaurants objectAtIndex:i] valueForKey:@"name"]];
//            
//        }
//        
//        
//    });
    
    //if([[[NSUserDefaults standardUserDefaults] valueForKey:@"NameOfRestro"] length] > 1)
//    {
//    
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
//    
//        NSString *urlString =[[NSString stringWithFormat:@"%@%@?restaurant=%@&page=0",SITE_URL,@"/restaurant/getRestaurants",[[NSUserDefaults standardUserDefaults] valueForKey:@"NameOfRestro"]]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
//
//    NSURL *URL = [NSURL URLWithString:urlString];
//    NSLog(@"%@",URL);
//    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
//        
//    
//    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
//        
//        
//        if (error) {
//            NSLog(@"Error: %@", error.description);
//            dispatch_async(dispatch_get_main_queue(), ^{[APP.hud setHidden:YES];});
//            
//        }
//        else {
//            //            NSLog(@"%@ %@", response, responseObject);
//            
//            if ([responseObject isKindOfClass:[NSArray class]])
//            {
//                NSArray *jsonArray = (NSArray *)responseObject  ;
//                NSLog(@"Json:%@",jsonArray);
//            }
//            
//            if ([responseObject isKindOfClass:[NSDictionary class]]) {
//                
////                if (Restaurantsall.count > 0)
////                {
//                    Restaurantsall = [[NSMutableArray alloc]init];
////                }
//                serverDict = (NSDictionary *)responseObject;
//                dict = [serverDict valueForKey:@"data"];
//                Restaurantsall = [dict valueForKey:@"name"];
//                NSLog(@"%@",Restaurantsall);
//                _arrIDs = [dict valueForKey:@"_id"];
////                [arrRestaurants addObjectsFromArray:[serverDict valueForKey:@"data"]];
//                
//                NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
//                [nc postNotificationName:@"serverDictFetched" object:self userInfo:dict];
////                return Restaurantsall;
//                //arrServers = [serverDict valueForKey:@"data"];
//                
//            }
//        }
//    }];
//    [dataTask resume];
    
    return Restaurantsall;

//    }
    return arrResto;
}





@end
