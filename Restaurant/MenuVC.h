//
//  MenuVC.h
//  Restaurant
//
//  Created by Parth Pandya on 09/02/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppUser.h"
#import "CustomerProfileCell.h"
#import "ProfileCell.h"
#import "MenuContentCell.h"
#import "AppDelegate.h"
#import "LoginView.h"
#import <MessageUI/MessageUI.h>


@interface MenuVC : UIViewController<UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end
