//
//  MenuVC.m
//  Restaurant
//
//  Created by Parth Pandya on 09/02/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "MenuVC.h"

@interface MenuVC ()
{
    NSMutableArray *imageArray;
    NSMutableArray *titleArray;
    NSMutableArray *customerRoleTitleArray;
    NSMutableArray *customerRoleImageArray;
}
@end

@implementation MenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.navigationController.navigationBar.barTintColor = [UIColor appMainColor];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    [menuButton setImage:[UIImage imageNamed:@"home.png"] forState:UIControlStateNormal];
    menuButton.imageEdgeInsets = UIEdgeInsetsMake(3, 4, 3, 4);
    [menuButton addTarget:self action:@selector(backToHomeView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    imageArray = [[NSMutableArray alloc] initWithObjects:@"",@"profile.png",@"share.png",@"feedback.png",@"terms.png", nil];
    
    titleArray = [[NSMutableArray alloc] initWithObjects:@"",@"Profile",@"Share App",@"Send feedback",@"Terms & Privacy Policies", nil];
    
    //    customerRoleImageArray = [[NSMutableArray alloc] initWithObjects:@"profile.png",@"profile.png",@"feedback.png",@"terms.png",@"logout.png", nil];
    customerRoleImageArray = [[NSMutableArray alloc] initWithObjects:@"profile.png",@"profile.png",@"feedback.png",@"terms.png", nil];
    
    //    customerRoleTitleArray = [[NSMutableArray alloc] initWithObjects:@"",@"Share App",@"Send feedback",@"Terms & Privacy Policies",@"Logout", nil];
    customerRoleTitleArray = [[NSMutableArray alloc] initWithObjects:@"",@"Share App",@"Send feedback",@"Terms & Privacy Policies", nil];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"toggleMenu"];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    
}


- (void) backToHomeView {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark - Tableview Delaget Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    AppUser *user = [[AppUser allObjects] firstObject];
    
    
    if ([user.role isEqualToString:@"customer"] || [user.role isEqualToString:@""] || user == nil) {
        
        return 4;
    }
    
    return 5;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([tableView isEqual:self.tableView])
    {
        return 1;
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath  {
    cell.backgroundColor = [UIColor clearColor];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AppUser *user = [[AppUser allObjects] firstObject];
    
    if (indexPath.row == 0) {
        
        if ([user.role isEqualToString:@"customer"] || [user.role isEqualToString:@""] || user == nil) {
            return 91;
        }
        else if ([user.role isEqualToString:@"server"] || [user.role isEqualToString:@"owner"])
        {
            return 171;
        }
        
    }
    return 75;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    AppUser *user = [[AppUser allObjects] firstObject];
    
    
    if (indexPath.row == 0)
    {
        if ([user.role isEqualToString:@"customer"] || [user.role isEqualToString:@""] || user == nil) {
            
            CustomerProfileCell *cell = [[CustomerProfileCell alloc] init];
            static NSString *simpleTableIdentifier = @"CustomerProfileCell";
            cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
            cell.lblName.text = [user.name capitalizedString];
            
            cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
            return cell;
            
        }
        else if ([user.role isEqualToString:@"server"])
        {
            Servers *newServer = [[Servers allObjects] firstObject];
            
            ProfileCell *cell = [[ProfileCell alloc] init];
            static NSString *simpleTableIdentifier = @"ProfileCell";
            cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
            cell.lblName.text = [user.name capitalizedString];
            [cell.imgProfile sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,newServer.imageURL]]
                              placeholderImage:[UIImage imageNamed:@"ic_user_b.png"]];
            cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
            return cell;
        }
        else if ([user.role isEqualToString:@"owner"])
        {
            OwnerProfile *aOwner = [[OwnerProfile allObjects] firstObject];
            
            ProfileCell *cell = [[ProfileCell alloc] init];
            static NSString *simpleTableIdentifier = @"ProfileCell";
            cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
            cell.lblName.text = [user.name capitalizedString];
            [cell.imgProfile sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,aOwner.profilePic]]
                               placeholderImage:[UIImage imageNamed:@"ic_user_b.png"]];
            cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
            return cell ;
        }
        
        
    }
    
    MenuContentCell *cell = [[MenuContentCell alloc] init];
    cell = [tableView dequeueReusableCellWithIdentifier:@"MenuContentCell" forIndexPath:indexPath];
    
    if ([user.role isEqualToString:@"customer"] || [user.role isEqualToString:@""] || user == nil) {
        
        cell.lblMenuTitle.text = [customerRoleTitleArray objectAtIndex:indexPath.row];
        
        if(indexPath.row == 1)
        {
            cell.imgMenuIcon.image = [UIImage imageNamed:@"share.png"];
        }
        if(indexPath.row == 2)
        {
            cell.imgMenuIcon.image = [UIImage imageNamed:@"feedback.png"];
        }
        if(indexPath.row == 3)
        {
            cell.imgMenuIcon.image = [UIImage imageNamed:@"terms.png"];
        }
        
    }
    else if ([user.role isEqualToString:@"server"] || [user.role isEqualToString:@"owner"])
    {
        cell.imgMenuIcon.image = [UIImage imageNamed:[imageArray objectAtIndex:indexPath.row]];
        cell.lblMenuTitle.text = [titleArray objectAtIndex:indexPath.row];
        
        if(indexPath.row == 4)
        {
            cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AppUser *user = [[AppUser allObjects] firstObject];
    
    
    if ([user.role isEqualToString:@"customer"] || [user.role isEqualToString:@""] || user == nil) {
        
        if (indexPath.row == 1) {
            NSLog(@"Share App");
            
            [RLMRealm defaultRealm];
            AppUser *user = [[AppUser allObjects] firstObject];
            
            NSString *message;
            
            if(user == nil)
            {
                message = [NSString stringWithFormat:@"I'm using Tapped App. It's simply an awesome app. Give it a Try"];
            }
            else
            {
                message = [NSString stringWithFormat:@"I'm using Tapped App. It's simply an awesome app. Give it a Try with Referral Code : \"%@\"",user.referralCode];
                
            }
            
            //this is your text string to share
           
            NSArray *activityItems = @[message];
            UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
            activityVC.excludedActivityTypes = @[UIActivityTypePostToWeibo,
                                                 UIActivityTypePrint,
                                                 UIActivityTypeCopyToPasteboard,
                                                 UIActivityTypeAssignToContact,
                                                 UIActivityTypeSaveToCameraRoll,
                                                 UIActivityTypeAddToReadingList,
                                                 UIActivityTypePostToFlickr,
                                                 UIActivityTypePostToVimeo,
                                                 UIActivityTypePostToTencentWeibo,
                                                 UIActivityTypeAirDrop];
            [self presentViewController:activityVC animated:false completion:nil];
            
            
        }
        if (indexPath.row == 2) {
            NSLog(@"Send Feedback");
            
            
            if ([MFMailComposeViewController canSendMail]) {
                MFMailComposeViewController *composeViewController = [[MFMailComposeViewController alloc] initWithNibName:nil bundle:nil];
                [composeViewController setMailComposeDelegate:self];
                [composeViewController setToRecipients:@[@"admin@mukesoft.com"]];
                [composeViewController setSubject:@"Feedback"];
                [self presentViewController:composeViewController animated:YES completion:nil];
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController * alert2=   [UIAlertController
                                                   alertControllerWithTitle:@"Mail services are not available"
                                                   message:@"Email is not configured on this device"
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert2 animated:NO completion:nil];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                        
                        
                        
                    });
                });
                
            }
            
        }
        if (indexPath.row == 3) {
            NSLog(@"Privacy Policy");
            NSLog(@"Privacy Policy");
            
            [self dismissViewControllerAnimated:NO completion:nil];
            [[NSUserDefaults standardUserDefaults] setBool:YES  forKey:@"goToPrivacyView"];

            
        }
        if (indexPath.row == 4) {
            NSLog(@"Log out");
            /*
             if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"isuserloggedin"] isEqualToString:@"yes"]) {
             
             dispatch_async(dispatch_get_main_queue(), ^{
             
             UIAlertController * alert2=   [UIAlertController
             alertControllerWithTitle:@"Logged Out Successfully"
             message:@""
             preferredStyle:UIAlertControllerStyleAlert];
             
             [self presentViewController:alert2 animated:NO completion:nil];
             
             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
             [alert2 dismissViewControllerAnimated:NO completion:nil];
             
             //                        MenuContentCell *cell = [[MenuContentCell alloc] init];
             //                        cell = [tableView dequeueReusableCellWithIdentifier:@"MenuContentCell" forIndexPath:indexPath];
             //                        cell.lblMenuTitle.text = @"Logout";
             //                        [tableView deselectRowAtIndexPath:indexPath animated:NO];
             
             
             RLMRealm *realm = [RLMRealm defaultRealm];
             [realm beginWriteTransaction];
             
             [realm deleteObjects:[AppUser allObjectsInRealm:realm]];
             
             [realm commitWriteTransaction];
             
             
             [tableView beginUpdates];
             [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
             [tableView endUpdates];
             
             
             });
             });
             
             
             GIDSignIn*sigNIn=[GIDSignIn sharedInstance];
             [sigNIn signOut];
             
             FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
             [login logOut];
             [FBSDKAccessToken setCurrentAccessToken:nil];
             [FBSDKProfile setCurrentProfile:nil];
             
             
             [[NSUserDefaults standardUserDefaults] setValue:@"no" forKey:@"isuserloggedin"];
             [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"commentText"];
             
             return;
             }
             
             LoginView *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
             [self.navigationController pushViewController:login animated:NO];
             
             [[NSUserDefaults standardUserDefaults] setValue:@"Home" forKey:@"Sender"];
             */
        }
    }
    else if ([user.role isEqualToString:@"server"] || [user.role isEqualToString:@"owner"])
    {
        if (indexPath.row == 1) {
            
            [self dismissViewControllerAnimated:NO completion:nil];
            [[NSUserDefaults standardUserDefaults] setBool:YES  forKey:@"goToProfileView"];
            
        }
        if (indexPath.row == 2) {
            
            
            NSLog(@"Share App");
            
            [RLMRealm defaultRealm];
            AppUser *user = [[AppUser allObjects] firstObject];
            
            NSString *message;
            
            if(user == nil)
            {
                message = [NSString stringWithFormat:@"I'm using Tapped App. It's simply an awesome app. Give it a Try"];
            }
            else
            {
                message = [NSString stringWithFormat:@"I'm using Tapped App. It's simply an awesome app. Give it a Try with Referral Code : \"%@\"",user.referralCode];
                
            }
            
            
          
            NSArray *activityItems = @[message];
            UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
            activityVC.excludedActivityTypes = @[UIActivityTypePostToWeibo,
                                                 UIActivityTypePrint,
                                                 UIActivityTypeCopyToPasteboard,
                                                 UIActivityTypeAssignToContact,
                                                 UIActivityTypeSaveToCameraRoll,
                                                 UIActivityTypeAddToReadingList,
                                                 UIActivityTypePostToFlickr,
                                                 UIActivityTypePostToVimeo,
                                                 UIActivityTypePostToTencentWeibo,
                                                 UIActivityTypeAirDrop];
            [self presentViewController:activityVC animated:false completion:nil];
            
            
            
            
        }
        if (indexPath.row == 3) {
            NSLog(@"Send Feedback");
            
            if ([MFMailComposeViewController canSendMail]) {
                MFMailComposeViewController *composeViewController = [[MFMailComposeViewController alloc] initWithNibName:nil bundle:nil];
                [composeViewController setMailComposeDelegate:self];
                [composeViewController setToRecipients:@[@"admin@mukesoft.com"]];
                [composeViewController setSubject:@"Feedback"];
                [self presentViewController:composeViewController animated:YES completion:nil];
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController * alert2=   [UIAlertController
                                                   alertControllerWithTitle:@"Mail services are not available"
                                                   message:@"Email is not configured on this device"
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert2 animated:NO completion:nil];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                        
                        
                        
                    });
                });
                
            }
            
        }
        if (indexPath.row == 4) {
            NSLog(@"Privacy Policy");
            [self dismissViewControllerAnimated:NO completion:nil];
            [[NSUserDefaults standardUserDefaults] setBool:YES  forKey:@"goToPrivacyView"];

        }
        
    }
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}
-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0) return nil;
        
    return indexPath;
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    //Add an alert in case of failure
    [self dismissViewControllerAnimated:NO completion:nil];
    [self backToHomeView];
}

@end
