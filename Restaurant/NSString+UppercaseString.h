//
//  NSString+UppercaseString.h
//  Restaurant
//
//  Created by HN on 02/12/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (UppercaseString)

+ (NSString *)upperCase:(NSString*)aString;

+ (NSString *)getFormattedDate:(NSString *)strDate;
+ (NSString *)getMonthlyDate:(NSString *)strDate;
+ (BOOL)validatePhone:(NSString *)phoneNumber;
+ (NSString *)getCommentTime:(NSString *)strDate;
@end
