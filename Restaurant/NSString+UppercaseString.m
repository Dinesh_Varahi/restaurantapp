//
//  NSString+UppercaseString.m
//  Restaurant
//
//  Created by HN on 02/12/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "NSString+UppercaseString.h"

@implementation NSString (UppercaseString)

+ (NSString *)upperCase:(NSString*)aString
{
    
    NSString *txt = aString;
    txt = [txt stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[txt substringToIndex:1] uppercaseString]];
    
    return txt;
    
}

+ (NSString *)getFormattedDate:(NSString *)strDate {
    
    NSDateFormatter *formatter       =   [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:SS.SSS'Z'"];
    NSDate *temp =   [formatter dateFromString:strDate];
    
    [formatter setDateFormat:@"MM-dd-YYYY"];
    [formatter setLocale:[NSLocale currentLocale]];
    
    NSString *returnStr     =   [formatter stringFromDate: temp];    
    
    
    return returnStr;
}

+ (NSString *)getCommentTime:(NSString *)strDate {
    
    NSDateFormatter *formatter       =   [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:SS.SSS'Z'"];
    NSDate *temp =   [formatter dateFromString:strDate];
    
    [formatter setDateFormat:@"MMM-dd-YYYY h:mm a"];
    [formatter setLocale:[NSLocale currentLocale]];
    
    NSString *returnStr     =   [formatter stringFromDate: temp];
    
    
    return returnStr;
}


+ (NSString *)getMonthlyDate:(NSString *)strDate {
    
    
    NSDateFormatter *formatter       =   [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ssZZZZZ"];
    NSDate *temp =   [formatter dateFromString:strDate];
    
    [formatter setDateFormat:@"MMM-yyyy"];
    [formatter setLocale:[NSLocale currentLocale]];
    
    NSString *returnStr     =   [formatter stringFromDate: temp];
    
    
    
    return returnStr;
    
    
    
}



+ (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString *numberRegEx = @"[0-9]{10}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    if ([numberTest evaluateWithObject:phoneNumber] == YES)
        return TRUE;
    else
        return FALSE;
    
}

@end
