//
//  NearerRestaurantCell.h
//  Restaurant
//
//  Created by Parth Pandya on 12/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NearerRestaurantCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblCityName;
@property (weak, nonatomic) IBOutlet UILabel *lblRestaurantName;
@property (weak, nonatomic) IBOutlet UIImageView *imgRestaurant;
@end
