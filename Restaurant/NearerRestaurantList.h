//
//  NearerRestaurantList.h
//  Restaurant
//
//  Created by Parth Pandya on 12/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "AppDelegate.h"
@interface NearerRestaurantList : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSDictionary *RestaurantInfo;
    NSMutableArray *arrRestaurant;
    NSMutableArray *arrCity;
    NSMutableArray *arrDistance;
    NSMutableArray *arrImages;
    NSMutableArray *arrIDs;
    AppLocationManager *locationController;
    
}

@property (weak, nonatomic) IBOutlet UITableView *tblRestaurantList;

@end
