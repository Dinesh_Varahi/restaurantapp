//
//  NearerRestaurantList.m
//  Restaurant
//
//  Created by Parth Pandya on 12/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "NearerRestaurantList.h"
#import "NearerRestaurantCell.h"
#import "RestaurantDetailsViewController.h"

@interface NearerRestaurantList ()

@end

@implementation NearerRestaurantList

- (void)viewDidLoad {
    [super viewDidLoad];//ShowRestaurantList
    // Do any additional setup after loading the view.
    [APP huddie];
    arrRestaurant = [[NSMutableArray alloc] init];
    arrDistance = [[NSMutableArray alloc] init];
    arrCity = [[NSMutableArray alloc] init];
    arrImages = [[NSMutableArray alloc] init];
    arrIDs = [[NSMutableArray alloc]init];
//    _tblRestaurantList.contentInset = UIEdgeInsetsZero;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor appMainColor];
     self.tblRestaurantList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _tblRestaurantList.backgroundColor = [UIColor whiteColor];
    _tblRestaurantList.delegate = self;
    _tblRestaurantList.dataSource = self;
    
//    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.0 green:0.55 blue:0.78 alpha:1];
    //self.navigationController.navigationBar.backgroundColor = [UIColor appMainColor];
    self.navigationController.navigationItem.title = @"NearBy Restaurants";
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadTable:)
                                                 name:@"ShowRestaurantList"
                                               object:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    locationController = [[AppLocationManager alloc] init];
    [locationController.locationManager startUpdatingLocation];
}
-(void)reloadTable:(NSNotification *)notification {
    //distanceFromYou,name,image
    if ([notification.name isEqualToString:@"ShowRestaurantList"])
    {
        RestaurantInfo = notification.userInfo;
        arrCity = [RestaurantInfo valueForKey:@"city"];
        arrRestaurant = [RestaurantInfo valueForKey:@"name"];
        arrIDs = [RestaurantInfo valueForKey:@"_id"];
        arrDistance = [RestaurantInfo valueForKey:@"distanceFromYou"];
        //arrImages = [RestaurantInfo valueForKey:@"image"];
        [APP.hud setHidden:YES];
        [_tblRestaurantList reloadData];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrRestaurant.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NearerRestaurantCell *cell = [[NearerRestaurantCell alloc] init];
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];

    cell.lblRestaurantName.text = [arrRestaurant objectAtIndex:indexPath.row];
    cell.lblCityName.text = [arrCity objectAtIndex:indexPath.row];
    cell.lblDistance.text = [NSString stringWithFormat:@"%@meters away",[arrDistance objectAtIndex:indexPath.row]];
    
    cell.imgRestaurant.image = [UIImage imageNamed:@"Restaurant.png"];
//    [cell.imgRestaurant sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,[arrImages objectAtIndex:indexPath.row]]]
//                             placeholderImage:[UIImage imageNamed:@"Restaurant.png"]];
   
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%@",[arrIDs objectAtIndex:indexPath.row]);
    [[NSUserDefaults standardUserDefaults] setValue:[arrIDs objectAtIndex:indexPath.row] forKey:@"RestroID"];
    
    RestaurantDetailsViewController *serverDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RestaurantDetailsViewController"];
    
    [self.navigationController pushViewController:serverDetailVC animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
