//
//  NetworkConnectivity.h
//  Restaurant
//
//  Created by Parth Pandya on 06/02/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Connectivity.h"
#import "AppDelegate.h"

@interface NetworkConnectivity : NSObject

+ (instancetype)sharedInstance;

+(BOOL)checkIfInternetIsAvailable;


@end
