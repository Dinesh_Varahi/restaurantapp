//
//  NetworkConnectivity.m
//  Restaurant
//
//  Created by Parth Pandya on 06/02/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "NetworkConnectivity.h"

@implementation NetworkConnectivity

+ (instancetype)sharedInstance {
    static id _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[[self class] alloc] init];
    });
    
    return _sharedInstance;
}



/*================================================================================================
 Check Internet Rechability
 =================================================================================================*/
+(BOOL)checkIfInternetIsAvailable
{
    BOOL reachable = NO;
    NetworkStatus netStatus = [APP.internetReachability currentReachabilityStatus];
    
    if(netStatus == ReachableViaWWAN || netStatus == ReachableViaWiFi)
    {
        reachable = YES;
    }
    else
    {
        reachable = NO;
    }
    return reachable;
}



@end
