//
//  OwnerDashboard.h
//  Restaurant
//
//  Created by Parth Pandya on 13/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AMSlideMenu/UIViewController+AMSlideMenu.h>
#import <AMSlideMenuMainViewController.h>
#import "AppDelegate.h"
#import "LoginView.h"
#import "OwnerRestoServerRatingsViewController.h"
#import "OwnerRestoServerCommentsViewController.h"
#import "OwnerJobsPostedViewController.h"
#import "OwnerPostJobViewController.h"
#import "OwnerServerDetailsViewController.h"
#import "OwnerProfileStep1ViewController.h"
#import "OwnerDashboardServerSearch.h"
#import "MenuVC.h"
#import "PrivcayPoliciesViewController.h"

@interface OwnerDashboard : UIViewController
{
    NSString *days;
    NSMutableDictionary *dictComments;
    NSMutableDictionary *Employees;
    NSMutableDictionary *dictRatings;
    NSMutableDictionary *dictRestaurant;
    NSMutableDictionary *TopPerformer;
    NSMutableDictionary *dictApplications;
    NSMutableDictionary *dictJobPosted;
    
    
}

@property (weak, nonatomic) IBOutlet UILabel *lblCityName;
@property (weak, nonatomic) IBOutlet UISegmentedControl *SegmentedControl;
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
@property (weak, nonatomic) IBOutlet UIImageView *imgOwner;
@property (weak, nonatomic) IBOutlet UIImageView *imgRestaurant;
@property (weak, nonatomic) IBOutlet UILabel *lblOwnerName;
@property (weak, nonatomic) IBOutlet UILabel *lblRestaurantName;
@property (weak, nonatomic) IBOutlet UITableView *tblOwner;
@property (weak, nonatomic) IBOutlet UIView *outerView;
@property (nonatomic) CGFloat lastContentOffset;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnMenu;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnLogout;


- (IBAction)btnSearch:(id)sender;
- (IBAction)segmentedControlPressed:(id)sender;

- (IBAction)btnLoginPressed:(id)sender;

@end
