//
//  OwnerDashboard.m
//  Restaurant
//
//  Created by Parth Pandya on 13/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "OwnerDashboard.h"
#import "OwnerJobPostedCell.h"
#import "OwnerServerInfoCell.h"
#import "OwnerDashboardServerSearch.h"
#import "OwnerRestaurantInfoCell.h"
#import "WorkingServerCell.h"


@interface OwnerDashboard ()

@end

@implementation OwnerDashboard

- (void)viewDidLoad {
    [super viewDidLoad];
    
    days = @"7";
    
    [self FetchDashboardDetails];
    [self FetchWorkingEmployees];
    
    _imgOwner.layer.cornerRadius = _imgOwner.frame.size.height/2;
    _imgOwner.layer.masksToBounds = YES;
   
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    
    UIVisualEffectView *visualEffectView;
    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    
    visualEffectView.frame = self.imgRestaurant.bounds;
    [self.imgRestaurant addSubview:visualEffectView];
    
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault]; //UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.shadowImage = [UIImage new];////
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    
    
}
-(void)viewWillAppear:(BOOL)animated {
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToJobsPostedVC) name:@"goToJobsPostedVC" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToPostaJobVC) name:@"goToPostJobVC" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToServerDetailVC) name:@"goToServerDetailVC" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToServerDetailVC1) name:@"goToServerDetail1VC" object:nil];
    
    
    self.title = @"DASHBOARD";
    
    [self FetchDashboardDetails];
    [self FetchWorkingEmployees];
    [self setMenuButton];
    [self setLogoutButton];
    
    
    // Go to profile View from Menu
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"goToProfileView"]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"goToProfileView"];
        NSLog(@"Go To Profile View");
        OwnerProfileStep1ViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerProfileStep1ViewController"];
        
        [self.navigationController pushViewController:profileVC animated:NO];
    }
    
    
    // Go to Privacy view
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"goToPrivacyView"]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"goToPrivacyView"];
        
        NSLog(@"Go To Policies View");
        PrivcayPoliciesViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivcayPoliciesViewController"];
        
        [self.navigationController pushViewController:profileVC animated:NO];
    }
    
}
-(void)viewDidDisappear:(BOOL)animated{
    self.title = @"";
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(void)setMenuButton {
    
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    [menuButton setImage:[UIImage imageNamed:@"menu_button.png"] forState:UIControlStateNormal];
    menuButton.imageEdgeInsets = UIEdgeInsetsMake(3, 4, 3, 4);
    [menuButton addTarget:self action:@selector(btnMenuPressed) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.btnLogout = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
}

-(void)setLogoutButton {
    
    
    NSString *flag = [[NSUserDefaults standardUserDefaults] valueForKey:@"isuserloggedin"];
    NSLog(@"flag:%@",flag);
    if([flag isEqualToString:@"yes"] ){
        
        //        NSString *title = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
        //        NSLog(@"Title:%@",title);
        [self.btnLogout setTitle:@"LOGOUT"];
        
    }
    else
    {
        [self.btnLogout setTitle:@"LOGIN"];
        
    }

    
    
}

-(void)btnMenuPressed {
    MenuVC *menuVC = [[self storyboard] instantiateViewControllerWithIdentifier:@"MenuVC"];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"toggleMenu"]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"toggleMenu"];
        
        UINavigationController *navSecondView = [[UINavigationController alloc] initWithRootViewController:menuVC];
        [self presentViewController:navSecondView animated:YES completion:nil];
        
    }
}

-(void)btnLogoutPressed {
    
    GIDSignIn*sigNIn=[GIDSignIn sharedInstance];
    [sigNIn signOut];
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    
    [realm deleteObjects:[AppUser allObjectsInRealm:realm]];
    
    [realm commitWriteTransaction];
    
    FBSDKLoginManager *FBlogin = [[FBSDKLoginManager alloc] init];
    [FBlogin logOut];
    [FBSDKAccessToken setCurrentAccessToken:nil];
    [FBSDKProfile setCurrentProfile:nil];
    [[NSUserDefaults standardUserDefaults] setValue:@"no" forKey:@"isuserloggedin"];
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"rememberMe"];
    LoginView *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginNav"];
    [self.navigationController presentViewController:login animated:NO completion:nil];
    
}

-(void)FetchDashboardDetails{
//    dispatch_async(dispatch_get_main_queue(), ^{
//    [APP huddie];
//    });
    
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc]
                                             initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activityView.center=self.view.center;
    [activityView startAnimating];
    [activityView hidesWhenStopped];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    [self.view addSubview:activityView];

    
    AppUser *user = [[AppUser allObjects] firstObject];
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"bearer %@",user.token] forHTTPHeaderField:@"Authorization"];
    
    
    
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?days=%@",SITE_URL,GET_OWNER_DASHBOARD,days]];
    //NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    //    [request setValue:[NSString stringWithFormat:@"bearer %@",user.token] forHTTPHeaderField:@"authorization"];
    
    
    [manager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        if ([responseObject isKindOfClass:[NSArray class]])
        {
            NSArray *jsonArray = (NSArray *)responseObject  ;
            NSLog(@"Json:%@",jsonArray);
            [activityView stopAnimating];
            dispatch_async(dispatch_get_main_queue(), ^{
               
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            });
        }
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dataDic = (NSDictionary *)responseObject;
            NSLog(@"%@",dataDic);
            
            NSDictionary *data = [dataDic valueForKey:@"data"];
            dictApplications = [data valueForKeyPath:@"jobApplication"];
            dictRestaurant = [data valueForKey:@"restaurant"];
            dictComments = [data valueForKey:@"commentInfo"];
            dictRatings = [data valueForKey:@"ratingInfo"];
            TopPerformer = [data valueForKey:@"topPerformer"];
            _lblOwnerName.text = [NSString upperCase:[data valueForKey:@"name"]];
            _lblRestaurantName.text = [NSString upperCase:[dictRestaurant valueForKey:@"name"]];
            _lblCityName.text = [data valueForKey:@"city"];
           
            [_imgOwner setShowActivityIndicatorView:YES];
            [_imgOwner sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,[data valueForKey:@"image"]]]
                               placeholderImage:[UIImage imageNamed:@"ic_user_b.png"]];
            NSArray *arrImages = [dictRestaurant valueForKey:@"image"];
            if (arrImages.count>0)
            {
                [_imgRestaurant setShowActivityIndicatorView:YES];
                [_imgRestaurant sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,[arrImages objectAtIndex:0]]]
                                  placeholderImage:[UIImage imageNamed:@"ic_user_b.png"]];
            }
            else
            {
                _imgRestaurant.image = [UIImage imageNamed:@"Restaurant.png"];
            }
            
            
            dictJobPosted = [data valueForKey:@"jobPosted"];
            [[NSUserDefaults standardUserDefaults] setValue:_lblRestaurantName.text forKey:@"RestoName"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"jobPosted"
                                                                object:self
                                                              userInfo:dictJobPosted];
            
            [_tblOwner reloadData];
          

            [activityView stopAnimating];
            dispatch_async(dispatch_get_main_queue(), ^{
               
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            });
        }
        
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        
        NSLog(@"Error: %@", error);
        [activityView stopAnimating];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        });
    }];
    
}

-(void)FetchWorkingEmployees{
    
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc]
                                             initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activityView.center=self.view.center;
    [activityView startAnimating];
    [activityView hidesWhenStopped];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    [self.view addSubview:activityView];

    
    AppUser *user = [[AppUser allObjects] firstObject];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"bearer %@",user.token] forHTTPHeaderField:@"Authorization"];
    
   
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,GET_MY_EMPLOYEES]];
  
    
    [manager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        if ([responseObject isKindOfClass:[NSArray class]])
        {
            NSArray *jsonArray = (NSArray *)responseObject  ;
            NSLog(@"Json:%@",jsonArray);
            [activityView stopAnimating];
            dispatch_async(dispatch_get_main_queue(), ^{
               
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            });
        }
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dataDic = (NSDictionary *)responseObject;
            NSLog(@"%@",dataDic);
            
            Employees = [dataDic valueForKey:@"data"];
            
        }
        
        
        [activityView stopAnimating];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        });
        
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [activityView stopAnimating];
        dispatch_async(dispatch_get_main_queue(), ^{
           
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        });
    }];
    
}


#pragma mark - Tableview Delegate Methods

-(CGFloat)heightForBasicCellAtIndexPath:(NSIndexPath *)indexPath {
    static OwnerServerInfoCell *sizingCell = nil;
    //create just once per programm launching
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tblOwner dequeueReusableCellWithIdentifier:@"ServerInfoCell"];
    });
    [self configureBasicCell:sizingCell atIndexPath:indexPath];
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}
-(CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}
-(void)configureBasicCell:(OwnerServerInfoCell *)Cell atIndexPath:(NSIndexPath *)indexPath {
    //make some configuration for your cell
    
    //OwnerServerInfoCell *Cell = [[OwnerServerInfoCell alloc] init];
    // Cell = [self.tblOwner dequeueReusableCellWithIdentifier:@"ServerInfoCell" forIndexPath:indexPath];
    //return Cell;
    //    ServerRating *aRating = ratingData[indexPath.row];
    //
    //    Cell.lblRatingDate.text = [NSString getFormattedDate:aRating.date];
    //
    //
    //    if (aRating.customerName.length == 0) {
    //        Cell.lblRatebBy.text = @"By:";
    //    }
    //    else {
    //        Cell.lblRatebBy.text = [NSString stringWithFormat:@"By: %@",[NSString upperCase:aRating.customerName]];
    //
    //    }
    //
    //    Cell.lblComment.text = aRating.comment;
    //
    //    self.ratingStars = @[Cell.imgStar1,Cell.imgStar2,Cell.imgStar3,Cell.imgStar4,Cell.imgStar5];
    //    self.rating = aRating.rating;
    //
    //    [self.ratingStars enumerateObjectsUsingBlock:^(UIImageView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
    //        [obj setImage:(idx < (NSInteger)self.rating ? [UIImage imageNamed:@"star_filled.png"] : [UIImage imageNamed:@"star_blank.png"])];
    //    }];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0)
    {
        return 4;
    }
    else if (section == 1)
    {
        
        return 1;
    }
    else
    {
        
        return 1;
    }
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (indexPath.section == 0)
    {
        if (indexPath.row == 2)
        {
            OwnerJobPostedCell *Cell = [[OwnerJobPostedCell alloc] init];
            Cell = [tableView dequeueReusableCellWithIdentifier:@"JobPostedCell" forIndexPath:indexPath];
            return Cell;
        }
        else
        {
            
            OwnerRestaurantInfoCell *Cell = [[OwnerRestaurantInfoCell alloc] init];
            Cell = [tableView dequeueReusableCellWithIdentifier:@"infoCell" forIndexPath:indexPath];
            
            if (indexPath.row == 0) {
                Cell.lblHeading.text = @"Server Ratings";
                Cell.lblDetail.text = [dictRatings valueForKey:@"lastRatingDate"];
                if ([dictRatings valueForKey:@"ratingCount"]) {
                    
                Cell.lblCount.text = [NSString stringWithFormat:@"%@",[dictRatings valueForKey:@"ratingCount"]];
                }
                
                
            }
            if (indexPath.row == 1) {
                Cell.lblHeading.text = @"Server Comments";
                Cell.lblDetail.text = [dictComments valueForKey:@"lastCommentDate"];
                
                if ([dictComments valueForKey:@"commentCount"]) {
                    Cell.lblCount.text = [NSString stringWithFormat:@"%@",[dictComments valueForKey:@"commentCount"]];
                    Cell.imgNotification.hidden = YES;
                    
                }
                
            }
            if (indexPath.row == 3) {
                Cell.lblHeading.text = @"Job Applications";
                Cell.lblDetail.text = [dictApplications valueForKey:@"lastApplicationDate"];
                if ([dictApplications valueForKey:@"applicationCount"]) {
                    Cell.lblCount.text = [NSString stringWithFormat:@"%@",[dictApplications valueForKey:@"applicationCount"]];
                    
                }
                
                
            }
            
            return Cell;
            
        }
    }
    else if (indexPath.section == 1)
    {
        OwnerServerInfoCell *Cell = [[OwnerServerInfoCell alloc] init];
        Cell = [tableView dequeueReusableCellWithIdentifier:@"ServerInfoCell" forIndexPath:indexPath];
        return Cell;
    }
    else
    {
        WorkingServerCell *Cell = [[WorkingServerCell alloc] init];
        Cell = [tableView dequeueReusableCellWithIdentifier:@"WorkingServerCell" forIndexPath:indexPath];
        return Cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"Section:%ld Row:%ld",(long)indexPath.section,(long)indexPath.row);
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            // Server Ratings
            NSLog(@"Server Rating cell Selected");
            
            OwnerRestoServerRatingsViewController *serverRatingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerRestoServerRatingsViewController"];
            
            [self.navigationController pushViewController:serverRatingVC animated:NO];
            
        }
        if (indexPath.row == 1) {
            // Server Comments
            NSLog(@"Server Comments cell Selected");
            OwnerRestoServerCommentsViewController *serverCommentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerRestoServerCommentsViewController"];
            [self.navigationController pushViewController:serverCommentVC animated:NO];
            
        }
        if (indexPath.row == 2) {
            // Jobs Posted
            NSLog(@"Server Jobs Posted cell Selected");
        }
        if (indexPath.row == 3) {
            // Job Applications
            NSLog(@"Job Applications cell Selected");
            OwnerJobApplicationViewController *jobApplicationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerJobApplicationViewController"];
            jobApplicationVC.jobID = -1;
            [self.navigationController pushViewController:jobApplicationVC animated:NO];
            
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // if ([self heightForBasicCellAtIndexPath:indexPath] < 78)
    if (indexPath.section == 0 && indexPath.row == 2)
    {
        return 218;
    }
    else if (indexPath.section == 0)
    {
        return 68;
    }
    else if (indexPath.section == 1)
    {
        return 193;
    }
    else
    {
        return 200;//[self heightForBasicCellAtIndexPath:indexPath];
    }
    // return [self heightForBasicCellAtIndexPath:indexPath];
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 1)
    {
        return @"Top 3 Performer";
    }
    else if (section == 2)
    {
        return @"All Servers/Waiters";
        
    }
    return @"";
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    //Get the page
    if (self.lastContentOffset < scrollView.contentOffset.y)
    {
        NSLog(@"Scrolling Down");
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TopPerformer"
                                                            object:self
                                                          userInfo:TopPerformer];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Employees"
                                                            object:self
                                                          userInfo:Employees];
    }
    
    self.lastContentOffset = scrollView.contentOffset.y;
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        //Get the page
        if (self.lastContentOffset < scrollView.contentOffset.y)
        {
            NSLog(@"Scrolling Down");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TopPerformer"                                                    object:self userInfo:TopPerformer];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Employees" object:self userInfo:Employees];
        }
        
        self.lastContentOffset = scrollView.contentOffset.y;
        
        
    }
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

- (void)goToJobsPostedVC {
    NSLog(@"Show Jobs Posted VC");
    
    OwnerJobsPostedViewController  *jobsPostedVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerJobsPostedViewController"];
    
    [self.navigationController pushViewController:jobsPostedVC animated:NO];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"goToJobsPostedVC"
                                                  object:nil];

    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"JobId"];
    
}
- (void)goToPostaJobVC {
    NSLog(@"Show Post a Job VC");
    
    OwnerPostJobViewController *postJobVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerPostJobViewController"];
    
    [self.navigationController pushViewController:postJobVC animated:NO];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"goToPostJobVC"
                                                  object:nil];

}
- (void)goToServerDetailVC {
    NSLog(@"Goto server details VC");
    
    OwnerServerDetailsViewController *serverDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerServerDetailsViewController"];
    
    [self.navigationController pushViewController:serverDetailVC animated:NO];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"goToServerDetailVC"
                                                  object:nil];
    
    
}
- (void)goToServerDetailVC1 {
    NSLog(@"Goto server details VC");
    
    OwnerServerDetailsViewController *serverDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerServerDetailsViewController"];
    
    [self.navigationController pushViewController:serverDetailVC animated:NO];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"goToServerDetail1VC"
                                                  object:nil];
    
    
}


- (IBAction)btnSearch:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setValue:_txtSearch.text forKey:@"serverName"];
    OwnerDashboardServerSearch *Obj = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerDashboardServerSearch"];
    [self.navigationController pushViewController:Obj animated:NO];
    
}
- (IBAction)segmentedControlPressed:(UISegmentedControl *)sender {
    
    NSInteger selectedSegment = sender.selectedSegmentIndex;
    
    if (selectedSegment == 0)
    {
        days = @"7";
    }
    else if (selectedSegment == 1)
    {
        days = @"30";
    }
    else
    {
        days = @"365";
    }
    [self FetchDashboardDetails];
}
- (IBAction)btnLoginPressed:(id)sender {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [APP huddie];
        
    });
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,SIGN_OUT_URL]];
    
    AppUser *user = [[AppUser allObjects] firstObject];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"bearer %@",user.token] forHTTPHeaderField:@"Authorization"];
    
    
    [manager POST:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        if ([responseObject isKindOfClass:[NSArray class]])
        {
            NSArray *jsonArray = (NSArray *)responseObject  ;
            NSLog(@"Json:%@",jsonArray);
        }
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dataDic = (NSDictionary *)responseObject;
            //                NSDictionary *Dict = [dataDic valueForKey:@"data"];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertController * alert2=   [UIAlertController
                                               alertControllerWithTitle:[dataDic valueForKey:@"result"]
                                               message:@""
                                               preferredStyle:UIAlertControllerStyleAlert];
                [APP.hud setHidden:YES];
                [self presentViewController:alert2 animated:NO completion:nil];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    
                    [alert2 dismissViewControllerAnimated:NO completion:nil];
                    
                    
                    GIDSignIn*sigNIn=[GIDSignIn sharedInstance];
                    [sigNIn signOut];
                    
                    RLMRealm *realm = [RLMRealm defaultRealm];
                    [realm beginWriteTransaction];
                    
                    [realm deleteObjects:[AppUser allObjectsInRealm:realm]];
                    
                    [realm commitWriteTransaction];
                    
                    FBSDKLoginManager *FBlogin = [[FBSDKLoginManager alloc] init];
                    [FBlogin logOut];
                    [FBSDKAccessToken setCurrentAccessToken:nil];
                    [FBSDKProfile setCurrentProfile:nil];
                    [[NSUserDefaults standardUserDefaults] setValue:@"no" forKey:@"isuserloggedin"];
                    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"rememberMe"];
                    
                    LoginView *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginNav"];
                    [self.navigationController presentViewController:login animated:NO completion:nil];

                });
            });
            
            
            
            return;
            
            
        }
        
        
        
    }
          failure:^(NSURLSessionTask *operation, NSError *error) {
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  [APP.hud setHidden:YES];
              });
              
              NSLog(@"Error: %@", error);
              
          }];

    
}

- (void)clearPersistedData {
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm deleteObjects:[Servers allObjectsInRealm:realm]];
    [realm deleteObjects:[AppUser allObjectsInRealm:realm]];
    [realm deleteObjects:[Restaurants allObjectsInRealm:realm]];
    [realm deleteObjects:[RestaurantImages allObjectsInRealm:realm]];
    [realm deleteObjects:[OwnerProfile allObjectsInRealm:realm]];
    
    [realm commitWriteTransaction];
    
    
}

@end
