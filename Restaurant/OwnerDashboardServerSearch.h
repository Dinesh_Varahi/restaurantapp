//
//  OwnerDashboardServerSearch.h
//  Restaurant
//
//  Created by Parth Pandya on 23/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerInfoCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "OwnerServerDetailsViewController.h"
@interface OwnerDashboardServerSearch : UIViewController
{
    NSString *strName;
    
    NSMutableArray *arrName;
    NSMutableArray *arrImage;
    NSMutableArray *arrRatings;
    NSMutableArray *arrIds;
    NSMutableArray *arrRestroName;
    NSMutableArray *arrExperiance;
    NSMutableArray *arrTotalRatings;
    NSDictionary *Experiance;
    int pageIndex;
}
@property (weak, nonatomic) IBOutlet UITextField *txtServerSearch;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic) CGFloat lastContentOffset;
@end
