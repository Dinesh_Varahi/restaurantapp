//
//  OwnerDashboardServerSearch.m
//  Restaurant
//
//  Created by Parth Pandya on 23/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "OwnerDashboardServerSearch.h"
#import "ServerSearchHeaderView.h"
#import "AppDelegate.h"

@interface OwnerDashboardServerSearch ()

@end

@implementation OwnerDashboardServerSearch

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    strName = [[NSUserDefaults standardUserDefaults] valueForKey:@"serverName"];
    _txtServerSearch.text = strName;
    self.title = @"Server List";
    arrImage = [[NSMutableArray alloc] init];
    arrRatings = [[NSMutableArray alloc] init];
    arrName = [[NSMutableArray alloc] init];
    arrIds = [[NSMutableArray alloc] init];
    arrExperiance = [[NSMutableArray alloc] init];
    arrRestroName = [[NSMutableArray alloc] init];
    arrTotalRatings = [[NSMutableArray alloc] init];
    [self FetchWorkingEmployees];
    // Do any additional setup after loading the view.
    
//    self.navigationController.navigationBar.topItem.title = nil;
//    self.navigationController.navigationItem.leftBarButtonItem.title = nil;
    [_txtServerSearch addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textDidChange:(id)sender{
//    [[NSUserDefaults standardUserDefaults] setValue:_txtServerSearch.text forKey:@"serverName"];
    strName = _txtServerSearch.text;
    [self FetchWorkingEmployees];
}

-(void)FetchWorkingEmployees
{
    AppUser *user = [[AppUser allObjects] firstObject];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"bearer %@",user.token] forHTTPHeaderField:@"Authorization"];
    
    NSURL *URL;
    if (strName.length > 0)
    {
        NSString *urlString =[[NSString stringWithFormat:@"%@%@?name=%@&page=%d",SITE_URL,GET_MY_EMPLOYEES,strName,pageIndex] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        URL = [NSURL URLWithString:urlString];
    }
    else
    {
        URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?name=&page=%d",SITE_URL,GET_MY_EMPLOYEES,pageIndex]];
    }
    
    
    [manager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        
        
        if ([responseObject isKindOfClass:[NSArray class]])
        {
            NSArray *jsonArray = (NSArray *)responseObject  ;
            NSLog(@"Json:%@",jsonArray);
        }
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dataDic = (NSDictionary *)responseObject;
            NSLog(@"%@",dataDic);
            
            NSDictionary *data = [dataDic valueForKey:@"data"];
            arrExperiance = [data valueForKey:@"experience"];
            
            arrName = [data valueForKey:@"name"];
//            for (int i=0; i < [Experiance count]; i++)
//            {
//                arrRestroName = [Experiance valueForKey:@"restaurantName"];
//            }
            
            arrRatings = [data valueForKey:@"avgRating"];
            arrImage = [data valueForKey:@"image"];
            arrIds = [data valueForKey:@"_idUserProfile"];
            arrTotalRatings = [data valueForKey:@"totalRatings"];
            [self.collectionView reloadData];
        }
        
        [APP.hud setHidden:YES];
        
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        
        
        [APP.hud setHidden:YES];
        
        
        NSLog(@"Error: %@", error);
    }];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return arrName.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ServerInfoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ServerinfoCell" forIndexPath:indexPath];
    
    Experiance = [arrExperiance objectAtIndex:indexPath.row];
    cell.lblServerName.text = [NSString upperCase:[arrName objectAtIndex:indexPath.row]];
    cell.Ratings.value = [[arrRatings objectAtIndex:indexPath.row] integerValue];
    cell.lblRestoName.text = [NSString upperCase:[[NSUserDefaults standardUserDefaults] valueForKey:@"RestoName"]];
    cell.lblTotalRatings.text = [NSString stringWithFormat:@"%@ Ratings",[arrTotalRatings objectAtIndex:indexPath.row]];
    cell.imgServer.layer.cornerRadius = cell.imgServer.frame.size.height/2;
    cell.imgServer.layer.masksToBounds = YES;
    
    cell.imgServer1.layer.cornerRadius = cell.imgServer1.frame.size.height/2;
    cell.imgServer1.layer.masksToBounds = YES;

    [cell.imgServer sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,[arrImage objectAtIndex:indexPath.row]]]
                      placeholderImage:[UIImage imageNamed:@"ic_user_b.png"]];
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    OwnerServerDetailsViewController *serverDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerServerDetailsViewController"];
    
     [[NSUserDefaults standardUserDefaults] setValue:[arrIds objectAtIndex:indexPath.row] forKey:@"ServerID"];
    
    [self.navigationController pushViewController:serverDetailVC animated:NO];
    
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    //Get the page
    if (self.lastContentOffset < scrollView.contentOffset.y)
    {
        NSLog(@"Scrolling Down");
        //pageIndex = scrollView.contentOffset.x / scrollView.bounds.size.width;
        
        pageIndex ++;
        [self FetchWorkingEmployees];
        
    }
    
    self.lastContentOffset = scrollView.contentOffset.y;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        ServerSearchHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        NSString *title = [[NSString alloc]initWithFormat:@"%ld Servers/Bartenders working",[arrName count]];
        headerView.lblCountOfServers.text = title;
        
        reusableview = headerView;
    }
    return reusableview;
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        //Get the page
        if (self.lastContentOffset < scrollView.contentOffset.y)
        {
            NSLog(@"Scrolling Down");
            //pageIndex = scrollView.contentOffset.x / scrollView.bounds.size.width;
            
            pageIndex ++;
            [self FetchWorkingEmployees];
            
        }
        
        self.lastContentOffset = scrollView.contentOffset.y;
        
        
    }
}

@end
