//
//  OwnerJobApplicationViewController.h
//  Restaurant
//
//  Created by HN on 12/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OwnerJobAppliedServerInfoCell.h"
#import "JobPosted.h"
#import "AppDelegate.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface OwnerJobApplicationViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UILabel *lblWorkProfile;
    __weak IBOutlet UILabel *lblJobID;
    __weak IBOutlet UILabel *lblJobPostedDate;
    NSDictionary *Dict;
    NSMutableArray *jobsArray;
    NSMutableArray *ExpArray;
    NSString *strProfile;
    NSString *strJobId;
    
    NSString *strStatus;
    NSInteger index;
    
   

    __weak IBOutlet UISwitch *swJobStatus;
    __weak IBOutlet UITableView *tblJobApplicationList;
    
    
}
- (IBAction)filterButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeight;
@property (weak, nonatomic) IBOutlet UIView *profileBackgroundView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnFilterPressed;
@property (nonatomic) CGFloat lastContentOffset;

@property (nonatomic) NSInteger jobID;


@end
