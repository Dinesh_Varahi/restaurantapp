//
//  OwnerJobApplicationViewController.m
//  Restaurant
//
//  Created by HN on 12/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "OwnerJobApplicationViewController.h"
#import "OwnerServerDetailsViewController.h"

@interface OwnerJobApplicationViewController ()
{
    int pageIndex;
    NSString *jobStatusFlag;
    NSMutableArray *arrJobs;
    NSMutableArray *arrExp;
    RLMResults<JobPosted *> *jobPostedData;
    RLMResults<ServerWorkExperience *> *serverExpData;
}
@end

@implementation OwnerJobApplicationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ((_jobID = -1)) {
//        _profileBackgroundView.hidden = YES;
        _viewHeight.constant = 0;
        lblWorkProfile.hidden = YES;
        lblJobPostedDate.hidden = YES;
        lblJobID.hidden = YES;
        swJobStatus.hidden = YES;
    }
    
//    self.navigationItem.rightBarButtonItems = nil;
    tblJobApplicationList.delegate = self;
    tblJobApplicationList.dataSource = self;
    
//    self.navigationController.navigationBar.topItem.title = nil;
//    self.navigationController.navigationItem.leftBarButtonItem.title = nil;
    
    arrJobs = [[NSMutableArray alloc] init];
    arrExp = [[NSMutableArray alloc] init];
    jobsArray = [[NSArray alloc] init];
    ExpArray = [[NSArray alloc] init];
    jobStatusFlag = @"OPEN";
    pageIndex = 0;
    [self GetPostedJobs];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setJobInfo:) name:@"JobDataDict" object:nil];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.title = @"Job Application";
}
-(void)setJobInfo:(NSNotification *)notification{
    
    if  ([notification.name isEqualToString:@"JobDataDict"])
    {
        
        Dict = notification.object;
        _profileBackgroundView.hidden = NO;
        NSString *tmpString = [[Dict valueForKey:@"Profile"] capitalizedString];
        lblWorkProfile.text = [NSString stringWithFormat:@"Profile: %@",tmpString];
        lblJobID.text = [NSString stringWithFormat:@"JOB ID: %@", [Dict valueForKey:@"JobID"]];
        lblJobPostedDate.text = [NSString getFormattedDate:[Dict valueForKey:@"Date"]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            _viewHeight.constant = 78;
            lblWorkProfile.hidden = NO;
            lblJobPostedDate.hidden = NO;
            lblJobID.hidden = NO;
            swJobStatus.hidden = NO;
            
        });


        NSLog(@"Data%@",Dict);
        
    }
}
#pragma mark - Tableview Delegate Methods

-(CGFloat)heightForBasicCellAtIndexPath:(NSIndexPath *)indexPath {
    static OwnerJobAppliedServerInfoCell *sizingCell = nil;
    //create just once per programm launching
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [tblJobApplicationList dequeueReusableCellWithIdentifier:@"cell"];
    });
    [self configureBasicCell:sizingCell atIndexPath:indexPath];
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}
//this method will calculate required height of cell
- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

- (void)configureBasicCell:(OwnerJobAppliedServerInfoCell *)Cell atIndexPath:(NSIndexPath *)indexPath {
    //make some configuration for your cell
    JobPosted  *aPostedJob = jobPostedData[indexPath.row];
    
    
    Cell.lblServerName.text = aPostedJob.serverName;
    Cell.serverAverageRating.value = aPostedJob.avgRating;
    Cell.lblTotalRatingCount.text = [NSString stringWithFormat:@"%ld Ratings",aPostedJob.totalRating];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //ratingData = [ServerRating allObjects];
    return arrJobs.count;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([self heightForBasicCellAtIndexPath:indexPath] < 78)
    {
        return 78;
    }
    return [self heightForBasicCellAtIndexPath:indexPath];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    OwnerJobAppliedServerInfoCell *Cell = [[OwnerJobAppliedServerInfoCell alloc] init];
    
    Cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    

//    JobPosted *aJob = [arrJobs objectAtIndex:indexPath.row];
    
    JobPosted *aJob;
    
    if (arrJobs.count > 0)
    {
        aJob = [arrJobs objectAtIndex:indexPath.row];
    }
    
    ServerWorkExperience *aExp;
    
    if (arrExp.count > 0)
    {
        aExp = [arrExp objectAtIndex:indexPath.row];
    }
    
    
    Cell.lblServerName.text = [aJob.serverName capitalizedString];
    Cell.btnFit.tag = indexPath.row;
    Cell.btnUnFit.tag = indexPath.row;
    if ([aJob.applicationStatus isEqualToString:@"0"])
    {
        Cell.btnFit.hidden = NO;
        Cell.btnUnFit.hidden = NO;
        Cell.lblApplicationStatus.hidden = YES;
    }
    else
    {
        Cell.btnFit.hidden = YES;
        Cell.btnUnFit.hidden = YES;
        Cell.lblApplicationStatus.hidden = NO;
        if ([aJob.applicationStatus isEqualToString:@"FIT"])
        {
            Cell.lblApplicationStatus.textColor = [UIColor greenColor];
        }
        else
        {
            Cell.lblApplicationStatus.textColor = [UIColor redColor];

        }
         Cell.lblApplicationStatus.text = [NSString stringWithFormat:@"Already marked as %@",aJob.applicationStatus];
        
    }
    [Cell.btnFit addTarget:self action:@selector(btnFit:) forControlEvents:UIControlEventTouchUpInside];
    [Cell.btnUnFit addTarget:self action:@selector(btnUnFit:) forControlEvents:UIControlEventTouchUpInside];
    
    [Cell.imgServerProfilePic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,aJob.imgURL]] placeholderImage:[UIImage imageNamed:@"ic_user_b.png"]];
    Cell.serverAverageRating.value = aJob.avgRating;
    Cell.lblTotalRatingCount.text = [NSString stringWithFormat:@"JOB ID: %ld",aJob.jobID];
    Cell.lblProfile.text = [NSString stringWithFormat:@"Profile: %@",[aJob.workProfile capitalizedString]];
    Cell.lblWorkingRestoName.text = aExp.restoName;
    
    return Cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    JobPosted *aJob;
    
    if (arrJobs.count > 0)
    {
        aJob = [arrJobs objectAtIndex:indexPath.row];
    }
    
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%ld",(long)aJob.serverID] forKey:@"ServerID"];
    OwnerServerDetailsViewController *serverDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerServerDetailsViewController"];
    
    [self.navigationController pushViewController:serverDetailVC animated:NO];
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        //        return @"22 Servers Working";
        return [NSString stringWithFormat:@"%lu Applications",(unsigned long)arrJobs.count];
    }
    
    return @"";
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    //Get the page
    if (self.lastContentOffset < scrollView.contentOffset.y)
    {
        NSLog(@"Scrolling Down");
        //pageIndex = scrollView.contentOffset.x / scrollView.bounds.size.width;
        
        pageIndex ++;
        [self GetPostedJobs];
        
    }
    
    
    self.lastContentOffset = scrollView.contentOffset.y;
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        //Get the page
        if (self.lastContentOffset < scrollView.contentOffset.y)
        {
            NSLog(@"Scrolling Down");
            //pageIndex = scrollView.contentOffset.x / scrollView.bounds.size.width;
            
            pageIndex ++;
            [self GetPostedJobs];
            
        }
        
        self.lastContentOffset = scrollView.contentOffset.y;
        
        
    }
}

-(void)ChangeApplicationStatus
{
    JobPosted *aJob;
    
    if (arrJobs.count > 0)
    {
        aJob = [arrJobs objectAtIndex:index];
    }
    
    [APP huddie];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@",SITE_URL,CHANGE_APPLICATION_STATUS] parameters:nil error:nil];
    
    AppUser *user = [[AppUser allObjects] firstObject];
    
    
    NSString *token = user.token;
    
    NSLog(@"Token:%@",token);
    
    [req addValue:[NSString stringWithFormat:@"bearer %@",token] forHTTPHeaderField:@"Authorization"];
    [req addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSMutableDictionary *dataDict = [NSMutableDictionary new];
    
    [dataDict setValue:[NSString stringWithFormat:@"%ld", (long)aJob.serverID] forKey:@"serverId"];
    
    [dataDict setValue:[NSString stringWithFormat:@"%ld", (long)aJob.jobID] forKey:@"openingId"];
    [dataDict setValue:strStatus forKey:@"status"];
    
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict options:0 error:nil];
    NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSLog(@"Post Job Data :%@",jsonString);
    
    
    [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse *response, id data, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        //            double status = (long)[httpResponse statusCode];
        NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
        
        
        if ([httpResponse statusCode] == 202) {
            if ([data isKindOfClass:[NSDictionary class]]){
                
                NSDictionary *dataDic = (NSDictionary *)data;
                NSLog(@"Data:%@",dataDic);
                
                NSInteger status = [[dataDic valueForKey:@"status"] integerValue];
                if(status == 202)
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        UIAlertController * alert2=   [UIAlertController
                                                       alertControllerWithTitle:@"Application Status changed successfully"
                                                       message:[dataDic valueForKey:@"result"]
                                                       preferredStyle:UIAlertControllerStyleAlert];
                        [APP.hud setHidden:YES];
                       
                        
                        [self presentViewController:alert2 animated:NO completion:nil];
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                            
                            [alert2 dismissViewControllerAnimated:NO completion:nil];
                            
                        });
                    });
                    
                }
                else if(status == 400)
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [APP.hud setHidden:YES];
                        UIAlertController * alert2=   [UIAlertController
                                                       alertControllerWithTitle:@"Sorry"
                                                       message:[dataDic valueForKey:@"result"]
                                                       preferredStyle:UIAlertControllerStyleAlert];
                        
                        [self presentViewController:alert2 animated:NO completion:nil];
                        
                        
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                            [alert2 dismissViewControllerAnimated:NO completion:nil];
                        });
                    });
                    
                }
                else if (status == 0)
                {
                    [APP.hud setHidden:YES];
                }
                else if (status == 401)
                {
                    [APP.hud setHidden:YES];
                }
            }
        }
        else if ([httpResponse statusCode] == 400) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [APP.hud setHidden:YES];
                UIAlertController * alert2=   [UIAlertController
                                               alertControllerWithTitle:@"Sorry"
                                               message:@"Error while connecting to server"
                                               preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert2 animated:NO completion:nil];
                
                
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [alert2 dismissViewControllerAnimated:NO completion:nil];
                });
            });
        }
        else if ([httpResponse statusCode] == 401) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [APP.hud setHidden:YES];
                UIAlertController * alert2=   [UIAlertController
                                               alertControllerWithTitle:@"Sorry"
                                               message:@"Error while connecting to server"
                                               preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert2 animated:NO completion:nil];
                
                
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [alert2 dismissViewControllerAnimated:NO completion:nil];
                });
            });
        }
        else if ([httpResponse statusCode] == 0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [APP.hud setHidden:YES];
                UIAlertController * alert2=   [UIAlertController
                                               alertControllerWithTitle:@"Sorry"
                                               message:@"Error while connecting to server"
                                               preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert2 animated:NO completion:nil];
                
                
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [alert2 dismissViewControllerAnimated:NO completion:nil];
                });
            });
        }
        
    }] resume];
    
    }

-(IBAction)btnFit:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    index = btn.tag;
    strStatus = @"FIT";
    [self ChangeApplicationStatus];

}

-(IBAction)btnUnFit:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    index = btn.tag;
    strStatus = @"UNFIT";
    
    [self ChangeApplicationStatus];

}
-(void)GetPostedJobs
{
    dispatch_async(dispatch_get_main_queue(), ^{
    
    [APP huddie];
    });
    NSURL *URL;
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"JobId"] length]> 0)
    {
        strJobId = [[NSUserDefaults standardUserDefaults] valueForKey:@"JobId"];
    }
    else
    {
        strJobId = @"";
    }
    if (strProfile.length > 0)
    {
        NSString *urlString =[[NSString stringWithFormat:@"%@%@?page=%d&profile=%@&jobId=%@",SITE_URL,GET_JOB_APPLIED,pageIndex,strProfile,strJobId] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

        
        URL = [NSURL URLWithString:urlString];
    }
    else
    {
        URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?page=%d&jobId=%@",SITE_URL,GET_JOB_APPLIED,pageIndex,strJobId]];
    }
    
    [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"JobId"];
//    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,GET_JOB_APPLIED]];

    
    NSLog(@"Job List URL:%@",URL);
    
    
    AppUser *user = [[AppUser allObjects] firstObject];
    AFHTTPSessionManager *ratingsManager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    //    ratingsManager.requestSerializer = [AFJSONRequestSerializer serializer];
    ratingsManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [ratingsManager.requestSerializer setValue:[NSString stringWithFormat:@"bearer %@",user.token] forHTTPHeaderField:@"Authorization"];
    
    
    [ratingsManager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        if ([responseObject isKindOfClass:[NSArray class]])
        {
            NSArray *jsonArray = (NSArray *)responseObject  ;
            NSLog(@"Json:%@",jsonArray);
        }
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dataDic = (NSDictionary *)responseObject;
            NSLog(@"Data:%@",dataDic);
            
//            NSDictionary *Dict = [dataDic valueForKey:@"data"];
//            NSLog(@"%lu",(unsigned long)Dict.count);
            
            jobsArray = [dataDic valueForKey:@"data"];
            
            if (jobsArray.count > 0) {
                
                for (int i=0; i<jobsArray.count; i++) {
                    NSDictionary *jobsDict = [jobsArray objectAtIndex:i];
                    JobPosted *aJobPosted = [[JobPosted alloc] init];
                    
                    aJobPosted.jobID = [[jobsDict valueForKey:@"_id"] integerValue];
                    aJobPosted.serverID = [[jobsDict valueForKey:@"_idServer"] integerValue];
                    aJobPosted.serverName = [jobsDict valueForKey:@"name"];
                    aJobPosted.jobStatus = [jobsDict valueForKey:@"status"];
                    aJobPosted.workProfile = [jobsDict valueForKey:@"profileType"];
                    aJobPosted.totalRating = [[jobsDict valueForKey:@"totalRatings"] integerValue];
                    aJobPosted.avgRating = [[jobsDict valueForKey:@"avgRating"] floatValue];
                    aJobPosted.imgURL = [jobsDict valueForKey:@"image"];
                    if ([[jobsArray objectAtIndex:i] valueForKey:@"applicationStatus"])
                    {
                        aJobPosted.applicationStatus = [[jobsArray objectAtIndex:i] valueForKey:@"applicationStatus"];
                    }
                    else
                    {
                        aJobPosted.applicationStatus = @"0";
                    }
                    
                    ExpArray = [jobsDict valueForKey:@"experience"];
                    for ( int j = 0;j < ExpArray.count; j++) {
                        NSDictionary *expDict = [ExpArray objectAtIndex:j];
                        ServerWorkExperience *aExp = [[ServerWorkExperience alloc] init];
                        
                        aExp.restoName = [expDict valueForKey:@"restaurantName"];
                        aExp.restoType = [expDict valueForKey:@"restaurantType"];
                        
                        aExp.city = [expDict valueForKey:@"city"];
                        
                        aExp.fromDate = [expDict valueForKey:@"fromDate"];
                        
                        [arrExp addObject:aExp];
                        
                    }
                    
                    [arrJobs addObject:aJobPosted];
                    
                }
                
            }
            
            
            
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [APP.hud setHidden:YES];

                });

                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [APP.hud setHidden:YES];
                [tblJobApplicationList reloadData];
            });
        }
        
    }
                failure:^(NSURLSessionTask *operation, NSError *error) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [APP.hud setHidden:YES];
                    });
                    
                    NSLog(@"Error: %@", error);
                    
                }];
    
}

- (void)closeJob:(id)sender
{
    if (![sender isOn]) {
        
        
        UISwitch *aSwitch = (UISwitch*)sender;
        NSLog(@"Tag:%ld",(long)aSwitch.tag);
        NSLog(@"Close Job");
        
        {
            NSMutableDictionary *dataDict = [NSMutableDictionary new];
            
            [dataDict setValue:[NSString stringWithFormat:@"%ld",(long)_jobID] forKey:@"jobId"];
            [dataDict setValue:@"CLOSED" forKey:@"jobStatus"];
            
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDict options:0 error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            
            NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@",SITE_URL,CHANGE_JOB_STATUS] parameters:nil error:nil];
            
            
            AppUser *user = [[AppUser allObjects] firstObject];
            
            NSString *token = user.token;
            
            NSLog(@"Token:%@",token);
            
            
            
            [req setValue:[NSString stringWithFormat:@"bearer %@",token] forHTTPHeaderField:@"Authorization"];
            [req addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [req addValue:@"application/json" forHTTPHeaderField:@"Accept"];
            
            [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
            
            [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse *response, id data, NSError *error) {
                
                //        NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                //double status = (long)[httpResponse statusCode];
                NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
                
                
                
                if ([data isKindOfClass:[NSDictionary class]])
                    
                {
                    
                    
                    NSDictionary *dataDic = (NSDictionary *)data;
                    NSLog(@"Data:%@",dataDic);
                    
                    NSInteger status = [[dataDic valueForKey:@"status"] integerValue];
                    if(status == 202)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            NSMutableArray *discardedJob = [NSMutableArray array];
                            JobPosted *aJob = [[JobPosted alloc] init];
                            
                            for (aJob in arrJobs) {
                                if (aJob.jobID == aSwitch.tag)
                                    [discardedJob addObject:aJob];
                            }
                            
                            [arrJobs removeObjectsInArray:discardedJob];
                            
                            [tblJobApplicationList reloadData];
                            
                            [APP.hud setHidden:YES];
                            
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"Job Closed"
                                                           message:@""
                                                           preferredStyle:UIAlertControllerStyleAlert];
                            
                            [self presentViewController:alert2 animated:NO completion:nil];
                            
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                
                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                                
                            });
                        });
                        
                        
                        
                    }
                    else if(status == 400)
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [APP.hud setHidden:YES];
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"Sorry"
                                                           message:[dataDic valueForKey:@"result"]
                                                           preferredStyle:UIAlertControllerStyleAlert];
                            
                            [self presentViewController:alert2 animated:NO completion:nil];
                            
                            
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                            });
                        });
                        
                    }
                    
                }
                
                
            }] resume];
            
        }
    }
}

- (IBAction)filterButtonPressed:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"FILTER BY" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [actionSheet dismissViewControllerAnimated:NO completion:nil];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Server" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
        if (arrJobs.count > 0) {
            [arrJobs removeAllObjects];
        }
        
        strProfile = @"server";
        pageIndex = 0;
        
        [self GetPostedJobs];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Bartender" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if (arrJobs.count > 0) {
            [arrJobs removeAllObjects];
        }
        
        strProfile = @"bartender";
        pageIndex = 0;
        [self GetPostedJobs];
        
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Busboy" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if (arrJobs.count > 0) {
            [arrJobs removeAllObjects];
        }
        
        strProfile = @"busboy";
        pageIndex = 0;
        [self GetPostedJobs];
        
    }]];
    
    
    [actionSheet addAction:[UIAlertAction  actionWithTitle:@"Clear Filter" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        if (arrJobs.count > 0) {
            [arrJobs removeAllObjects];
        }
        
        pageIndex = 0;
        strProfile = @"";
        [self GetPostedJobs];
        
        
    }]];
    
    [self presentViewController:actionSheet animated:NO completion:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.title = @"";
}
@end
