//
//  OwnerJobAppliedServerInfoCell.h
//  Restaurant
//
//  Created by Parth Pandya on 21/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"
#import "JobPosted.h"

@interface OwnerJobAppliedServerInfoCell : UITableViewCell
{
    NSString *strStatus;
    NSInteger index;
    
    RLMResults<JobPosted  *> *arrJobs;
}
@property (weak, nonatomic) IBOutlet UILabel *lblProfile;
@property (weak, nonatomic) IBOutlet UIImageView *imgServerProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *lblServerName;
@property (weak, nonatomic) IBOutlet UILabel *lblWorkingRestoName;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalRatingCount;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *serverAverageRating;
@property (weak, nonatomic) IBOutlet UIButton *btnFit;
- (IBAction)btnFitPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblApplicationStatus;

@property (weak, nonatomic) IBOutlet UIButton *btnUnFit;
- (IBAction)btnUnFitPressed:(id)sender;





@end
