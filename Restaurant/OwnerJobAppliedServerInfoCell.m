//
//  OwnerJobAppliedServerInfoCell.m
//  Restaurant
//
//  Created by Parth Pandya on 21/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "OwnerJobAppliedServerInfoCell.h"
#import "AppDelegate.h"

@implementation OwnerJobAppliedServerInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.imgServerProfilePic.layer.cornerRadius = self.imgServerProfilePic.frame.size.width / 2;
    
    self.imgServerProfilePic.layer.masksToBounds = YES;
    self.imgServerProfilePic.contentMode = UIViewContentModeScaleAspectFill;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (IBAction)btnFitPressed:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    index = btn.tag;
    strStatus = @"FIT";
    
}
- (IBAction)btnUnFitPressed:(id)sender {
    UIButton *btn = (UIButton *)sender;
    index = btn.tag;
    strStatus = @"UNFIT";
}
@end
