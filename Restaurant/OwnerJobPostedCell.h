//
//  OwnerJobPostedCell.h
//  Restaurant
//
//  Created by Parth Pandya on 13/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PNChartDelegate.h"
#import "PNChart.h"


@interface OwnerJobPostedCell : UITableViewCell<PNChartDelegate>

@property (weak, nonatomic) IBOutlet UIProgressView *ProgressBar;
@property (weak, nonatomic) IBOutlet UILabel *lblClosed;
@property (weak, nonatomic) IBOutlet UILabel *lblOpened;
@property (weak, nonatomic) IBOutlet UIButton *btnViewJobsPosted;
@property (weak, nonatomic) IBOutlet UIButton *btnPostANewJob;
@property (nonatomic) PNPieChart *pieChart;

- (IBAction)btnPostNewJobPressed:(id)sender;
- (IBAction)btnViewJobPosted:(id)sender;

@end
