//
//  OwnerJobPostedCell.m
//  Restaurant
//
//  Created by Parth Pandya on 13/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "OwnerJobPostedCell.h"

@implementation OwnerJobPostedCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 5.0f);
    _ProgressBar.transform = transform;
    _ProgressBar.layer.cornerRadius = 5.0;
    _ProgressBar.layer.masksToBounds = YES;
    _ProgressBar.hidden = YES;//jobPosted
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(PlotValues:) name:@"jobPosted" object:nil];
    
    
    
    // Initialization code
}

-(void)PlotValues:(NSNotification *)notification
{
    if ([notification.name isEqualToString:@"jobPosted"])
    {
        NSDictionary *dict = notification.userInfo;
        
        _lblClosed.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"CLOSEDJobs"]];
        _lblOpened.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"OPENJobs"]];
        NSArray *items = @[[PNPieChartDataItem dataItemWithValue:[[dict valueForKey:@"CLOSEDJobs"]integerValue] color:[UIColor blackColor]],
                           [PNPieChartDataItem dataItemWithValue:[[dict valueForKey:@"OPENJobs"]integerValue] color:[UIColor appMainColor] description:@""],
                           ];
        
        self.pieChart = [[PNPieChart alloc] initWithFrame:CGRectMake((CGFloat) self.contentView.frame.size.width/1.82, 25, 130.0, 130.0) items:items];
        self.pieChart.descriptionTextColor = [UIColor whiteColor];
        self.pieChart.descriptionTextFont  = [UIFont fontWithName:@"Avenir-Medium" size:11.0];
        self.pieChart.descriptionTextShadowColor = [UIColor clearColor];
        self.pieChart.showAbsoluteValues = NO;
        self.pieChart.showOnlyValues = NO;
        [self.pieChart strokeChart];
        
        
        self.pieChart.legendStyle = PNLegendItemStyleStacked;
        self.pieChart.legendFont = [UIFont boldSystemFontOfSize:12.0f];
        
        //    UIView *legend = [self.pieChart getLegendWithMaxWidth:200];
        //    [legend setFrame:CGRectMake(130, 350, legend.frame.size.width, legend.frame.size.height)];
        //    [self.view addSubview:legend];
        //
        [self.contentView addSubview:self.pieChart];

        
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (IBAction)btnPostNewJobPressed:(id)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"goToPostJobVC" object:nil];
    
}

- (IBAction)btnViewJobPosted:(id)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"goToJobsPostedVC" object:nil];
    
}
@end

