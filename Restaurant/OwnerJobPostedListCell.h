//
//  OwnerJobPostedListCell.h
//  Restaurant
//
//  Created by Parth Pandya on 20/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OwnerJobPostedListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblWorkProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblJobID;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UISwitch *jodStatusSwitch;
@property (weak, nonatomic) IBOutlet UILabel *lblCount;
@property (weak, nonatomic) IBOutlet UIImageView *imgNotification;




@end
