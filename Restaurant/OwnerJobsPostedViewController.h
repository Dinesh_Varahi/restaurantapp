//
//  OwnerJobsPostedViewController.h
//  Restaurant
//
//  Created by Parth Pandya on 20/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OwnerJobPostedListCell.h"
#import "JobPosted.h"
#import "AppDelegate.h"
#import "AppUser.h"
#import "OwnerJobApplicationViewController.h"

@interface OwnerJobsPostedViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *arrApplicantCount;
    NSMutableArray *arrID;
    NSMutableArray *arrStatus;
    NSMutableArray *arrCreatedAt;
    NSMutableArray *arrProfileType;
    
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)jobStatusChanged:(UISegmentedControl *)sender;
@property (nonatomic) CGFloat lastContentOffset;
- (IBAction)filterJobsPressed:(UIBarButtonItem *)sender;
@property (strong,nonatomic)NSMutableArray *arrJobs;

@end
