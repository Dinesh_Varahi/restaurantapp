//
//  OwnerPostCommentReplyViewController.h
//  Restaurant
//
//  Created by Parth Pandya on 24/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OwnerPostCommentReplyViewController : UIViewController<UITextViewDelegate>
{
    CGFloat animatedDistance;
}
@property (weak, nonatomic) IBOutlet UILabel *lblComment;

@property (weak, nonatomic) IBOutlet UITextView *txtReply;

- (IBAction)btnPostReplyPressed:(id)sender;


@end
