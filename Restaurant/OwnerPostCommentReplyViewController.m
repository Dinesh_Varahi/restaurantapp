//
//  OwnerPostCommentReplyViewController.m
//  Restaurant
//
//  Created by Parth Pandya on 24/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "OwnerPostCommentReplyViewController.h"
#import "AppDelegate.h"

@interface OwnerPostCommentReplyViewController ()

@end

@implementation OwnerPostCommentReplyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _txtReply.delegate = self;
    _txtReply.layer.borderWidth = 1.0f;
    _txtReply.layer.cornerRadius = 5.0f;
    _txtReply.layer.masksToBounds = YES;
    _txtReply.layer.borderColor = [UIColor redColor].CGColor;
    _lblComment.text = [NSString stringWithFormat:@"\"%@\"",[[NSUserDefaults standardUserDefaults] valueForKey:@"comment"]] ;
    
    self.title = @"Post Reply";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
//    NSRange resultRange = [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet] options:NSBackwardsSearch];
//    if ([text length] == 1 && resultRange.location != NSNotFound) {
//        [textView resignFirstResponder];
//        return NO;
//    }
    
    if ([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        // Return FALSE so that the final '\n' character doesn't get added
        return NO;
    }
    // For any other character return TRUE so that the text gets added to the view
    return YES;

    
}

-(void)textViewDidBeginEditing:(UITextView *)textView {
    CGRect textFieldRect =
    [self.view.window convertRect:textView.bounds fromView:textView];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    
}
-(void)textViewDidEndEditing:(UITextView *)textView {
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    [self.view endEditing:YES];
    
}

- (IBAction)btnPostReplyPressed:(id)sender {
    
    NSMutableDictionary *dataDict = [NSMutableDictionary new];
    
    AppUser *user = [[AppUser allObjects] firstObject];
    
    
    [dataDict setValue:_txtReply.text forKey:@"reply"];
    [dataDict setValue:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"ratingId"]] forKey:@"ratingId"];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDict options:0 error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@",SITE_URL,POST_REPLY_COMMENT] parameters:nil error:nil];
    
    
    NSString *token = user.token;
    
    
    [req setValue:[NSString stringWithFormat:@"bearer %@",token] forHTTPHeaderField:@"Authorization"];
    [req addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse *response, id data, NSError *error) {
        
        [APP huddie];
        //        NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        //double status = (long)[httpResponse statusCode];
        NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
        
        if ([data isKindOfClass:[NSDictionary class]])
            
        {
            
            NSDictionary *dataDic = (NSDictionary *)data;
            NSLog(@"Data:%@",dataDic);
            
            NSInteger status = [[dataDic valueForKey:@"status"] integerValue];
            [APP.hud setHidden:YES];
            if(status == 202)
            {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [APP.hud setHidden:YES];
                    
                    UIAlertController * alert2=   [UIAlertController
                                                   alertControllerWithTitle:@"Comment Posted"
                                                   message:@"Comment Posted Successfully"
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert2 animated:NO completion:nil];
                    
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                        [self.navigationController popViewControllerAnimated:YES];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadCommentData" object:nil];
                        
                    });
                });
                
                
                
            }
            else if(status == 400)
            {
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [APP.hud setHidden:YES];
                    UIAlertController * alert2=   [UIAlertController
                                                   alertControllerWithTitle:@"Sorry"
                                                   message:[dataDic valueForKey:@"result"]
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert2 animated:NO completion:nil];
                    
                    
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                        [self.navigationController popViewControllerAnimated:YES];
                    });
                });
                
            }
            
        }
        
        
    }] resume];
    
}
@end
