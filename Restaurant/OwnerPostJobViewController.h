//
//  OwnerPostJobViewController.h
//  Restaurant
//
//  Created by Parth Pandya on 21/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIKit.h>
#import "OwnerJobPostedListCell.h"
#import "JobPosted.h"
#import "AppDelegate.h"
#import "AppUser.h"
#import "OwnerJobApplicationViewController.h"


@interface OwnerPostJobViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UITextField *txtWorkProfile;
@property (weak, nonatomic) IBOutlet UITextField *txtExperience;
@property (nonatomic) CGFloat lastContentOffset;
@property (strong,nonatomic)NSMutableArray *arrJobs;

- (IBAction)btnPostJobPressed:(id)sender;
- (IBAction)btnFilterPressed:(id)sender;


@end
