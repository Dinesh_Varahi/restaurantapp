//
//  OwnerPostJobViewController.m
//  Restaurant
//
//  Created by Parth Pandya on 21/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "OwnerPostJobViewController.h"

@interface OwnerPostJobViewController ()
{
    int pageIndex;
    NSString *jobStatusFlag;
    NSString *strProfile;
    NSString *sortOrder;
    
    RLMResults<JobPosted *> *jobPostedData;
}
@end

@implementation OwnerPostJobViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.txtExperience.delegate = self;
    self.txtWorkProfile.delegate = self;
    
    
    _arrJobs = [[NSMutableArray alloc] init];
    jobStatusFlag = @"OPEN";
    pageIndex = 0;
    [self GetPostedJobs];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.title = @"Post a Job";
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.title = @"";
}
- (IBAction)btnPostJobPressed:(id)sender {
    
    
    if((_txtWorkProfile.text.length <= 0) || (_txtExperience.text.length <= 0)  )
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Please Enter Job Information"
                                           message:@"Work Profile and Experience required"
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:NO completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:NO completion:nil];
                
            });
            return ;
        });
 
    }
    else
    {
        [APP huddie];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@",SITE_URL,POST_JOB] parameters:nil error:nil];
        
        AppUser *user = [[AppUser allObjects] firstObject];
        
        Restaurants *aRestaurant = [[Restaurants allObjects] firstObject];
        
        NSString *token = user.token;
        
        NSLog(@"Token:%@",token);
        
        [req addValue:[NSString stringWithFormat:@"bearer %@",token] forHTTPHeaderField:@"Authorization"];
        [req addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [req addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        NSMutableDictionary *dataDict = [NSMutableDictionary new];
        
        [dataDict setValue:[NSString stringWithFormat:@"%ld", (long)aRestaurant.restroID] forKey:@"_idRestaurant"];
        
        [dataDict setValue:[self.txtWorkProfile.text lowercaseString] forKey:@"profileType"];
        [dataDict setValue:self.txtExperience.text forKey:@"experience"];
        
        
        NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict options:0 error:nil];
        NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        NSLog(@"Post Job Data :%@",jsonString);
        
        
        [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
        
        [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse *response, id data, NSError *error) {
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            //            double status = (long)[httpResponse statusCode];
            NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
            
            
            if ([httpResponse statusCode] == 202) {
                if ([data isKindOfClass:[NSDictionary class]]){
                    
                    NSDictionary *dataDic = (NSDictionary *)data;
                    NSLog(@"Data:%@",dataDic);
                    
                    NSInteger status = [[dataDic valueForKey:@"status"] integerValue];
                    if(status == 202)
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"Job Posted Successfully"
                                                           message:[dataDic valueForKey:@"result"]
                                                           preferredStyle:UIAlertControllerStyleAlert];
                            [APP.hud setHidden:YES];
                            self.txtWorkProfile.text = @"";
                            self.txtExperience.text = @"";
                            [self presentViewController:alert2 animated:NO completion:nil];
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                
                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                                
                                if (_arrJobs.count > 0) {
                                    [_arrJobs removeAllObjects];
                                }
                                
                                
                                sortOrder = @"";
                                pageIndex = 0;
                                strProfile = @"";
                                
                                [self GetPostedJobs];
                                
                            });
                        });
                        
                    }
                    else if(status == 400)
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [APP.hud setHidden:YES];
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"Sorry"
                                                           message:[dataDic valueForKey:@"result"]
                                                           preferredStyle:UIAlertControllerStyleAlert];
                            
                            [self presentViewController:alert2 animated:NO completion:nil];
                            
                            
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                            });
                        });
                        
                    }
                    else if (status == 0)
                    {
                        [APP.hud setHidden:YES];
                    }
                    else if (status == 401)
                    {
                        [APP.hud setHidden:YES];
                    }
                }
            }
            else if ([httpResponse statusCode] == 400) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [APP.hud setHidden:YES];
                    UIAlertController * alert2=   [UIAlertController
                                                   alertControllerWithTitle:@"Sorry"
                                                   message:@"Error while connecting to server"
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert2 animated:NO completion:nil];
                    
                    
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                    });
                });
            }
            else if ([httpResponse statusCode] == 401) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [APP.hud setHidden:YES];
                    UIAlertController * alert2=   [UIAlertController
                                                   alertControllerWithTitle:@"Sorry"
                                                   message:@"Error while connecting to server"
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert2 animated:NO completion:nil];
                    
                    
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                    });
                });
            }
            else if ([httpResponse statusCode] == 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [APP.hud setHidden:YES];
                    UIAlertController * alert2=   [UIAlertController
                                                   alertControllerWithTitle:@"Sorry"
                                                   message:@"Error while connecting to server"
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert2 animated:NO completion:nil];
                    
                    
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                    });
                });
            }
            
        }] resume];

    }
    
}

- (IBAction)btnFilterPressed:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"FILTER BY" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [actionSheet dismissViewControllerAnimated:NO completion:nil];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Server" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
        {
            [_arrJobs removeAllObjects];
        }
        
        strProfile = @"server";
        pageIndex = 0;
        
        [self GetPostedJobs];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Bartender" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        {
            [_arrJobs removeAllObjects];
        }
        
        strProfile = @"bartender";
        pageIndex = 0;
        [self GetPostedJobs];
        
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Busboy" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        {
            [_arrJobs removeAllObjects];
        }
        
        strProfile = @"busboy";
        pageIndex = 0;
        [self GetPostedJobs];
        
    }]];
    
    //    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Date" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    //
    //        if (_arrJobs.count > 0) {
    //            [_arrJobs removeAllObjects];
    //        }
    //
    //        sortOrder = @"Ascending";
    //        pageIndex = 0;
    //        strProfile = @"";
    //        [self GetPostedJobs];
    //
    //
    //    }]];
    
    [actionSheet addAction:[UIAlertAction  actionWithTitle:@"Clear Filter" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        if (_arrJobs.count > 0) {
            [_arrJobs removeAllObjects];
        }
        
        sortOrder = @"";
        pageIndex = 0;
        strProfile = @"";
        [self GetPostedJobs];
        
        
    }]];
    
    [self presentViewController:actionSheet animated:NO completion:nil];
    
    
    
    
    
}

#pragma mark - Textfield Delegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == _txtWorkProfile) {
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            [self dismissViewControllerAnimated:NO completion:nil];
            //        [self dismissViewControllerAnimated:YES completion:nil];
            // [self.view endEditing:YES];
            
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Server" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            
            _txtWorkProfile.text = @"Server";
            [textField resignFirstResponder];
            
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Bartender" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            _txtWorkProfile.text = @"Bartender";
            [textField resignFirstResponder];
            
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Busboy" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            _txtWorkProfile.text = @"Busboy";
            [textField resignFirstResponder];
            
        }]];
        
        [self presentViewController:actionSheet animated:NO completion:nil];
        
        return NO;
    }
    else if(textField == _txtExperience)
    {
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            [self dismissViewControllerAnimated:NO completion:nil];
            //        [self dismissViewControllerAnimated:YES completion:nil];
            // [self.view endEditing:YES];
            
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"0-1 Years" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            
            _txtExperience.text = @"0-1 Years";
            [textField resignFirstResponder];
            
        }]];

        [actionSheet addAction:[UIAlertAction actionWithTitle:@"1-2 Years" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            
            _txtExperience.text = @"1-2 Years";
            [textField resignFirstResponder];
            
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"2-4 Years" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            _txtExperience.text = @"2-4 Years";
            [textField resignFirstResponder];
            
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"4-6 Years" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            _txtExperience.text = @"4-6 Years";
            [textField resignFirstResponder];
            
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"6-8 Years" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            _txtExperience.text = @"6-8 Years";
            [textField resignFirstResponder];
            
        }]];

        [actionSheet addAction:[UIAlertAction actionWithTitle:@"8-10 Years" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            _txtExperience.text = @"8-10 Years";
            [textField resignFirstResponder];
            
        }]];

        
        [self presentViewController:actionSheet animated:NO completion:nil];
        
        return NO;

    }
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [[self view] endEditing:YES];
    
}


#pragma mark - Tableview Delegate Methods

-(CGFloat)heightForBasicCellAtIndexPath:(NSIndexPath *)indexPath {
    static OwnerJobPostedListCell *sizingCell = nil;
    //create just once per programm launching
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tableView dequeueReusableCellWithIdentifier:@"cell"];
    });
    [self configureBasicCell:sizingCell atIndexPath:indexPath];
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}
//this method will calculate required height of cell
- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

- (void)configureBasicCell:(OwnerJobPostedListCell *)Cell atIndexPath:(NSIndexPath *)indexPath {
    //make some configuration for your cell
    JobPosted  *aPostedJob = jobPostedData[indexPath.row];
    
    
    Cell.lblWorkProfile.text = aPostedJob.workProfile;
    Cell.lblJobID.text = [NSString stringWithFormat:@"%ld",(long)aPostedJob.jobID];
    Cell.lblDate.text = aPostedJob.date;
    Cell.lblCount.text = [NSString stringWithFormat:@"%ld",(long)aPostedJob.count];
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //ratingData = [ServerRating allObjects];
    return _arrJobs.count;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([self heightForBasicCellAtIndexPath:indexPath] < 78)
    {
        return 78;
    }
    return [self heightForBasicCellAtIndexPath:indexPath];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    OwnerJobPostedListCell *Cell = [[OwnerJobPostedListCell alloc] init];
    
    Cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    
    if ([jobStatusFlag isEqualToString:[NSString upperCase:@"CLOSE"]]) {
        
        NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
        
        tmpArray = [NSMutableArray arrayWithArray:[_arrJobs filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"jobStatus = %@", jobStatusFlag]]];
        
        
        [_arrJobs addObjectsFromArray:tmpArray];
        
        NSLog(@"%@",_arrJobs);
        
        
    }
    
    JobPosted *aJob;
    
    if (_arrJobs.count > 0)
    {
        aJob = [_arrJobs objectAtIndex:indexPath.row];
    }
    
    
    Cell.lblWorkProfile.text = [[aJob.workProfile capitalizedString] capitalizedString];
    Cell.lblCount.text = [NSString stringWithFormat:@"%ld",(long)aJob.count];
    Cell.lblDate.text = [NSString stringWithFormat:@"POSTED: %@",[NSString getFormattedDate:aJob.date]];
    
    Cell.lblJobID.text =[NSString stringWithFormat:@"JOB ID: %ld",(long)aJob.jobID];
    
    if([aJob.jobStatus isEqualToString:@"OPEN"])
    {
        [Cell.jodStatusSwitch setOn:YES animated:YES];
    }
    else
    {
        [Cell.jodStatusSwitch setOn:NO animated:YES];
    }
    
    Cell.jodStatusSwitch.tag = aJob.jobID;
    
    [Cell.jodStatusSwitch addTarget:self action:@selector(closeJob:) forControlEvents:UIControlEventValueChanged];
    
    return Cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    JobPosted *aJob = [_arrJobs objectAtIndex:indexPath.row];
    NSLog(@"A job:%@",aJob);
    
    NSMutableDictionary *jobData = [[NSMutableDictionary alloc] init];
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%ld",(long)aJob.jobID] forKey:@"JobId"];
    [jobData setValue:aJob.workProfile forKey:@"Profile"];
    [jobData setValue:aJob.date forKey:@"Date"];
    
    [jobData setValue:[NSString stringWithFormat:@"%ld",(long)aJob.jobID ] forKey:@"JobID"];
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%ld",(long)aJob.jobID] forKey:@"JobId"];
    OwnerJobApplicationViewController *jobApplicationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerJobApplicationViewController"];
    jobApplicationVC.jobID = aJob.jobID;
    
    [self.navigationController pushViewController:jobApplicationVC animated:NO];
    
    dispatch_after(2, dispatch_get_main_queue(), ^(void){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"JobDataDict" object:jobData];
    });
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return @"Previous Job Posts";
    }
    
    return @"";
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    //Get the page
    if (self.lastContentOffset < scrollView.contentOffset.y)
    {
        NSLog(@"Scrolling Down");
        //pageIndex = scrollView.contentOffset.x / scrollView.bounds.size.width;
        
        pageIndex ++;
        [self GetPostedJobs];
        
    }
    
    
    self.lastContentOffset = scrollView.contentOffset.y;
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        //Get the page
        if (self.lastContentOffset < scrollView.contentOffset.y)
        {
            NSLog(@"Scrolling Down");
            //pageIndex = scrollView.contentOffset.x / scrollView.bounds.size.width;
            
            pageIndex ++;
            [self GetPostedJobs];
            
        }
        
        self.lastContentOffset = scrollView.contentOffset.y;
        
        
    }
}

-(void)GetPostedJobs
{
    [APP huddie];
    
    
    //     /owner/getJobListing?page=0&jobStatus=OPEN&sort=descending&profile=BarTender
    
    if (sortOrder.length <= 0)
    {
        sortOrder =@"";
    }
    
    if (strProfile.length <= 0 ) {
        strProfile = @"";
    }
    
    NSString *urlString =[[NSString stringWithFormat:@"%@%@?page=%d&jobStatus=%@&sort=%@&profile=%@",SITE_URL,GET_JOB_POSTED_LIST,pageIndex,jobStatusFlag,sortOrder,strProfile] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

    
    NSURL *URL = [NSURL URLWithString:urlString];
    
    NSLog(@"Job List URL:%@",URL);
    
    
    AppUser *user = [[AppUser allObjects] firstObject];
    AFHTTPSessionManager *ratingsManager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    //    ratingsManager.requestSerializer = [AFJSONRequestSerializer serializer];
    ratingsManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [ratingsManager.requestSerializer setValue:[NSString stringWithFormat:@"bearer %@",user.token] forHTTPHeaderField:@"Authorization"];
    
    
    [ratingsManager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        if ([responseObject isKindOfClass:[NSArray class]])
        {
            NSArray *jsonArray = (NSArray *)responseObject  ;
            NSLog(@"Json:%@",jsonArray);
        }
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dataDic = (NSDictionary *)responseObject;
            NSLog(@"Data:%@",dataDic);
            
            NSArray *jobsArray = [dataDic valueForKey:@"data"];
            
            if (jobsArray.count > 0) {
                for (int i=0; i<jobsArray.count; i++) {
                    NSDictionary *jobsDict = [jobsArray objectAtIndex:i];
                    JobPosted *aJobPosted = [[JobPosted alloc] init];
                    aJobPosted.count = [[jobsDict valueForKey:@"applicantCount"] integerValue];
                    aJobPosted.jobID = [[jobsDict valueForKey:@"_id"] integerValue];
                    aJobPosted.date = [jobsDict valueForKey:@"createdAt"];
                    aJobPosted.jobStatus = [jobsDict valueForKey:@"status"];
                    aJobPosted.workProfile = [jobsDict valueForKey:@"profileType"];
                    
                    [_arrJobs addObject:aJobPosted];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [APP.hud setHidden:YES];
                    [self.tableView reloadData];
                });
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [APP.hud setHidden:YES];
                    
                });
                
                
            }
            
        }
        
    }
                failure:^(NSURLSessionTask *operation, NSError *error) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [APP.hud setHidden:YES];
                    });
                    
                    NSLog(@"Error: %@", error);
                    
                }];
    
}

- (void)closeJob:(id)sender
{
    if (![sender isOn]) {
        
        
        UISwitch *aSwitch = (UISwitch*)sender;
        NSLog(@"Tag:%ld",(long)aSwitch.tag);
        NSLog(@"Close Job");
        
        {
            NSMutableDictionary *dataDict = [NSMutableDictionary new];
            
            [dataDict setValue:[NSString stringWithFormat:@"%ld",(long)aSwitch.tag] forKey:@"jobId"];
            [dataDict setValue:@"CLOSED" forKey:@"jobStatus"];
            
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDict options:0 error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            
            NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@",SITE_URL,CHANGE_JOB_STATUS] parameters:nil error:nil];
            
            
            AppUser *user = [[AppUser allObjects] firstObject];
            
            NSString *token = user.token;
            
            NSLog(@"Token:%@",token);
            
            
            
            [req setValue:[NSString stringWithFormat:@"bearer %@",token] forHTTPHeaderField:@"Authorization"];
            [req addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [req addValue:@"application/json" forHTTPHeaderField:@"Accept"];
            
            [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
            
            [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse *response, id data, NSError *error) {
                
                //        NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                //double status = (long)[httpResponse statusCode];
                NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
                
                
                
                if ([data isKindOfClass:[NSDictionary class]])
                    
                {
                    
                    
                    NSDictionary *dataDic = (NSDictionary *)data;
                    NSLog(@"Data:%@",dataDic);
                    
                    NSInteger status = [[dataDic valueForKey:@"status"] integerValue];
                    if(status == 202)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            //                            NSMutableArray *discardedJob = [NSMutableArray array];
                            //                            JobPosted *aJob = [[JobPosted alloc] init];
                            //
                            //                            for (aJob in _arrJobs) {
                            //                                if (aJob.jobID == aSwitch.tag)
                            //                                    [discardedJob addObject:aJob];
                            //                            }
                            //
                            //                            [_arrJobs removeObjectsInArray:discardedJob];
                            //
                            //                            [self.tableView reloadData];
                            
                            [APP.hud setHidden:YES];
                            
                            
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"Jod Closed"
                                                           message:@""
                                                           preferredStyle:UIAlertControllerStyleAlert];
                            
                            [self presentViewController:alert2 animated:NO completion:nil];
                            
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                
                                if (_arrJobs.count > 0) {
                                    [_arrJobs removeAllObjects];
                                }
                                sortOrder = @"";
                                pageIndex = 0;
                                strProfile = @"";
                                
                                [self GetPostedJobs];
                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                                
                            });
                        });
                        
                        
                        
                    }
                    else if(status == 400)
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [APP.hud setHidden:YES];
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"Sorry"
                                                           message:[dataDic valueForKey:@"result"]
                                                           preferredStyle:UIAlertControllerStyleAlert];
                            
                            [self presentViewController:alert2 animated:NO completion:nil];
                            
                            
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                            });
                        });
                        
                    }
                    
                }
                
                
            }] resume];
            
        }
    }
}


@end
