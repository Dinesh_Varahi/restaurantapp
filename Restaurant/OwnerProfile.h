//
//  OwnerProfile.h
//  Restaurant
//
//  Created by Parth Pandya on 17/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <Realm/Realm.h>
#import "Restaurants.h"


@interface OwnerProfile : RLMObject

@property NSString *fName;
@property NSString *mName;
@property NSString *lName;
@property NSString *email;
@property NSString *city;
@property NSString *zip;
@property NSString *mobileNumber;
@property NSString *profilePic;
@property NSInteger profileID;
@property RLMArray<RestaurantImages *><RestaurantImages> *restoImageArray;
@property RLMArray<Restaurants *><Restaurants> *ownerRestaurants;
@end

RLM_ARRAY_TYPE(OwnerProfile)
