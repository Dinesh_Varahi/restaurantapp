//
//  OwnerProfileStep1ViewController.h
//  Restaurant
//
//  Created by HN on 03/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MVPlaceSearchTextField.h"
#import "AddressResolver.h"
#import "ResizeImage.h"
#import "OwnerProfile.h"
#import "OwnerProfileStep2ViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NewServer.h"

@class AddressResolver;
@class ResizeImage;


@interface OwnerProfileStep1ViewController : UIViewController<UINavigationControllerDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate,PlaceSearchTextFieldDelegate,UIImagePickerControllerDelegate>
{
    
    __weak IBOutlet UITextField *txtName;
    __weak IBOutlet UITextField *txtEmailID;
    __weak IBOutlet UITextField *txtZIPCode;
    __weak IBOutlet UITextField *txtMobileNumber;
    __weak IBOutlet UIImageView *imgProfilePic;
    NSDictionary *locationInfo;
    NSString *strImageBase64;
}
- (IBAction)btnSavePressed:(id)sender;

@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *txtCity;


@end
