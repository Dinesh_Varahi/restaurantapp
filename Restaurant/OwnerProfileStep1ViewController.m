//
//  OwnerProfileStep1ViewController.m
//  Restaurant
//
//  Created by HN on 03/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "OwnerProfileStep1ViewController.h"

@interface OwnerProfileStep1ViewController ()
{
    CLLocationCoordinate2D locationCoord;
    
}
@end

@implementation OwnerProfileStep1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [txtName setDelegate:self];
    [txtEmailID setDelegate:self];
    [txtZIPCode setDelegate:self];
    [_txtCity setDelegate:self];
    [txtMobileNumber setDelegate:self];
    
    self.navigationController.navigationBar.topItem.title = nil;
    self.navigationController.navigationItem.leftBarButtonItem.title = nil;
    
    [[NSUserDefaults standardUserDefaults] setValue:@"true" forKey:@"isprofileImageURL"];
//
//    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];

    
    UITapGestureRecognizer *tapToSelectImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToSelectImage:)];
    tapToSelectImage.delegate = self;
    [tapToSelectImage setNumberOfTapsRequired:1];
    [tapToSelectImage setNumberOfTouchesRequired:1];
    [imgProfilePic addGestureRecognizer:tapToSelectImage];
    [imgProfilePic setUserInteractionEnabled:YES];
    
    imgProfilePic.layer.cornerRadius = imgProfilePic.frame.size.width/2;
    imgProfilePic.layer.masksToBounds = YES;
    imgProfilePic.contentMode = UIViewContentModeScaleAspectFill;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateOwnerProfileData) name:@"profileFetchedForStep1VC" object:nil];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    
    [self customTextField];
    [self updateOwnerProfileData];
    
    self.title = @"Registration";
    
    //    [self updateOwnerProfileData];
}

-(void)viewDidAppear:(BOOL)animated
{
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60) forBarMetrics:UIBarMetricsDefault];
}


- (void)updateOwnerProfileData
{
    
    OwnerProfile *aOwner = [[OwnerProfile allObjects] firstObject];
    NSLog(@"Owner Profile Step1:%@",aOwner);
    
    txtName.text =aOwner.fName;
    
    txtEmailID.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"ownerEmail"];
    
    AppUser *user = [[AppUser allObjects] firstObject];
    if (user.isOwnerAccountUpdated == 1)
    {
        txtEmailID.text = aOwner.email;
    }
    _txtCity.text =aOwner.city ;
    txtZIPCode.text =aOwner.zip ;
    txtMobileNumber.text=aOwner.mobileNumber ;
    
    [imgProfilePic setShowActivityIndicatorView:YES];
    
    [imgProfilePic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,aOwner.profilePic]]
                     placeholderImage:[UIImage imageNamed:@"ic_user_b.png"]];
    
}

- (void) customTextField {
    
    CALayer *border;
    CGFloat borderWidth ;
    
    _txtCity.borderStyle = UITextBorderStyleNone;
    
    border = [CALayer layer];
    borderWidth = 1.5;
    border.borderColor = [UIColor blackColor].CGColor;
    border.frame = CGRectMake(0,_txtCity.frame.size.height - borderWidth , _txtCity.frame.size.width, _txtCity.frame.size.height);
    border.borderWidth = borderWidth;
    [_txtCity.layer addSublayer:border];
    _txtCity.layer.masksToBounds = YES;
}

- (void)tapToSelectImage:(UITapGestureRecognizer *)tapRecognizer
{
    if (tapRecognizer.state == UIGestureRecognizerStateEnded)
    {
        CGFloat frameHeight = imgProfilePic.frame.size.height;
        CGRect imageViewFrame = CGRectInset(imgProfilePic.bounds, 0.0, (CGRectGetHeight(imgProfilePic.frame) - frameHeight) / 2.0 );
        BOOL userTappedOnimageView = (CGRectContainsPoint(imageViewFrame, [tapRecognizer locationInView:imgProfilePic]));
        if (userTappedOnimageView)
        {
            [self selectPhotos];
            [self saveData];
            
        }
        
    }
    
}
- (void)selectPhotos {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
   picker  = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Photos" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
        picker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
        
        [self presentViewController:picker animated:NO completion:nil];
        
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
        
        [self presentViewController:picker animated:NO completion:nil];
        
    }]];
    
    [self presentViewController:actionSheet animated:NO completion:nil];
    
}

// Image Picker Delegate


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *originalImage =  info[UIImagePickerControllerOriginalImage];
    
    if([picker sourceType] == UIImagePickerControllerSourceTypeCamera)
    {
        UIImageWriteToSavedPhotosAlbum(originalImage,
                                       self,
                                       @selector(image:finishedSavingWithError:contextInfo:),
                                       nil);
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        imgProfilePic.image = originalImage;
    });
    
    UIImage *tmpImage = [self resizeImage:originalImage];
    
    strImageBase64 = [self encodeToBase64String:tmpImage];
    
//        NewServer *newServer = [[NewServer alloc] init];
//        RLMRealm *realm = [RLMRealm defaultRealm];
//        newServer.profilePic = imageString;
//        [realm transactionWithBlock:^{
//            [realm addObject:newServer];
//        }];
    
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        NSLog(@"Unable Save Image");
    } else{
        NSLog(@"Image Saved to Album Successfully!!");
        imgProfilePic.image = image;

        
        strImageBase64 = [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        
    }
}


- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [[self view] endEditing:YES];
    
}

#pragma mark - Place search Textfield Delegates

-(void)setupAutoCompleteTextField {
    
    _txtCity.placeSearchDelegate = self;
    
    _txtCity.strApiKey = kGoogleAPIKey;
    
    _txtCity.superViewOfList = self.view;
    
    _txtCity.autoCompleteShouldHideOnSelection = YES;
    
    _txtCity.maximumNumberOfAutoCompleteRows  = 2;
    
    _txtCity.partOfAutoCompleteRowHeightToCut = 0.2;
    
    
}

-(void)placeSearch:(MVPlaceSearchTextField*)textField ResponseForSelectedPlace:(GMSPlace*)responseDict {
    [self.view endEditing:YES];
    //    [_txtCity resignFirstResponder];
    
    NSLog(@"SELECTED ADDRESS :%@",responseDict);
    NSLog(@"City Name:%@",responseDict.types);
    
    locationCoord.latitude = responseDict.coordinate.latitude;
    locationCoord.longitude = responseDict.coordinate.longitude;
    
    if (responseDict)
    {
        locationInfo = [[NSDictionary alloc] init];
        locationInfo = [[AddressResolver sharedInstance] getAddressDetails:locationCoord];
        _txtCity.text = [locationInfo valueForKey:@"city"];
        txtZIPCode.text = [locationInfo valueForKey:@"zipcode"];
        
    }
    
    
}
-(void)placeSearchWillShowResult:(MVPlaceSearchTextField*)textField {
    
}

-(void)placeSearch:(MVPlaceSearchTextField*)textField ResultCell:(UITableViewCell*)cell withPlaceObject:(PlaceObject*)placeObject atIndex:(NSInteger)index {
    
    cell.contentView.backgroundColor = [UIColor whiteColor];
}

- (void)placeSearchWillHideResult:(MVPlaceSearchTextField*)textField {
    
}


- (IBAction)btnSavePressed:(id)sender {
    
    if((txtName.text.length <= 0) || (txtEmailID.text.length <= 0) || (txtZIPCode.text.length <= 0) ||
       (txtMobileNumber.text.length <= 0) || (_txtCity.text.length <= 0)){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Please Enter All Information"
                                           message:@"Information should not be Empty"                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:YES completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:YES completion:nil];
                
            });
        });
    }
   else if (![NSString validatePhone:txtMobileNumber.text]) {
        
        NSLog(@"Please validate Mobile Number");
       dispatch_async(dispatch_get_main_queue(), ^{
           
           UIAlertController * alert2=   [UIAlertController
                                          alertControllerWithTitle:@"Please enter valid Mobile Number"
                                          message:@""                                           preferredStyle:UIAlertControllerStyleAlert];
           
           [self presentViewController:alert2 animated:NO completion:nil];
           
           
           
           dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
               [alert2 dismissViewControllerAnimated:NO completion:nil];
           });
           
       });
       
    }
    else {
        
        [self saveData];
        
        OwnerProfileStep2ViewController *stepTwoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerProfileStep2ViewController"];
        
        [self.navigationController pushViewController:stepTwoVC animated:NO];
        
    }
    
}

- (void)saveData
{
    
    if((txtName.text.length <= 0) || (txtEmailID.text.length <= 0) || (txtZIPCode.text.length <= 0) ||
       (txtMobileNumber.text.length <= 0) || (_txtCity.text.length <= 0)){
        
    }
    else {
        
        @try {
            RLMRealm *realm = [RLMRealm defaultRealm];
            
            OwnerProfile *aOwner = [[OwnerProfile allObjects]firstObject];
            
            if(aOwner == nil)
            {
                aOwner = [[OwnerProfile alloc] init];
            }
            
            [realm beginWriteTransaction];
            aOwner.fName = txtName.text;
            aOwner.email = txtEmailID.text;
            aOwner.city = _txtCity.text;
            aOwner.zip = txtZIPCode.text;
            aOwner.mobileNumber = txtMobileNumber.text;
            
            if (strImageBase64.length > 0) {
                aOwner.profilePic = strImageBase64;
                [[NSUserDefaults standardUserDefaults] setValue:@"false" forKey:@"isprofileImageURL"];
            }
            
            
            [realm addObject:aOwner];
            [realm commitWriteTransaction];
        } @catch (NSException *exception) {
            NSLog(@"OwnerProfileStep1 Data Save Exception");
        }
        
        
    }

}

- (UIImage *)resizeImage:(UIImage *)image {
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 300.0;
    float maxWidth = 400.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
    
}
@end
