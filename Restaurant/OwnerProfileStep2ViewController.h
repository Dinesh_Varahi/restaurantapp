//
//  OwnerProfileStep2ViewController.h
//  Restaurant
//
//  Created by HN on 03/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OwnerProfile.h"
#import "MLPAutoCompleteTextField.h"
#import "MVPlaceSearchTextField.h"
#import "DEMODataSource.h"
#import "Restaurants.h"
#import "AutocompletionTableView.h"
#import "AppDelegate.h"
#import "OwnerDashboard.h"
#import "ResizeImage.h"
#import <SDWebImage/UIImageView+WebCache.h>

@class ResizeImage;

@protocol AutocompletionTableViewDelegate;

@interface OwnerProfileStep2ViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate,MLPAutoCompleteTextFieldDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIGestureRecognizerDelegate>
{
    NSString *restraurantID;
    DEMODataSource *objDemoDataSource;
    RLMResults<Restaurants  *> *allRestaurants;
    NSString *strImageBase64;
    NSMutableArray *arrRestaurantType;
    NSMutableArray *arrRestaurantCity;
    NSMutableArray *arrRestaurantZIP;
    NSMutableArray *arrName;
    NSMutableArray *arrRestaurantIds;
    
    NSString *strBool1;
    NSString *strBool2;
    NSString *strBool3;
    NSString *strBool4;
    NSString *strBool5;
    NSInteger restoID;
}
@property (nonatomic, strong) AutocompletionTableView *autoCompleter;
@property (weak, nonatomic) IBOutlet MLPAutoCompleteTextField *txtRestaurantName;


@property (weak, nonatomic) IBOutlet UICollectionView *imageCollectionView;
- (IBAction)btnProfileUpdatePressed:(id)sender;
- (IBAction)btnSavePressed:(id)sender;

//@property (weak, nonatomic) IBOutlet MLPAutoCompleteTextField *txtRestaurantName;
//@property (weak, nonatomic) IBOutlet UITextField *txtRestaurantName;
@property (weak, nonatomic) IBOutlet UITextField *txtRestoType;

@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *txtCity;
@property (weak, nonatomic) IBOutlet UITextField *txtZIP;

@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *txtLandmark;
@property (weak, nonatomic) IBOutlet UIImageView *restaurantImage;

- (IBAction)addNewImagePressed:(id)sender;


@end
