//
//  OwnerProfileStep2ViewController.m
//  Restaurant
//
//  Created by HN on 03/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//
// Set restaurant ID '-1' if restaurant not selected from dropdown

#import "OwnerProfileStep2ViewController.h"
#import "DEMOCustomAutoCompleteCell.h"

@interface OwnerProfileStep2ViewController ()
{
    NSMutableArray *arryOfImage;
    CGFloat animatedDistance;
}
@end

@implementation OwnerProfileStep2ViewController
@synthesize autoCompleter = _autoCompleter;

- (AutocompletionTableView *)autoCompleter
{
    if (!_autoCompleter)
    {
        NSMutableDictionary *options = [NSMutableDictionary dictionaryWithCapacity:2];
        [options setValue:[NSNumber numberWithBool:YES] forKey:ACOCaseSensitive];
        [options setValue:nil forKey:ACOUseSourceFont];
        
        _autoCompleter = [[AutocompletionTableView alloc] initWithTextField: _txtRestaurantName inViewController:self withOptions:options];
        _autoCompleter.autoCompleteDelegate = self;
        NSMutableArray *arrResto = [[NSMutableArray alloc]init];
        for (int i =0; i < [allRestaurants count]; i++)
        {
            [arrResto addObject:[[allRestaurants objectAtIndex:i] valueForKey:@"name"]];
        }
        _autoCompleter.suggestionsDictionary = arrResto;//[NSArray arrayWithObjects:@"hostel",@"caret",@"carrot",@"house",@"horse", nil];
    }
    return _autoCompleter;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    
    // Do any additional setup after loading the view.
    self.imageCollectionView.delegate = self;
    self.txtRestaurantName.delegate = self;
    self.txtRestoType.delegate = self;
    self.txtCity.delegate = self;
    self.txtZIP.delegate = self;
    self.txtLandmark.delegate = self;
    //restoID = -1;
    
    strBool1 = @"true";
    
    allRestaurants = [Restaurants allObjects];
    
    objDemoDataSource = [[DEMODataSource alloc] init];
    
    
    AppUser *aUser = [[AppUser allObjects] firstObject];
    
    if (aUser.isOwnerAccountUpdated == 1)
    {
        
        Restaurants *aRestaurant = [[Restaurants allObjects] firstObject];
        
        _txtRestaurantName.text = aRestaurant.name;
        _txtRestoType.text = aRestaurant.restoType;
        _txtCity.text = aRestaurant.city;
        _txtZIP.text = aRestaurant.zipCode;
        _txtLandmark.text = aRestaurant.landmark;
        restoID = aRestaurant.restroID;
        
        RestaurantImages *restoImage = [[RestaurantImages allObjects] firstObject];
        
        NSLog(@"Image URL:%@",restoImage.imageURL);
        
        [_restaurantImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,restoImage.imageURL]]
                            placeholderImage:[UIImage imageNamed:@"add_image.png"]];
        
        
        
    }
    
    self.txtCity.borderStyle = UITextBorderStyleNone;
    
    self.title = @"Registration";
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1.5;
    border.borderColor = [UIColor blackColor].CGColor;
    border.frame = CGRectMake(0,self.txtCity.frame.size.height - borderWidth , self.txtCity.frame.size.width + 50, self.txtCity.frame.size.height);
    border.borderWidth = borderWidth;
    [self.txtCity.layer addSublayer:border];
    self.txtCity.layer.masksToBounds = YES;
    
    CALayer *border2 = [CALayer layer];
    CGFloat borderWidth2 = 1.5;
    border2.borderColor = [UIColor blackColor].CGColor;
    border2.frame = CGRectMake(0,self.txtCity.frame.size.height - borderWidth2 , self.txtCity.frame.size.width + 50, self.txtCity.frame.size.height);
    border2.borderWidth = borderWidth2;
    [self.txtLandmark.layer addSublayer:border2];
    self.txtLandmark.layer.masksToBounds = YES;
    
    
    CALayer *border1 = [CALayer layer];
    CGFloat borderWidth1 = 1.5;
    border1.borderColor = [UIColor blackColor].CGColor;
    border1.frame = CGRectMake(0,self.txtRestaurantName.frame.size.height - borderWidth1 , self.txtRestaurantName.frame.size.width + 50, self.txtRestaurantName.frame.size.height);
    border1.borderWidth = borderWidth1;
    [self.txtRestaurantName.layer addSublayer:border1];
    self.txtRestaurantName.layer.masksToBounds = YES;
    
    
    self.txtRestaurantName.autoCompleteTableCellBackgroundColor = [UIColor whiteColor];
    self.txtRestaurantName.autoCompleteShouldHideOnSelection = YES;
    self.txtRestaurantName.autoCompleteShouldHideClosingKeyboard = YES;
    
    [_txtRestaurantName addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    //    [self setupAutoCompleteTextField];
    
    
    
    //    [self.autocompleteTextField setBorderStyle:UITextBorderStyleRoundedRect];
    
    self.txtRestaurantName.borderStyle = UITextGranularityLine;
    self.txtLandmark.borderStyle = UITextGranularityLine;
    if ([[[UIDevice currentDevice] systemVersion] compare:@"6.0" options:NSNumericSearch] != NSOrderedAscending) {
        [self.txtRestaurantName registerAutoCompleteCellClass:[DEMOCustomAutoCompleteCell class]
                                       forCellReuseIdentifier:@"CustomCellId"];
    }
    else{
        //Turn off bold effects on iOS 5.0 as they are not supported and will result in an exception
        self.txtRestaurantName.applyBoldEffectToAutoCompleteSuggestions = NO;
    }
    
    
    
    
    UITapGestureRecognizer *tapToSelectImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToSelectImage:)];
    tapToSelectImage.delegate = self;
    [tapToSelectImage setNumberOfTapsRequired:1];
    [tapToSelectImage setNumberOfTouchesRequired:1];
    [_restaurantImage addGestureRecognizer:tapToSelectImage];
    [_restaurantImage setUserInteractionEnabled:YES];
    
    _restaurantImage.layer.masksToBounds = YES;
    _restaurantImage.contentMode = UIViewContentModeScaleAspectFill;
    
    arryOfImage = [[NSMutableArray alloc] init];//serverDictFetched
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(serverFetched:) name:@"serverDictFetched" object:nil];
    
}

-(void)serverFetched:(NSNotification *)notification
{
    if ([notification.name isEqualToString:@"serverDictFetched"])
    {
        NSDictionary *dict = notification.userInfo;
        arrRestaurantType = [dict valueForKey:@"restaurantType"];
        arrRestaurantCity = [dict valueForKey:@"city"];
        arrRestaurantIds = [dict valueForKey:@"_id"];
        arrRestaurantZIP = [dict valueForKey:@"zipCode"];
        arrName = [dict valueForKey:@"name"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setCustomTextfields{
    
    
    CALayer *restoNameBorder;
    CGFloat restoNameBorderWidth ;
    
    _txtRestaurantName.borderStyle = UITextBorderStyleNone;
    
    
    restoNameBorder = [CALayer layer];
    restoNameBorderWidth = 1.5;
    restoNameBorder.borderColor = [UIColor blackColor].CGColor;
    restoNameBorder.frame = CGRectMake(0,_txtRestaurantName.frame.size.height - restoNameBorderWidth , _txtRestaurantName.frame.size.width, _txtRestaurantName.frame.size.height);
    restoNameBorder.borderWidth = restoNameBorderWidth;
    [_txtRestaurantName.layer addSublayer:restoNameBorder];
    _txtRestaurantName.layer.masksToBounds = YES;
    
    self.navigationController.navigationBar.topItem.title = @"";
    CALayer *cityBorder;
    CGFloat cityBorderWidth ;
    
    _txtCity.borderStyle = UITextBorderStyleNone;
    
    cityBorder = [CALayer layer];
    cityBorderWidth = 1.5;
    cityBorder.borderColor = [UIColor blackColor].CGColor;
    cityBorder.frame = CGRectMake(0,_txtCity.frame.size.height - cityBorderWidth , _txtCity.frame.size.width, _txtCity.frame.size.height);
    cityBorder.borderWidth = cityBorderWidth;
    [_txtCity.layer addSublayer:cityBorder];
    _txtCity.layer.masksToBounds = YES;
    
    CALayer *landmarkBorder;
    CGFloat landmarkBorderWidth ;
    
    _txtLandmark.borderStyle = UITextBorderStyleNone;
    
    landmarkBorder = [CALayer layer];
    landmarkBorderWidth = 1.5;
    landmarkBorder.borderColor = [UIColor blackColor].CGColor;
    landmarkBorder.frame = CGRectMake(0,_txtLandmark.frame.size.height - landmarkBorderWidth , _txtLandmark.frame.size.width, _txtLandmark.frame.size.height);
    landmarkBorder.borderWidth = landmarkBorderWidth;
    [_txtLandmark.layer addSublayer:landmarkBorder];
    _txtLandmark.layer.masksToBounds = YES;
}


#pragma mark - Textfield Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField == _txtRestoType)
    {
        [textField resignFirstResponder];
        [self restaurantType];
    }
    else
    {
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 2 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    }
    
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    [self.view endEditing:YES];
    
}


#pragma mark - Image Picker Delegate methods

- (void)tapToSelectImage:(UITapGestureRecognizer *)tapRecognizer{
    if (tapRecognizer.state == UIGestureRecognizerStateEnded)
    {
        CGFloat frameHeight = _restaurantImage.frame.size.height;
        CGRect imageViewFrame = CGRectInset(_restaurantImage.bounds, 0.0, (CGRectGetHeight(_restaurantImage.frame) - frameHeight) / 2.0 );
        BOOL userTappedOnimageView = (CGRectContainsPoint(imageViewFrame, [tapRecognizer locationInView:_restaurantImage]));
        if (userTappedOnimageView)
        {
            [self selectPhotos];
            
        }
        
    }
    
}
- (void)selectPhotos {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Photos" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
        picker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
        
        [self presentViewController:picker animated:NO completion:nil];
        
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
        
        [self presentViewController:picker animated:NO completion:nil];
        
    }]];
    
    [self presentViewController:actionSheet animated:NO completion:nil];
    
}

// Image Picker Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *originalImage =  info[UIImagePickerControllerOriginalImage];
    _restaurantImage.image = originalImage;
    
    UIImage *tempImage = [[ResizeImage sharedInstance] resizeImage:originalImage];
    
    strImageBase64 = [[ResizeImage sharedInstance] encodeToBase64String:tempImage];
    
    strBool1 = @"false";
    [arryOfImage addObject:strImageBase64];
    
    NSLog(@"Array Of Image:%@",arryOfImage);
    
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return [arryOfImage count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.clipsToBounds = YES;
    
    UIImageView *images = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
    
    [cell addSubview:images];
    
    UIButton *btnDelete = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    btnDelete.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:162.0/255.0 blue:84.0/255.0 alpha:1.0];
    btnDelete.tag = indexPath.row;
    [btnDelete setImage:[UIImage imageNamed:@"ic_action_close.png"] forState:UIControlStateNormal];
    [btnDelete addTarget:self action:@selector(btnDelete:)forControlEvents:UIControlEventTouchUpInside];
    btnDelete.layer.cornerRadius =  btnDelete.frame.size.height /2;
    btnDelete.layer.masksToBounds = YES;
    btnDelete.layer.borderWidth = 0;
    [cell addSubview:btnDelete];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(50.0f, 50.0f);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return self.view.frame.size.width/32; // This is the minimum inter item spacing, can be more
}

- (IBAction)btnProfileUpdatePressed:(id)sender {
    
    if((_txtRestaurantName.text.length <= 0) || (_txtRestoType.text.length <= 0) ||
       (_txtZIP.text.length <= 0) || (_txtCity.text.length <= 0)){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Please Enter All Information"
                                           message:@"Information should not be Empty"                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:YES completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:YES completion:nil];
                
            });
        });
    }
    
    else{
        
        [APP huddie];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@",SITE_URL,UPDATE_OWNER_PROFILE] parameters:nil error:nil];
        
        AppUser *user = [[AppUser allObjects] firstObject];
        OwnerProfile *aProfile = [[OwnerProfile allObjects] firstObject];
        //Restaurants *aRestaurant = [[Restaurants allObjects] firstObject];
        
        NSString *token = user.token;
        
        NSLog(@"Token:%@",token);
        
        [req addValue:[NSString stringWithFormat:@"bearer %@",token] forHTTPHeaderField:@"Authorization"];
        [req addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [req addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        NSMutableDictionary *dataDict = [NSMutableDictionary new];
        
        [dataDict setValue:[NSString stringWithFormat:@"%ld", (long)restoID] forKey:@"restaurantID"];
        [dataDict setValue:_txtRestaurantName.text forKey:@"restaurantName"];
        [dataDict setValue:_txtRestoType.text forKey:@"restaurantType"];
        [dataDict setValue:_txtCity.text  forKey:@"restaurantCity"];
        [dataDict setValue:_txtZIP.text forKey:@"restaurantZipCode"];
        [dataDict setValue:aProfile.fName forKey:@"name"];
        [dataDict setValue:aProfile.zip forKey:@"zip"];
        [dataDict setValue:aProfile.mobileNumber forKey:@"mobileNo"];
        [dataDict setValue:_txtLandmark.text forKey:@"restaurantLandmark"];
        
        [dataDict setValue:aProfile.city forKey:@"city"];
        
        if(aProfile.profilePic.length > 0 )
        {
            [dataDict setValue:aProfile.profilePic forKey:@"profileImage"];
            [dataDict setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"isprofileImageURL"] forKey:@"isprofileImageURL"];
        }
        
        NSMutableArray *restoImageArray = [[NSMutableArray alloc]init];
        NSLog(@"Image Array Count:%lu",(unsigned long)[arryOfImage count]);
        
        if (arryOfImage.count > 0)
        {
            for (int i = 0; i < [arryOfImage count]; i++)
            {
                
                NSLog(@"Exp:%@",[arryOfImage objectAtIndex:i]);
                
                NSMutableDictionary *imgObject = [NSMutableDictionary new];
                
                [imgObject setValue:[arryOfImage objectAtIndex:i] forKey:@"image"];
                [imgObject setValue:strBool1 forKey:@"isURL"];
                [restoImageArray addObject:imgObject];
            }

        }
        else
        {
            
            NSMutableDictionary *imgObject = [NSMutableDictionary new];
            
            if (aProfile.restoImageArray.count > 0)
            {
                [imgObject setValue:[[aProfile.restoImageArray objectAtIndex:0] valueForKey:@"imageURL"] forKey:@"image"];
                
                [imgObject setValue:strBool1 forKey:@"isURL"];
                
                [restoImageArray addObject:imgObject];

            }
            
        }

        
        if (restoImageArray.count > 0) {
            
            [dataDict setValue:restoImageArray forKey:@"restaurantImage"];
            }
        
        
        NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict options:0 error:nil];
        NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        NSLog(@"Updated Profile :%@",jsonString);
        
        
        [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
        
        [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse *response, id data, NSError *error) {
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            //            double status = (long)[httpResponse statusCode];
            NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
            
            
            if ([httpResponse statusCode] == 202) {
                if ([data isKindOfClass:[NSDictionary class]]){
                    
                    NSDictionary *dataDic = (NSDictionary *)data;
                    NSLog(@"Data:%@",dataDic);
                    
                    NSInteger status = [[dataDic valueForKey:@"status"] integerValue];
                    if(status == 202)
                    {
                        RLMRealm *realm = [RLMRealm defaultRealm];
                        
                        AppUser *user = [[AppUser allObjects] firstObject];
                        
                        [realm beginWriteTransaction];
                        user.isOwnerAccountUpdated = 1;
                        [realm addObject:user];
                        [realm commitWriteTransaction];
                        
                        [self fetchUpdatedProfile];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"Profile Updated"
                                                           message:[dataDic valueForKey:@"result"]
                                                           preferredStyle:UIAlertControllerStyleAlert];
                            [APP.hud setHidden:YES];
                            [self presentViewController:alert2 animated:NO completion:nil];
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                
                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                                
                                OwnerDashboard *ownerLanding = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerDashboard"];
                                //                            ownerLanding.navigationItem.leftBarButtonItem = nil;
                                ownerLanding.navigationItem.hidesBackButton = YES;
                                [self.navigationController pushViewController:ownerLanding animated:NO];
                                
                            });
                        });
                        
                    }
                    else if(status == 400)
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [APP.hud setHidden:YES];
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"Sorry"
                                                           message:[dataDic valueForKey:@"result"]
                                                           preferredStyle:UIAlertControllerStyleAlert];
                            
                            [self presentViewController:alert2 animated:NO completion:nil];
                            
                            
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                            });
                        });
                        
                    }
                    else if (status == 0)
                    {
                        [APP.hud setHidden:YES];
                    }
                    else if (status == 401)
                    {
                        [APP.hud setHidden:YES];
                    }
                }
            }
            else if ([httpResponse statusCode] == 400) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [APP.hud setHidden:YES];
                    UIAlertController * alert2=   [UIAlertController
                                                   alertControllerWithTitle:@"Sorry"
                                                   message:@"Error while connecting to server"
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert2 animated:NO completion:nil];
                    
                    
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                    });
                });
            }
            else if ([httpResponse statusCode] == 401) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [APP.hud setHidden:YES];
                    UIAlertController * alert2=   [UIAlertController
                                                   alertControllerWithTitle:@"Sorry"
                                                   message:@"Error while connecting to server"
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert2 animated:NO completion:nil];
                    
                    
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                    });
                });
            }
            else if ([httpResponse statusCode] == 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [APP.hud setHidden:YES];
                    UIAlertController * alert2=   [UIAlertController
                                                   alertControllerWithTitle:@"Sorry"
                                                   message:@"Error while connecting to server"
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert2 animated:NO completion:nil];
                    
                    
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                    });
                });
            }
            
        }] resume];
        
        
    }
    
    
    
    
}
- (void)fetchUpdatedProfile {
    
    AppUser *user = [[AppUser allObjects] firstObject];
    
    
    [[LocalData sharedInstance] FetchOwnerProfile:user.token completion:^(BOOL result) {
        
        if (result)
        {
            NSLog(@"Login: Owner Profile Fetched");
            
            
        }
        else{
            NSLog(@"Login: Error while fetching Owner Profile");
            
        }
        
    }];
}

-(void)restaurantType{
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [actionSheet dismissViewControllerAnimated:NO completion:nil];
        [self.view endEditing:YES];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Ethnic" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        _txtRestoType.text = @"Ethnic";
        
        [self.view endEditing:YES];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Fast Food" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        _txtRestoType.text = @"Fast Food";
        
        [self.view endEditing:YES];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Fast Casual" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        _txtRestoType.text = @"Fast Casual";
        [self.view endEditing:YES];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Casual Dinning" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        _txtRestoType.text = @"Casual Dinning";
        [self.view endEditing:YES];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Family Style" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        _txtRestoType.text = @"Family Style";
        
        [self.view endEditing:YES];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Fine Dinning" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
        _txtRestoType.text = @"Fine Dinning";
        [self.view endEditing:YES];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cafe" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        _txtRestoType.text = @"Cafe";
        
        [self.view endEditing:YES];
    }]];
    
    
    [self presentViewController:actionSheet animated:NO completion:nil];
}
- (IBAction)btnSavePressed:(id)sender {
    
    if((_txtRestaurantName.text.length <= 0) || (_txtRestoType.text.length <= 0) ||
       (_txtZIP.text.length <= 0) || (_txtCity.text.length <= 0)){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Please Enter All Information"
                                           message:@"Information should not be Empty"                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:YES completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:YES completion:nil];
                
            });
        });
    }
    
    else{
        @try {
            RLMRealm *realm = [RLMRealm defaultRealm];
            
            Restaurants *aRestaurant = [[Restaurants allObjects] firstObject];
            
            if (aRestaurant == nil)
            {
                aRestaurant = [[Restaurants alloc] init];
            }
            
            [realm beginWriteTransaction];
            
            aRestaurant.name = _txtRestaurantName.text;
            aRestaurant.restoType = _txtRestoType.text;
            aRestaurant.city = _txtCity.text;
            aRestaurant.zipCode = _txtZIP.text;
            aRestaurant.landmark = _txtLandmark.text;
            
            [realm addObject:aRestaurant];
            
            [realm commitWriteTransaction];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertController * alert2=   [UIAlertController
                                               alertControllerWithTitle:@"Profile Data saved successfully"
                                               message:@""
                                               preferredStyle:UIAlertControllerStyleAlert];
                [APP.hud setHidden:YES];
                [self presentViewController:alert2 animated:NO completion:nil];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    
                    [alert2 dismissViewControllerAnimated:NO completion:nil];
                    
                    //                OwnerDashboard *ownerLanding = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerDashboard"];
                    //                //                            ownerLanding.navigationItem.leftBarButtonItem = nil;
                    //                ownerLanding.navigationItem.hidesBackButton = YES;
                    //                [self.navigationController pushViewController:ownerLanding animated:NO];
                    
                });
            });
            
        } @catch (NSException *exception) {
            NSLog(@"OwnerProfileStep1 Data Save Exception:%@",exception.description);
        }
    }
    
    
    
}

-(void)textDidChange:(id)sender
{
    restoID = -1;
    [[NSUserDefaults standardUserDefaults] setValue:_txtRestaurantName.text forKey:@"NameOfRestro"];
    
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [[self view] endEditing:YES];
    
}

#pragma mark - Restaurant Autocomplete

- (BOOL)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
shouldStyleAutoCompleteTableView:(UITableView *)autoCompleteTableView
               forBorderStyle:(UITextBorderStyle)borderStyle;
{
    
    
    
    return YES;
}

- (BOOL)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
          shouldConfigureCell:(UITableViewCell *)cell
       withAutoCompleteString:(NSString *)autocompleteString
         withAttributedString:(NSAttributedString *)boldedString
        forAutoCompleteObject:(id<MLPAutoCompletionObject>)autocompleteObject
            forRowAtIndexPath:(NSIndexPath *)indexPath;
{
    //This is your chance to customize an autocomplete tableview cell before it appears in the autocomplete tableview
    cell.backgroundColor = [UIColor lightGrayColor];
    
    return YES;
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
  didSelectAutoCompleteString:(NSString *)selectedString
       withAutoCompleteObject:(id<MLPAutoCompletionObject>)selectedObject
            forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(selectedObject){
        NSLog(@"selected object from autocomplete menu %@ with string %@", selectedObject, [selectedObject autocompleteString]);
    } else {
        NSLog(@"selected string '%@' from autocomplete menu", selectedString);
        
        
        restraurantID = [objDemoDataSource.dict valueForKey:@"_id"];
        
        self.txtRestoType.text = @"";
        self.txtZIP.text = @"";
        self.txtRestaurantName.text = @"";
        self.txtCity.text = @"";
        self.txtLandmark.text = @"";
        self.txtRestaurantName.text = selectedString;
        
        NSInteger Index = [arrName indexOfObject:selectedString];
        self.txtCity.text = [arrRestaurantCity objectAtIndex:Index];
        self.txtZIP.text = [arrRestaurantZIP objectAtIndex:Index];
        self.txtRestoType.text = [arrRestaurantType objectAtIndex:Index];
        restoID = [[arrRestaurantIds objectAtIndex:Index] integerValue];
        
    }
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField willHideAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view will be removed from the view hierarchy");
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField willShowAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view will be added to the view hierarchy");
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField didHideAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view ws removed from the view hierarchy");
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField didShowAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view was added to the view hierarchy");
}


- (IBAction)addNewImagePressed:(id)sender {
    [self selectPhotos];
}
@end
