//
//  OwnerRestaurantInfoCell.h
//  Restaurant
//
//  Created by Parth Pandya on 13/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OwnerRestaurantInfoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblHeading;
@property (weak, nonatomic) IBOutlet UILabel *lblDetail;
@property (weak, nonatomic) IBOutlet UIImageView *imgNotification;
@property (weak, nonatomic) IBOutlet UILabel *lblCount;
@end
