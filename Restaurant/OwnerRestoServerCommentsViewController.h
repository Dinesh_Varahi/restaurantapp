//
//  OwnerRestoServerCommentsViewController.h
//  Restaurant
//
//  Created by Parth Pandya on 20/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OwnerServerCommentCell.h"
#import "ServerComment.h"
#import "AppDelegate.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "OwnerPostCommentReplyViewController.h"


@interface OwnerRestoServerCommentsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    NSArray *ratingsArray;
    NSMutableArray *arrRepliedComments;
}
- (IBAction)dayFilterChanged:(UISegmentedControl *)sender;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) CGFloat lastContentOffset;

@property (weak, nonatomic) IBOutlet UITextField *txtServerSearch;

@end
