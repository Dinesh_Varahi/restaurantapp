//
//  OwnerRestoServerCommentsViewController.m
//  Restaurant
//
//  Created by Parth Pandya on 20/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "OwnerRestoServerCommentsViewController.h"

@interface OwnerRestoServerCommentsViewController ()
{
    int pageIndex;
    int dayToFilter;
    
    NSString *strName;
    
    NSMutableArray *arrComments;
    
    RLMResults<ServerComment  *> *serverCommentData;
}
@end

@implementation OwnerRestoServerCommentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    
    _txtServerSearch.delegate = self;
    arrComments = [[NSMutableArray alloc] init];
    ratingsArray = [[NSArray alloc] init];
    arrRepliedComments = [[NSMutableArray alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    dayToFilter = 7;
    pageIndex = 0;
    
    
    [self GetRatingsAndComments];
    [_txtServerSearch addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.title = @"Server Comments";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GetRatingsAndComments) name:@"reloadCommentData" object:nil];
}


- (IBAction)dayFilterChanged:(UISegmentedControl *)sender {
    
    if (sender.selectedSegmentIndex == 0) {
        dayToFilter = 7;
        pageIndex = 0;
        
        //if (arrComments.count > 0)
        {
            [arrComments removeAllObjects];
        }
    }
    else if (sender.selectedSegmentIndex == 1)
    {
        dayToFilter = 30;
        
        pageIndex = 0;
        
        //if (arrComments.count > 0)
        {
            [arrComments removeAllObjects];
        }
    }
    else if (sender.selectedSegmentIndex == 2)
    {
        dayToFilter = 365;
        
        pageIndex = 0;
        
        //if (arrComments.count > 0)
        {
            [arrComments removeAllObjects];
        }
    }
    
    [self GetRatingsAndComments];
    
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == _txtServerSearch)
    {
        
//
    }
    return YES;

}

- (void)textDidChange:(id)sender{
    strName = _txtServerSearch.text;
    [arrComments removeAllObjects];
    pageIndex = 0;
    [self GetRatingsAndComments];
    
}
#pragma mark - Tableview Delegate Methods

-(CGFloat)heightForBasicCellAtIndexPath:(NSIndexPath *)indexPath {
    static OwnerServerCommentCell *sizingCell = nil;
    //create just once per programm launching
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tableView dequeueReusableCellWithIdentifier:@"cell"];
    });
    [self configureBasicCell:sizingCell atIndexPath:indexPath];
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}
//this method will calculate required height of cell
- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

- (void)configureBasicCell:(OwnerServerCommentCell *)Cell atIndexPath:(NSIndexPath *)indexPath {
    //make some configuration for your cell
    ServerComment *aComment = serverCommentData[indexPath.row];
    
    //    Cell.lblRatingDate.text = [NSString getFormattedDate:aRating.date];
    
    Cell.lblServerName.text = aComment.serverName;
    Cell.lblComment.text = aComment.comment;
    Cell.serverRating.value = aComment.rating;
    Cell.serverAvgRatings.value = aComment.avgRating;
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //ratingData = [ServerRating allObjects];
    return arrComments.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([self heightForBasicCellAtIndexPath:indexPath] < 78)
    {
        return 78;
    }
    return [self heightForBasicCellAtIndexPath:indexPath];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    OwnerServerCommentCell *Cell = [[OwnerServerCommentCell alloc] init];
    
    Cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
  
    ServerComment *aComment;
    if (arrComments.count > 0) {
     aComment = [arrComments objectAtIndex:indexPath.row];
    }
    
    
    Cell.lblServerName.text = [NSString upperCase:aComment.serverName];
    
    Cell.lblNameDate.text = [NSString stringWithFormat:@"By: %@  %@",aComment.customerName,[NSString getFormattedDate:aComment.date]];
    Cell.lblAvgRating.text = [NSString stringWithFormat:@"%ld",(long)aComment.avgRating];
    //Cell.serverAvgRatings.value = aComment.avgRating;
    
    Cell.txtAvgRatings.text = [NSString stringWithFormat:@"%ld",(long)aComment.avgRating];
    Cell.txtAvgRatings.rightViewMode = UITextFieldViewModeAlways;
    Cell.txtAvgRatings.textAlignment = NSTextAlignmentRight;
    Cell.txtAvgRatings.rightView = Cell.serverAvgRatings;
    Cell.txtAvgRatings.borderStyle = UITextBorderStyleNone;
    //Cell.serverAvgRatings.value = aComment.avgRating;
    Cell.serverRating.value = aComment.rating;
//    Cell.serverRating.value = aComment.rating;
    
    if (![aComment.reply isEqualToString:@"No Reply"])
    {
        [Cell.btnPostReply setTitle:@"Reply Posted" forState:UIControlStateNormal];
        Cell.btnPostReply.userInteractionEnabled = NO;
    }
    else
    {
        [Cell.btnPostReply setTitle:@"Post Reply" forState:UIControlStateNormal];
        Cell.btnPostReply.userInteractionEnabled = YES;
        
    }
    
    Cell.lblComment.text = aComment.comment;
    
    [Cell.imgServerProfile sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,aComment.serverProfileImgURL]]
                             placeholderImage:[UIImage imageNamed:@"ic_user_b.png"]];
    Cell.btnPostReply.tag = indexPath.row;
    [Cell.btnPostReply addTarget:self action:@selector(postReplyPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    return Cell;
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return [NSString stringWithFormat:@"%lu Comments",(unsigned long)arrComments.count];
    }
    
    return @"";
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    //Get the page
    if (self.lastContentOffset < scrollView.contentOffset.y)
    {
        NSLog(@"Scrolling Down");
        //pageIndex = scrollView.contentOffset.x / scrollView.bounds.size.width;
        
        pageIndex ++;
        [self GetRatingsAndComments];
        
    }
    
    
    self.lastContentOffset = scrollView.contentOffset.y;
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        //Get the page
        if (self.lastContentOffset < scrollView.contentOffset.y)
        {
            NSLog(@"Scrolling Down");
            //pageIndex = scrollView.contentOffset.x / scrollView.bounds.size.width;
            
            pageIndex ++;
            [self GetRatingsAndComments];
            
        }
        
        self.lastContentOffset = scrollView.contentOffset.y;
        
        
    }
}


-(void)viewWillDisappear:(BOOL)animated
{
    self.title = @"";
}
-(void)GetRatingsAndComments
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [APP huddie];
        
    });
    //[APP huddie];
    
    
    //    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?days=%d&page=%d",SITE_URL,GET_RATINGS,dayToFilter,pageIndex]];
    //    ?days=7&page=0
    
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?days=%d&page=%d&name=%@",SITE_URL,GET_COMMENTS,dayToFilter,pageIndex,_txtServerSearch.text]];
    
    NSLog(@"Comment URL:%@",URL);
    
    
    AppUser *user = [[AppUser allObjects] firstObject];
    AFHTTPSessionManager *ratingsManager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    //    ratingsManager.requestSerializer = [AFJSONRequestSerializer serializer];
    ratingsManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [ratingsManager.requestSerializer setValue:[NSString stringWithFormat:@"bearer %@",user.token] forHTTPHeaderField:@"Authorization"];
    
    
    [ratingsManager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        
        if ([responseObject isKindOfClass:[NSArray class]])
        {
            NSArray *jsonArray = (NSArray *)responseObject  ;
            NSLog(@"Json:%@",jsonArray);
            dispatch_async(dispatch_get_main_queue(), ^{
                [APP.hud setHidden:YES];
                
            });
        }
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dataDic = (NSDictionary *)responseObject;
            NSDictionary *Dict = [dataDic valueForKey:@"data"];
            
            NSLog(@"Data Count:%lu",(unsigned long)Dict.count);
            NSLog(@"Comment Data :%@",[dataDic valueForKey:@"data"]);
         
            ratingsArray = [dataDic valueForKey:@"data"];
            
            arrRepliedComments = [dataDic valueForKey:@"repliedComment"];
            
            if (ratingsArray.count>0) {
                for(int i = 0;i< ratingsArray.count; i++)
                {
                    NSDictionary *commentDict = [ratingsArray objectAtIndex:i];
                    
                    ServerComment *aComment = [[ServerComment alloc] init];
                    
                    aComment.customerName = [[commentDict valueForKey:@"customerName"] capitalizedString];
                    aComment.avgRating = [[commentDict valueForKey:@"avgRating"] floatValue];
                    aComment.serverProfileImgURL = [commentDict valueForKey:@"bartenderImage"];
                    aComment.serverName = [commentDict valueForKey:@"bartenderName"];
                    aComment.date = [commentDict valueForKey:@"createdAt"];
                    aComment.rating = [[commentDict valueForKey:@"rating"] floatValue];
                    aComment.comment = [commentDict valueForKey:@"comment"];
                    aComment.ratingID = [[commentDict valueForKey:@"_id"] integerValue];
                    
                    if ([commentDict valueForKey:@"repliedComment"])
                    {
                        aComment.reply = [[commentDict valueForKey:@"repliedComment"] valueForKey:@"reply"];
                    }
                    else
                    {
                        aComment.reply = @"No Reply";
                    }
                    [arrComments addObject:aComment];
                    
                }
                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [APP.hud setHidden:YES];
                [self.tableView reloadData];
            });
            
            
        }
        
    }
                failure:^(NSURLSessionTask *operation, NSError *error) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [APP.hud setHidden:YES];
                         
                    });
                    
                    NSLog(@"Error: %@", error);
                    
                }];
    
}

- (IBAction)postReplyPressed:(id)sender {
    
    UIButton *aButton = (UIButton *)sender;
    NSLog(@"Button Tag: %d Array Count:%d",aButton.tag,ratingsArray.count);
    
//    NSDictionary *Dict = [ratingsArray objectAtIndex:aButton.tag];
    ServerComment *aComment = [arrComments objectAtIndex:aButton.tag];
    [[NSUserDefaults standardUserDefaults] setValue:aComment.comment   forKey:@"comment"];
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%ld",(long)aComment.ratingID] forKey:@"ratingId"];
    
    OwnerPostCommentReplyViewController *postReply = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerPostCommentReplyViewController"];
    [self.navigationController pushViewController:postReply animated:NO];
    
    
}

- (IBAction)dateFilterChanged:(UISegmentedControl *)sender {
    
    if (sender.selectedSegmentIndex == 0) {
        dayToFilter = 7;
        pageIndex = 0;
        
        if (arrComments.count > 0)
        {
            [arrComments removeAllObjects];
        }
    }
    else if (sender.selectedSegmentIndex == 1)
    {
        dayToFilter = 30;
        
        pageIndex = 0;
        
        if (arrComments.count > 0)
        {
            [arrComments removeAllObjects];
        }
    }
    else if (sender.selectedSegmentIndex == 2)
    {
        dayToFilter = 365;
        
        pageIndex = 0;
        
        if (arrComments.count > 0)
        {
            [arrComments removeAllObjects];
        }
    }
    
    [self GetRatingsAndComments];
}


@end
