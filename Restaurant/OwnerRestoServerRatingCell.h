//
//  OwnerRestoServerRatingCell.h
//  Restaurant
//
//  Created by Parth Pandya on 20/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface OwnerRestoServerRatingCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *lblServerName;
@property (weak, nonatomic) IBOutlet UILabel *lblRestoName;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalRatings;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *avgRatings;


@end
