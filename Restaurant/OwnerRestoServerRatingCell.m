//
//  OwnerRestoServerRatingCell.m
//  Restaurant
//
//  Created by Parth Pandya on 20/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "OwnerRestoServerRatingCell.h"

@implementation OwnerRestoServerRatingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.imgProfilePic.layer.cornerRadius = self.imgProfilePic.frame.size.width / 2;
    
    self.imgProfilePic.layer.masksToBounds = YES;
    self.imgProfilePic.contentMode = UIViewContentModeScaleAspectFill;
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
