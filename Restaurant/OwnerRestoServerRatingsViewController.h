//
//  OwnerRestoServerRatingsViewController.h
//  Restaurant
//
//  Created by Parth Pandya on 20/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OwnerRestoServerRatingCell.h"
#import "Servers.h"
#import "AppDelegate.h"
#import "AppUser.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "OwnerServerDetailsViewController.h"

@interface OwnerRestoServerRatingsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    
    int pageIndex;
}

@property (weak, nonatomic) IBOutlet UITableView *tblServerRatings;
- (IBAction)dateFilterChanged:(UISegmentedControl *)sender;

@property (nonatomic) CGFloat lastContentOffset;
@property (weak, nonatomic) IBOutlet UITextField *txtServerSearch;

@end
