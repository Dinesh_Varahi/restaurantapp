//
//  OwnerRestoServerRatingsViewController.m
//  Restaurant
//
//  Created by Parth Pandya on 20/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "OwnerRestoServerRatingsViewController.h"

@interface OwnerRestoServerRatingsViewController ()
{
    
    int dayToFilter;
    NSMutableArray *arrRatings;
    NSString *strName;
    
    RLMResults<Servers  *> *serverRatingData;
}
@end

@implementation OwnerRestoServerRatingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    pageIndex = 0;
    arrRatings = [[NSMutableArray alloc] init];
    self.tblServerRatings.delegate = self;
    self.tblServerRatings.dataSource = self;
    dayToFilter = 7;
    [self GetRatingsAndComments];
    
    
    [_txtServerSearch addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    
    self.title = @"Server Ratings";
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.title = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textDidChange:(id)sender{
    
    strName = _txtServerSearch.text;
    
    pageIndex = 0;
    
    if (arrRatings.count > 0)
    {
        [arrRatings removeAllObjects];
    }
    
    [self GetRatingsAndComments];
}

#pragma mark - Tableview Delegate Methods

-(CGFloat)heightForBasicCellAtIndexPath:(NSIndexPath *)indexPath {
    static OwnerRestoServerRatingCell *sizingCell = nil;
    //create just once per programm launching
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tblServerRatings dequeueReusableCellWithIdentifier:@"cell"];
    });
    [self configureBasicCell:sizingCell atIndexPath:indexPath];
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}
//this method will calculate required height of cell
- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

- (void)configureBasicCell:(OwnerRestoServerRatingCell *)Cell atIndexPath:(NSIndexPath *)indexPath {
    //make some configuration for your cell
    Servers *aServer = serverRatingData[indexPath.row];
    
    //    Cell.lblRatingDate.text = [NSString getFormattedDate:aRating.date];
    Cell.lblServerName.text = aServer.firstName;
    
    Cell.lblRestoName.text = aServer.role;
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //ratingData = [ServerRating allObjects];
    return arrRatings.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Servers *aServer;
    
    if (arrRatings.count > 0)
    {
        aServer = [arrRatings objectAtIndex:indexPath.row];
    }
    
   
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%ld",(long)aServer.serverID] forKey:@"ServerID"];
    OwnerServerDetailsViewController *serverDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerServerDetailsViewController"];
    
    [self.navigationController pushViewController:serverDetailVC animated:NO];
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([self heightForBasicCellAtIndexPath:indexPath] < 78)
    {
        return 78;
    }
    return [self heightForBasicCellAtIndexPath:indexPath];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    OwnerRestoServerRatingCell *Cell = [[OwnerRestoServerRatingCell alloc] init];
    
    Cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    
    Servers *aServer;
    
    if (arrRatings.count > 0)
    { 
       aServer = [arrRatings objectAtIndex:indexPath.row];
    }
    
    
    
    Cell.lblServerName.text = [NSString upperCase:aServer.firstName];
    //if (aServer.role.length >0)
    {
        Cell.lblRestoName.text = [NSString upperCase:[[NSUserDefaults standardUserDefaults] valueForKey:@"RestoName"]];;
    }
//    else{
//        Cell.lblRestoName.text = @"No Profile";
//    }
    Cell.lblTotalRatings.text = [NSString stringWithFormat:@"%ld Ratings",(long)aServer.toalRatings];
    Cell.avgRatings.value = aServer.avgRating;
    
    [Cell.imgProfilePic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,aServer.imageURL]]
                          placeholderImage:[UIImage imageNamed:@"ic_user_b.png"]];
    
    
    
    return Cell;
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return [NSString stringWithFormat:@"%lu Ratings",(unsigned long)arrRatings.count];
    }
    
    return @"";
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    //Get the page
    if (self.lastContentOffset < scrollView.contentOffset.y)
    {
        NSLog(@"Scrolling Down");
        //pageIndex = scrollView.contentOffset.x / scrollView.bounds.size.width;
        
        pageIndex ++;
        [self GetRatingsAndComments];
        
    }
    
    
    self.lastContentOffset = scrollView.contentOffset.y;
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        //Get the page
        if (self.lastContentOffset < scrollView.contentOffset.y)
        {
            NSLog(@"Scrolling Down");
            //pageIndex = scrollView.contentOffset.x / scrollView.bounds.size.width;
            
            pageIndex ++;
            [self GetRatingsAndComments];
            
        }
        
        self.lastContentOffset = scrollView.contentOffset.y;
        
        
    }
}

-(void)GetRatingsAndComments
{
    [APP huddie];
 
    //    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?days=%d&page=%d",SITE_URL,GET_RATINGS,dayToFilter,pageIndex]];
    
    NSString *urlString =[[NSString stringWithFormat:@"%@%@?days=%d&name=%@&page=%d",SITE_URL,GET_RATINGS,dayToFilter,_txtServerSearch.text,pageIndex] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

    
    NSURL *URL = [NSURL URLWithString:urlString];
    
    NSLog(@"Rating URL:%@",URL);
    
    
    AppUser *user = [[AppUser allObjects] firstObject];
    AFHTTPSessionManager *ratingsManager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    //    ratingsManager.requestSerializer = [AFJSONRequestSerializer serializer];
    ratingsManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [ratingsManager.requestSerializer setValue:[NSString stringWithFormat:@"bearer %@",user.token] forHTTPHeaderField:@"Authorization"];
    
    [ratingsManager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        if ([responseObject isKindOfClass:[NSArray class]])
        {
            NSArray *jsonArray = (NSArray *)responseObject  ;
            NSLog(@"Json:%@",jsonArray);
            dispatch_async(dispatch_get_main_queue(), ^{
                [APP.hud setHidden:YES];
               
            });
        }
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dataDic = (NSDictionary *)responseObject;
            NSDictionary *Dict = [dataDic valueForKey:@"data"];
            
            NSLog(@"Server Rating Data:%@",Dict);
            
            NSArray *ratingsArray = [dataDic valueForKey:@"data"];
            
            if (ratingsArray.count>0) {
                
                NSLog(@"Rating Total Count:%lu",(unsigned long)ratingsArray.count);
                
                for(int i=0;i<ratingsArray.count;i++)
                {
                    NSDictionary *ratingDict = [ratingsArray objectAtIndex:i];
                    
                    NSLog(@"Rating Count:%d",i);
                    
                    Servers *aServer = [[Servers alloc] init];
                    
                    aServer.serverID = [[ratingDict valueForKey:@"_idUserProfile"] integerValue];
                    aServer.firstName = [ratingDict valueForKey:@"name"];
                    aServer.role = [ratingDict valueForKey:@"designation"];
                    aServer.avgRating = [[ratingDict valueForKey:@"avgRating"] floatValue];
                    aServer.toalRatings = [[ratingDict valueForKey:@"totalRatings"] integerValue];
                    
                    aServer.imageURL = [ratingDict valueForKey:@"image"];
                    
                    [arrRatings addObject:aServer];
                    
                }
            }

            dispatch_async(dispatch_get_main_queue(), ^{
                [APP.hud setHidden:YES];
                [self.tblServerRatings reloadData];
            });
        }
        
        
        
    }
                failure:^(NSURLSessionTask *operation, NSError *error) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [APP.hud setHidden:YES];
                    });
                    
                    NSLog(@"Error: %@", error);
                    
                }];
    
}


- (IBAction)dateFilterChanged:(UISegmentedControl *)sender {
    
    if (sender.selectedSegmentIndex == 0) {
        dayToFilter = 7;
        
        pageIndex = 0;
        
        if (arrRatings.count > 0)
        {
            [arrRatings removeAllObjects];
        }
    }
    else if (sender.selectedSegmentIndex == 1)
    {
        dayToFilter = 30;
        pageIndex = 0;
        
        if (arrRatings.count > 0)
        {
            [arrRatings removeAllObjects];
        }
    }
    else if (sender.selectedSegmentIndex == 2)
    {
        dayToFilter = 365;
        pageIndex = 0;
        
        if (arrRatings.count > 0)
        {
            [arrRatings removeAllObjects];
        }
    }
    
    [self GetRatingsAndComments];
}
@end
