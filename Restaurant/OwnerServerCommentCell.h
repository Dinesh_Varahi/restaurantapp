//
//  OwnerServerCommentCell.h
//  Restaurant
//
//  Created by Parth Pandya on 21/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface OwnerServerCommentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgServerProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblComment;
@property (weak, nonatomic) IBOutlet UILabel *lblNameDate;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *serverRating;
@property (weak, nonatomic) IBOutlet UILabel *lblServerName;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *serverAvgRatings;
@property (weak, nonatomic) IBOutlet UIButton *btnPostReply;
@property (weak, nonatomic) IBOutlet UITextField *txtAvgRatings;
@property (weak, nonatomic) IBOutlet UILabel *lblAvgRating;


@end
