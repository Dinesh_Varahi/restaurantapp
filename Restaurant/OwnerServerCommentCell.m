//
//  OwnerServerCommentCell.m
//  Restaurant
//
//  Created by Parth Pandya on 21/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "OwnerServerCommentCell.h"

@implementation OwnerServerCommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.imgServerProfile.layer.cornerRadius = self.imgServerProfile.frame.size.width / 2;
    
    self.imgServerProfile.layer.masksToBounds = YES;
    self.imgServerProfile.contentMode = UIViewContentModeScaleAspectFill;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
