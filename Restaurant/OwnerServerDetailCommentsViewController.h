//
//  OwnerServerDetailCommentsViewController.h
//  Restaurant
//
//  Created by Parth Pandya on 23/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface OwnerServerDetailCommentsViewController : UIViewController
{
    NSString *serverID;
    int pageIndex;
    NSDictionary *serverDict;
    NSMutableArray *arrComments;
    NSMutableArray *arrRating;
    NSMutableArray *arrGivenBy;
    NSMutableArray *arrUpdatedAt;
    NSMutableArray *arrServers;
    NSDictionary *serverinfo;
    NSMutableArray *arrExperiance;
    NSDictionary *serverExperiance;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgServer;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRatings;
@property (weak, nonatomic) IBOutlet UIProgressView *progress1;
@property (weak, nonatomic) IBOutlet UIProgressView *progress2;

@property (weak, nonatomic) IBOutlet UIProgressView *progress3;

@property (weak, nonatomic) IBOutlet UIProgressView *progress4;

@property (weak, nonatomic) IBOutlet UIProgressView *progress5;

@property (weak, nonatomic) IBOutlet UILabel *lblOneRating;
@property (weak, nonatomic) IBOutlet UILabel *lbltwoRating;
@property (weak, nonatomic) IBOutlet UILabel *lblthreeRating;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalRatings;
@property (weak, nonatomic) IBOutlet UILabel *lblfourRating;
@property (weak, nonatomic) IBOutlet UILabel *lblfiveRating;
@property (weak, nonatomic) IBOutlet UITableView *tblComments;

@end
