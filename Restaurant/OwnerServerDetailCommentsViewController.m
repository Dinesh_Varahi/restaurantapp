//
//  OwnerServerDetailCommentsViewController.m
//  Restaurant
//
//  Created by Parth Pandya on 23/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "OwnerServerDetailCommentsViewController.h"
#import "Servers.h"
#import "OwnerServerProfileCommentCell.h"
#import "AppDelegate.h"

@interface OwnerServerDetailCommentsViewController ()

@end

@implementation OwnerServerDetailCommentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrComments = [[NSMutableArray alloc] init];
    arrRating = [[NSMutableArray alloc] init];
    arrUpdatedAt = [[NSMutableArray alloc] init];
    arrGivenBy = [[NSMutableArray alloc] init];
    
    self.tblComments.estimatedRowHeight = 135.0;
    
    
    
    
    self.imgServer.layer.cornerRadius = self.imgServer.frame.size.height/2;
    self.imgServer.layer.masksToBounds = YES;
    
    self.tblComments.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self GetRatingsAndComments];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewWillAppear:) name:@"OwnerServerDetailsAvailabel" object:nil];
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    Servers *server = [[Servers allObjects] firstObject];
    
    self.starRatings.value = server.avgRating;
    self.lblOneRating.text = [NSString stringWithFormat:@"%ld",(long)server.oneRatings];
    self.lbltwoRating.text = [NSString stringWithFormat:@"%ld",(long)server.twoRatings];
    self.lblthreeRating.text = [NSString stringWithFormat:@"%ld",(long)server.threeRatings];
    self.lblfourRating.text = [NSString stringWithFormat:@"%ld",(long)server.fourRatings];
    self.lblfiveRating.text = [NSString stringWithFormat:@"%ld",(long)server.fiveRatings];
    _lblTotalRatings.text = [NSString stringWithFormat:@"%ld Ratings",(long)server.toalRatings];
    
    [_imgServer sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,server.imageURL]]placeholderImage:[UIImage imageNamed:@"ic_user_b.png"]];
     
    float TotalRatings = server.toalRatings;
    self.progress1.progress = server.oneRatings/TotalRatings;
    self.progress2.progress = server.twoRatings/TotalRatings;
    self.progress3.progress = server.threeRatings/TotalRatings;
    self.progress4.progress = server.fourRatings/TotalRatings;
    self.progress5.progress = server.fiveRatings/TotalRatings;
    [self.tblComments setNeedsLayout];
    [self.tblComments layoutIfNeeded];
//
}

- (void)viewDidAppear:(BOOL)animated {
    [self.tblComments reloadData];
    
}

- (void)FetchProfileDetails {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    AppUser *user = [[AppUser allObjects] firstObject];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"bearer %@",user.token] forHTTPHeaderField:@"Authorization"];
    
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?serverId=%@&page=%d",SITE_URL,GET_ALLSERVER_RATINGS,[[NSUserDefaults standardUserDefaults] valueForKey:@"ServerID"],pageIndex]];
    NSLog(@"%@",URL);
    [manager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        if ([responseObject isKindOfClass:[NSArray class]])
        {
            NSArray *jsonArray = (NSArray *)responseObject  ;
            NSLog(@"Json:%@",jsonArray);
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            });

        }
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dataDic = (NSDictionary *)responseObject;
            NSLog(@"%@",dataDic);
            
            serverDict = (NSDictionary *)responseObject;
            serverinfo = [[NSDictionary alloc]init];
            serverinfo = [serverDict valueForKey:@"data"];
            NSLog(@"%@",serverinfo);
            //                serverExperiance = [serverinfo valueForKey:@"experience"];
            arrComments = [serverDict valueForKey:@"data"];
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tblComments reloadData];
                
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

            });
            

        }
    }failure:^(NSURLSessionTask *operation, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        });
        
        NSLog(@"Error: %@", error);
    }];
    
    
    
}

- (void)GetRatingsAndComments{
    //Servers *newServer = [[Servers allObjects] firstObject];
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@?page=%d",SITE_URL,RATAINGS_AND_COMMENTS,[[NSUserDefaults standardUserDefaults] valueForKey:@"ServerID"],pageIndex]];
    //    NSLog(@"Rating URL:%@",URL);
    
    
    AppUser *user = [[AppUser allObjects] firstObject];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"bearer %@",user.token] forHTTPHeaderField:@"Authorization"];
    
    
    [manager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        if ([responseObject isKindOfClass:[NSArray class]])
        {
            NSArray *jsonArray = (NSArray *)responseObject  ;
            NSLog(@"Json:%@",jsonArray);
        }
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dataDic = (NSDictionary *)responseObject;
            NSDictionary *Dict = [dataDic valueForKey:@"data"];
            
            arrRating = [Dict valueForKey:@"rating"];
            arrComments = [Dict valueForKey:@"comment"];
            arrGivenBy = [Dict valueForKey:@"name"];
            arrUpdatedAt = [Dict valueForKey:@"updatedAt"];
           
            [_tblComments reloadData];
            
        }
        
    }
         failure:^(NSURLSessionTask *operation, NSError *error) {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 [APP.hud setHidden:YES];
             });
             
             NSLog(@"Error: %@", error);
             
         }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//-(CGFloat)heightForBasicCellAtIndexPath:(NSIndexPath *)indexPath {
//    static OwnerServerProfileCommentCell *sizingCell = nil;
//    //create just once per programm launching
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        sizingCell = [self.tblComments dequeueReusableCellWithIdentifier:@"OwnerServerProfileCommentCell"];
//    });
//    [self configureBasicCell:sizingCell atIndexPath:indexPath];
//    return [self calculateHeightForConfiguredSizingCell:sizingCell];
//}
////this method will calculate required height of cell
//- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
//    [sizingCell setNeedsLayout];
//    [sizingCell layoutIfNeeded];
//    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
//    return size.height;
//}

//- (void)configureBasicCell:(OwnerServerProfileCommentCell *)Cell atIndexPath:(NSIndexPath *)indexPath {
//    //make some configuration for your cell
//    Cell.starRatings.value = [[arrRating objectAtIndex:indexPath.row] integerValue];
//    Cell.lblDate.text = [NSString getFormattedDate:[arrUpdatedAt objectAtIndex:indexPath.row]];
//    Cell.lblName.text = [NSString stringWithFormat:@"By : %@",[NSString upperCase:[arrGivenBy objectAtIndex:indexPath.row]]];
//    Cell.lblComment.text = [arrComments objectAtIndex:indexPath.row];
//    
//}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return arrComments.count;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    UITableViewCell *Cell;

    OwnerServerProfileCommentCell *Cell = [[OwnerServerProfileCommentCell alloc] init];
    Cell = [tableView dequeueReusableCellWithIdentifier:@"OwnerServerProfileCommentCell" forIndexPath:indexPath];
    Cell.starRatings.value = [[arrRating objectAtIndex:indexPath.row] integerValue];
    Cell.lblDate.text = [NSString getFormattedDate:[arrUpdatedAt objectAtIndex:indexPath.row]];
    NSString *tmpName = [arrGivenBy objectAtIndex:indexPath.row];
    
    Cell.lblName.text = [NSString stringWithFormat:@"By : %@",[tmpName capitalizedString]];
    Cell.lblComment.text = [arrComments objectAtIndex:indexPath.row];
    
    return Cell;
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (arrComments.count == 0)
    {
        return @"No Comments";
    }
    return [NSString stringWithFormat:@"%lu Comments",(unsigned long)arrComments.count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;
}

@end
