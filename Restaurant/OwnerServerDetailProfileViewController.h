//
//  OwnerServerDetailProfileViewController.h
//  Restaurant
//
//  Created by Parth Pandya on 23/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface OwnerServerDetailProfileViewController : UIViewController
{
    NSString *serverID;
    
    NSDictionary *serverDict;
    NSMutableArray *arrExperinace;
    NSMutableArray *arrServers;
    NSDictionary *serverinfo;
    NSMutableArray *arrExperiance;
    NSDictionary *serverExperiance;
}
@property (weak, nonatomic) IBOutlet UIProgressView *progress1;
@property (weak, nonatomic) IBOutlet UIProgressView *progress2;
@property (weak, nonatomic) IBOutlet UIProgressView *progress3;
@property (weak, nonatomic) IBOutlet UIProgressView *progress4;
@property (weak, nonatomic) IBOutlet UIProgressView *progress5;

@property (weak, nonatomic) IBOutlet UILabel *lblRating1;
@property (weak, nonatomic) IBOutlet UILabel *lblRating2;
@property (weak, nonatomic) IBOutlet UILabel *lblRating3;
@property (weak, nonatomic) IBOutlet UILabel *lblRating4;
@property (weak, nonatomic) IBOutlet UILabel *lblRating5;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalCount;

@property (weak, nonatomic) IBOutlet UILabel *lblNickName;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblExperiance;
@property (weak, nonatomic) IBOutlet UILabel *lblMobileNo;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRatings;
@property (weak, nonatomic) IBOutlet UIImageView *imgServer;
@property (weak, nonatomic) IBOutlet UITableView *tblPreviousExperiance;


@end
