//
//  OwnerServerDetailProfileViewController.m
//  Restaurant
//
//  Created by Parth Pandya on 23/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "OwnerServerDetailProfileViewController.h"
#import "AppDelegate.h"
#import "OwnerServerExperianceCell.h"

@interface OwnerServerDetailProfileViewController ()

@end

@implementation OwnerServerDetailProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrServers = [[NSMutableArray alloc]init];
    
    self.imgServer.layer.cornerRadius = self.imgServer.frame.size.height/2;
    self.imgServer.layer.masksToBounds = YES;
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    serverID = [[NSUserDefaults standardUserDefaults] valueForKey:@"ServerID"];
    [self FetchProfileDetails];
}

-(void)FetchProfileDetails {
    
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc]
                                             initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activityView.center=self.view.center;
    [activityView startAnimating];
    [activityView hidesWhenStopped];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    [self.view addSubview:activityView];
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSString *urlString =[[NSString stringWithFormat:@"%@%@?serverId=%@",SITE_URL,GET_BARTENDER_BY_ID,[[NSUserDefaults standardUserDefaults] valueForKey:@"ServerID"]] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSURL *URL = [NSURL URLWithString:urlString];
    NSLog(@"%@",URL);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        
        
        if (error) {
            NSLog(@"Error: %@", error.description);
            [activityView stopAnimating];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            });
          
            
        }
        else {
            //            NSLog(@"%@ %@", response, responseObject);
            
            if ([responseObject isKindOfClass:[NSArray class]])
            {
                NSArray *jsonArray = (NSArray *)responseObject  ;
                NSLog(@"Json:%@",jsonArray);
                [activityView stopAnimating];
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                });

            }
            
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                RLMRealm *Obj = [RLMRealm defaultRealm];
                
                [Obj beginWriteTransaction];
                [Obj deleteObjects:[Servers allObjectsInRealm:Obj]];
                [Obj commitWriteTransaction];
                
                serverDict = (NSDictionary *)responseObject;
                serverinfo = [[NSDictionary alloc]init];
                serverinfo = [serverDict valueForKey:@"data"];
                NSLog(@"%@",serverinfo);
//                serverExperiance = [serverinfo valueForKey:@"experience"];
                arrExperiance = [serverinfo valueForKey:@"experience"];
                @try {
                    RLMRealm *realm = [RLMRealm defaultRealm];
                    
                    Servers *server = [[Servers allObjects]firstObject];
                    
                    if(server == nil)
                    {
                        server = [[Servers alloc] init];
                    }
                    
                    [realm beginWriteTransaction];
                    server.firstName = [serverinfo valueForKey:@"fname"];
                    server.nickName = [serverinfo valueForKey:@"nickName"];
                    server.mobileNumber = [serverinfo valueForKey:@"mobileNo"];
                    server.imageURL = [serverinfo valueForKey:@"image"];
                    server.avgRating = [[serverinfo valueForKey:@"avgRating"] floatValue];
                    server.oneRatings = [[serverinfo valueForKey:@"oneRatings"] integerValue];
                    server.twoRatings = [[serverinfo valueForKey:@"twoRatings"] integerValue];
                    server.threeRatings = [[serverinfo valueForKey:@"threeRatings"] integerValue];
                    server.fourRatings = [[serverinfo valueForKey:@"fourRatings"] integerValue];
                    server.fiveRatings = [[serverinfo valueForKey:@"fiveRatings"] integerValue];
                    server.toalRatings = [[serverinfo valueForKey:@"totalRatings"] integerValue];
                    
                    
                    [realm addObject:server];
                    [realm commitWriteTransaction];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"OwnerServerDetailsAvailabel" object:nil];
                } @catch (NSException *exception) {
                    NSLog(@"OwnerProfileStep1 Data Save Exception");
                    [activityView stopAnimating];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                    });

                }

                
                
                [activityView stopAnimating];
                dispatch_async(dispatch_get_main_queue(), ^{
                [self setServerInfo];
                    [self.tblPreviousExperiance reloadData];
                    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                });

                
            }
        }
    }];
    [dataTask resume];

}

-(void)setServerInfo {
    self.lblEmail.text = [serverinfo valueForKey:@"email"];
    _lblNickName.text = [NSString upperCase:[serverinfo valueForKey:@"nickName"]];
    _lblLocation.text = [serverinfo valueForKey:@"city"];
    _lblExperiance.text = [NSString stringWithFormat:@"%@",[serverinfo valueForKey:@"totalExperience"]];
    [_imgServer sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,[serverinfo valueForKey:@"image"]]]
                      placeholderImage:[UIImage imageNamed:@"ic_user_b.png"]];
    _lblMobileNo.text = [serverinfo valueForKey:@"mobileNo"];
    _lblTotalCount.text = [NSString stringWithFormat:@"%@ Ratings",[serverinfo valueForKey:@"totalRatings"]];
    _starRatings.value = [[serverinfo valueForKey:@"avgRating"] floatValue];
    self.lblRating1.text = [NSString stringWithFormat:@"%@",[serverinfo valueForKey:@"oneRatings"]];
    self.lblRating2.text = [NSString stringWithFormat:@"%@",[serverinfo valueForKey:@"twoRatings"]];
    self.lblRating3.text = [NSString stringWithFormat:@"%@",[serverinfo valueForKey:@"threeRatings"]];
    self.lblRating4.text = [NSString stringWithFormat:@"%@",[serverinfo valueForKey:@"fourRatings"]];
    self.lblRating5.text = [NSString stringWithFormat:@"%@",[serverinfo valueForKey:@"fiveRatings"]];
    float TotalRatings = [[serverinfo valueForKey:@"totalRatings"]integerValue];
    self.progress1.progress = [[serverinfo valueForKey:@"oneRatings"] integerValue]/TotalRatings;
    self.progress2.progress = [[serverinfo valueForKey:@"twoRatings"] integerValue]/TotalRatings;
    self.progress3.progress = [[serverinfo valueForKey:@"threeRatings"] integerValue]/TotalRatings;
    self.progress4.progress = [[serverinfo valueForKey:@"fourRatings"] integerValue]/TotalRatings;
    self.progress5.progress = [[serverinfo valueForKey:@"fiveRatings"] integerValue]/TotalRatings;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return arrExperiance.count;
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    UITableViewCell *Cell;
    OwnerServerExperianceCell *Cell = [[OwnerServerExperianceCell alloc] init];
    
    serverExperiance = [arrExperiance objectAtIndex:indexPath.row];
    Cell = [tableView dequeueReusableCellWithIdentifier:@"OwnerServerExperianceCell" forIndexPath:indexPath];
    Cell.lblRestaurantName.text = [serverExperiance valueForKey:@"restaurantName"];
    Cell.lblCity.text = [serverExperiance valueForKey:@"city"];
    Cell.lblProfile.text = [[serverExperiance valueForKey:@"workProfile"] capitalizedString];
    if ([[serverExperiance valueForKey:@"toDate"] length] > 0)
    {
        Cell.lblExperiance.text = [NSString stringWithFormat:@"%@ to %@",[serverExperiance valueForKey:@"fromDate"],[serverExperiance valueForKey:@"toDate"]];
    }
    else
    {
        Cell.lblExperiance.text = [NSString stringWithFormat:@"%@ to present",[serverExperiance valueForKey:@"fromDate"]];
    }
    
    Cell.lblRatings.text = [NSString stringWithFormat:@"%@",[[arrExperiance objectAtIndex:indexPath.row] valueForKey:@"ratingCount"]];
    Cell.lblComments.text = [NSString stringWithFormat:@"%@",[[arrExperiance objectAtIndex:indexPath.row] valueForKey:@"commentCount"]];
    
    return Cell;
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Previous Employers";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

@end
