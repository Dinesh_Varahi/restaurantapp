//
//  OwnerServerDetailRatingsViewController.h
//  Restaurant
//
//  Created by Parth Pandya on 23/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"
#import "AppDelegate.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface OwnerServerDetailRatingsViewController : UIViewController
{
    NSString *serverID;
    int pageIndex;
    NSDictionary *serverDict;
    NSMutableArray *arrRatings;
    NSMutableArray *arrServers;
    NSDictionary *serverinfo;
    NSMutableArray *arrExperiance;
    NSDictionary *serverExperiance;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgServer;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalRatings;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRating;
@property (weak, nonatomic) IBOutlet UIProgressView *Progress1;
@property (weak, nonatomic) IBOutlet UIProgressView *Progress2;
@property (weak, nonatomic) IBOutlet UIProgressView *Progress3;
@property (weak, nonatomic) IBOutlet UIProgressView *Progress4;
@property (weak, nonatomic) IBOutlet UIProgressView *Progress5;
@property (weak, nonatomic) IBOutlet UITableView *tblRatings;

@property (weak, nonatomic) IBOutlet UILabel *OneRatings;
@property (weak, nonatomic) IBOutlet UILabel *twoRatings;
@property (weak, nonatomic) IBOutlet UILabel *threeRatings;
@property (weak, nonatomic) IBOutlet UILabel *fourRatings;
@property (weak, nonatomic) IBOutlet UILabel *fiveRatings;
@end
