//
//  OwnerServerDetailRatingsViewController.m
//  Restaurant
//
//  Created by Parth Pandya on 23/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "OwnerServerDetailRatingsViewController.h"
#import "Servers.h"
#import "OwnerServerRatingCell.h"

@interface OwnerServerDetailRatingsViewController ()

@end

@implementation OwnerServerDetailRatingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrRatings = [[NSMutableArray alloc] init];
    [self FetchProfileDetails];//OwnerServerDetailsAvailabel
    self.tblRatings.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.imgServer.layer.cornerRadius = self.imgServer.frame.size.height/2;
    self.imgServer.layer.masksToBounds = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewWillAppear:) name:@"OwnerServerDetailsAvailabel" object:nil];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    Servers *server = [[Servers allObjects] firstObject];
    
    self.starRating.value = server.avgRating;
    self.OneRatings.text = [NSString stringWithFormat:@"%ld",(long)server.oneRatings];
    self.twoRatings.text = [NSString stringWithFormat:@"%ld",(long)server.twoRatings];
    self.threeRatings.text = [NSString stringWithFormat:@"%ld",(long)server.threeRatings];
    self.fourRatings.text = [NSString stringWithFormat:@"%ld",(long)server.fourRatings];
    self.fiveRatings.text = [NSString stringWithFormat:@"%ld",(long)server.fiveRatings];
    [_imgServer sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,server.imageURL]]placeholderImage:[UIImage imageNamed:@"ic_user_b.png"]];
    _lblTotalRatings.text = [NSString stringWithFormat:@"%ld Ratings",(long)server.toalRatings];
    float TotalRatings = server.toalRatings;
    self.Progress1.progress = server.oneRatings/TotalRatings;
    self.Progress2.progress = server.twoRatings/TotalRatings;
    self.Progress3.progress = server.threeRatings/TotalRatings;
    self.Progress4.progress = server.fourRatings/TotalRatings;
    self.Progress5.progress = server.fiveRatings/TotalRatings;
//
}


- (void)FetchProfileDetails {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    
    AppUser *user = [[AppUser allObjects] firstObject];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"bearer %@",user.token] forHTTPHeaderField:@"Authorization"];
    
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?serverId=%@&page=%d",SITE_URL,GET_ALLSERVER_RATINGS,[[NSUserDefaults standardUserDefaults] valueForKey:@"ServerID"],pageIndex]];
    NSLog(@"%@",URL);
    [manager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        if ([responseObject isKindOfClass:[NSArray class]])
        {
            NSArray *jsonArray = (NSArray *)responseObject  ;
            NSLog(@"Json:%@",jsonArray);
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            });

        }
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dataDic = (NSDictionary *)responseObject;
            NSLog(@"%@",dataDic);
                
                serverDict = (NSDictionary *)responseObject;
                serverinfo = [[NSDictionary alloc]init];
                serverinfo = [serverDict valueForKey:@"data"];
                NSLog(@"%@",serverinfo);
                arrRatings = [serverDict valueForKey:@"data"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tblRatings reloadData];
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            });

            }
    }failure:^(NSURLSessionTask *operation, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        });
  
        NSLog(@"Error: %@", error);
    }];

    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return arrRatings.count;
}


- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    UITableViewCell *Cell;
    OwnerServerRatingCell *Cell = [[OwnerServerRatingCell alloc] init];
    NSDictionary *Dict = [arrRatings objectAtIndex:indexPath.row];
    Cell = [tableView dequeueReusableCellWithIdentifier:@"OwnerServerRatingCell" forIndexPath:indexPath];
    Cell.starRatings.value = [[Dict valueForKey:@"rating"] integerValue];
    Cell.lblDate.text = [NSString getFormattedDate:[Dict valueForKey:@"updatedAt"]];
    if ([[Dict valueForKey:@"name"] length] > 0)
    {
        Cell.lblRatedBy.text = [NSString upperCase:[NSString stringWithFormat:@"By : %@",[[Dict valueForKey:@"name"] capitalizedString]]];
    }
    else
    {
        Cell.lblRatedBy.text = [NSString stringWithFormat:@"By : Annonymus"];
    }
    
    
    
    return Cell;
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (arrRatings.count == 0)
    {
        return @"No Ratings";
    }
    return [NSString stringWithFormat:@"%lu Ratings",(unsigned long)arrRatings.count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


@end
