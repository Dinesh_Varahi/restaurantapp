//
//  OwnerServerDetailsPager.h
//  Restaurant
//
//  Created by Parth Pandya on 23/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OwnerServerDetailsPager : UIViewController <UIScrollViewDelegate,UITabBarDelegate>

@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;
- (IBAction)changePage:(id)sender;

- (void)previousPage;
- (void)nextPage;
@property (weak, nonatomic) IBOutlet UITabBar *tabBar;

@end
