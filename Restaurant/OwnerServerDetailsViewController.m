//
//  OwnerServerDetailsViewController.m
//  Restaurant
//
//  Created by Parth Pandya on 23/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "OwnerServerDetailsViewController.h"

@interface OwnerServerDetailsViewController ()

@end

@implementation OwnerServerDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"OwnerServerDetailProfileViewController"]];
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"OwnerServerDetailRatingsViewController"]];
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"OwnerServerDetailCommentsViewController"]];
}


@end
