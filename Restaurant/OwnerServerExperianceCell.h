//
//  OwnerServerExperianceCell.h
//  Restaurant
//
//  Created by Parth Pandya on 24/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OwnerServerExperianceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblCity;
@property (weak, nonatomic) IBOutlet UILabel *lblRestaurantName;
@property (weak, nonatomic) IBOutlet UILabel *lblRatings;
@property (weak, nonatomic) IBOutlet UILabel *lblComments;
@property (weak, nonatomic) IBOutlet UILabel *lblExperiance;

@end
