//
//  OwnerServerInfoCell.h
//  Restaurant
//
//  Created by Parth Pandya on 13/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OwnerServerDetailsViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface OwnerServerInfoCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>
    {
        NSDictionary *dictPerformers;
        NSMutableArray *arrName;
        NSMutableArray *arrImages;
        NSMutableArray *arrRatings;
        NSMutableArray *totalRatings;
        NSMutableArray *arrIds;
    }
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
