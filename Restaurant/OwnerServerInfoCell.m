//
//  OwnerServerInfoCell.m
//  Restaurant
//
//  Created by Parth Pandya on 13/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "OwnerServerInfoCell.h"
#import "ServerInfoCell.h"

@implementation OwnerServerInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    
    arrRatings = [[NSMutableArray alloc]init];
    arrImages = [[NSMutableArray alloc] init];
    arrName = [[NSMutableArray alloc] init];
    totalRatings = [[NSMutableArray alloc] init];
    arrIds = [[NSMutableArray alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(arrAssign:)
                                                 name:@"TopPerformer"
                                               object:nil];
    // Initialization code
}


-(void)arrAssign:(NSNotification *)notification
{
    //    arrPerformers = notification.userInfo;
    if ([notification.name isEqualToString:@"TopPerformer"])
    {
        dictPerformers = notification.userInfo;
        arrName = [dictPerformers valueForKey:@"name"];
        arrImages = [dictPerformers valueForKey:@"image"];
        arrRatings = [dictPerformers valueForKey:@"avgRating"];
        totalRatings = [dictPerformers valueForKey:@"totalRatings"];
        arrIds = [dictPerformers valueForKey:@"_idUserProfile"];
        [self.collectionView reloadData];
    }
    
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return arrName.count;

    
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ServerInfoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ServerinfoCell" forIndexPath:indexPath];
    cell.lblServerName.text = [NSString upperCase:[arrName objectAtIndex:indexPath.row]];
    cell.Ratings.value = [[arrRatings objectAtIndex:indexPath.row] integerValue];
    cell.lblRestoName.text = [NSString upperCase:[[NSUserDefaults standardUserDefaults] valueForKey:@"RestoName"]];
    cell.lblTotalRatings.text = [NSString stringWithFormat:@"%ld Ratings",(long)[[totalRatings objectAtIndex:indexPath.row] integerValue]];
    
    cell.imgServer.layer.cornerRadius = cell.imgServer.frame.size.height/2;
    cell.imgServer.layer.masksToBounds = YES;

    
    cell.imgServer1.layer.cornerRadius = cell.imgServer1.frame.size.height/2;
    cell.imgServer1.layer.masksToBounds = YES;

    [cell.imgServer sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,[arrImages objectAtIndex:indexPath.row]]]
                     placeholderImage:[UIImage imageNamed:@"ic_user_b.png"]];

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    [[NSUserDefaults standardUserDefaults] setValue:[arrIds objectAtIndex:indexPath.row] forKey:@"ServerID"];
     [[NSNotificationCenter defaultCenter] postNotificationName:@"goToServerDetailVC" object:nil];
}

@end
