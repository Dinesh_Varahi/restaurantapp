//
//  OwnerServerProfileCommentCell.h
//  Restaurant
//
//  Created by Parth Pandya on 24/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface OwnerServerProfileCommentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRatings;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblComment;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@end
