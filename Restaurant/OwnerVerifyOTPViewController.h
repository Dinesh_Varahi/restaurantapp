//
//  OwnerVerifyOTPViewController.h
//  Restaurant
//
//  Created by HN on 12/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "AppUser.h"
#import "OwnerDashboard.h"
#import "OwnerProfileStep1ViewController.h"

@interface OwnerVerifyOTPViewController : UIViewController<UITextFieldDelegate>
{
    
    __weak IBOutlet UITextField *txtOTP;
}

- (IBAction)btnSubmitPressed:(id)sender;
- (IBAction)btnResendOTPPressed:(id)sender;





@end
