//
//  OwnerVerifyOTPViewController.m
//  Restaurant
//
//  Created by HN on 12/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "OwnerVerifyOTPViewController.h"

@interface OwnerVerifyOTPViewController ()

@end

@implementation OwnerVerifyOTPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];

     [txtOTP setDelegate:self];
    
    
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


- (IBAction)btnSubmitPressed:(id)sender
{
    if((txtOTP.text.length <= 0)){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Please Enter OTP"
                                           message:@""                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:YES completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:YES completion:nil];
                
            });
        });
    }

    else
    {
    [APP huddie];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@",SITE_URL,VALIDATE_OWNER_ACCOUNT] parameters:nil error:nil];
    
    AppUser *user = [[AppUser allObjects] firstObject];
    NSString *token = user.token;
    
    NSLog(@"Token:%@",token);
    
    [req addValue:[NSString stringWithFormat:@"bearer %@",token] forHTTPHeaderField:@"Authorization"];
    [req addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSMutableDictionary *dataDict = [NSMutableDictionary new];
    
    [dataDict setValue:txtOTP.text forKey:@"otp"];
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict options:0 error:nil];
    NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSLog(@"OTP Verification Data :%@",jsonString);
    
    
    [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse *response, id data, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        //            double status = (long)[httpResponse statusCode];
        NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
        
        if (httpResponse.statusCode == 0) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertController * alert2=   [UIAlertController
                                               alertControllerWithTitle:@"Please try again"
                                               message:@"Error while connecting to Server"
                                               preferredStyle:UIAlertControllerStyleAlert];
                [APP.hud setHidden:YES];
                [self presentViewController:alert2 animated:NO completion:nil];
                
                
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [alert2 dismissViewControllerAnimated:NO completion:nil];
                });
            });
        }
        else if (httpResponse.statusCode == 202) {
            if ([data isKindOfClass:[NSDictionary class]]){
                
                NSDictionary *dataDic = (NSDictionary *)data;
                NSLog(@"Data:%@",dataDic);
                
                NSInteger status = [[dataDic valueForKey:@"status"] integerValue];
                if(status == 202)
                {
                    RLMRealm *realm = [RLMRealm defaultRealm];
                    AppUser *user = [[AppUser allObjects] firstObject];
                    
                    [realm beginWriteTransaction];
                    user.isVerified = 1;
                    [realm addObject:user];
                    [realm commitWriteTransaction];
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        UIAlertController * alert2=   [UIAlertController
                                                       alertControllerWithTitle:@"Account Verified"
                                                       message:[dataDic valueForKey:@"result"]
                                                       preferredStyle:UIAlertControllerStyleAlert];
                        [APP.hud setHidden:YES];
                        [self presentViewController:alert2 animated:NO completion:nil];
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                            
                            
                            
                            
                            [alert2 dismissViewControllerAnimated:NO completion:nil];
                            
                            [self navigationAfterValidation];
                            
                        });
                    });
                    
                }
                else if(status == 400)
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        UIAlertController * alert2=   [UIAlertController
                                                       alertControllerWithTitle:@"Sorry"
                                                       message:[dataDic valueForKey:@"result"]
                                                       preferredStyle:UIAlertControllerStyleAlert];
                        [APP.hud setHidden:YES];
                        [self presentViewController:alert2 animated:NO completion:nil];
                        
                        
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                            [alert2 dismissViewControllerAnimated:NO completion:nil];
                        });
                    });
                    
                }
                
            }
        }
        else if (httpResponse.statusCode == 400) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertController * alert2=   [UIAlertController
                                               alertControllerWithTitle:@"Please try again"
                                               message:@"Error while connecting to Server"
                                               preferredStyle:UIAlertControllerStyleAlert];
                [APP.hud setHidden:YES];
                [self presentViewController:alert2 animated:NO completion:nil];
                
                
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [alert2 dismissViewControllerAnimated:NO completion:nil];
                });
            });
        }
        
        
        
    }] resume];
    }
    
    
}

- (IBAction)btnResendOTPPressed:(id)sender
{
    
        {
        
        [APP huddie];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:[NSString stringWithFormat:@"%@%@",SITE_URL,RESEND_OTP] parameters:nil error:nil];
        
        AppUser *user = [[AppUser allObjects] firstObject];
        NSString *token = user.token;
        
        NSLog(@"Token:%@",token);
        
        [req addValue:[NSString stringWithFormat:@"bearer %@",token] forHTTPHeaderField:@"Authorization"];
        [req addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [req addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse *response, id data, NSError *error) {
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            //            double status = (long)[httpResponse statusCode];
            NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
            
            if ([httpResponse statusCode] == 202) {
                if ([data isKindOfClass:[NSDictionary class]]){
                    
                    NSDictionary *dataDic = (NSDictionary *)data;
                    NSLog(@"Data:%@",dataDic);
                    
                    NSInteger status = [[dataDic valueForKey:@"status"] integerValue];
                    if(status == 202)
                    {
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"OTP successfully sent "
                                                           message:[dataDic valueForKey:@"result"]
                                                           preferredStyle:UIAlertControllerStyleAlert];
                            [APP.hud setHidden:YES];
                            [self presentViewController:alert2 animated:NO completion:nil];
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, longAlertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                
                                
                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                                
                                
                                
                            });
                        });
                        
                    }
                    else if(status == 400)
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [APP.hud setHidden:YES];
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"Sorry"
                                                           message:[dataDic valueForKey:@"result"]
                                                           preferredStyle:UIAlertControllerStyleAlert];
                            
                            [self presentViewController:alert2 animated:NO completion:nil];
                            
                            
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                            });
                        });
                        
                    }
                    else if(status == 404)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [APP.hud setHidden:YES];
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"Sorry"
                                                           message:@"Could not connect to Server"
                                                           preferredStyle:UIAlertControllerStyleAlert];
                            
                            [self presentViewController:alert2 animated:NO completion:nil];
                            
                            
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                            });
                        });
                    }
                }
            }
            else if ([httpResponse statusCode] == 400)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [APP.hud setHidden:YES];
                    UIAlertController * alert2=   [UIAlertController
                                                   alertControllerWithTitle:@"Sorry"
                                                   message:@"Error while connecting to server"
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert2 animated:NO completion:nil];
                    
                    
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                    });
                });
            }
            else if ([httpResponse statusCode] == 404)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [APP.hud setHidden:YES];
                    UIAlertController * alert2=   [UIAlertController
                                                   alertControllerWithTitle:@"Sorry"
                                                   message:@"Could not connect to Server"
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert2 animated:NO completion:nil];
                    
                    
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                    });
                });
            }
            
            
        }] resume];
        
    }
    
    
    
    
}
- (void)navigationAfterValidation
{
    AppUser *user = [[AppUser allObjects] firstObject];
    
    if (user.isOwnerAccountUpdated == 0) {
        [APP.hud setHidden:YES];
        OwnerProfileStep1ViewController *ownerProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerProfileStep1ViewController"];
        
        ownerProfile.navigationItem.leftBarButtonItem = nil;
        ownerProfile.navigationItem.hidesBackButton = YES;
        [self.navigationController pushViewController:ownerProfile animated:NO];
    }
    else{
        [APP.hud setHidden:YES];
        OwnerDashboard *ownerLanding = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerDashboard"];
//            ownerLanding.navigationItem.leftBarButtonItem = nil;
            ownerLanding.navigationItem.hidesBackButton = YES;
        [self.navigationController pushViewController:ownerLanding animated:NO];
        
    }
    
}

@end
