//
//  ViewController.h
//  PageViewController
//
//  Created by Tom Fewster on 11/01/2012.
//

#import <UIKit/UIKit.h>


@interface PagerViewController : UIViewController <UIScrollViewDelegate,UITabBarDelegate>

@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;

- (IBAction)changePage:(id)sender;

- (void)previousPage;
- (void)nextPage;
@property (weak, nonatomic) IBOutlet UITabBar *tabBar;
@property (strong,nonatomic) UIBarButtonItem *editProfileButton;
@property (strong,nonatomic) UIBarButtonItem *btnLogOut;
@end
