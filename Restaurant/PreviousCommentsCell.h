//
//  PreviousCommentsCell.h
//  Restaurant
//
//  Created by Parth Pandya on 06/02/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreviousCommentsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgCustomer;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerComment;
@property (weak, nonatomic) IBOutlet UILabel *lblOwnerComment;
@property (weak, nonatomic) IBOutlet UILabel *lblOwnerName;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerName;
@property (weak, nonatomic) IBOutlet UIImageView *imgOwner;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerCommentDate;


@end
