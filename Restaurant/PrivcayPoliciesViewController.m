//
//  PrivcayPoliciesViewController.m
//  Restaurant
//
//  Created by Parth Pandya on 10/02/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "PrivcayPoliciesViewController.h"

@interface PrivcayPoliciesViewController ()

@end

@implementation PrivcayPoliciesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated   {
    
    self.title = @"Terms and Policies";
    [self setNavigationBar];
}

- (void)viewWillDisappear:(BOOL)animated    {
    
    self.title = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btnClosePressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)setNavigationBar{
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    
    self.navigationController.view.backgroundColor = [UIColor appMainColor];
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    
}
@end
