//
//  ProfileStep1ViewController.h
//  Restaurant
//
//  Created by HN on 16/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileStep2ViewController.h"
#import "Servers.h"
#import "step1InfoCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ProfileStep1ViewController : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,ProfileStep1Delegate>
{
    
    //step1InfoCell *Cell;
}



- (IBAction)btnSavePressed:(id)sender;

@property (strong, nonatomic) NSString *aSender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;



@end
