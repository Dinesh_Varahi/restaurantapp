//
//  ProfileStep1ViewController.m
//  Restaurant
//
//  Created by HN on 16/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "ProfileStep1ViewController.h"

@interface ProfileStep1ViewController ()
{
    NSString *userImageString;
    
    
    NSString *strFirstName;
    NSString *strMiddleName;
    NSString *strlastName;
    NSString *strNickName;
    NSString *strEmail;
    NSString *strCity;
    NSString *strZip;
    NSString *strQualification;
    NSString *strSSNumber;
    NSString *strMobileNumber;
    UITextField *activeField;
    BOOL imageSelected;
    
}

@end


@implementation ProfileStep1ViewController
@synthesize aSender = _aSender;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewWillAppear:) name:@"profileFetched1" object:nil];
    
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(selectPhotos)
                                                 name:@"CaptureImage"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveAndContinuePressed:) name:@"btnSavePressed" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(validateMobileAlert) name:@"validateMobile" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(validateMobileAlert) name:@"validateMobile1" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(validateDataAlert) name:@"validateData" object:nil];
    [[NSUserDefaults standardUserDefaults] setValue:@"true" forKey:@"isimageURL"];
    
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    [self loadServerInfo];
    [self registerForKeyboardNotifications];
   // [self setNavigationBar1];
    
    if ([self.aSender isEqualToString:@"Register"]) {
     //   [self setNavigationBar];
    }
}

- (void)loadServerInfo {
    [self.tableView reloadData];
}

- (void)setNavigationBar1 {

    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    
    self.navigationController.view.backgroundColor = [UIColor appMainColor];
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor appMainColor]];

}

- (void)resetNavigationBar {
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    
    self.navigationController.view.backgroundColor = [UIColor appMainColor];
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor appMainColor]];
}
- (void)setNavigationBar{
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    
    self.navigationController.view.backgroundColor = [UIColor appMainColor];
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor appMainColor]];
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)selectPhotos {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Photos" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
        picker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
        
        [self presentViewController:picker animated:NO completion:nil];
        
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
        
        [self presentViewController:picker animated:NO completion:nil];
        
    }]];
    
    [self presentViewController:actionSheet animated:NO completion:nil];
    
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    
    UIImage *originalImage =  info[UIImagePickerControllerOriginalImage];
    
    UIImage *tmpImage = [self resizeImage:originalImage];
    
    userImageString = [self encodeToBase64String:tmpImage];    
    
    imageSelected = YES;
    
    NSString *str = @"data:image/jpg;base64,";
    str = [str stringByAppendingString:userImageString];
    
    Servers *aServer = [[Servers allObjects] firstObject];
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    aServer.imageURL = userImageString;
    [[NSUserDefaults standardUserDefaults] setValue:@"false" forKey:@"isimageURL"];
    [realm commitWriteTransaction];
    
    //Cell.imgProfilePic.image = originalImage;
    [self.tableView reloadData];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}
- (UIImage *)resizeImage:(UIImage *)image {
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 300.0;
    float maxWidth = 400.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
    
}

-(void)showAlert {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIAlertController * alert2=   [UIAlertController
                                       alertControllerWithTitle:@"Please Enter All Information"
                                       message:@"Information can not be Empty"                                           preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert2 animated:NO completion:nil];
        
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [alert2 dismissViewControllerAnimated:NO completion:nil];
            
        });
        
    });
}
- (void)validateMobileAlert
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIAlertController * alert2=   [UIAlertController
                                       alertControllerWithTitle:@"Please enter valid Mobile Number"
                                       message:@""                                           preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert2 animated:NO completion:nil];
        
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [alert2 dismissViewControllerAnimated:NO completion:nil];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"setMobileNumberFR" object:nil];
        });
        
    });
}

-(void) validateDataAlert
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIAlertController * alert2=   [UIAlertController
                                       alertControllerWithTitle:@"Please Enter All Information"
                                       message:@"Information should not be Empty"                                           preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert2 animated:NO completion:nil];
        
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [alert2 dismissViewControllerAnimated:NO completion:nil];
            
        });
    });
}
- (void)saveAndContinuePressed:(NSNotification *)notification
{
    
    Servers *server = [[Servers allObjects]firstObject];
    NSLog(@"server Info:%@",server);
    
    if((server.firstName.length <= 0)  || (server.lastName.length <= 0) ||
       (server.nickName.length <= 0) || (server.email.length <= 0) || (server.city.length <= 0) || (server.zipCode.length <= 0)|| (server.mobileNumber.length <= 0)){
        
        [self validateDataAlert];
        
    }
    else {
        
        ProfileStep2ViewController *stepTwoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileStep2ViewController"];
        
        
        [self.navigationController pushViewController:stepTwoVC animated:NO];
        
    }
    
}
-(void)redirectToProfile2 {
    ProfileStep2ViewController *stepTwoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileStep2ViewController"];
    //stepTwoVC.serverInfo = tmpDict;
    
    [self.navigationController pushViewController:stepTwoVC animated:NO];

}

- (IBAction)btnSavePressed:(id)sender {
    
}


#pragma mark - Tableview Delegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    step1InfoCell *Cell = [[step1InfoCell alloc]init];
    
    static NSString *simpleTableIdentifier = @"step1Cell";
    Cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
    
    Cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    Cell.delegate = self;
    
    Servers *newServer = [[Servers allObjects] firstObject];
    
    NSLog(@"Profile View: Server Info:%@",newServer);
    
//    [Cell.txtFirstName setDelegate:self];
    Cell.txtFirstName.text = newServer.firstName;
    strFirstName = newServer.firstName;
    
//    [Cell.txtMiddleName setDelegate:self];
    Cell.txtMiddleName.text = newServer.middleName;
    strMiddleName = newServer.middleName;
    
//    [Cell.txtLastName setDelegate:self];
    Cell.txtLastName.text = newServer.lastName;
    strlastName = newServer.lastName;
    
//    [Cell.txtNickName setDelegate:self];
    Cell.txtNickName.text = newServer.nickName;
    strNickName = newServer.nickName;
    
//    [Cell.txtEmail setDelegate:self];
    Cell.txtEmail.text = newServer.email;
    strEmail = newServer.email;
    
//    [Cell.txtCity setDelegate:self];
    Cell.txtCity.text = newServer.city;
    strCity = newServer.city;
    
//    [Cell.txtZip setDelegate:self];
    Cell.txtZip.text = newServer.zipCode;
    strZip = newServer.zipCode;
    
//    [Cell.txtQualification setDelegate:self];
    Cell.txtQualification.text = newServer.qualification;
    strQualification = newServer.qualification;
    
    Cell.txtSSNumber.text = newServer.ssNumber;
    
    Cell.txtMobileNumber.text = newServer.mobileNumber;
    // Assign Save Button
    
//    [Cell.btnSaveAndContinue addTarget:self action:@selector(btnSavePressed:) forControlEvents:UIControlEventTouchUpInside];
    
    // Assigning Image
    
    if (imageSelected) {
        
     
        NSLog(@"Image Data:%@",userImageString);
  
        NSData *data = [[NSData alloc]initWithBase64EncodedString:userImageString options:NSDataBase64DecodingIgnoreUnknownCharacters];
        
        
        UIImage* image = [[UIImage alloc] initWithData:data];
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (Cell.tag == indexPath.row) {
                    
                    Cell.imgProfilePic.image = image;
                    Cell.imgProfilePic.layer.cornerRadius = Cell.imgProfilePic.layer.frame.size.width / 2;
                    Cell.imgProfilePic.layer.masksToBounds = YES;
                    Cell.imgProfilePic.contentMode = UIViewContentModeScaleAspectFill;
                    
//                    [Cell setNeedsLayout];
                }
            });
        }
        else{
            if (Cell.tag == indexPath.row) {
                Cell.imgProfilePic.image = [UIImage imageNamed:@"ic_user_b.png"];
                Cell.imgProfilePic.layer.cornerRadius = Cell.imgProfilePic.layer.frame.size.width / 2;
                Cell.imgProfilePic.layer.masksToBounds = YES;
                Cell.imgProfilePic.contentMode = UIViewContentModeScaleAspectFill;

//                [Cell setNeedsLayout];
            }
            
            
        }
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^(void) {
        });
        
    }
    else {
        
        [Cell.imgProfilePic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,newServer.imageURL]]
                                 placeholderImage:[UIImage imageNamed:@"ic_user_b.png"]];
        
        
//        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,newServer.imageURL]];
//        
//        NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//            if (data) {
//                UIImage *image = [UIImage imageWithData:data];
//                if (image) {
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        step1InfoCell *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
//                        if (updateCell)
//                        updateCell.imgProfilePic.image = image;
//                    });
//                }
//                else {
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        step1InfoCell *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
//                                            if (updateCell)
//                                            updateCell.imgProfilePic.image = [UIImage imageNamed:@"ic_user_b.png"];
//                    });
//                }
//            }
//        }];
//        [task resume];
        
    }
    
   
    return Cell;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
     [tableView deselectRowAtIndexPath:indexPath animated:NO];
}
    

// First Name
- (void)step1Cell:(step1InfoCell *)view didEndEditingFirstNameTextField:(UITextField *)firstName {
    
    strFirstName = firstName.text;
}
// Middle Name
- (void)step1Cell:(step1InfoCell *)view didEndEditingMiddleNameTextField:(UITextField *)middleName {
    strMiddleName = middleName.text;
}

// Last Name
- (void)step1Cell:(step1InfoCell *)view didEndEditingLastNameTextField:(UITextField *)lastName {
    
    strlastName = lastName.text;
}

// Nick Name
- (void)step1Cell:(step1InfoCell *)view didEndEditingNickNameTextField:(UITextField *)nickName {
    
    strNickName = nickName.text;
    
}

// Email
-(void)step1Cell:(step1InfoCell *)view didEndEditingEmailTextField:(UITextField *)email {
    
    strEmail = email.text;
}

// City
- (void)step1Cell:(step1InfoCell *)view didEndEditingCityNameTextField:(UITextField *)city {
    
    strCity = city.text;
    
    
}

// ZIP
- (void)step1Cell:(step1InfoCell *)view didEndEditingZipTextField:(UITextField *)zip {
    strZip = zip.text;
}

// Qualification
- (void)step1Cell:(step1InfoCell *)view didEndEditingQualificationTextField:(UITextField *)qualification {
    strQualification = qualification.text;
}

// SSNumber
- (void)step1Cell:(step1InfoCell *)view didEndEditingSSNumberTextField:(UITextField *)ssNumber {
    
    strSSNumber = ssNumber.text;
}

// MobileNumber
- (void)step1Cell:(step1InfoCell *)view didEndEditingMobileNumberTextField:(UITextField *)mobileNumber {
    strMobileNumber = mobileNumber.text;
    
}
#pragma mark - Textfield Delegate methods
- (void)registerForKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWasShown:)
     
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillBeHidden:)
     
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    
    
}

// Called when the UIKeyboardDidShowNotification is sent.

- (void)keyboardWasShown:(NSNotification*)aNotification {
    
    NSDictionary* info = [aNotification userInfo];
    
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    
    _tableView.contentInset = contentInsets;
    
    _tableView.scrollIndicatorInsets = contentInsets;
    
    
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    
    // Your app might not need or want this behavior.
    
    CGRect aRect = self.view.frame;
    
    aRect.size.height -= kbSize.height;
    
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        
        [self.tableView scrollRectToVisible:activeField.frame animated:YES];
        
    }
    
}

// Called when the UIKeyboardWillHideNotification is sent

- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    _tableView.contentInset = contentInsets;
    
    _tableView.scrollIndicatorInsets = contentInsets;
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    activeField = textField;
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    activeField = nil;
    
}
@end
