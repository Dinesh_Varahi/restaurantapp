//
//  ProfileStep2ViewController.h
//  Restaurant
//
//  Created by HN on 16/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Servers.h"
#import "AppUser.h"
#import "AppDelegate.h"
#import "ServerWorkExperience.h"
#import "ActionSheetStringPicker.h"
#import "ActionSheetDatePicker.h"
#import "ServerLandingViewController.h"
#import "ExperianceCell.h"
#import "ServerWorkExperience.h"
#import "textFieldCell.h"
#import "ApplyForJobCell.h"


@interface ProfileStep2ViewController : UIViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
{
    
    __weak IBOutlet UIBarButtonItem *btnUpdate;
    __weak IBOutlet UITextField *txtRestaurantName;
    __weak IBOutlet UITextField *txtRestaurantType;
    __weak IBOutlet UITextField *txtWorkProfile;
    __weak IBOutlet UITextField *txtCity;
    __weak IBOutlet UITextField *txtZipCode;
    __weak IBOutlet UIButton *btnAddJob;
    __weak IBOutlet UIButton *btnApplyForJob;
    __weak IBOutlet UITextField *txtFromDate;
    __weak IBOutlet UITextField *txtToDate;
    __weak IBOutlet UISwitch *swCurrentJob;
    UITextField *activeField;
    
    RLMResults<ServerWorkExperience  *> *arrServerWorkExperience;
}

- (IBAction)swCurretJobToggled:(id)sender;
- (IBAction)btnAddJobPressed:(id)sender;
- (IBAction)btnApplyForJobPressed:(id)sender;
- (IBAction)btnUpdateProfilePressed:(id)sender;
- (IBAction)btnSerlectRestaurant:(id)sender;

@property (strong,nonatomic) NSMutableDictionary *serverInfo;
@property (weak, nonatomic) IBOutlet UITableView *tblExperiance;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableviewHeight;




@end
