//
//  ProfileStep2ViewController.m
//  Restaurant
//
//  Created by HN on 16/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "ProfileStep2ViewController.h"
#import "Restaurants.h"
#import "ApplyForJobViewController.h"

@interface ProfileStep2ViewController ()
{
    NSString *strCurrentJob;
    NSMutableArray *restaurantNamesArray;
    
    NSString *fromDateString;
    NSString *toDateString;
    NSInteger restaurantID;
    BOOL isSelectedFromList;
}
@property (nonatomic, strong) RLMResults *ServerWxperience;
@property (nonatomic, strong) RLMResults *restaurantList;
@property (nonatomic, assign) NSInteger selectedIndex;
@end

@implementation ProfileStep2ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"Step1 Info:%@",self.serverInfo);
    
    [btnUpdate setTitleTextAttributes:@{
                                        NSFontAttributeName:[UIFont systemFontOfSize:13.0 weight:UIFontWeightMedium],
                                        NSForegroundColorAttributeName: [UIColor whiteColor]
                                        } forState:UIControlStateNormal];
    _ServerWxperience = [ServerWorkExperience allObjects];
    _restaurantList = [Restaurants allObjects];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tableReload)
                                                 name:@"reloadTable"
                                               object:nil];//ApplyForJobPressed
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applyForJob)
                                                 name:@"ApplyForJobPressed"//fillAllTheData
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(alertShow)
                                                 name:@"fillAllTheData"//actionSheet
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(ActionSheet)
                                                 name:@"actionSheet"                                               object:nil];//RestaurantTypes
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(btnUpdateProfilePressed:)
                                                 name:@"SaveButtonPressed"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(restaurantSelected:)
                                                 name:@"restoSelected"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(restaurantType)
                                                 name:@"RestaurantTypes"
                                               object:nil];//DateNotify
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(DateAlert)
                                                 name:@"DateNotify"
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dateAlert)
                                                 name:@"DateNotify"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(ZIPAlert)
                                                 name:@"ZIPNotify"
                                               object:nil];


    
    
    
    NSLog(@"Experience:%@",_ServerWxperience);
    
     self.navigationController.navigationBar.barTintColor = [UIColor appMainColor];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    [txtRestaurantName setDelegate:self];
    [txtRestaurantType setDelegate:self];
    [txtWorkProfile setDelegate:self];
    [txtCity setDelegate:self];
    [txtZipCode setDelegate:self];
    [txtFromDate setDelegate:self];
    [txtToDate setDelegate:self];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    _tableviewHeight.constant = self.view.frame.size.height;
    
    //    NSString *str = @"YES";
    //
    //    ServerWorkExperience *aExperience = [[ServerWorkExperience objectsWhere:@"isCurrentJob = %@",str] firstObject];
    //
    //    txtRestaurantName.text = aExperience.restoName;
    //    txtRestaurantType.text = aExperience.restoType;
    //    txtWorkProfile.text = aExperience.workProfile;
    //    txtCity.text = aExperience.city;
    //    txtZipCode.text = aExperience.zipCode;
    //    txtFromDate.text = aExperience.fromDate;
    //    txtToDate.text = aExperience.toDate;
    //
    //    if ([aExperience.isCurrentJob isEqualToString:@"YES"]) {
    //
    //        [swCurrentJob setOn:YES];
    //
    //    }
    //
    //    [txtRestaurantName addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    //
    
}

-(void)ZIPAlert
{
    UIAlertController * alert2=   [UIAlertController
                                   alertControllerWithTitle:@"Attention"
                                   message:@"Enter Valid ZIP code."
                                   preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert2 animated:NO completion:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert2 dismissViewControllerAnimated:NO completion:nil];
    });

}

-(void)DateAlert
{
    UIAlertController * alert2=   [UIAlertController
                                   alertControllerWithTitle:@"Attention"
                                   message:@"From date could not be greater than To date."
                                   preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert2 animated:NO completion:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert2 dismissViewControllerAnimated:NO completion:nil];
    });

}

-(void)alertShow
{
    UIAlertController * alert2=   [UIAlertController
                                   alertControllerWithTitle:@"Attention"
                                   message:@"Kindly complete all the information"
                                   preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert2 animated:NO completion:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert2 dismissViewControllerAnimated:NO completion:nil];
    });
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"fillAllTheData"
//                                                  object:nil];
}

-(void)applyForJob
{
    ApplyForJobViewController *applyForJobVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ApplyForJobViewController"];
    
    [self.navigationController pushViewController:applyForJobVC animated:NO];
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"ApplyForJobPressed"
//                                                  object:nil];
//    
}

- (void)textDidChange:(id)sender {
    
    UITextField* searchField = (UITextField *) sender;
    
    
    if(searchField == txtRestaurantName) {
        
        if(searchField.text.length == 0)
        {
            
            
        }
        else {
            
        }
        
    }
}

- (void)dateAlert{
    {
        UIAlertController * alert2=   [UIAlertController
                                       alertControllerWithTitle:@"Attention"
                                       message:@"From date could not be greater than To date."
                                       preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert2 animated:NO completion:nil];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [alert2 dismissViewControllerAnimated:NO completion:nil];
        });
        
    }
}
-(void)restaurantType{
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
   
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [actionSheet dismissViewControllerAnimated:NO completion:nil];
        [self.view endEditing:YES];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Ethnic" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [[NSUserDefaults standardUserDefaults]setValue:@"Ethnic" forKey:@"restaurantTypeSelected"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"restaurantTypeSelected" object:self];
        
        [self.view endEditing:YES];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Fast Food" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [[NSUserDefaults standardUserDefaults]setValue:@"Fast Food" forKey:@"restaurantTypeSelected"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"restaurantTypeSelected" object:self];
        
        [self.view endEditing:YES];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Fast Casual" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [[NSUserDefaults standardUserDefaults]setValue:@"Fast Casual" forKey:@"restaurantTypeSelected"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"restaurantTypeSelected" object:self];
        
        [self.view endEditing:YES];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Casual Dinning" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [[NSUserDefaults standardUserDefaults]setValue:@"Casual Dinning" forKey:@"restaurantTypeSelected"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"restaurantTypeSelected" object:self];
        
        [self.view endEditing:YES];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Family Style" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [[NSUserDefaults standardUserDefaults]setValue:@"Family Style" forKey:@"restaurantTypeSelected"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"restaurantTypeSelected" object:self];
        
        [self.view endEditing:YES];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Fine Dinning" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [[NSUserDefaults standardUserDefaults]setValue:@"Fine Dinning" forKey:@"restaurantTypeSelected"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"restaurantTypeSelected" object:self];
        
        [self.view endEditing:YES];
    }]];

    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cafe" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [[NSUserDefaults standardUserDefaults]setValue:@"Cafe" forKey:@"restaurantTypeSelected"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"restaurantTypeSelected" object:self];
        
        [self.view endEditing:YES];
    }]];

    
    [_tblExperiance endEditing:YES];
    [self presentViewController:actionSheet animated:NO completion:nil];
}

-(void)ActionSheet
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    textFieldCell *cell = [[textFieldCell alloc]init];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [actionSheet dismissViewControllerAnimated:NO completion:nil];
//        [self dismissViewControllerAnimated:YES completion:nil];
       // [self.view endEditing:YES];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Server" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [[NSUserDefaults standardUserDefaults]setValue:@"Server" forKey:@"workProfile"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"workProfile" object:self];
        
        cell.txtWorkProfile.text = @"Server";
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Bartender" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [[NSUserDefaults standardUserDefaults]setValue:@"Bartender" forKey:@"workProfile"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"workProfile" object:self];
        
        cell.txtWorkProfile.text = @"Bartender";
        
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"BusBoy" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [[NSUserDefaults standardUserDefaults]setValue:@"BusBoy" forKey:@"workProfile"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"workProfile" object:self];
        
        cell.txtWorkProfile.text = @"BusBoy";
        
    }]];
    
    [self presentViewController:actionSheet animated:NO completion:nil];
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"actionSheet"
//                                                  object:nil];
}

- (void)restaurantSelected:(NSNotification *)notification {
    
    NSString *restroID = [notification object];
    if (restroID.length > 0 ) {
        
        restaurantID = [restroID integerValue];
        NSLog(@"Selected Restro:%ld",(long)restaurantID);
    }
   
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"restoSelected"
//                                                  object:nil];
//    
    
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    activeField = textField;
    
    if (textField == txtWorkProfile) {
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            [self dismissViewControllerAnimated:NO completion:nil];
            
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Server" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            txtWorkProfile.text = @"Server";
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Bartender" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            txtWorkProfile.text = @"Bartender";
            
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"BusBoy" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            txtWorkProfile.text = @"BusBoy";
        }]];
        
        [self.view endEditing:YES];
        [self presentViewController:actionSheet animated:NO completion:nil];

    }
}
 
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)swCurretJobToggled:(id)sender {
    
    UISwitch *mySwitch = (UISwitch *)sender;
    
    if ([mySwitch isOn]) {
        
        strCurrentJob = @"YES";
        
    } else {
        strCurrentJob = @"NO";
        
    }
    
    
}

-(void)tableReload {
    [_tblExperiance reloadData];
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"reloadTable"
//                                                  object:nil];
}

- (IBAction)btnAddJobPressed:(id)sender {
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    ServerWorkExperience *Experiance = [[ServerWorkExperience alloc]init];
    
    [realm beginWriteTransaction];
    
    Experiance.restoName = txtRestaurantName.text;
    Experiance.restoType = txtRestaurantType.text;
    Experiance.workProfile = txtWorkProfile.text;
    Experiance.city = txtCity.text;
    Experiance.zipCode = txtZipCode.text;
    Experiance.fromDate = txtFromDate.text;
    Experiance.toDate = txtToDate.text;
    Experiance.workingRestroID = restaurantID;
    
    [realm addObject:Experiance];
    
    [realm commitWriteTransaction];
    
}

- (IBAction)btnApplyForJobPressed:(id)sender {
}


- (IBAction)btnUpdateProfilePressed:(id)sender {
    
    //    RLMRealm *realm = [RLMRealm defaultRealm];
    //    NSError *error;
    [APP huddie];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@",SITE_URL,UPDATE_SERVER_PROFILE] parameters:nil error:nil];
    
    AppUser *user = [[AppUser allObjects] firstObject];
    NSString *token = user.token;
    
    NSLog(@"Token:%@",token);
    
    [req addValue:[NSString stringWithFormat:@"bearer %@",token] forHTTPHeaderField:@"Authorization"];
    [req addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSMutableArray *expArray = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < [_ServerWxperience count]; i++)
    {
        ServerWorkExperience *workExp = [_ServerWxperience objectAtIndex:i];
        
        NSLog(@"Exp:%@",workExp);
        NSMutableDictionary *expDict = [NSMutableDictionary new];
        [expDict setValue:workExp.isCurrentJob forKey:@"isCurrentJob"];
        [expDict setValue:workExp.restoName forKey:@"restaurantName"];
        [expDict setValue:workExp.restoType forKey:@"restaurantType"];
        [expDict setValue:workExp.workProfile forKey:@"workProfile"];
        [expDict setValue:workExp.city forKey:@"city"];
        [expDict setValue:workExp.zipCode forKey:@"zip"];
        [expDict setValue:[NSString stringWithFormat:@"%ld",(long)workExp.workingRestroID] forKey:@"_idWorksInRestaurant"];
        [expDict setValue:workExp.fromDate forKey:@"fromDate"];
        [expDict setValue:workExp.toDate forKey:@"toDate"];
        
        [expArray addObject:expDict];
    }
    
    Servers *aServer = [[Servers allObjects] firstObject];
    
    NSMutableDictionary *dataDict = [NSMutableDictionary new];
    
    [dataDict setValue:[NSString stringWithFormat:@"%ld",(long)aServer.serverID] forKey:@"_ServerId"];
    [dataDict setValue:[self.serverInfo valueForKey:@"firstName"] forKey:@"fName"];
    [dataDict setValue:[self.serverInfo valueForKey:@"middleName"] forKey:@"mName"];
    [dataDict setValue:[self.serverInfo valueForKey:@"lastName"] forKey:@"lName"];
    [dataDict setValue:[self.serverInfo valueForKey:@"nickName"] forKey:@"nickName"];
    [dataDict setValue:[self.serverInfo valueForKey:@"zip"] forKey:@"zip"];
    [dataDict setValue:[self.serverInfo valueForKey:@"city"] forKey:@"city"];
    [dataDict setValue:[self.serverInfo valueForKey:@"mobileNumber"] forKey:@"mobileNo"];
    
    [dataDict setValue:aServer.firstName forKey:@"fName"];
    [dataDict setValue:aServer.middleName forKey:@"mName"];
    [dataDict setValue:aServer.lastName forKey:@"lName"];
    [dataDict setValue:aServer.nickName forKey:@"nickName"];
    [dataDict setValue:aServer.zipCode forKey:@"zip"];
    
            NSString *location;
            location = [[AddressResolver sharedInstance] validateAddress:aServer.city];
    [dataDict setValue:location forKey:@"city"];
    [dataDict setValue:aServer.mobileNumber forKey:@"mobileNo"];
    
    
    if (![aServer.qualification isEqualToString:@""]) {
        [dataDict setValue:aServer.qualification forKey:@"qualification"];
    }
    if (![aServer.ssNumber isEqualToString:@""]) {
        [dataDict setValue:aServer.ssNumber forKey:@"securityNumber"];
    }
    if (![aServer.imageURL isEqualToString:@""]) {
        [dataDict setValue:aServer.imageURL forKey:@"image"];
        [dataDict setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"isimageURL"] forKey:@"isimageURL"];
    }
    
    [dataDict setValue:expArray forKey:@"experience"];
    
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict options:0 error:nil];
    NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSLog(@"Updated Profile :%@",jsonString);
    
    
    [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse *response, id data, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        //            double status = (long)[httpResponse statusCode];
        NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
        
        if ([data isKindOfClass:[NSDictionary class]]){
            
            NSDictionary *dataDic = (NSDictionary *)data;
            NSLog(@"Data:%@",dataDic);
            
            NSInteger status = [[dataDic valueForKey:@"status"] integerValue];
            if(status == 202)
            {
                [self fetchUpdatedProfile];
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController * alert2=   [UIAlertController
                                                   alertControllerWithTitle:@"Profile Updated"
                                                   message:[dataDic valueForKey:@"result"]
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert2 animated:NO completion:nil];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [APP.hud setHidden:YES];
                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                        
                        //if ([_viewSender isEqualToString:@"registration" ]) {
                            
                            ServerLandingViewController *landingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"navLanding"];
//                            [self.navigationController pushViewController:landingVC animated:YES];
                        [self.navigationController presentViewController:landingVC animated:NO completion:nil];
                        
                        
                        //}
//                        //else {
//                            for (UIViewController *controller in self.navigationController.viewControllers) {
//                                
//                                if ([controller isKindOfClass:[ServerLandingViewController class]]) {
//                                    [self.navigationController popToViewController:controller animated:YES];
//                                    break;
//                                }
                           // }
                       // }
                        
                    });
                });
                
            }
            else if(status == 400)
            {
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [APP.hud setHidden:YES];
                    UIAlertController * alert2=   [UIAlertController
                                                   alertControllerWithTitle:@"Sorry"
                                                   message:[dataDic valueForKey:@"result"]
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert2 animated:NO completion:nil];
                    
                    
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                    });
                });
                
            }
            
        }
        
    }] resume];
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"SaveButtonPressed"
//                                                  object:nil];
//    
}



- (void)fetchUpdatedProfile {
    
    AppUser *user = [[AppUser allObjects] firstObject];
    
    [[LocalData sharedInstance] FetchServerProfile:user.token completion:^(BOOL result) {
        
        if (result)
        {
            NSLog(@"Login: Server Profile Fetched");
            
            
        }
        else{
            NSLog(@"Login: Error while fetching Server Profile");
            
        }
        
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    arrServerWorkExperience = [ServerWorkExperience allObjects];
    if (arrServerWorkExperience.count > 0) {
        //_tableviewHeight.constant = 80.0f;
    }
    return [arrServerWorkExperience count] + 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0)
    {
        textFieldCell *Cell = [[textFieldCell alloc]init];
        static NSString *simpleTableIdentifier = @"textFieldCell";
        Cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        if (arrServerWorkExperience.count > 0)
        {
            Cell.lblPreviousEmployer.hidden = NO;
        }
        return Cell;
    }
    else if (indexPath.row > 0 && indexPath.row <= [arrServerWorkExperience count])
    {
        ExperianceCell *Cell = [[ExperianceCell alloc]init];
        static NSString *simpleTableIdentifier = @"ExperianceCell";
        Cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        
        Cell.btnEdit.tag = indexPath.row - 1;
//        Cell.lblRestaurantName.text = [[arrServerWorkExperience objectAtIndex:indexPath.row - 1] valueForKey:@"restoName"];

        if ([[[arrServerWorkExperience objectAtIndex:indexPath.row - 1] valueForKey:@"toDate"] isEqualToString:@""])
        {
            Cell.lblFromToDate.text = [NSString stringWithFormat:@"%@ to %@",[[arrServerWorkExperience objectAtIndex:indexPath.row - 1] valueForKey:@"fromDate"],@"Present"];
        }
        else{

        Cell.lblFromToDate.text = [NSString stringWithFormat:@"%@ to %@",[[arrServerWorkExperience objectAtIndex:indexPath.row - 1] valueForKey:@"fromDate"],[[arrServerWorkExperience objectAtIndex:indexPath.row - 1] valueForKey:@"toDate"]];

        }
//        Cell.lblWorkProfile.text = [[arrServerWorkExperience objectAtIndex:indexPath.row - 1] valueForKey:@"workProfile"];
        
//        Cell.lblLocation.text = [[arrServerWorkExperience objectAtIndex:indexPath.row - 1] valueForKey:@"city"];
        
        Cell.lblRestaurantName.text = [NSString upperCase:[[arrServerWorkExperience objectAtIndex:indexPath.row - 1] valueForKey:@"restoName"]];
        
        Cell.lblWorkProfile.text = [NSString upperCase:[[arrServerWorkExperience objectAtIndex:indexPath.row - 1] valueForKey:@"workProfile"]];
        Cell.lblLocation.text = [NSString upperCase:[[arrServerWorkExperience objectAtIndex:indexPath.row - 1] valueForKey:@"city"]];
        
        
        return Cell;
    }
    else
    {
        ApplyForJobCell *Cell = [[ApplyForJobCell alloc]init];
        static NSString *simpleTableIdentifier = @"ApplyForJobCell";
        Cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        return Cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 443;
    }
    return 80;
}


#pragma mark - Textfield Delegate methods
- (void)registerForKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWasShown:)
     
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillBeHidden:)
     
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    
    
}

// Called when the UIKeyboardDidShowNotification is sent.

- (void)keyboardWasShown:(NSNotification*)aNotification {
    
    NSDictionary* info = [aNotification userInfo];
    
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    
    _tblExperiance.contentInset = contentInsets;
    
    _tblExperiance.scrollIndicatorInsets = contentInsets;
    
    
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    
    // Your app might not need or want this behavior.
    
    CGRect aRect = self.view.frame;
    
    aRect.size.height -= kbSize.height;
    
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        
        [self.tblExperiance scrollRectToVisible:activeField.frame animated:YES];
        
    }
    
}

// Called when the UIKeyboardWillHideNotification is sent

- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    _tblExperiance.contentInset = contentInsets;
    
    _tblExperiance.scrollIndicatorInsets = contentInsets;
    
}



-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    activeField = nil;
    
}


@end
