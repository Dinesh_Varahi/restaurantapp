//
//  RatingSuccessAlertViewController.h
//  Restaurant
//
//  Created by HN on 20/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "LoginView.h"
#import <QuartzCore/QuartzCore.h>



@protocol SuccessAlertDelegate<NSObject>
@required
- (void)dismissAlert;

@end



@interface RatingSuccessAlertViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate>
{
    
    __weak IBOutlet UIView *alertBackground;
    UIActivityIndicatorView *activityView;
    Servers *newServer;
   
}

- (IBAction)closeButtonPressed:(id)sender;

- (IBAction)noButtonPressed:(id)sender;
- (IBAction)submitButtonPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblAlertMessage;
@property (strong, nonatomic) IBOutlet UILabel *lblAlertTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtComment;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;

@property (strong,nonatomic) NSString *alertTitle;
@property (strong,nonatomic) NSString *alertMessage;
@property (strong,nonatomic) NSString *aSender;
@property (weak,nonatomic) NSString *serverID;
@property (weak, nonatomic) NSString *isAlreadyCommented;

@property (weak, nonatomic) IBOutlet UIImageView *starImage1;
@property (weak, nonatomic) IBOutlet UIImageView *starImage2;
@property (weak, nonatomic) IBOutlet UIImageView *starImage3;
@property (weak, nonatomic) IBOutlet UIImageView *starImage4;
@property (weak, nonatomic) IBOutlet UIImageView *starImage5;
@property (weak, nonatomic) IBOutlet UITextView *commentView;

@property (nonatomic,weak) id<SuccessAlertDelegate> delegate;
@end

