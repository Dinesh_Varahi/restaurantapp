//
//  RatingSuccessAlertViewController.m
//  Restaurant
//
//  Created by HN on 20/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "RatingSuccessAlertViewController.h"

@interface RatingSuccessAlertViewController ()
{
    
    CGFloat animatedDistance;
}
@property (strong, nonatomic) NSArray<UIImageView *> *imageViewArray;
@end

@implementation RatingSuccessAlertViewController
@synthesize aSender = _aSender, alertTitle, alertMessage;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    alertBackground.layer.cornerRadius = 10;
    

    self.lblAlertTitle.text = alertTitle;
    self.lblAlertMessage.text = alertMessage;

//    
    [self.commentView setDelegate:self];

    

    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    _commentView.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"commentText"];
        
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"commentText"];
    _lblMessage.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"message"];
    
    NSInteger ratingValue = [[[NSUserDefaults standardUserDefaults]valueForKey:@"givenRatings"] intValue];
    
    self.imageViewArray = @[self.starImage1,self.starImage2,self.starImage3,self.starImage4,self.starImage5];
    
    [self.imageViewArray enumerateObjectsUsingBlock:^(UIImageView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj setImage:(idx < (NSInteger)ratingValue ? [UIImage imageNamed:@"star_big_red.png"] : [UIImage imageNamed:@"star_big_gray.png"])];
    }];

    _commentView.layer.borderWidth = 1.5f;
    [_commentView.layer setCornerRadius:10.0f];
    _commentView.layer.borderColor = [[UIColor textFieldBorderColor] CGColor];
    
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    NSRange resultRange = [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet] options:NSBackwardsSearch];
    if ([text length] == 1 && resultRange.location != NSNotFound) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView {
    CGRect textFieldRect =
    [self.view.window convertRect:textView.bounds fromView:textView];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    
}
-(void)textViewDidEndEditing:(UITextView *)textView {
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    [self.view endEditing:YES];
    
}

- (IBAction)closeButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];


}

- (IBAction)noButtonPressed:(id)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ratingSuccessAlertNoButton" object:self];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [[self view] endEditing:YES];
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
        
    
}
- (void)textFieldDidEndEditing:(UITextField *)textfield {
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    [self.view endEditing:YES];
    
}
- (BOOL)textFieldShouldReturn:(UITextField*)textField {
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)submitButtonPressed:(id)sender {
    [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"dismissalert"];
    //RLMRealm *realm = [RLMRealm defaultRealm];
    [self.view endEditing:YES];
//    [_txtComment resignFirstResponder];
    [_commentView resignFirstResponder];
    
    [APP huddie];
    
    self.serverID = [[NSUserDefaults standardUserDefaults]valueForKey:@"ServerID"];
    NSLog(@"ServerID:%@",self.serverID);
    NSPredicate *predicate;
    predicate = [NSPredicate predicateWithFormat:@"serverID = %d",[self.serverID integerValue]];
    newServer = [[Servers objectsWithPredicate:predicate] firstObject];
    
    NSLog(@"User = %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"token"]);
    NSLog(@"Server = %@",newServer);
    
    
//    if(_txtComment.text.length <=0)
        if(_commentView.text.length <=0)
    {
        [APP.hud setHidden:YES];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            
                HomeViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                
                UINavigationController *navHome = [[UINavigationController alloc] initWithRootViewController:homeVC];
                
                [self presentViewController:navHome animated:NO completion:nil];
                
            });
        });
        
    }
    
    else if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"isuserloggedin"] isEqualToString:@"no"] || ![[NSUserDefaults standardUserDefaults] valueForKey:@"isuserloggedin"]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [APP.hud setHidden:YES];
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Authentication Required"
                                           message:@"For posting comment User Authentication required"
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:NO completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:NO completion:nil];
                
                
                
                LoginView *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                
                [[NSUserDefaults standardUserDefaults] setValue:@"Comment" forKey:@"Sender"];
//                [[NSUserDefaults standardUserDefaults] setValue:_txtComment.text forKey:@"commentText"];
                [[NSUserDefaults standardUserDefaults] setValue:_commentView.text forKey:@"commentText"];
                
                UINavigationController *navLogin = [[UINavigationController alloc] initWithRootViewController:loginVC];
                
                [self presentViewController:navLogin animated:NO completion:nil];
                
                
            });
        });
    }
    else {
        
        {
            NSMutableDictionary *dataDict = [NSMutableDictionary new];
            

            [dataDict setValue:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"ServerID"]] forKey:@"_idBartender"];
            [dataDict setValue:_commentView.text forKey:@"comment"];
            [dataDict setValue:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"_idRating"]] forKey:@"_idRating"];
            
            [dataDict setValue:self.isAlreadyCommented forKey:@"isAlreadyCommented"];
            
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDict options:0 error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            
            NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@",SITE_URL,POST_COMMENT_AND_RATING_URL] parameters:nil error:nil];
            
            
            NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
            
            NSLog(@"Token:%@",token);
            
            
            
            [req setValue:[NSString stringWithFormat:@"bearer %@",token] forHTTPHeaderField:@"Authorization"];
            [req addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [req addValue:@"application/json" forHTTPHeaderField:@"Accept"];
            
            [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
            
            [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse *response, id data, NSError *error) {
                
                //        NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                //double status = (long)[httpResponse statusCode];
                NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
                
                
                
                if ([data isKindOfClass:[NSDictionary class]])
                    
                {
//                    RLMRealm *realm = [RLMRealm defaultRealm];
                    
                    NSDictionary *dataDic = (NSDictionary *)data;
                    NSLog(@"Data:%@",dataDic);
                    
                    NSInteger status = [[dataDic valueForKey:@"status"] integerValue];
                    if(status == 202)
                    {
                        
                        
                        
//                        NSDictionary  *serverData;
//                        NSPredicate *predicate;
                        
                       // [realm beginWriteTransaction];
//                        Servers *server;
//                        
//                        serverData = [dataDic valueForKey:@"data"];
//                        //for (serverData in [dataDic valueForKey:@"data"])
//                        {
//                            predicate = [NSPredicate predicateWithFormat:@"serverID = %ld",[[serverData valueForKey:@"_idUserProfile"] integerValue]];
//                            server =  [[Servers objectsWithPredicate:predicate] firstObject];
//                            
//                            NSLog(@"Rated Server:%@",server);
//                            
//                            server.toalRatings = [[serverData valueForKey:@"totalRatings"] integerValue];
//                            server.avgRating = [[serverData valueForKey:@"avgRating"] floatValue];
//                            server.oneRatings = [[serverData valueForKey:@"oneRatings"] integerValue ];
//                            server.twoRatings = [[serverData valueForKey:@"twoRatings"] integerValue];
//                            server.threeRatings = [[serverData valueForKey:@"threeRatings"] integerValue];
//                            server.fourRatings = [[serverData valueForKey:@"fourRatings"] integerValue];
//                            server.fiveRatings = [[serverData valueForKey:@"fiveRatings"] integerValue];
//                            server.totalComments = [[serverData valueForKey:@"totalComments"] integerValue];
//                            
//                        }
                        
                        @try {
                            //[realm addObject:server];
                            
                            
                        } @catch (NSException *exception) {
                            NSLog(@"Post Comment To Server Exception:%@",exception.description);
                        }
                       // [realm commitWriteTransaction];
                        
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [APP.hud setHidden:YES];
                            
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"Comment Posted"
                                                           message:@"Comment Posted Successfully"
                                                           preferredStyle:UIAlertControllerStyleAlert];
                            
                            [self presentViewController:alert2 animated:NO completion:nil];
                            
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                
                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                                [[LocalData sharedInstance] FetchAllRestaurants:^(BOOL result) {
                                    if (result)
                                    {
                                        HomeViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                                        
                                        //    [self.navigationController presentViewController:homeVC animated:YES completion:nil];
                                        
                                        UINavigationController *navHome = [[UINavigationController alloc] initWithRootViewController:homeVC];
                                        
                                        [self presentViewController:navHome animated:NO completion:nil];
                                    }
                                    else{
                                        HomeViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                                        
                                        //    [self.navigationController presentViewController:homeVC animated:YES completion:nil];
                                        
                                        UINavigationController *navHome = [[UINavigationController alloc] initWithRootViewController:homeVC];
                                        
                                        [self presentViewController:navHome animated:NO completion:nil];
                                    }
                                    
                                }];

                            });
                        });
                        
                        
                        
                    }
                    else if(status == 400)
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                        [APP.hud setHidden:YES];    
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"Sorry"
                                                           message:[dataDic valueForKey:@"result"]
                                                           preferredStyle:UIAlertControllerStyleAlert];
                            
                            [self presentViewController:alert2 animated:NO completion:nil];
                            
                            
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                            });
                        });
                        
                    }
                    
                }
                
                
            }] resume];
            
        }
        
        
    }
}


@end
