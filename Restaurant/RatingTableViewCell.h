//
//  RatingTableViewCell.h
//  Restaurant
//
//  Created by HN on 29/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RatingTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *imgStar1;
@property (weak, nonatomic) IBOutlet UIImageView *imgStar2;
@property (weak, nonatomic) IBOutlet UIImageView *imgStar3;
@property (weak, nonatomic) IBOutlet UIImageView *imgStar4;
@property (weak, nonatomic) IBOutlet UIImageView *imgStar5;

@property (weak, nonatomic) IBOutlet UILabel *lblRatebBy;
@property (weak, nonatomic) IBOutlet UILabel *lblRatingDate;
@property (weak, nonatomic) IBOutlet UILabel *lblComment;



@end
