//
//  RatingViewController.h
//  Restaurant
//
//  Created by HN on 29/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RatingTableViewCell.h"
#import "AppDelegate.h"
#import "LocalData.h"
#import "HCSStarRatingView.h"
#import "ServerRating.h"

@interface RatingViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>
{
    RLMResults<ServerRating  *> *ratingData;
    __weak IBOutlet UIProgressView *pvRateOne;
    __weak IBOutlet UIProgressView *pvRateTwo;
    __weak IBOutlet UIProgressView *pvRateThree;
    __weak IBOutlet UIProgressView *pvRateFour;
    __weak IBOutlet UIProgressView *pvRateFive;
    
    __weak IBOutlet UILabel *lblRateOne;
    __weak IBOutlet UILabel *lblRateTwo;
    __weak IBOutlet UILabel *lblRateThree;
    __weak IBOutlet UILabel *lblRateFour;
    __weak IBOutlet UILabel *lblRateFive;
    
    __weak IBOutlet UIImageView *starImage1;
    __weak IBOutlet UIImageView *starImage2;
    __weak IBOutlet UIImageView *starImage3;
    __weak IBOutlet UIImageView *starImage4;
    __weak IBOutlet UIImageView *starImage5;
    
    __weak IBOutlet UILabel *lblTotalRatings;
    
    NSMutableArray *arrRatings;
    NSMutableArray *arrComments;
    NSMutableArray *arrGivenBy;
    NSMutableArray *arrUpdatedAt;
    
    int pageIndex;
    
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) CGFloat lastContentOffset;

@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRatings;

- (void)setServerInfo;
@end
