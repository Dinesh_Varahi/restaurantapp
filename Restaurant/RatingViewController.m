//
//  RatingViewController.m
//  Restaurant
//
//  Created by HN on 29/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "RatingViewController.h"

@interface RatingViewController ()
{
    //RLMResults *ratingData;
    
}
@property (strong, nonatomic) NSArray<UIImageView *> *ratingStars;
@property (nonatomic) NSInteger rating;
@property (strong, nonatomic) NSArray<UIImageView *> *imageViewArray;
@end

@implementation RatingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    arrComments = [[NSMutableArray alloc] init];
    arrUpdatedAt = [[NSMutableArray alloc] init];
    arrGivenBy = [[NSMutableArray alloc]init];
    arrRatings = [[NSMutableArray alloc] init];
    self.tableView.showsVerticalScrollIndicator = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.title = @"Ratings";
    pageIndex = 0;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tabBarController.navigationItem.leftBarButtonItem = nil;
    [self GetRatingsAndComments];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setServerInfo) name:@"ratingFetched" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reSetNavigationBar)
                                                 name:@"rsetRatingsNavigation"
                                               object:nil];


}
- (void)viewWillAppear:(BOOL)animated {
    
    [self setTitle:@"My Comments"];
    self.navigationItem.title = @"My Comments";
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [self setServerInfo];
}

-(void)GetRatingsAndComments
{
    Servers *newServer = [[Servers allObjects] firstObject];
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%ld?page=%d",SITE_URL,RATAINGS_AND_COMMENTS,(long)newServer.serverID,pageIndex]];
    //    NSLog(@"Rating URL:%@",URL);
    
    
    AppUser *user = [[AppUser allObjects] firstObject];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"bearer %@",user.token] forHTTPHeaderField:@"Authorization"];
    
    
    [manager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        if ([responseObject isKindOfClass:[NSArray class]])
        {
            NSArray *jsonArray = (NSArray *)responseObject  ;
            NSLog(@"Json:%@",jsonArray);
        }
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dataDic = (NSDictionary *)responseObject;
            NSDictionary *Dict = [dataDic valueForKey:@"data"];
            
            arrRatings = [Dict valueForKey:@"rating"];
            arrComments = [Dict valueForKey:@"comment"];
            arrGivenBy = [Dict valueForKey:@"name"];
            arrUpdatedAt = [Dict valueForKey:@"updatedAt"];
//            [arrRatings addObject:[Dict valueForKey:@"rating"]];
//            [arrComments addObject:[Dict valueForKey:@"comment"]];
//            [arrGivenBy addObject:[Dict valueForKey:@"name"]];
//            [arrUpdatedAt addObject:[Dict valueForKey:@"updatedAt"]];
            [_tableView reloadData];
            
            

        }
        
        
        
    }
    failure:^(NSURLSessionTask *operation, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [APP.hud setHidden:YES];
        });
        
        NSLog(@"Error: %@", error);
        
    }];

}

- (void)reSetNavigationBar{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBarTintColor:[UIColor clearColor]];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    
    [self GetRatingsAndComments];
    [self setServerInfo];
    
}
- (void)setServerInfo {
    [RLMRealm defaultRealm];
    //Enable Comment Button
    
    @try {
    Servers *newServer = [[Servers allObjects] firstObject];
        
        if (newServer == nil) {
            newServer = [[Servers alloc] init];
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            _starRatings.value = newServer.avgRating;
            lblRateOne.text = [NSString stringWithFormat:@"%ld",(long)newServer.oneRatings];
            lblRateTwo.text = [NSString stringWithFormat:@"%ld",(long)newServer.twoRatings];
            lblRateThree.text = [NSString stringWithFormat:@"%ld",(long)newServer.threeRatings];
            lblRateFour.text = [NSString stringWithFormat:@"%ld",(long)newServer.fourRatings];
            lblRateFive.text = [NSString stringWithFormat:@"%ld",(long)newServer.fiveRatings];
            float TotalRatings = newServer.toalRatings;
            
            pvRateOne.progress = newServer.oneRatings/TotalRatings;
            pvRateTwo.progress = newServer.twoRatings/TotalRatings;
            pvRateThree.progress = newServer.threeRatings/TotalRatings;
            pvRateFour.progress = newServer.fourRatings/TotalRatings;
            pvRateFive.progress = newServer.fiveRatings/TotalRatings;
            
            
            self.imageViewArray = @[starImage1,starImage2,starImage3,starImage4,starImage5];
            lblTotalRatings.text = [NSString stringWithFormat:@"%ld Ratings",(long)newServer.toalRatings];
            
            [self.imageViewArray enumerateObjectsUsingBlock:^(UIImageView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [obj setImage:(idx < (NSInteger)newServer.avgRating ? [UIImage imageNamed:@"star_blank.png"] : [UIImage imageNamed:@"star_filled.png"])];
            }];
            
            [self setTableData];
            
        });
    } @catch (NSException *exception) {
        NSLog(@"Set Server Rating Exception:%@",exception.description);
    }
    
    
    
   }
- (void)setTableData {
    
    
    NSLog(@"Servers Rating:%@",ratingData);
    
    [self.tableView reloadData];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    


}

-(CGFloat)heightForBasicCellAtIndexPath:(NSIndexPath *)indexPath {
    static RatingTableViewCell *sizingCell = nil;
    //create just once per programm launching
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tableView dequeueReusableCellWithIdentifier:@"cell"];
    });
    [self configureBasicCell:sizingCell atIndexPath:indexPath];
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}
//this method will calculate required height of cell
- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

- (void)configureBasicCell:(RatingTableViewCell *)Cell atIndexPath:(NSIndexPath *)indexPath {
    //make some configuration for your cell
    ServerRating *aRating = ratingData[indexPath.row];
    
    Cell.lblRatingDate.text = [NSString getFormattedDate:aRating.date];
    
    
    if (aRating.customerName.length == 0)
    {
        Cell.lblRatebBy.text = @"By:";
    }
    else {
        
        NSString *tmpString =[arrGivenBy objectAtIndex:indexPath.row];
        
        
        Cell.lblRatebBy.text = [NSString stringWithFormat:@"By: %@",[tmpString capitalizedString]];
        
    }
    
    Cell.lblComment.numberOfLines = 20;
    Cell.lblComment.text = [NSString stringWithFormat:@"%@", [arrComments objectAtIndex:indexPath.row]];
    
    self.ratingStars = @[Cell.imgStar1,Cell.imgStar2,Cell.imgStar3,Cell.imgStar4,Cell.imgStar5];
    if (arrRatings.count > 0)
    {
        self.rating = [[arrRatings objectAtIndex:indexPath.row] integerValue];
    }
    
    
    [self.ratingStars enumerateObjectsUsingBlock:^(UIImageView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj setImage:(idx < (NSInteger)self.rating ? [UIImage imageNamed:@"star_filled.png"] : [UIImage imageNamed:@"star_blank.png"])];
    }];

}

#pragma mark - TableView delegate Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //ratingData = [ServerRating allObjects];
    return arrRatings.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    RatingTableViewCell *Cell = [[RatingTableViewCell alloc] init];
    
//    UILabel  * label = [[UILabel alloc] initWithFrame:CGRectMake(38, 5, Cell.lblComment.frame.size.width, 9999)];
//    label.numberOfLines=0;
//    label.font = [UIFont fontWithName:UIFontTextStyleBody size:14.0];
//    label.text = [NSString stringWithFormat:@"%@", [arrComments objectAtIndex:indexPath.row]];
//    
//    CGSize maximumLabelSize = CGSizeMake(Cell.lblComment.frame.size.width, 9999);
//    CGSize expectedSize = [label sizeThatFits:maximumLabelSize];
//    return expectedSize.height;
    if ([self heightForBasicCellAtIndexPath:indexPath] < 78)
    {
        return 78;
    }
    return [self heightForBasicCellAtIndexPath:indexPath];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    RatingTableViewCell *Cell = [[RatingTableViewCell alloc] init];
    
    Cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    
    NSString *strDate = [arrUpdatedAt objectAtIndex:indexPath.row];
    Cell.lblRatingDate.text = [NSString getFormattedDate:strDate];
    
    if (arrGivenBy.count == 0) {
        Cell.lblRatebBy.text = @"By:";
    }
    else {
        
        
        Cell.lblRatebBy.text = [NSString stringWithFormat:@"By: %@",[[arrGivenBy objectAtIndex:indexPath.row] capitalizedString]];//[NSString upperCase:[arrGivenBy objectAtIndex:indexPath.row]]];
        
    }
    
    Cell.lblComment.text = [NSString stringWithFormat:@"%@", [arrComments objectAtIndex:indexPath.row]];//aRating.comment;
    
    self.ratingStars = @[Cell.imgStar1,Cell.imgStar2,Cell.imgStar3,Cell.imgStar4,Cell.imgStar5];
    if (arrRatings.count > 0)
    {
        self.rating = [[arrRatings objectAtIndex:indexPath.row] integerValue];//aRating.rating;

    }
    
    [self.ratingStars enumerateObjectsUsingBlock:^(UIImageView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj setImage:(idx < (NSInteger)self.rating ? [UIImage imageNamed:@"star_filled.png"] : [UIImage imageNamed:@"star_blank.png"])];
    }];

    return Cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (arrRatings.count == 0)
    {
        return @"No Comments";
    }
    return [NSString stringWithFormat:@"%lu Comments",(unsigned long)arrRatings.count];
}



- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    //Get the page
    if (self.lastContentOffset < scrollView.contentOffset.y)
    {
        NSLog(@"Scrolling Down");
        //pageIndex = scrollView.contentOffset.x / scrollView.bounds.size.width;
        
        pageIndex ++;
        [self GetRatingsAndComments];
        
    }
    
    
    self.lastContentOffset = scrollView.contentOffset.y;
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        //Get the page
        if (self.lastContentOffset < scrollView.contentOffset.y)
        {
            NSLog(@"Scrolling Down");
            //pageIndex = scrollView.contentOffset.x / scrollView.bounds.size.width;
            
            pageIndex ++;
            [self GetRatingsAndComments];
            
        }
        
        self.lastContentOffset = scrollView.contentOffset.y;
        
        
    }
}


@end
