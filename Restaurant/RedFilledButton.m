//
//  RedFilledButton.m
//  Restaurant
//
//  Created by HN on 21/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "RedFilledButton.h"

@implementation RedFilledButton

- (id)initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
    
    if (self) {
        
//        self.backgroundColor = [UIColor clearColor];
        
        [self.titleLabel setFont:[UIFont systemFontOfSize:13 weight:UIFontWeightMedium]];
        
        self.clipsToBounds = YES;
        self.layer.cornerRadius = self.frame.size.height / 2;
//        self.layer.borderColor = [UIColor appMainColor].CGColor;
        self.backgroundColor = [UIColor appMainColor];
//        self.layer.borderWidth = 2.0f;
    }
    
    return self;
    
}


@end
