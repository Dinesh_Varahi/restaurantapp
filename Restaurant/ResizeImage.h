//
//  ResizeImage.h
//  Restaurant
//
//  Created by HN on 12/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResizeImage : NSObject

+ (instancetype)sharedInstance;

- (UIImage *)resizeImage:(UIImage *)image;

- (NSString *)encodeToBase64String:(UIImage *)image;


@end
