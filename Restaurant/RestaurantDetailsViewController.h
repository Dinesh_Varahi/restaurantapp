//
//  RestaurantDetailsViewController.h
//  Restaurant
//
//  Created by HN on 03/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerSearchCollectionFlowLayout.h"
#import "ServerSearchCollectionViewCell.h"
#import "ServerSearchHeaderView.h"
#import "ServerDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>


#import "Servers.h"

@interface RestaurantDetailsViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate>
{
    RLMResults<Servers  *> *serverList;
    //Servers *server;
    NSDictionary *serverDict;
    NSDictionary *serverData;
    NSMutableArray *arrExperinace;
    NSMutableArray *arrServers;
    NSMutableArray *arrDataServer;

    NSMutableArray *allJobsArray;
}

@property (weak, nonatomic) IBOutlet UIImageView *imgRestaurant;
@property (weak, nonatomic) IBOutlet UIImageView *imgTitleBack;
@property (weak, nonatomic) IBOutlet UILabel *lblRestaurantName;
@property (weak, nonatomic) IBOutlet UILabel *lblCity;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;


@property (strong,nonatomic) NSString *restaurantID;


@end
