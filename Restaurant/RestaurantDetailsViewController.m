//
//  RestaurantDetailsViewController.m
//  Restaurant
//
//  Created by HN on 03/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "RestaurantDetailsViewController.h"

@interface RestaurantDetailsViewController ()
{
    Restaurants *newRestaurant;
    NSString *aSender;
    
    
}
@property(nonatomic,weak)RLMResults *restaurants;
@property (nonatomic, strong) RLMResults *ServerArray;
@end

@implementation RestaurantDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault]; //UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.shadowImage = [UIImage new];////UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    
    
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    
    UIVisualEffectView *visualEffectView;
    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    
    visualEffectView.frame = self.imgTitleBack.bounds;
    [self.imgTitleBack addSubview:visualEffectView];
    
    
    [self.collectionView setDelegate:self];
    [self.collectionView setDataSource:self];
     self.collectionView.collectionViewLayout = [[ServerSearchCollectionFlowLayout alloc] init];
    [self assignCollectionCellNib];
    
    self.restaurantID = [[NSUserDefaults standardUserDefaults] valueForKey:@"RestroID"];
    NSLog(@"RestaurantID:%@",self.restaurantID);
    NSPredicate *predicate;
    predicate = [NSPredicate predicateWithFormat:@"restroID = %ld",[self.restaurantID integerValue]];
    newRestaurant = [[Restaurants objectsWithPredicate:predicate] firstObject];
    
    NSInteger ID = [self.restaurantID integerValue];
    NSPredicate *predicate1;
    predicate1 = [NSPredicate predicateWithFormat:@"workingRestroID = %d",ID];
    //server = [[Servers objectsWithPredicate:predicate1] firstObject];
    _ServerArray = [Servers objectsWithPredicate:predicate1];
    
    
    
    
    [self RequestForListOfServer];
//    NSLog(@"Restaurant:%@",newRestaurant);

    
    if ([aSender isEqualToString:@"AppLaunch"])
    {
        UIImage* image3 = [UIImage imageNamed:@"left.png"];
        CGRect frameimg = CGRectMake(0, 0, image3.size.width, image3.size.height);
        UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
        [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
        [someButton addTarget:self action:@selector(goToHomeView)
             forControlEvents:UIControlEventTouchUpInside];
        [someButton setShowsTouchWhenHighlighted:YES];
        
        UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
        self.navigationItem.leftBarButtonItem=mailbutton;
        
        
    }
    
}

- (void)RequestForListOfServer {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?restaurantId=%ld",SITE_URL,GET_RESTAURANT_BY_ID,[[[NSUserDefaults standardUserDefaults] valueForKey:@"RestroID"] integerValue]]];
    NSLog(@"%@",URL);
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        
        
        if (error) {
            NSLog(@"Error: %@", error.description);
            dispatch_async(dispatch_get_main_queue(), ^{[APP.hud setHidden:YES];});
            
        }
        else {
            
            if ([responseObject isKindOfClass:[NSArray class]])
            {
                NSArray *jsonArray = (NSArray *)responseObject  ;
                NSLog(@"Json:%@",jsonArray);
            }
            
            if ([responseObject isKindOfClass:[NSDictionary class]])
            {
                
                serverDict = (NSDictionary *)responseObject;
                arrServers = [serverDict valueForKey:@"data"];
                arrDataServer = [arrServers valueForKey:@"servers"];
                [self assignRestaurantInfo];
                

                [_collectionView reloadData];
            }
        }
    }];
    [dataTask resume];
    
}

- (void)goToHomeView {
    [[LocalData sharedInstance] FetchAllRestaurants:^(BOOL result) {
        if (result)
        {
            HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
            [self.navigationController pushViewController:home animated:NO];
        }
        else{
            HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
            [self.navigationController pushViewController:home animated:NO];
        }
        
    }];
    
}

- (void)assignRestaurantInfo{
    
    
    self.lblRestaurantName.text = [arrServers valueForKey:@"name"];
    self.lblCity.text = [arrServers valueForKey:@"city"];
   
    NSArray *arrayImages = [arrServers valueForKey:@"image"];
    
    
    NSLog(@"Image:%@",[NSString stringWithFormat:@"%@%@",SITE_URL,newRestaurant.imageURL]);
    
    if (arrayImages.count > 0)
    {
        [self.imgRestaurant sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,[arrayImages objectAtIndex:0]]]
                              placeholderImage:[UIImage imageNamed:@"Restaurant.png"]];
    }
    else
    {
        self.imgRestaurant.image = [UIImage imageNamed:@"Restaurant.png"];
    }
   
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self assignCollectionCellNib];
}

- (void)assignCollectionCellNib {
    [self.collectionView registerNib:[UINib nibWithNibName:@"ServerSearchCollectionViewCell" bundle:[NSBundle mainBundle]]
          forCellWithReuseIdentifier:@"ServerSearchCollectionViewCell"];
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Collectionview Delegates

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    
    return CGSizeMake(0., 50.);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
       return arrDataServer.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    ServerSearchCollectionViewCell *serverCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ServerSearchCollectionViewCell" forIndexPath:indexPath];
    
    arrDataServer = [arrServers valueForKey:@"servers"];
    serverData = [arrDataServer objectAtIndex:indexPath.row];
    // Assigning Name
    serverCell.serverName.text = [serverData valueForKey:@"name"];
    NSLog(@"%@",[serverData valueForKey:@"name"]);
    serverCell.starRatings.value = [[serverData valueForKey:@"avgRating"]floatValue];
    serverCell.ratings.text = [NSString stringWithFormat:@"%@ Ratings",[serverData valueForKey:@"totalRatings"]];
    serverCell.starRatings.userInteractionEnabled = NO;
    NSLog(@"%@",[serverData valueForKey:@"avgRating"]);
    //    serverCell.imageView
    //for (NSDictionary *serverExperiance in [serverData valueForKey:@"experience"])
    serverCell.imageView.layer.cornerRadius = serverCell.imageView.frame.size.height/2;
    serverCell.imageView.layer.masksToBounds = YES;
    [serverCell.imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,[arrDataServer valueForKey:@"image"]]]
                          placeholderImage:[UIImage imageNamed:@"ic_user_b.png"]];
    {
        
        arrExperinace = [serverData valueForKey:@"experience"];
        for (int i = 0; i < [arrExperinace count]; i++)
        {
            NSDictionary *Dict = [arrExperinace objectAtIndex:i];
            NSLog(@"%@",[Dict valueForKey:@"isCurrentJob"]);
            
            if ([[Dict valueForKey:@"isCurrentJob"] isEqualToString:@"YES"])
            {
                serverCell.restaurantName.text = [arrServers valueForKey:@"name"];//[Dict valueForKey:@"restaurantName"];
            }
            
        }
        
    }
    
    return serverCell;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    if (self.ServerArray.count == 0) {
        UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
        NSInteger numberOfCells = self.view.frame.size.width / flowLayout.itemSize.width;
        NSInteger edgeInsets = (self.view.frame.size.width - (numberOfCells * flowLayout.itemSize.width)) / (numberOfCells + 1);
        return UIEdgeInsetsMake(0, edgeInsets, 0, edgeInsets);
    }
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *headerCell;
    
    int count;
    
    
    count = (int)arrDataServer.count;
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        ServerSearchHeaderView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"headerView" forIndexPath:indexPath];
        
        NSString *resultString;
        if (count == 0)
        {
            resultString = @"No Result Found";
            header.lblHeaderTitle.text = resultString;
        }
        else if(count <= 1)
        {
            resultString = @"Result Found";
            header.lblHeaderTitle.text = [NSString stringWithFormat:@"%d %@",count,resultString];
        }
        else
        {
            resultString = @"Results Found";
            header.lblHeaderTitle.text = [NSString stringWithFormat:@"%d %@",count,resultString];
        }
        
        
        headerCell = header;
        
    }
    
    return headerCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    serverData = [arrDataServer objectAtIndex:indexPath.row];
    NSString *serverID = [serverData valueForKey:@"_idUserProfile"];
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@",serverID] forKey:@"ServerID"];
    
    ServerDetailViewController *serverDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ServerDetailViewController"];
    
    [self.navigationController pushViewController:serverDetailVC animated:NO];
//    Servers *newServer = self.ServerArray[indexPath.row];
//    
//    ServerDetailViewController *serverDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ServerDetailViewController"];
//    //serverDetailVC.serverID = [NSString stringWithFormat:@"%ld",(long)newServer.serverID];
//    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%ld",(long)newServer.serverID] forKey:@"ServerID"];
//    
//    [self.navigationController pushViewController:serverDetailVC animated:NO];
    
    
    
}


@end
