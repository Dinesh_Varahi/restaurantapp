//
//  RestaurantImages.h
//  Restaurant
//
//  Created by HN on 25/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <Realm/Realm.h>

@interface RestaurantImages : RLMObject

@property NSInteger restroID;
@property NSString *imageURL;
@end

// This protocol enables typed collections. i.e.:
// RLMArray<RestaurantImages>
RLM_ARRAY_TYPE(RestaurantImages)
