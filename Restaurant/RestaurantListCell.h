//
//  RestaurantListCell.h
//  Restaurant
//
//  Created by Parth Pandya on 25/12/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface RestaurantListCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageview;
@property (weak, nonatomic) IBOutlet UILabel *lblRestroName;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *RestroRatings;
@property (weak, nonatomic) IBOutlet UILabel *lblCity;

@end
