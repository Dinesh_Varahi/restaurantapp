//
//  Restaurants.h
//  Restaurant
//
//  Created by HN on 25/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <Realm/Realm.h>
#import "RestaurantImages.h"
#import "Servers.h"

@interface Restaurants : RLMObject

@property NSString *name;
@property NSString *restoType;
@property NSInteger restroID;
@property NSInteger avgRating;
@property NSString *city;
@property NSString *zipCode;
@property NSString *imageURL; //To b converted to Array.
@property NSString *ownerEmail;
@property NSString *ownerName;
@property RLMArray<Servers *><Servers> *server;
@property NSString *latitude;
@property NSString *longitude;
@property NSString *landmark;
@end

// This protocol enables typed collections. i.e.:
// RLMArray<Restaurants>
RLM_ARRAY_TYPE(Restaurants)
