//
//  ServerComment.h
//  Restaurant
//
//  Created by Parth Pandya on 21/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <Realm/Realm.h>

@interface ServerComment : RLMObject

@property NSString *customerName;
@property float avgRating;
@property NSString *reply;
@property NSInteger commentID;
@property NSString *serverProfileImgURL;
@property NSString *serverName;
@property NSString *date;
@property float rating;
@property NSString *comment;
@property NSInteger ratingID;

@end

RLM_ARRAY_TYPE(ServerComment)
