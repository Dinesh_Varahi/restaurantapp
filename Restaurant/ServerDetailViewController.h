//
//  ServerProfileViewController.h
//  Restaurant
//
//  Created by HN on 17/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RatingSuccessAlertViewController.h"
#import "Servers.h"
#import "LocalData.h"
#import "HCSStarRatingView.h"
#import "LoginView.h"
#import <SDWebImage/UIImageView+WebCache.h>


@class LocalData;




@interface ServerDetailViewController : UIViewController<UITextFieldDelegate,UITableViewDelegate>
{
    NSDictionary *serverDict;
    NSMutableArray *arrExperinace;
    NSMutableArray *arrCurrentRestaurants;
    NSMutableArray *arrServers;
    NSDictionary *serverinfo;
    CGSize expectedLabelSize;
    int pageIndex;
    
    NSString *restroID;
    NSDictionary *CommentsDict;
    
    
    NSMutableArray *arrOwnerName;
    NSMutableArray *arrCustomerName;
    NSMutableArray *arrCustomerImages;
    NSMutableArray *arrCustomerCommentDate;
    NSMutableArray *arrOwnerImage;
    NSMutableArray *arrOwnerImages;
}

@property (weak, nonatomic) IBOutlet UIImageView *blurrImageview;
@property (weak, nonatomic) IBOutlet UIImageView *serverProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *serverName;
@property (weak, nonatomic) IBOutlet UILabel *totalRatings;
@property (weak, nonatomic) IBOutlet UILabel *restaurantName;
@property (weak, nonatomic) IBOutlet UIImageView *starImage1;
@property (weak, nonatomic) IBOutlet UIImageView *starImage2;
@property (weak, nonatomic) IBOutlet UIImageView *starImage3;
@property (weak, nonatomic) IBOutlet UIImageView *starImage4;
@property (weak, nonatomic) IBOutlet UIImageView *starImage5;
@property (strong, nonatomic) NSMutableArray *arrOwnerComments;
@property (strong, nonatomic) NSMutableArray *arrCustomerComments;

@property (weak, nonatomic) IBOutlet UILabel *lblRating1;
@property (weak, nonatomic) IBOutlet UILabel *lblRating2;
@property (weak, nonatomic) IBOutlet UILabel *lblRating3;
@property (weak, nonatomic) IBOutlet UILabel *lblRating4;
@property (weak, nonatomic) IBOutlet UILabel *lblRating5;


@property (weak, nonatomic) IBOutlet UIProgressView *progressRating1;
@property (weak, nonatomic) IBOutlet UIProgressView *progressRating2;
@property (weak, nonatomic) IBOutlet UIProgressView *progressRating3;
@property (weak, nonatomic) IBOutlet UIProgressView *progressRating4;
@property (weak, nonatomic) IBOutlet UIProgressView *progressRating5;


@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRatings;
@property (weak, nonatomic) IBOutlet UITextField *txtComment;

@property (weak, nonatomic) IBOutlet UILabel *lblTotalComments;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalRatings;


@property (weak, nonatomic) IBOutlet UIButton *btnPostComment;

@property (weak, nonatomic) IBOutlet UIButton *starButton1;
@property (weak, nonatomic) IBOutlet UIButton *starButton2;
@property (weak, nonatomic) IBOutlet UIButton *starButton3;
@property (weak, nonatomic) IBOutlet UIButton *starButton4;
@property (weak, nonatomic) IBOutlet UIButton *starButton5;

@property (weak,nonatomic) NSString *serverID;

@property (weak, nonatomic) IBOutlet UITableView *tblPreviousComments;
@property (strong, nonatomic) IBOutlet UISegmentedControl *workingRestaurantSegment;
- (IBAction)restaurantSegmentPressed:(id)sender;


//- (IBAction)ratingButtonPressed:(id)sender;
- (IBAction)starRatingPressed:(UIButton *)sender;
- (IBAction)postCommentButtonPressed:(UIButton *)sender;

- (void)dismissAlert;
- (IBAction)btnPostCommentPressed:(id)sender;


@end
