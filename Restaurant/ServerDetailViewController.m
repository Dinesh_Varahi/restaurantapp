//
//  ServerProfileViewController.m
//  Restaurant
//
//  Created by HN on 17/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "ServerDetailViewController.h"
#import "PreviousCommentsCell.h"

@interface ServerDetailViewController ()
{
    __weak IBOutlet UIView *background1;
    
    __weak IBOutlet UIView *background2;
    
    __weak IBOutlet UIImageView *imgServerPic;
    
    RatingSuccessAlertViewController   *ratingAlert;
    
    Servers *newServer;
    AppUser *newUser;
    
    CGFloat animatedDistance;
    UIActivityIndicatorView *activityView;
}
@property (strong, nonatomic) NSArray<UIButton *> *buttonArray;
@property (nonatomic) NSInteger rating;

@property (strong, nonatomic) NSArray<UIImageView *> *imageViewArray;
@property (nonatomic) NSInteger displayRating;
@end

@implementation ServerDetailViewController
@synthesize starButton1 = _starButton1,starButton2 = _starButton2, starButton3 = _starButton3, starButton4 = _starButton4, starButton5 = _starButton5;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    
    arrServers = [[NSMutableArray alloc]init];
    _arrCustomerComments = [[NSMutableArray alloc] init];
    _arrOwnerComments = [[NSMutableArray alloc] init];
    arrCurrentRestaurants = [[NSMutableArray alloc] init];
    arrExperinace = [[NSMutableArray alloc] init];
    arrOwnerImage = [[NSMutableArray alloc] init];
    arrOwnerName = [[NSMutableArray alloc] init];
    arrCustomerName = [[NSMutableArray alloc] init];
    arrCustomerImages = [[NSMutableArray alloc] init];
    arrCustomerCommentDate = [[NSMutableArray alloc] init];
    
    _workingRestaurantSegment.hidden = YES;
    self.tblPreviousComments.showsVerticalScrollIndicator = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.title = @"Details";
    pageIndex = 0;
    self.tblPreviousComments.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    activityView = [[UIActivityIndicatorView alloc]
                    initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    ratingAlert = [[RatingSuccessAlertViewController alloc]init];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(popView)
                                                 name:@"ratingSuccessAlertNoButton"
                                               object:nil];
    restroID = @"";
    
    activityView.center=self.view.center;
    [activityView hidesWhenStopped];
    [self.view addSubview:activityView];
    
    [self.txtComment  setDelegate:self];
    
    self.serverProfilePic.layer.cornerRadius = self.serverProfilePic.frame.size.width /2 ;
    self.serverProfilePic.layer.masksToBounds = YES;
    
    
    background2.layer.cornerRadius = 5;
    background2.layer.masksToBounds = NO;
    background2.layer.shadowColor = [UIColor grayColor].CGColor;
    background2.layer.shadowOpacity = 0.8;
    background2.layer.shadowRadius = 2;
    background2.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    
    self.serverID = [[NSUserDefaults standardUserDefaults]valueForKey:@"ServerID"];
    NSLog(@"ServerID:%@",self.serverID);
    
    [self RequestForListOfServer];
    [self getPreviousComments];
    imgServerPic.layer.cornerRadius = imgServerPic.frame.size.width / 2;
    
    self.buttonArray = @[self.starButton1,self.starButton2,self.starButton3,self.starButton4,self.starButton5];
    
    self.tblPreviousComments.estimatedRowHeight = 102.0;
    self.tblPreviousComments.rowHeight = UITableViewAutomaticDimension;
    
    [self.tblPreviousComments setNeedsLayout];
    [self.tblPreviousComments layoutIfNeeded];
    
}

- (void) viewDidAppear:(BOOL)animated {
    
    [self.tblPreviousComments reloadData];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    
    
    NSPredicate *predicate;
    predicate = [NSPredicate predicateWithFormat:@"serverID = %ld",[self.serverID integerValue]];
    newServer = [[Servers objectsWithPredicate:predicate] firstObject];
    
    NSLog(@"User = %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"token"]);
    NSLog(@"Server = %@",newServer);
    
    //[self setServerInfo];
}
- (void) viewWillDisappear:(BOOL)animated {
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
}
- (void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)popView {
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)RequestForListOfServer{
    dispatch_async(dispatch_get_main_queue(), ^{
        [APP huddie];
        [arrCurrentRestaurants removeAllObjects];
    });
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?serverId=%@",SITE_URL,GET_BARTENDER_BY_ID,[[NSUserDefaults standardUserDefaults] valueForKey:@"ServerID"]]];
    NSLog(@"%@",URL);
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        
        
        if (error) {
            NSLog(@"Error: %@", error.description);
            dispatch_async(dispatch_get_main_queue(), ^{[APP.hud setHidden:YES];});
            
        }
        else {
            //            NSLog(@"%@ %@", response, responseObject);
            
            if ([responseObject isKindOfClass:[NSArray class]])
            {
                NSArray *jsonArray = (NSArray *)responseObject  ;
                NSLog(@"Json:%@",jsonArray);
            }
            
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                
                
                serverDict = (NSDictionary *)responseObject;
                serverinfo = [[NSDictionary alloc]init];
                serverinfo = [serverDict valueForKey:@"data"];
                arrExperinace = [serverinfo valueForKey:@"experience"];
                
                for (int i = 0; i < [arrExperinace count]; i++)
                {
                    if ([[[arrExperinace objectAtIndex:i] valueForKey:@"isCurrentJob"] isEqualToString:@"YES"])
                    {
                        if (arrCurrentRestaurants.count < 3)
                        {
                            [arrCurrentRestaurants addObject:[arrExperinace objectAtIndex:i]];
                        }
                        
                    }
                }
                [arrServers addObjectsFromArray:[serverDict valueForKey:@"data"]];
                
                [self setServerInfo];
//                [self getPreviousComments];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [APP.hud setHidden:YES];
                });
            }
        }
    }];
    [dataTask resume];
    
}

- (void)getPreviousComments {
    dispatch_async(dispatch_get_main_queue(), ^{
       // [APP huddie];
    });
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?idBartender=%@&page=%d",SITE_URL,GET_PREVIOUS_COMMENTS,[[NSUserDefaults standardUserDefaults] valueForKey:@"ServerID"],pageIndex]];
    NSLog(@"%@",URL);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    
    NSLog(@"Token:%@",token);
    
    
    
    [request setValue:[NSString stringWithFormat:@"bearer %@",token] forHTTPHeaderField:@"Authorization"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];

    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        
        
        if (error) {
            NSLog(@"Error: %@", error.description);
            dispatch_async(dispatch_get_main_queue(), ^{[APP.hud setHidden:YES];});
            
        }
        else {
            //            NSLog(@"%@ %@", response, responseObject);
            
            if ([responseObject isKindOfClass:[NSArray class]])
            {
                NSArray *jsonArray = (NSArray *)responseObject  ;
                NSLog(@"Json:%@",jsonArray);
            }
            
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                
                
                CommentsDict = (NSDictionary *)responseObject;
                NSDictionary *Dict = [CommentsDict valueForKey:@"data"];
                arrCustomerImages = [Dict valueForKey:@"customerImage"];
                arrCustomerName = [Dict valueForKey:@"customerName"];
                arrCustomerCommentDate = [Dict valueForKey:@"commentedAt"];
                NSArray *Comment = [Dict valueForKey:@"comment"];
                for (int i = 0; i < [Comment count]; i++)
                {
                     [_arrCustomerComments addObject:[Comment objectAtIndex:i]];
                }
                NSArray *reply = [Dict valueForKey:@"repliedComment"];
                for (int i = 0; i < [reply count]; i++)
                {
                    if ([reply objectAtIndex:i] == [NSNull null])
                    {
                        [_arrOwnerComments addObject:@"0"];
                        [arrOwnerImage addObject:@"0"];
                        [arrOwnerName addObject:@"0"];
                    }
                    else
                    {
//                    NSArray *arr = [reply valueForKey:@"reply"];
                        [_arrOwnerComments addObject:[[reply objectAtIndex:i] valueForKey:@"reply"]];
                        NSDictionary *Dict = [reply objectAtIndex:i];
                        if ([Dict valueForKey:@"ownerImage"]) {
                            [arrOwnerImage addObject:[[reply objectAtIndex:i] valueForKey:@"ownerImage"]];
                            [arrOwnerName addObject:[[reply objectAtIndex:i] valueForKey:@"ownerName"]];
                        }
                        else
                        {
                            [arrOwnerImage addObject:@"0"];
                            [arrOwnerName addObject:@"Taral"];
                        }
                        
                    }
                }
                [self.tblPreviousComments reloadData];
//                serverinfo = [[NSDictionary alloc]init];
//                serverinfo = [serverDict valueForKey:@"data"];
//                [arrServers addObjectsFromArray:[serverDict valueForKey:@"data"]];
                
                //[self setServerInfo];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [APP.hud setHidden:YES];
                });
            }
        }
    }];
    [dataTask resume];

}
- (void)setServerInfo {
    
    //Enable Comment Button
    
    
    NSLog(@"Server Detaisl:%@",newServer);
    
    // Assigning Name
    {
        NSLog(@"%@",[serverinfo valueForKey:@"name"]);
        self.serverName.text = [NSString upperCase:[serverinfo valueForKey:@"name"]];
    }
    
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for (int i = 0; i < [arrCurrentRestaurants count]; i++)
    {
        [arr addObject:[[arrCurrentRestaurants objectAtIndex:i] valueForKey:@"restaurantName"]];
    }
    if (arr.count > 1)
    {
        
        UISegmentedControl *segment = [[UISegmentedControl alloc]initWithItems:arr];
        segment.frame = CGRectMake(35, 230, 250, 35);
        
        [segment addTarget:self action:@selector(restaurantSegmentPressed:) forControlEvents: UIControlEventValueChanged];
        segment.tintColor = [UIColor appMainColor];
        //segment.selectedSegmentIndex = 1;
        [self.view addSubview:segment];
    }
    else
    {
        if (arrCurrentRestaurants.count > 0)
        {
            restroID = [[arrCurrentRestaurants objectAtIndex:0] valueForKey:@"_idWorksInRestaurant"];
        }
        
    }
    
    
    //Assigning Total Ratings to that Server
    self.starRatings.value = [[serverinfo valueForKey:@"avgRating"] floatValue];
    
    self.lblTotalRatings.text = [NSString stringWithFormat:@"%@",[serverinfo valueForKey:@"totalRatings"]];//[NSString stringWithFormat:@"%ld",newServer.toalRatings];
    
    self.lblTotalComments.text = [NSString stringWithFormat:@"%@",[serverinfo valueForKey:@"totalComments"]];//[NSString stringWithFormat:@"%ld",newServer.totalComments];
    
    self.lblRating1.text = [NSString stringWithFormat:@"%@",[serverinfo valueForKey:@"oneRatings"]];
    self.lblRating2.text = [NSString stringWithFormat:@"%@",[serverinfo valueForKey:@"twoRatings"]];
    self.lblRating3.text = [NSString stringWithFormat:@"%@",[serverinfo valueForKey:@"threeRatings"]];
    self.lblRating4.text = [NSString stringWithFormat:@"%@",[serverinfo valueForKey:@"fourRatings"]];
    self.lblRating5.text = [NSString stringWithFormat:@"%@",[serverinfo valueForKey:@"fiveRatings"]];
    float TotalRatings = [[serverinfo valueForKey:@"totalRatings"] floatValue];
    
    self.progressRating1.progress = [[serverinfo valueForKey:@"oneRatings"] intValue]/TotalRatings;
    self.progressRating2.progress = [[serverinfo valueForKey:@"twoRatings"] intValue]/TotalRatings;
    self.progressRating3.progress = [[serverinfo valueForKey:@"threeRatings"] intValue]/TotalRatings;
    self.progressRating4.progress = [[serverinfo valueForKey:@"fourRatings"] intValue]/TotalRatings;
    self.progressRating5.progress = [[serverinfo valueForKey:@"fiveRatings"] intValue]/TotalRatings;
    
    [_serverProfilePic setShowActivityIndicatorView:YES];
    [self.serverProfilePic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,[serverinfo valueForKey:@"image"]]]
                             placeholderImage:[UIImage imageNamed:@"ic_user_b.png"]];
    
}

- (void)dissmissView:(id)sender {
    
    [self dismissViewControllerAnimated:NO completion:nil];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _arrCustomerComments.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //        UITableViewCell *Cell;
    PreviousCommentsCell *Cell = [[PreviousCommentsCell alloc] init];
    Cell = [_tblPreviousComments dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    
    Cell.lblCustomerName.text = [[arrCustomerName objectAtIndex:indexPath.row] capitalizedString];
    
    Cell.lblCustomerCommentDate.text = [NSString getCommentTime:[arrCustomerCommentDate objectAtIndex:indexPath.row]];
    
    Cell.lblCustomerComment.text = [NSString stringWithFormat:@"%@", [_arrCustomerComments objectAtIndex:indexPath.row]];
    
    
    
    if ((arrOwnerName.count > 0) && (_arrOwnerComments.count > 0) && (![[arrOwnerName objectAtIndex:indexPath.row] isEqualToString:@"0"]) && (![[_arrOwnerComments objectAtIndex:indexPath.row] isEqualToString:@"0"])) {
    
        Cell.imgOwner.hidden = NO;
        Cell.lblOwnerName.hidden = NO;
        Cell.lblOwnerComment.hidden = NO;
        
            Cell.lblOwnerName.text = [[arrOwnerName objectAtIndex:indexPath.row] capitalizedString];
            Cell.lblOwnerComment.text = [NSString stringWithFormat:@"%@",[_arrOwnerComments objectAtIndex:indexPath.row]];
     
    }
    else{
        Cell.imgOwner.hidden = YES;
        Cell.lblOwnerName.hidden = YES;
        Cell.lblOwnerComment.hidden = YES;
    }
    
    
    Cell.imgCustomer.image = [UIImage imageNamed:@"ic_user_b.png"];
    Cell.imgOwner.image = [UIImage imageNamed:@"ic_user_b.png"];
    
    Cell.imgCustomer.layer.cornerRadius = Cell.imgCustomer.frame.size.height/2;
    Cell.imgCustomer.layer.masksToBounds = YES;
    
    Cell.imgOwner.layer.cornerRadius = Cell.imgOwner.frame.size.height/2;
    Cell.imgOwner.layer.masksToBounds = YES;
    [Cell.imgCustomer sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,[arrCustomerImages objectAtIndex:indexPath.row] ]]
                        placeholderImage:[UIImage imageNamed:@"ic_user_b.png"]];
    [Cell.imgOwner sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,[arrOwnerImage objectAtIndex:indexPath.row]]]
                     placeholderImage:[UIImage imageNamed:@"ic_user_b.png"]];
    return Cell;
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return [NSString stringWithFormat:@" %lu Comments",(unsigned long)_arrCustomerComments.count];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    if ([[_arrOwnerComments objectAtIndex:indexPath.row] isEqualToString:@"0"])
//    {
//        return 74;
//    }
    return UITableViewAutomaticDimension;
    
  /*
    PreviousCommentsCell *Cell = [[PreviousCommentsCell alloc] init];

    // CustomerName
    
    int CNLabelHeight = [StringUtils findHeightForText: Cell.lblCustomerName.text havingWidth:Cell.lblCustomerName.frame.size.width andFont:[UIFont systemFontOfSize:13.0f]];
    
    
    CNLabelHeight += [StringUtils findHeightForText:Cell.lblCustomerName.text havingWidth:Cell.lblCustomerName.frame.size.width andFont:[UIFont systemFontOfSize:14.0f]];
    
    
    int CCLableHeight = [StringUtils findHeightForText: Cell.lblCustomerComment.text havingWidth:Cell.lblCustomerComment.frame.size.width andFont:[UIFont systemFontOfSize:13.0f]];
    
    
    CCLableHeight += [StringUtils findHeightForText:Cell.lblCustomerComment.text havingWidth:Cell.lblCustomerComment.frame.size.width andFont:[UIFont systemFontOfSize:14.0f]];
    
    
    int ONLabelHeight = [StringUtils findHeightForText: Cell.lblOwnerName.text havingWidth:Cell.lblOwnerName.frame.size.width andFont:[UIFont systemFontOfSize:13.0f]];
    
    
    ONLabelHeight += [StringUtils findHeightForText:Cell.lblOwnerName.text havingWidth:Cell.lblOwnerName.frame.size.width andFont:[UIFont systemFontOfSize:14.0f]];
    
    
    int OCLabelHeight = [StringUtils findHeightForText: Cell.lblOwnerComment.text havingWidth:Cell.lblOwnerComment.frame.size.width andFont:[UIFont systemFontOfSize:13.0f]];
    
    
    OCLabelHeight += [StringUtils findHeightForText:Cell.lblOwnerComment.text havingWidth:Cell.lblOwnerComment.frame.size.width andFont:[UIFont systemFontOfSize:14.0f]];
    
    
    int height = [StringUtils findHeightForText: Cell.lblOwnerComment.text havingWidth:Cell.lblOwnerComment.frame.size.width andFont:[UIFont systemFontOfSize:13.0f]];
    
    
    height += [StringUtils findHeightForText:Cell.lblOwnerComment.text havingWidth:Cell.lblOwnerComment.frame.size.width andFont:[UIFont systemFontOfSize:14.0f]];
    
    return CNLabelHeight + CCLableHeight + ONLabelHeight + OCLabelHeight + 102; //important to know the size of your custom cell without the height of the variable label
    
   */
    
    
}


- (void)dismissAlert {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)btnPostCommentPressed:(id)sender {
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [[self view] endEditing:YES];
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    
    
}
- (void)textFieldDidEndEditing:(UITextField *)textfield {
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    [self.view endEditing:YES];
    
}
- (BOOL)textFieldShouldReturn:(UITextField*)textField {
    [textField resignFirstResponder];
    return YES;
}


- (IBAction)postCommentButtonPressed:(UIButton *)sender {
    
    
    
    
    [self.view endEditing:YES];
    [_txtComment resignFirstResponder];
    
    if(_txtComment.text.length <=0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Please Enter Comment"
                                           message:@""
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:NO completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:NO completion:nil];
            });
        });
        return;
        
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"isuserloggedin"] isEqualToString:@"no"]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Authentication Required"
                                           message:@"For posting comment User Authentication required"
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:NO completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:NO completion:nil];
                
                [[NSUserDefaults standardUserDefaults] setValue:@"Comment" forKey:@"Sender"];
                
                LoginView *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:NO];
                
            });
        });
    }
    else {
        
        ratingAlert = [self.storyboard instantiateViewControllerWithIdentifier:@"RatingSuccessAlertViewController"];
        ratingAlert.alertTitle = @"Thank You for Commenting";
        ratingAlert.alertMessage = @"Would you like to post Comment on other servers?";
        ratingAlert.transitioningDelegate = (id)self;
        ratingAlert.modalPresentationStyle = UIModalPresentationCustom;
        
        
        
        
        [self.navigationController presentViewController:ratingAlert animated:NO completion:nil];
        
        
        
        /*
         
         {
         NSMutableDictionary *dataDict = [NSMutableDictionary new];
         
         [dataDict setValue:[NSString stringWithFormat:@"%ld",(long)newServer.serverID] forKey:@"_idBartender"];
         [dataDict setValue:_txtComment.text forKey:@"comment"];
         
         
         NSError *error;
         NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDict options:0 error:&error];
         NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
         
         AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
         
         NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@",SITE_URL,POST_COMMENT_URL] parameters:nil error:nil];
         
         
         NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
         
         NSLog(@"Token:%@",token);
         
         
         
         [req setValue:[NSString stringWithFormat:@"bearer %@",token] forHTTPHeaderField:@"Authorization"];
         
         [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
         
         [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse *response, id data, NSError *error) {
         
         //        NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
         
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
         double status = (long)[httpResponse statusCode];
         NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
         
         
         
         if ([data isKindOfClass:[NSDictionary class]])
         
         {
         
         NSDictionary *dataDic = (NSDictionary *)data;
         NSLog(@"Data:%@",dataDic);
         
         NSInteger status = [[dataDic valueForKey:@"status"] integerValue];
         if(status == 202)
         {
         
         [activityView stopAnimating];
         
         RatingSuccessAlertViewController   *ratingAlert = [self.storyboard instantiateViewControllerWithIdentifier:@"RatingSuccessAlertViewController"];
         
         ratingAlert.transitioningDelegate = (id)self;
         ratingAlert.modalPresentationStyle = UIModalPresentationCustom;
         
         
         [self.navigationController presentViewController:ratingAlert animated:YES completion:nil];
         
         }
         else if(status == 400)
         {
         [activityView stopAnimating];
         
         dispatch_async(dispatch_get_main_queue(), ^{
         
         UIAlertController * alert2=   [UIAlertController
         alertControllerWithTitle:@"Sorry"
         message:[dataDic valueForKey:@"result"]
         preferredStyle:UIAlertControllerStyleAlert];
         
         [self presentViewController:alert2 animated:YES completion:nil];
         
         
         
         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
         [alert2 dismissViewControllerAnimated:YES completion:nil];
         });
         });
         
         }
         
         }
         
         
         }] resume];
         
         }
         */
        
        _txtComment.text = @"";
    }
    
    
}

- (IBAction)starRatingPressed:(UIButton *)sender {
    
    //    [activityView startAnimating];
    
//    RLMRealm *realm = [RLMRealm defaultRealm];
    
    NSInteger buttonIndex = [self.buttonArray indexOfObject:sender];
    if (buttonIndex == NSNotFound) {   return;    }
    
    self.rating = buttonIndex +1;
    
    
    if ([restroID isEqualToString:@""])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Attention"
                                           message:@"Please select restaurant of the server for which you want to rate him"
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:NO completion:nil];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:NO completion:nil];
            });
        });

    }
    else
    {
        //[NSUserDefaults standardUserDefaults]setValue:[] forKey:@""
        [APP huddie];
        NSDate *today = [NSDate date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/ddyyyy"];
        NSString *dateString = [dateFormat stringFromDate:today];
        
        NSMutableDictionary *dataDict = [NSMutableDictionary new];
//        NSLog(@"%@",[serverinfo valueForKey:@"_id"]);
        [dataDict setValue:[NSString stringWithFormat:@"%@",[serverinfo valueForKey:@"_id"]] forKey:@"_idBartender"];//
        [dataDict setValue:[NSString stringWithFormat:@"%ld", (long)self.rating] forKey:@"rating"];
        [dataDict setValue:dateString forKey:@"today"];
        [dataDict setValue:restroID forKey:@"_idForRestaurant"];
        
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDict options:0 error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@",SITE_URL,POST_RATING_URL] parameters:nil error:nil];
        //        NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
        //
        //        NSLog(@"Token:%@",token);
        //
        //        [req addValue:[NSString stringWithFormat:@"bearer %@",token] forHTTPHeaderField:@"Authorization"];
        [req addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [req addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
        
        [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse *response, id data, NSError *error) {
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            //            double status = (long)[httpResponse statusCode];
            NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
            
            if ([data isKindOfClass:[NSDictionary class]]){
                
                NSDictionary *dataDic = (NSDictionary *)data;
                NSLog(@"Data:%@",dataDic);
                
                NSInteger status = [[dataDic valueForKey:@"status"] integerValue];
                if(status == 202)
                {
                    [self RequestForListOfServer];
                    
                    [[LocalData sharedInstance] FetchAllRestaurants:^(BOOL result) {
                        if (result)
                        {
                        }
                    }];
                    NSDictionary *dictData = [dataDic valueForKey:@"data"];
                    [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"_idRating"]] forKey:@"_idRating"];
                    [self.buttonArray enumerateObjectsUsingBlock:^(UIButton * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        //        [obj setTitle:(idx <= buttonIndex ? @"★" : @"☆") forState:UIControlStateNormal];
                        [obj setImage:(idx <= buttonIndex ? [UIImage imageNamed:@"star_big_red.png"] : [UIImage imageNamed:@"star_big_gray.png"]) forState:UIControlStateNormal];
                    }];
                    
                    [activityView stopAnimating];
//                    NSDictionary  *serverData;
//                    
//                    NSPredicate *predicate;
                    
                    //[realm beginWriteTransaction];
                    //                    Servers *server;
                    //
                    //                    serverData = [dataDic valueForKey:@"data"];
                    //                    //for (serverData in [dataDic valueForKey:@"data"])
                    //                    {
                    //                        predicate = [NSPredicate predicateWithFormat:@"serverID = %ld",[[serverData valueForKey:@"_idUserProfile"] integerValue]];
                    //                        server =  [[Servers objectsWithPredicate:predicate] firstObject];
                    //
                    //                        NSLog(@"Rated Server:%@",server);
                    //
                    //                        server.toalRatings = [[serverData valueForKey:@"totalRatings"] integerValue];
                    //                        server.avgRating = [[serverData valueForKey:@"avgRating"] floatValue];
                    //                        server.oneRatings = [[serverData valueForKey:@"oneRatings"] integerValue ];
                    //                        server.twoRatings = [[serverData valueForKey:@"twoRatings"] integerValue];
                    //                        server.threeRatings = [[serverData valueForKey:@"threeRatings"] integerValue];
                    //                        server.fourRatings = [[serverData valueForKey:@"fourRatings"] integerValue];
                    //                        server.fiveRatings = [[serverData valueForKey:@"fiveRatings"] integerValue];
                    //
                    //
                    //                        server.RatingId = [[serverData valueForKey:@"_idRating"] integerValue];
                    
                    //server.isRated = @"yes";
                    
                    //  }
                    
                    @try {
                        //[realm addObject:server];
                        
                        
                    } @catch (NSException *exception) {
                        NSLog(@"Post Comment To Server Exception:%@",exception.description);
                    }
                    
                    // [realm commitWriteTransaction];
                    
                    [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"isRated"];
                    
                    [APP.hud setHidden:YES];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        
                    });
                    
                    ratingAlert = [self.storyboard instantiateViewControllerWithIdentifier:@"RatingSuccessAlertViewController"];
                    NSDictionary *dict = [dataDic valueForKey:@"data"];
                    if ([[dict valueForKey:@"isAlreadyRated"] intValue] == 1 && [[dict valueForKey:@"isAlreadyCommented"] intValue] == 1)
                    {
                        ratingAlert.alertTitle = @"Update Your Review";
                        ratingAlert.alertMessage = @"Comments";
                        ratingAlert.isAlreadyCommented = @"YES";
                        ratingAlert.lblMessage.text = @"It seems you already rated & reviewed this Bartender/Server today, still you can edit and update your review";
                        
                        [[NSUserDefaults standardUserDefaults]setValue: @"It seems you already rated & reviewed this Bartender/Server today, still you can edit and update your review"forKey:@"message"];
                        [[NSUserDefaults standardUserDefaults]setValue:[dict valueForKey:@"comment"] forKey:@"commentText"];
                        
                    }
                    else
                    {
                        ratingAlert.alertTitle = @"Post Your Review";
                        ratingAlert.alertMessage = @"Comments";
                        ratingAlert.lblMessage.text = @"You have successfully rated this Bartender/Server you can share your review by giving comment";
                        
                        [[NSUserDefaults standardUserDefaults]setValue: @"You have successfully rated this Bartender/Server you can share your review by giving comment"forKey:@"message"];
                        ratingAlert.isAlreadyCommented = @"NO";
                    }
                    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"rating"]] forKey:@"givenRatings"];
                    ratingAlert.transitioningDelegate = (id)self;
                    ratingAlert.modalPresentationStyle = UIModalPresentationCustom;
                    
                    [self.navigationController presentViewController:ratingAlert animated:NO completion:nil];
                    
                    
                }
                else if(status == 400)
                {
                    //                    [activityView stopAnimating];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [APP.hud setHidden:YES];
                        UIAlertController * alert2=   [UIAlertController
                                                       alertControllerWithTitle:@"Sorry"
                                                       message:[dataDic valueForKey:@"result"]
                                                       preferredStyle:UIAlertControllerStyleAlert];
                        
                        [self presentViewController:alert2 animated:NO completion:nil];
                        
                        
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                            [alert2 dismissViewControllerAnimated:NO completion:nil];
                        });
                    });
                    
                }
                
            }
            
            
        }] resume];
        
    }
    
    
}

- (IBAction)restaurantSegmentPressed:(UISegmentedControl *)sender {
    restroID = [[arrCurrentRestaurants objectAtIndex:sender.selectedSegmentIndex] valueForKey:@"_idWorksInRestaurant"];
}


@end
