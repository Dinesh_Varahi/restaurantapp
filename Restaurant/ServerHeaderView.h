//
//  ServerHeaderView.h
//  Restaurant
//
//  Created by HN on 28/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MVPlaceSearchTextField.h"
#import <QuartzCore/QuartzCore.h>
#import "AddressResolver.h"
#import "CustomButton.h"


@class AddressResolver;
@class ServerHeaderView;

@protocol HeaderViewDelegate <NSObject>
-(void)headerView:(ServerHeaderView*)view didEndEditingServerNameTextField:(UITextField*)serverName;
-(void)headerView:(ServerHeaderView*)view didEndEditingCityNameTextField:(UITextField*)cityName;
-(void)headerView:(ServerHeaderView*)view didEndEditingRestaurantNameTextField:(UITextField*)restaurantName;
-(void)headerView:(ServerHeaderView*)view resolvedAddressSelected:(NSString*)location;
@end

@interface ServerHeaderView : UICollectionReusableView<UITextFieldDelegate,PlaceSearchTextFieldDelegate>
{
     id <HeaderViewDelegate> delegate;    
}
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnSearch;
@property (strong, nonatomic) IBOutlet UITextField *txtServerName;
@property (strong, nonatomic) IBOutlet MVPlaceSearchTextField *txtLocation;
@property (strong, nonatomic) IBOutlet UITextField *txtRestaurantName;
@property (strong, nonatomic) IBOutlet UIButton *overlayButton;

@property (nonatomic, weak) id <HeaderViewDelegate> delegate;

@end
