//
//  ServerHeaderView.m
//  Restaurant
//
//  Created by HN on 28/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "ServerHeaderView.h"

@implementation ServerHeaderView

@synthesize delegate = _delegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.txtServerName.delegate = self;
    self.txtLocation.delegate = self;
    self.txtRestaurantName.delegate = self;
    
    self.txtLocation.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"location"];
    
    self.lblHeaderTitle.text = @"Top Bartender/Server List";
    self.txtLocation.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"location"];
    
    [self.btnSearch addTarget:self action:@selector(hideKeyboard:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self customTextField];
    [self setupAutoCompleteTextField];
    [self setChangeLocationButton];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    textField.textColor = [UIColor whiteColor];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
//    self.view.frame  = CGRectMake(0, -30, self.view.frame.size.width,self.view.frame.size.height)    ;
    
    
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    
//    self.view.frame  = CGRectMake(0, 0, self.view.frame.size.width,self.view.frame.size.height)    ;
    
    if (textField == _txtServerName) {
        NSLog(@"Server:%@",_txtServerName.text);
        [self.delegate headerView:self didEndEditingServerNameTextField:textField];
        
    }
    if (textField == _txtLocation) {
        NSLog(@"City:%@",_txtLocation.text);
        NSString *location;

        if (_txtLocation.text.length > 0) {
            location = [[AddressResolver sharedInstance] validateAddress:_txtLocation.text];
            
            _txtLocation.text = location;
        }
    }
    if (textField == _txtRestaurantName) {
        NSLog(@"Restaurant:%@",_txtRestaurantName.text);
       
        [self.delegate headerView:self didEndEditingRestaurantNameTextField:textField];
    }
    
    
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.superview endEditing:YES];
}

- (void) customTextField {
    
    CALayer *border;
    CGFloat borderWidth ;
    
    _txtLocation.borderStyle = UITextBorderStyleNone;
    
    border = [CALayer layer];
    borderWidth = 1.5;
    border.borderColor = [UIColor textFieldBorderColor].CGColor;
    border.frame = CGRectMake(0,_txtLocation.frame.size.height - borderWidth , _txtLocation.frame.size.width, _txtLocation.frame.size.height);
    border.borderWidth = borderWidth;
    [_txtLocation.layer addSublayer:border];
    _txtLocation.layer.masksToBounds = YES;
}


#pragma mark - Place search Textfield Delegates

-(void)setupAutoCompleteTextField {
    
    _txtLocation.placeSearchDelegate = self;
    
    _txtLocation.strApiKey = kGoogleAPIKey;
    
    _txtLocation.superViewOfList = self.superview;
    
    _txtLocation.autoCompleteShouldHideOnSelection = YES;
    
    _txtLocation.maximumNumberOfAutoCompleteRows  = 4;
    
    _txtLocation.partOfAutoCompleteRowHeightToCut = 2.0;
    
    
}

-(void)placeSearch:(MVPlaceSearchTextField*)textField ResponseForSelectedPlace:(GMSPlace*)responseDict {
    [self.superview endEditing:YES];
    [_txtLocation resignFirstResponder];
    
    NSLog(@"SELECTED ADDRESS :%@",responseDict);
    NSLog(@"City Name:%@",responseDict.types);
    
    NSString *location;
    
    if (_txtLocation.text.length > 0) {
        
        
    location = [[AddressResolver sharedInstance] validateAddress:_txtLocation.text];
        
        _txtLocation.text = location;
        
        [self.delegate headerView:self resolvedAddressSelected:location];
        
    }
    
    if (textField == _txtLocation) {
//        fromLocationCoord.latitude = responseDict.coordinate.latitude;
//        fromLocationCoord.longitude = responseDict.coordinate.longitude;
        //        fromLocationString = responseDict.formattedAddress;
        
        
    }
    
}
-(void)placeSearchWillShowResult:(MVPlaceSearchTextField*)textField {
    
}
-(void)placeSearchWillHideResult:(MVPlaceSearchTextField*)textField {
    
}
-(void)placeSearch:(MVPlaceSearchTextField*)textField ResultCell:(UITableViewCell*)cell withPlaceObject:(PlaceObject*)placeObject atIndex:(NSInteger)index {
    if(index%2==0){
        cell.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    }else{
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
}

- (void) setChangeLocationButton {
    _overlayButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_overlayButton setTitle:@"Change Location" forState:UIControlStateNormal];
    [_overlayButton addTarget:self action:@selector(changeLocationPressed:)
            forControlEvents:UIControlEventTouchUpInside];
    [_overlayButton setFrame:CGRectMake(0, 0, _txtLocation.frame.size.width/2.5, 28)];
    _overlayButton.titleLabel.font = [UIFont systemFontOfSize:14];
    
    _overlayButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    
    // Assign the overlay button to a stored text field
    self.txtLocation.rightView = _overlayButton;
    self.txtLocation.rightViewMode = UITextFieldViewModeAlways;
}

- (void) changeLocationPressed:(id)sender {
    NSLog(@"Change Location");
    
    
}

- (void) hideKeyboard:(id)sender {
    
   
[self.superview endEditing:YES];

}

@end
