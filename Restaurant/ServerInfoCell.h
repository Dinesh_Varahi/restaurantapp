//
//  ServerInfoCell.h
//  Restaurant
//
//  Created by Parth Pandya on 13/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface ServerInfoCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgServer;
@property (weak, nonatomic) IBOutlet UILabel *lblServerName;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *Ratings;
@property (weak, nonatomic) IBOutlet UILabel *lblRestoName;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalRatings;


@property (weak, nonatomic) IBOutlet UIImageView *imgServer1;
@property (weak, nonatomic) IBOutlet UILabel *lblServerName1;
@property (weak, nonatomic) IBOutlet UILabel *lblRestoName1;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *Ratings1;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalRatings1;


@end
