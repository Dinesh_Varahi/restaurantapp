//
//  ServerInfoCell.m
//  Restaurant
//
//  Created by Parth Pandya on 13/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "ServerInfoCell.h"

@implementation ServerInfoCell


- (void)bindWithModel:(id)model {
    // Do whatever you may need to bind with your data and
    // tell the collection view cell's contentView to resize
    _imgServer.layer.cornerRadius = _imgServer.frame.size.height/2;
    _imgServer.layer.masksToBounds = YES;
    
    _imgServer1.layer.cornerRadius = _imgServer1.frame.size.height/2;
    _imgServer1.layer.masksToBounds = YES;
    [self.contentView setNeedsLayout];
}
@end
