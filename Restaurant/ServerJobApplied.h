//
//  ServerJobApplied.h
//  Restaurant
//
//  Created by HN on 30/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <Realm/Realm.h>

@interface ServerJobApplied : RLMObject

@property NSString *restoName;
@property NSString *workProfile;
@property NSString *city;
@property NSString *zipCode;
@property NSInteger jobID;
@property NSString *jobStatus;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<ServerJobApplied>
RLM_ARRAY_TYPE(ServerJobApplied)
