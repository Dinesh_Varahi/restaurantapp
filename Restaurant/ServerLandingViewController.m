//
//  ServerLandingViewController.m
//  Restaurant
//
//  Created by HN on 21/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "ServerLandingViewController.h"

@interface ServerLandingViewController ()///<>

@end

@implementation ServerLandingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [super viewDidLoad];
    
    
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"DashboardViewController"]];
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"ServerRatingsVC"]];
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"RatingViewController"]];
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"JobsAppliedViewController"]];
    
    
    
}

- (void)viewDidAppear:(BOOL)animated {

    
//    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"goToProfileView"]) {
//        
//        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"goToProfileView"];
//        NSLog(@"Go To Profile View");
//        ProfileStep1ViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileStep1ViewController"];
//        
//            [self.navigationController pushViewController:profileVC animated:NO];
//    }

}

- (void) setLoggedInUser {
    
    NSString *flag = [[NSUserDefaults standardUserDefaults] valueForKey:@"isuserloggedin"];
    NSLog(@"flag:%@",flag);
    if([flag isEqualToString:@"yes"] ){
        
        //        NSString *title = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
        //        NSLog(@"Title:%@",title);
        
        self.editProfileButton = [[UIBarButtonItem alloc]
                                  initWithTitle:@"LOGOUT"
                                  style:UIBarButtonItemStylePlain
                                  target:self
                                  action:@selector(btnLogOutPressed:)];
        
    }
    else
    {
        
        self.editProfileButton = [[UIBarButtonItem alloc]
                                  initWithTitle:@"LOGIN"
                                  style:UIBarButtonItemStylePlain
                                  target:self
                                  action:@selector(btnLogOutPressed:)];
        
    }
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    //    [self setNavigationBar];
    [self setLoggedInUser];
    
    self.navigationItem.rightBarButtonItem = self.editProfileButton;
    
    [self.editProfileButton setTitleTextAttributes:@{
                                                     NSFontAttributeName: [UIFont systemFontOfSize:13.0 weight:UIFontWeightMedium],
                                                     NSForegroundColorAttributeName: [UIColor whiteColor]
                                                     } forState:UIControlStateNormal];
    
    
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    [menuButton setImage:[UIImage imageNamed:@"menu_button.png"] forState:UIControlStateNormal];
    menuButton.imageEdgeInsets = UIEdgeInsetsMake(3, 4, 3, 4);
    [menuButton addTarget:self action:@selector(btnMenuPressed:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.btnLogOut = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    
    
    //    self.navigationItem.leftBarButtonItem = self.btnLogOut;
    
    
    
    [self.btnLogOut setTitleTextAttributes:@{
                                             NSFontAttributeName: [UIFont systemFontOfSize:13.0 weight:UIFontWeightMedium],
                                             NSForegroundColorAttributeName: [UIColor whiteColor]
                                             } forState:UIControlStateNormal];
    
    
    // Go to profile View from Menu
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"goToProfileView"]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"goToProfileView"];
        NSLog(@"Go To Profile View");
        ProfileStep1ViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileStep1ViewController"];
        
        [self.navigationController pushViewController:profileVC animated:NO];
    }

    
    // Go to Privacy view
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"goToPrivacyView"]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"goToPrivacyView"];
        
        NSLog(@"Go To Policies View");
        PrivcayPoliciesViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivcayPoliciesViewController"];
        
        [self.navigationController pushViewController:profileVC animated:NO];
    }
    
    
}

- (IBAction)btnMenuPressed:(id)sender{
    
    MenuVC *menuVC = [[self storyboard] instantiateViewControllerWithIdentifier:@"MenuVC"];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"toggleMenu"]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"toggleMenu"];
        
        UINavigationController *navSecondView = [[UINavigationController alloc] initWithRootViewController:menuVC];
        [self presentViewController:navSecondView animated:YES completion:nil];
        
    }
    
    
    
}


- (IBAction)btnLogOutPressed:(id)sender {
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [APP huddie];
        
    });
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,SIGN_OUT_URL]];
    
    AppUser *user = [[AppUser allObjects] firstObject];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"bearer %@",user.token] forHTTPHeaderField:@"Authorization"];
    
    
    [manager POST:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        if ([responseObject isKindOfClass:[NSArray class]])
        {
            NSArray *jsonArray = (NSArray *)responseObject  ;
            NSLog(@"Json:%@",jsonArray);
        }
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dataDic = (NSDictionary *)responseObject;
            //                NSDictionary *Dict = [dataDic valueForKey:@"data"];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertController * alert2=   [UIAlertController
                                               alertControllerWithTitle:[dataDic valueForKey:@"result"]
                                               message:@""
                                               preferredStyle:UIAlertControllerStyleAlert];
                [APP.hud setHidden:YES];
                [self presentViewController:alert2 animated:NO completion:nil];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    
                    [alert2 dismissViewControllerAnimated:NO completion:nil];
                    
                    GIDSignIn*sigNIn=[GIDSignIn sharedInstance];
                    [sigNIn signOut];
                    
                    RLMRealm *realm = [RLMRealm defaultRealm];
                    [realm beginWriteTransaction];
                    
                    [realm deleteObjects:[AppUser allObjectsInRealm:realm]];
                    
                    [realm commitWriteTransaction];
                    
                    FBSDKLoginManager *FBlogin = [[FBSDKLoginManager alloc] init];
                    [FBlogin logOut];
                    [FBSDKAccessToken setCurrentAccessToken:nil];
                    [FBSDKProfile setCurrentProfile:nil];
                    [[NSUserDefaults standardUserDefaults] setValue:@"no" forKey:@"isuserloggedin"];
                    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"rememberMe"];
                   
                    LoginView *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginNav"];
                    [self.navigationController presentViewController:login animated:NO completion:nil];

                });
            });
            
            
            
            return;
            
            
        }
        
        
        
    }
          failure:^(NSURLSessionTask *operation, NSError *error) {
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  [APP.hud setHidden:YES];
              });
              
              NSLog(@"Error: %@", error);
              
          }];

    
    
}

- (void)setNavigationBar{
    
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    
    //    self.navigationController.view.backgroundColor = [UIColor appMainColor];
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor appMainColor]];
    
    //    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
    //                                                         forBarMetrics:UIBarMetricsDefault];
}

- (void)clearPersistedData {
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm deleteObjects:[Servers allObjectsInRealm:realm]];
    [realm deleteObjects:[AppUser allObjectsInRealm:realm]];
    [realm deleteObjects:[ServerRating allObjectsInRealm:realm]];
    [realm deleteObjects:[Restaurants allObjectsInRealm:realm]];
    [realm deleteObjects:[RestaurantImages allObjectsInRealm:realm]];
    [realm deleteObjects:[ServerWorkExperience allObjectsInRealm:realm]];
    
    [realm commitWriteTransaction];
    
    
}

@end
