//
//  ServerListCell.m
//  Restaurant
//
//  Created by HN on 04/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "ServerListCell.h"
#import <UIKit/UIKit.h>

@implementation ServerListCell

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.imageView.layer.cornerRadius = (self.imageView.frame.size.height/2);
    self.imageView.image = nil;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        self.imageView.image = nil;
        
    }
    return self;
}


- (void)drawRect:(CGRect)rect
{
    self.imageView.layer.cornerRadius = self.imageView.frame.size.height / 2;
    self.imageView.clipsToBounds = YES;
    
    self.background.layer.cornerRadius = 10;
    self.background.clipsToBounds = YES;
    
}
- (IBAction)restaurantButtonPressed:(id)sender {
}
@end
