//
//  ServerListCustomFlowLayout.m
//  Restaurant
//
//  Created by HN on 04/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "ServerListCustomFlowLayout.h"

@implementation ServerListCustomFlowLayout
- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.minimumLineSpacing = 2.0;
        self.minimumInteritemSpacing = 1.0;
        
//        self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        self.scrollDirection = UICollectionViewScrollDirectionVertical;
        
    }
    return self;
}
- (CGSize)itemSize
{
    NSInteger numberOfColumns = 3;
    
    CGFloat itemWidth = (CGRectGetWidth(self.collectionView.frame) - (numberOfColumns - 1)) / numberOfColumns;
    CGFloat itemHeight = (CGRectGetHeight(self.collectionView.frame)) / 3.5;
    
    return CGSizeMake(itemWidth, itemHeight);
    
}
@end
