//
//  ServerProfile.h
//  Restaurant
//
//  Created by HN on 01/12/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <Realm/Realm.h>

@interface ServerProfile : RLMObject


@end

// This protocol enables typed collections. i.e.:
// RLMArray<ServerProfile>
RLM_ARRAY_TYPE(ServerProfile)
