//
//  ServerRating.h
//  Restaurant
//
//  Created by HN on 30/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <Realm/Realm.h>

@interface ServerRating : RLMObject

@property NSInteger rating;
@property NSString *customerName;
@property NSString *date;
@property NSString *comment;
@end

// This protocol enables typed collections. i.e.:
// RLMArray<ServerRating>
RLM_ARRAY_TYPE(ServerRating)
