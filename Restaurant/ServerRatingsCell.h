//
//  ServerRatingsCell.h
//  Restaurant
//
//  Created by Parth Pandya on 01/02/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface ServerRatingsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRatings;
@property (weak, nonatomic) IBOutlet UILabel *lblGivenBy;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@end
