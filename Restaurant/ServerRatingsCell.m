//
//  ServerRatingsCell.m
//  Restaurant
//
//  Created by Parth Pandya on 01/02/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "ServerRatingsCell.h"

@implementation ServerRatingsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setNeedsLayout];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
