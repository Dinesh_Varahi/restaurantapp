//
//  ServerRatingsVC.h
//  Restaurant
//
//  Created by Parth Pandya on 01/02/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface ServerRatingsVC : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
    int pageIndex;
    NSDictionary *serverDict;
    NSMutableArray *arrRatings;
    NSMutableArray *arrServers;
    NSDictionary *serverinfo;
    NSMutableArray *arrExperiance;
    NSDictionary *serverExperiance;

}
@property (weak, nonatomic) IBOutlet UIProgressView *progress5;
@property (weak, nonatomic) IBOutlet UIProgressView *progress4;
@property (weak, nonatomic) IBOutlet UIProgressView *progress3;
@property (weak, nonatomic) IBOutlet UIProgressView *progress2;

@property (weak, nonatomic) IBOutlet UIProgressView *progress1;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalRatingsCount;
@property (weak, nonatomic) IBOutlet UILabel *lblOneRating;
@property (weak, nonatomic) IBOutlet UILabel *lblTwoRating;
@property (weak, nonatomic) IBOutlet UITableView *tblRatings;
@property (weak, nonatomic) IBOutlet UILabel *lblThreeRating;
@property (weak, nonatomic) IBOutlet UILabel *lblFourRating;
@property (weak, nonatomic) IBOutlet UILabel *lblFiveRating;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRatings;
@end
