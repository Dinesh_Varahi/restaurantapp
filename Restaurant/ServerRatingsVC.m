//
//  ServerRatingsVC.m
//  Restaurant
//
//  Created by Parth Pandya on 01/02/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "ServerRatingsVC.h"
#import "AppDelegate.h"
#import "ServerRatingsCell.h"

@interface ServerRatingsVC ()

@end

@implementation ServerRatingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _tblRatings.delegate = self;
    _tblRatings.dataSource = self;
    _tblRatings.estimatedRowHeight = 74.0;
    [self FetchProfileDetails];
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    Servers *server = [[Servers allObjects] firstObject];
    
    self.starRatings.value = server.avgRating;
    self.lblOneRating.text = [NSString stringWithFormat:@"%ld",(long)server.oneRatings];
    self.lblTwoRating.text = [NSString stringWithFormat:@"%ld",(long)server.twoRatings];
    self.lblThreeRating.text = [NSString stringWithFormat:@"%ld",(long)server.threeRatings];
    self.lblFourRating.text = [NSString stringWithFormat:@"%ld",(long)server.fourRatings];
    self.lblFiveRating.text = [NSString stringWithFormat:@"%ld",(long)server.fiveRatings];
    
    _lblTotalRatingsCount.text = [NSString stringWithFormat:@"%ld Ratings",(long)server.toalRatings];
    float TotalRatings = server.toalRatings;
    self.progress1.progress = server.oneRatings/TotalRatings;
    self.progress2.progress = server.twoRatings/TotalRatings;
    self.progress3.progress = server.threeRatings/TotalRatings;
    self.progress4.progress = server.fourRatings/TotalRatings;
    self.progress5.progress = server.fiveRatings/TotalRatings;
    //
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)FetchProfileDetails {
    AppUser *user = [[AppUser allObjects] firstObject];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"bearer %@",user.token] forHTTPHeaderField:@"Authorization"];
    
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?serverId=%@&page=%d",SITE_URL,GET_ALLSERVER_RATINGS,[[NSUserDefaults standardUserDefaults] valueForKey:@"ServerID"],pageIndex]];
    NSLog(@"%@",URL);
    [manager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        if ([responseObject isKindOfClass:[NSArray class]])
        {
            NSArray *jsonArray = (NSArray *)responseObject  ;
            NSLog(@"Json:%@",jsonArray);
        }
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dataDic = (NSDictionary *)responseObject;
            NSLog(@"%@",dataDic);
            
            
            
            
            serverDict = (NSDictionary *)responseObject;
            serverinfo = [[NSDictionary alloc]init];
            serverinfo = [serverDict valueForKey:@"data"];
            NSLog(@"%@",serverinfo);
            arrRatings = [serverDict valueForKey:@"data"];                     [self.tblRatings reloadData];
            
        }
    }failure:^(NSURLSessionTask *operation, NSError *error) {
        
        
        
        [APP.hud setHidden:YES];
        
        
        NSLog(@"Error: %@", error);
    }];
    
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return arrRatings.count;
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //    UITableViewCell *Cell;
    ServerRatingsCell *Cell = [[ServerRatingsCell alloc] init];
    NSDictionary *Dict = [arrRatings objectAtIndex:indexPath.row];
    Cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    Cell.starRatings.value = [[Dict valueForKey:@"rating"] integerValue];
    Cell.lblDate.text = [NSString getFormattedDate:[Dict valueForKey:@"updatedAt"]];
    if ([[Dict valueForKey:@"name"] length] > 0)
    {
        NSString *tmpString = [[Dict valueForKey:@"name"] capitalizedString];
        
        Cell.lblGivenBy.text = [NSString stringWithFormat:@"By : %@",tmpString];
    }
    else
    {
        Cell.lblGivenBy.text = [NSString stringWithFormat:@"By : Annonymus"];
    }
    
    
    
    return Cell;
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (arrRatings.count == 0)
    {
        return @"No Ratings";
    }
    return [NSString stringWithFormat:@"%lu Ratings",(unsigned long)arrRatings.count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;
}

@end
