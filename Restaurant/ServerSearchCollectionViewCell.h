//
//  ServerSearchCollectionViewCell.h
//  Restaurant
//
//  Created by HN on 19/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface ServerSearchCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *serverName;
@property (weak, nonatomic) IBOutlet UILabel *ratings;

@property (weak, nonatomic) IBOutlet UILabel *restaurantName;

@property (strong, nonatomic) IBOutlet UIView *background;

@property (weak, nonatomic) IBOutlet UIButton *btnRate;

@property (weak, nonatomic) IBOutlet UIImageView *star1;
@property (weak, nonatomic) IBOutlet UIImageView *star2;
@property (weak, nonatomic) IBOutlet UIImageView *star3;
@property (weak, nonatomic) IBOutlet UIImageView *star4;
@property (weak, nonatomic) IBOutlet UIImageView *star5;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRatings;

@end


