//
//  ServerSearchCollectionViewCell.m
//  Restaurant
//
//  Created by HN on 19/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "ServerSearchCollectionViewCell.h"

@implementation ServerSearchCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.btnRate.titleLabel setFont:[UIFont systemFontOfSize:12 weight:UIFontWeightMedium]];

}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.imageView.layer.cornerRadius = (self.imageView.frame.size.height/2);
    self.imageView.image = nil;
    [self.btnRate.titleLabel setFont:[UIFont systemFontOfSize:12 weight:UIFontWeightMedium]];

}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        self.imageView.image = nil;
        
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    self.imageView.layer.cornerRadius = self.imageView.frame.size.height / 2;
    self.imageView.clipsToBounds = YES;
    
    self.background.layer.cornerRadius = 10;
    self.background.clipsToBounds = YES;
    
}

@end

