//
//  ServerSearchEmptyCollectionViewCell.h
//  Restaurant
//
//  Created by HN on 19/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServerSearchEmptyCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *backgroundBanner;

@property (weak, nonatomic) IBOutlet UIButton *addServerButton;


@end

