//
//  ServerSearchHeaderView.h
//  Restaurant
//
//  Created by HN on 28/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServerSearchHeaderView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblCountOfServers;
@end
