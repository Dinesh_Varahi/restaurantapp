//
//  ServerSearchViewController.h
//  Restaurant
//
//  Created by HN on 18/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerSearchCollectionViewCell.h"
#import "ServerSearchCollectionFlowLayout.h"
//#import "ServerSearchEmptyCollectionViewCell.h"
#import "AddServerViewController.h"
#import "ServerSearchHeaderView.h"
#import "Servers.h"
#import "Restaurants.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ServerSearchViewController : UIViewController <UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    
    __weak IBOutlet UIView *noServerFoundView;
    BOOL isFiltering;
    __weak IBOutlet UIView *noServerFoundContainerView;
    NSDictionary *serverDict;
    NSDictionary *serverData;
    NSMutableArray *arrExperinace;
    NSMutableArray *arrServers;
    int pageIndex;
    
    
}
@property (weak, nonatomic) IBOutlet UITextField *txtServerName;
@property (weak, nonatomic) IBOutlet UITextField *txtLocation;
@property (weak, nonatomic) IBOutlet UITextField *txtRestaurantName;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic) CGFloat lastContentOffset;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnLogin;

@property (strong, nonatomic) NSString *serverName;
@property (strong, nonatomic) NSString *location;
@property (strong, nonatomic) NSString *restaurantName;

- (IBAction)btnAddNewServerPressed:(id)sender;



@end




