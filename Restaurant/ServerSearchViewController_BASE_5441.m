//
//  ServerSearchViewController.m
//  Restaurant
//
//  Created by HN on 18/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "ServerSearchViewController.h"
#import "ServerListCustomFlowLayout.h"

@interface ServerSearchViewController ()

@property (nonatomic, strong) RLMResults *ServerArray;
@property (nonatomic, strong) RLMResults *filteredServerArray;
@property  BOOL isFiltered;

@property (strong, nonatomic) NSArray<UIImageView *> *ratingStars;
@property (nonatomic) NSInteger rating;

@end


@implementation ServerSearchViewController

- (void) viewDidLoad {
    [super viewDidLoad];
    [self setNavigationbar];
    
    NSLog(@"Server:%@, City:%@, Restaurant:%@:",self.serverName,self.location,self.restaurantName);
    self.txtServerName.textColor = [UIColor whiteColor];
    self.txtLocation.textColor = [UIColor whiteColor];
    self.txtRestaurantName.textColor = [UIColor whiteColor];
    
    @try {
        
        NSPredicate *predicate;
        
        // Filter by server
        if ((self.serverName.length > 0) && (self.location.length <= 0) && (self.restaurantName.length <= 0)) {
            predicate = [NSPredicate predicateWithFormat:@"serverName BEGINSWITH[c] %@",self.serverName];
            self.ServerArray = [Servers objectsWithPredicate:predicate];
        }
        
        // Filter by location
        if ((self.serverName.length <= 0) && (self.location.length > 0) && (self.restaurantName.length <= 0)) {
            predicate = [NSPredicate predicateWithFormat:@"city BEGINSWITH[c] %@",self.location];
            self.ServerArray = [Servers objectsWithPredicate:predicate];
        }
        
        // Filter by restaurant
        if ((self.serverName.length <= 0) && (self.location.length <= 0) && (self.restaurantName.length > 0)) {
            predicate = [NSPredicate predicateWithFormat:@"restaurantName BEGINSWITH[c] %@",self.restaurantName];
            self.ServerArray = [Servers objectsWithPredicate:predicate];
        }
        
        // Filter By Server and location
        if ((self.serverName.length > 0) && (self.location.length > 0)) {
            predicate = [NSPredicate predicateWithFormat:@"serverName BEGINSWITH[c] %@ AND city BEGINSWITH[c] %@",self.serverName,self.location];
            self.ServerArray = [Servers objectsWithPredicate:predicate];
        }
        
        // Filter By location and Restaurant
        if ((self.location.length > 0) && (self.restaurantName.length > 0)) {
            predicate = [NSPredicate predicateWithFormat:@"city BEGINSWITH[c] %@ AND restaurantName BEGINSWITH[c] %@",self.serverName,self.restaurantName];
            self.ServerArray = [Servers objectsWithPredicate:predicate];
        }
        
        // Filter By Server and Restaurant
        if ((self.serverName.length > 0) && (self.restaurantName.length > 0)) {
            predicate = [NSPredicate predicateWithFormat:@"serverName BEGINSWITH[c] %@ AND restaurantName BEGINSWITH[c] %@",self.serverName,self.restaurantName];
            self.ServerArray = [Servers objectsWithPredicate:predicate];
        }
        
        
    } @catch (NSException *exception) {
        NSLog(@"Exception:%@",exception.description);
    } @finally {
        
        //        self.ServerArray = [Servers allObjects];
    }
    
    NSLog(@"Searched Server:%@",_ServerArray);
    //self.filteredServerArray = self.ServerArray;
    
    [_txtServerName setDelegate:self];
    [_txtLocation  setDelegate:self];
    [_txtRestaurantName setDelegate:self];
    
    _txtServerName.text = self.serverName;
    _txtLocation.text = self.location;
    _txtRestaurantName.text = self.restaurantName;
    
    [self.collectionView setDelegate:self];
    [self.collectionView setDataSource:self];
    
    
    if (self.ServerArray.count > 0) {
        self.collectionView.hidden = NO;
        noServerFoundView.hidden = YES;
    }
    else{
        self.collectionView.hidden = YES;
        noServerFoundView.hidden = NO;
        
    }
    
    [_txtServerName addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_txtLocation addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_txtRestaurantName addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [self setLoggedInUser];
}
- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    //        self.navigationController.view.backgroundColor = [UIColor appMainColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
}
- (void) viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:YES];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"ServerSearchCollectionViewCell" bundle:[NSBundle mainBundle]]
          forCellWithReuseIdentifier:@"ServerSearchCollectionViewCell"];
    
    
    [self setNavigationbar];
    [self setLoggedInUser];
}

- (void) setLoggedInUser {
    
    NSString *flag = [[NSUserDefaults standardUserDefaults] valueForKey:@"isuserloggedin"];
    NSLog(@"flag:%@",flag);
    if([flag isEqualToString:@"yes"] ){
        
        //        NSString *title = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
        //        NSLog(@"Title:%@",title);
        [self.btnLogin setTitle:@"LOGOUT"];
        
    }
    else
    {
        [self.btnLogin setTitle:@"LOGIN"];
    }
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textDidChange:(id)sender {
    
    UITextField* searchField = (UITextField *) sender;
    
    if (searchField == _txtServerName) {
        if(searchField.text.length == 0)
        {
            self.isFiltered = FALSE;
            
        }
        else {
            self.isFiltered = TRUE;
            
            NSPredicate *predicate;
            
            if (_txtLocation.text.length > 0 && _txtRestaurantName.text.length > 0) {
                
                
                
            }
            else if (_txtLocation.text > 0 && _txtRestaurantName.text.length <=0 )
            {
                
            }
            else if (_txtLocation.text <=0 && _txtRestaurantName.text.length > 0)
            {
                
            }
//            predicate = [NSPredicate predicateWithFormat:@"serverName BEGINSWITH[c] %@",searchField.text];
            
            predicate = [NSPredicate predicateWithFormat:@"serverName BEGINSWITH[c] %@ AND city BEGINSWITH[c] %@ AND restaurantName BEGINSWITH[c] %@",searchField.text,_txtLocation.text,_txtRestaurantName.text];
            
            self.filteredServerArray = [Servers objectsWithPredicate:predicate];
            NSLog(@"Filtered Data:%@",self.filteredServerArray);
        }
    }
    if (searchField == _txtLocation) {
        if(searchField.text.length == 0)
        {
            self.isFiltered = FALSE;
            
        }
        else {
            self.isFiltered = TRUE;
            
            NSPredicate *predicate;
            
            predicate = [NSPredicate predicateWithFormat:@"city BEGINSWITH[c] %@",searchField.text];
            self.filteredServerArray = [Servers objectsWithPredicate:predicate];
            NSLog(@"Filtered Data:%@",self.filteredServerArray);
            
        }
        
    }
    if(searchField == _txtRestaurantName) {
        
        if(searchField.text.length == 0)
        {
            self.isFiltered = FALSE;
            
        }
        else {
            self.isFiltered = TRUE;
            
            NSPredicate *predicate;
            
            if (_txtServerName.text.length > 0)
            {
            predicate = [NSPredicate predicateWithFormat:@"restaurantName BEGINSWITH[c] %@",searchField.text];
            }
            else if (_txtServerName.text.length > 0)
            {
                
            }
            predicate = [NSPredicate predicateWithFormat:@"restaurantName BEGINSWITH[c] %@",searchField.text];
            self.filteredServerArray = [Servers objectsWithPredicate:predicate];
            NSLog(@"Filtered Data:%@",self.filteredServerArray);
            
        }
        
    }
    
    
    
    
    if (_txtServerName.text.length == 0 && _txtLocation.text.length == 0 && _txtRestaurantName.text.length == 0) {
        
        self.collectionView.hidden = YES;
        noServerFoundView.hidden = NO;
        //        self.ServerArray = [Servers allObjects];
        
    }
    else if (_txtServerName.text.length == 0 && _txtLocation.text.length == 0 && _txtRestaurantName.text.length == 0 && self.filteredServerArray.count == 0) {
        self.collectionView.hidden = YES;
        noServerFoundView.hidden = NO;
    }
    else {
        
        self.collectionView.hidden = NO;
        noServerFoundView.hidden = YES;
    }
    
    if (self.ServerArray.count > 0 || self.filteredServerArray > 0) {
        
        self.collectionView.hidden = NO;
        
        [self.collectionView reloadData];
        
    }
    
    
    
    
}
- (void)setNavigationbar {
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault]; //UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.shadowImage = [UIImage new];////UIImageNamed:@"transparent.png"
    self.navigationController.view.backgroundColor = [UIColor appMainColor];
    [self.navigationController.navigationBar setBarTintColor:self.view.backgroundColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    self.view.frame  = CGRectMake(0, -15, self.view.frame.size.width,self.view.frame.size.height)
    ;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    
    self.view.frame  = CGRectMake(0, 0, self.view.frame.size.width,self.view.frame.size.height)
    ;
}
-(IBAction)showServerDetails:(id)sender {
    
    UIButton *tmpButton = (UIButton*)sender;
    [self navigateToServerDetails:tmpButton.tag];
    
}

-(void)navigateToServerDetails:(NSInteger)index {
    
    Servers *newServer = self.ServerArray[index];
    
    ServerDetailViewController *serverDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ServerDetailViewController"];
    serverDetailVC.serverID = [NSString stringWithFormat:@"%ld",(long)newServer.serverID];
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%ld",(long)newServer.serverID] forKey:@"ServerID"];
    
    [self.navigationController pushViewController:serverDetailVC animated:NO];
    
    
}

- (IBAction)loginButtonPressed:(id)sender {
    
    
    
    if ([self.btnLogin.title isEqualToString:@"LOGOUT"]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Logged Out Successfully"
                                           message:@""
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:NO completion:nil];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:NO completion:nil];
            });
        });
        
        
        GIDSignIn*sigNIn=[GIDSignIn sharedInstance];
        [sigNIn signOut];
        
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logOut];
        [FBSDKAccessToken setCurrentAccessToken:nil];
        [FBSDKProfile setCurrentProfile:nil];
        
        [self.btnLogin setTitle:@"LOGIN"];
        
        [[NSUserDefaults standardUserDefaults] setValue:@"no" forKey:@"isuserloggedin"];
        
        return;
    }
    
    LoginView *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.navigationController pushViewController:login animated:NO];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"Home" forKey:@"Sender"];
    
    
    
    
}


#pragma mark Collectionview Delegate Methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    
    return CGSizeMake(0., 50.);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    
    if (self.isFiltered)
    {
        
        return self.filteredServerArray.count ;
    }
    
    return self.ServerArray.count;
    
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if((self.ServerArray.count == 0 && !(self.isFiltered)))
    {
        //        ServerSearchEmptyCollectionViewCell *emptyCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ServerSearchEmptyCollectionViewCell" forIndexPath:indexPath];
        //
        //        [emptyCell.addServerButton addTarget:self action:@selector(addNewServerPressed:) forControlEvents:UIControlEventTouchUpInside];
        //
        //        return emptyCell;
        
    }
    
    Servers *newServer ;
    
    if (self.isFiltered) {
        
        newServer = self.filteredServerArray[indexPath.row];
    }
    else{
        newServer = self.ServerArray[indexPath.row];
    }
    
    ServerSearchCollectionViewCell *serverCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ServerSearchCollectionViewCell" forIndexPath:indexPath];
    
    // Assigning Name
    
    if(newServer.serverName.length > 0)
    {
        
        NSString *txt = newServer.serverName;
        txt = [txt stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[txt substringToIndex:1] uppercaseString]];
        
        serverCell.serverName.text = txt;
    }
    
    
    // Assigning Image
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,newServer.imageURL]];
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    ServerSearchCollectionViewCell *updateCell = (id)[collectionView cellForItemAtIndexPath:indexPath];
                    if (updateCell)
                        updateCell.imageView.image = image;
                });
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    ServerSearchCollectionViewCell *updateCell = (id)[collectionView cellForItemAtIndexPath:indexPath];
                    if (updateCell)
                        updateCell.imageView.image = [UIImage imageNamed:@"ic_user_b.png"];;
                });
            }
        }
    }];
    [task resume];
    
    // Assigning Ratings
    
    if (newServer.toalRatings <= 0) {
        serverCell.ratings.text = [NSString stringWithFormat:@"0 Ratings"];
    }
    else
    {
        serverCell.ratings.text = [NSString stringWithFormat:@"%ld Ratings",(long)newServer.toalRatings];
    }
    
    self.ratingStars = @[serverCell.star1,serverCell.star2,serverCell.star3,serverCell.star4,serverCell.star5];
    self.rating = newServer.toalRatings ;
    
    
    [self.ratingStars enumerateObjectsUsingBlock:^(UIImageView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj setImage:(idx < (NSInteger)newServer.avgRating ? [UIImage imageNamed:@"star_filled.png"] : [UIImage imageNamed:@"star_blank.png"])];
    }];
    
    // Rating Button
    serverCell.starRatings.value = newServer.avgRating;
    serverCell.btnRate.tag = indexPath.row;
    [serverCell.btnRate addTarget:self action:@selector(showServerDetails:) forControlEvents:UIControlEventTouchUpInside];
    
    
    // Assiginig Restaurant Name
    
    if (newServer.restaurantName.length > 0)
    {
        serverCell.restaurantName.text = newServer.restaurantName;
    }
    
    return serverCell;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    if (self.ServerArray.count == 0 && self.filteredServerArray.count == 0) {
        
        UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
        NSInteger numberOfCells = self.view.frame.size.width / flowLayout.itemSize.width;
        NSInteger edgeInsets = (self.view.frame.size.width - (numberOfCells * flowLayout.itemSize.width)) / (numberOfCells + 1);
        return UIEdgeInsetsMake(0, edgeInsets, 0, edgeInsets);
    }
    
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *headerCell;
    
    int count;
    
    if (self.isFiltered) {
        
        count = (int)self.filteredServerArray.count;
    }
    else{
        count = (int)self.ServerArray.count;
    }
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        ServerSearchHeaderView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"headerView" forIndexPath:indexPath];
        
        NSString *resultString;
        if(count <= 1)
        {
            resultString = @"Result Found";
        }
        else
        {
            resultString = @"Results Found";
        }
        header.lblHeaderTitle.text = [NSString stringWithFormat:@"%d %@",count,resultString];
        
        headerCell = header;
        
    }
    
    return headerCell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (self.ServerArray.count == 0) {
        
        //CGFloat cellWidth = self.collectionView.frame.size.width / 1.2 ;
        //CGFloat cellHeight = self.collectionView.frame.size.height / 1.2 ;
        //size = CGSizeMake(cellWidth, cellHeight);
        
        //return size;
        
    }
    
    if (self.view.frame.size.width == 320)
    {
        return CGSizeMake(self.view.frame.size.width/3.34,self.view.frame.size.height/3.65);

    }
    return CGSizeMake(self.view.frame.size.width/3.34,self.view.frame.size.height/3.85);
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    if(self.ServerArray.count == 0)
    {
        return ;
    }
    
    [self navigateToServerDetails:indexPath.row];
}



#pragma mark collectionview cell action method implementations
- (void)addNewServerPressed:(id)sender {
    NSLog(@"Add Server Pressed");
    
    AddServerViewController *addServer = [self.storyboard instantiateViewControllerWithIdentifier:@"AddServerViewController"];
    
    
    [self.navigationController pushViewController:addServer animated:NO];
    //    [self.navigationController  presentViewController:navAddServer animated:YES completion:nil];
    
    
}

- (void)rateButtonPressed:(id)sender {
    UIButton *button = (UIButton*)sender;
    NSLog(@"Button at %ld",(long)button.tag);
    
    //        LoginView *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    //        [self.navigationController  pushViewController:login animated:YES];
    
    
    ServerDetailViewController *serverDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"ServerDetailViewController"];
    UINavigationController *navServerDetail = [[UINavigationController alloc] initWithRootViewController:serverDetail];
    [self.navigationController  presentViewController:navServerDetail animated:NO completion:nil];
    
}

- (IBAction)btnAddNewServerPressed:(id)sender {
    
    NSLog(@"Add Server Pressed");
    
    
    AddServerViewController *addServer = [self.storyboard instantiateViewControllerWithIdentifier:@"AddServerViewController"];
    
    [[NSUserDefaults standardUserDefaults]setValue:self.txtServerName.text forKey:@"serverName"];
    [[NSUserDefaults standardUserDefaults]setValue:self.txtLocation.text forKey:@"serverLocation"];
    [self.navigationController pushViewController:addServer animated:NO];
    
}
@end

