//
//  ServerWorkExperience.h
//  Restaurant
//
//  Created by HN on 30/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <Realm/Realm.h>

@interface ServerWorkExperience : RLMObject
@property NSString *restoName;
@property NSString *restoType;
@property NSString *workProfile;
@property NSString *city;
@property NSString *zipCode;
@property NSString *fromDate;
@property NSString *toDate;
@property NSString *isCurrentJob;
@property NSInteger workingRestroID;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<ServerWorkExperience>
RLM_ARRAY_TYPE(ServerWorkExperience)
