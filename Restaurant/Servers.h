//
//  Servers.h
//  Restaurant
//
//  Created by HN on 25/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <Realm/Realm.h>
#import "ServerWorkExperience.h"
#import "ServerRating.h"
#import "ServerJobApplied.h"


@interface Servers : RLMObject

@property NSInteger workingRestroID;
@property NSInteger serverID;
@property NSInteger reward;
@property NSString *email;
@property NSString *role;
@property NSString *city;
@property NSString *mobileNumber;
@property NSString *zipCode;
@property NSString *imageURL;
@property NSString *serverName;
@property NSString *firstName;
@property NSString *middleName;
@property NSString *lastName;
@property NSString *nickName;
@property NSString *restaurantName;
@property NSString *ssNumber;
@property NSString *qualification;
@property float avgRating;
@property NSInteger jobsInPastThirtyDays;
@property NSString *lastComment;
@property NSString *lastRating;
@property NSInteger toalRatings;
@property NSInteger totalComments;
@property NSInteger totalJobsApplied;
@property NSInteger RatingId;
@property NSInteger oneRatings;
@property NSInteger twoRatings;
@property NSInteger threeRatings;
@property NSInteger fourRatings;
@property NSInteger fiveRatings;
@property NSString *isRated;
@property NSString *isCommented;
@property NSString *referralCode;
@property RLMArray<ServerWorkExperience *><ServerWorkExperience> *workExperience;
@property RLMArray<ServerRating *><ServerRating> *ratingAndComment;
@property RLMArray<ServerJobApplied *><ServerJobApplied> *jobApplied;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<Servers>
RLM_ARRAY_TYPE(Servers)
