//
//  ShowMapViewController.h
//  Restaurant
//
//  Created by HN on 04/12/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "AllAvailableJobs.h"

@interface ShowMapViewController : UIViewController
{
    RLMResults<AllAvailableJobs  *> *allAvailableJobs;

}

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
- (IBAction)btnClosePressed:(id)sender;



@end
