//
//  ShowMapViewController.m
//  Restaurant
//
//  Created by HN on 04/12/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "ShowMapViewController.h"

@interface ShowMapViewController ()
{
    float latitude;
    float longitude;
}

@end

@implementation ShowMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    allAvailableJobs = [AllAvailableJobs allObjects];
    
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
//                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];

    latitude = [[[NSUserDefaults standardUserDefaults] valueForKey:@"restaurantLatitude"] floatValue];//[[[allAvailableJobs objectAtIndex:index] valueForKey:@"latitude"] floatValue];
    longitude = [[[NSUserDefaults standardUserDefaults] valueForKey:@"restaurantLongitude"] floatValue];//[[[allAvailableJobs objectAtIndex:index] valueForKey:@"longitude"] floatValue];
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude,longitude);
    
    self.navigationItem.title = [[NSUserDefaults standardUserDefaults] valueForKey:@"buttontag"];
    self.navigationController.navigationBar.backgroundColor = [UIColor appMainColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate addressDictionary:nil];
    
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    [mapItem setName:@"Name/text on destination annotation pin"];
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = coordinate;
    point.title = @"Location Of";
    point.subtitle = [[NSUserDefaults standardUserDefaults] valueForKey:@"buttontag"];
    
    [self.mapView addAnnotation:point];
    
    [self performSelector:@selector(zoomInToMyLocation)
               withObject:nil
               afterDelay:2];
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated {
    [self setNavigationBar];
}
- (void)viewWillDisappear:(BOOL)animated {
    
    [self reSetNavigationBar];
}
- (void)setNavigationBar{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    
}
- (void)reSetNavigationBar{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBarTintColor:[UIColor clearColor]];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    
    
}
-(void)zoomInToMyLocation {
    MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
    region.center.latitude = latitude ;
    region.center.longitude = longitude;
    region.span.longitudeDelta = 0.015f;
    region.span.latitudeDelta = 0.015f;
    [self.mapView setRegion:region animated:YES];
}
- (IBAction)btnClosePressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
