//
//  SignUpViewController.h
//  Restaurant
//
//  Created by HN on 14/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "AppUser.h"
#import "ServerLandingViewController.h"
#import "ProfileStep1ViewController.h"
#import "OwnerDashboard.h"
#import "OwnerProfileStep1ViewController.h"


@interface SignUpViewController : UIViewController <UITextFieldDelegate,NSURLSessionDelegate,UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtEmailID;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UITextField *txtUserRole;
@property (weak, nonatomic) IBOutlet UIButton *btnRemeberMe;

@property (weak, nonatomic) IBOutlet UITextField *txtReferralCode;

- (IBAction)loginButtonPressed:(id)sender;

- (IBAction)signupButtonPressed:(id)sender;

- (IBAction)selectRolePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *userSelectionSegment;
- (IBAction)userSegmentPressed:(id)sender;


@end
