//
//  SignUpViewController.m
//  Restaurant
//
//  Created by HN on 14/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "SignUpViewController.h"

@interface SignUpViewController ()
{
        
    NSString *userImageString;
    NSString *userRole;
    UIActivityIndicatorView *activityView;
    
    BOOL toggle;
}
@end

@implementation SignUpViewController 

- (void)viewDidLoad{
    [super viewDidLoad];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault]; //UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.shadowImage = [UIImage new];////UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    [self.userSelectionSegment setSelectedSegmentIndex:UISegmentedControlNoSegment];
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"Sender"] isEqualToString:@"Comment"])
    {
        [self.userSelectionSegment setSelectedSegmentIndex:1];
        userRole = @"customer";
    }
    
    _userImage.layer.cornerRadius = _userImage.frame.size.width/2;
    _userImage.layer.masksToBounds = YES;
    _userImage.contentMode = UIViewContentModeScaleAspectFill;

    activityView = [[UIActivityIndicatorView alloc]
                   initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    activityView.center=self.view.center;
    [activityView hidesWhenStopped];
    [self.view addSubview:activityView];

   
    self.txtEmailID.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"userEmailFromSocialMedia"];
    
    [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"userEmailFromSocialMedia"];
    [_txtUsername setDelegate:self];
    [_txtEmailID setDelegate:self];
    [_txtPassword setDelegate:self];
    [_txtConfirmPassword setDelegate:self];
    [_txtUserRole setDelegate:self];
    [_txtReferralCode setDelegate:self];
    
    // Image Selection by tapping on imageview
    UITapGestureRecognizer *tapSelectedImage = [[UITapGestureRecognizer alloc]initWithTarget:self
                                                                                      action:@selector(tappedToSelectImage:)];
    tapSelectedImage.delegate = self;
    [tapSelectedImage setNumberOfTapsRequired:1];
    [tapSelectedImage setNumberOfTouchesRequired:1];
    [_userImage addGestureRecognizer:tapSelectedImage];
    [_userImage setUserInteractionEnabled:YES];

    UIFont *font = [UIFont systemFontOfSize:12.0f];
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:font
                                                           forKey:NSFontAttributeName];
    [_userSelectionSegment setTitleTextAttributes:attributes
                                    forState:UIControlStateNormal];
}


#pragma mark - Textfield Delegate methods

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    textField.textColor = [UIColor whiteColor];
    
    if (textField == _txtReferralCode)
    {
        NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        int length = (int)[currentString length];
        if (length > 9) {
            return NO;
        }

    }
        return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    
    if (textField == _txtUserRole) {
        
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            [self dismissViewControllerAnimated:NO completion:nil];
            
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Customer" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            _txtUserRole.text = @"Customer";
            
            userRole = @"customer";
            
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Bartender/Server" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            _txtUserRole.text = @"Bartender/Server";
            
            userRole = @"server";
            
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Restaurant Owner" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            _txtUserRole.text = @"Restaurant Owner";
            userRole = @"owner";
            
        }]];
        
        [self.view endEditing:YES];
        [self presentViewController:actionSheet animated:NO completion:nil];
        return NO;
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    if (textField == _txtConfirmPassword) {
        if ([textField hasText]) {
            // Confirm password not empty
        }
        else
        {
            
        }
    }
}
- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    [self.view setFrame:CGRectMake(0,-52,self.view.frame.size.width,self.view.frame.size.height)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
}


- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}


#pragma mark Image Picker delegate methods

- (IBAction)tappedToSelectImage:(UITapGestureRecognizer *)tapRecognizer {
    
    
    if (tapRecognizer.state == UIGestureRecognizerStateEnded)
    {
        CGFloat frameHeight = _userImage.frame.size.height;
        CGRect imageViewFrame = CGRectInset(_userImage.bounds, 0.0, (CGRectGetHeight(_userImage.frame) - frameHeight) / 2.0 );
        BOOL userTappedOnimageView = (CGRectContainsPoint(imageViewFrame, [tapRecognizer locationInView:_userImage]));
        if (userTappedOnimageView)
        {
            [self selectPhotos];
            
        }
        
    }
    
    
}
- (void)selectPhotos {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Photos" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
        picker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
        
        [self presentViewController:picker animated:NO completion:nil];
        
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
        
        [self presentViewController:picker animated:NO completion:nil];
        
    }]];
    
    [self presentViewController:actionSheet animated:NO completion:nil];
    
}

// Image Picker Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *originalImage =  info[UIImagePickerControllerOriginalImage];
    _userImage.image = originalImage;
    
    UIImage *tmpImage = [self resizeImage:originalImage];
    
    //NSString *imageString = [self encodeToBase64String:tmpImage];
    userImageString = [self encodeToBase64String:tmpImage];
    
    
    

    [self dismissViewControllerAnimated:NO completion:nil];
}

- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginButtonPressed:(id)sender {
    
    [self.navigationController popViewControllerAnimated:NO];
    
}

- (IBAction)signupButtonPressed:(id)sender {
    
    if (userRole.length <= 0)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"User Role required"
                                           message:@"Please select your role"                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:NO completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:NO completion:nil];
            });
            
        });

    }
    else if ([_txtUsername.text isEqualToString:@""])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"User name required"
                                           message:@"Please enter user name"                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:NO completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:NO completion:nil];
            });
            
        });
    }
    
    else if ([_txtEmailID.text isEqualToString:@""])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Email ID required"
                                           message:@"Please enter Email ID"                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:NO completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:NO completion:nil];
            });
            
        });
    }
    else if ([_txtPassword.text isEqualToString:@""])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Password required"
                                           message:@"Please enter password"                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:NO completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:NO completion:nil];
            });
            
        });
    }
    else if ([_txtConfirmPassword.text isEqualToString:@""])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Confirmed password required"
                                           message:@"Please enter confirmed password"                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:NO completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:NO completion:nil];
            });
            
        });
    }
    
    
    else if (![self isPasswordMatching:_txtPassword.text andConfirmPassword:_txtConfirmPassword.text]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Passwords do not match"
                                           message:@""
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:NO completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:NO completion:nil];
            });
            
            
            
        });
        
    }
    else 
    {
        
        [APP huddie];
        
        NSError *error;
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,SIGN_UP_URL]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setHTTPMethod:@"POST"];
        
        NSMutableDictionary *dataDict = [NSMutableDictionary new];
        
        NSString *strUserName = [_txtUsername.text lowercaseString];
       
        NSLog(@"User Name Converted To Lowercase:%@",strUserName);
        
        [dataDict setValue:strUserName forKey:@"username"];
        [dataDict setValue:_txtEmailID.text forKey:@"email"];
        [dataDict setValue:_txtPassword.text forKey:@"password"];
        [dataDict setValue:_txtReferralCode.text forKey:@"referralCode"];
        
//        [dataDict setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserRole"] forKey:@"role"];
        [dataDict setValue:userRole forKey:@"role"];
        
        NSString *deviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"DeviceToken"];
        [dataDict setValue:deviceToken forKey:@"deviceTokenForNotification"];
        [dataDict setValue:@"iOS" forKey:@"deviceType"];
        
//        AppUser *newUser;
//        
//        if(newUser.profilePic.length >0)
//        {
//        [dataDict setValue:newUser.profilePic forKey:@"image"];
//        }
        
//        if (userImageString.length > 0) {
//          [dataDict setValue:userImageString forKey:@"image"];
//        }
        
        
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:dataDict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        
        NSString *postStr;
        if (! postData) {
            NSLog(@"Got an error: %@", error);
            
        } else {
            NSString *jsonString = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
            
            postStr = [NSString stringWithFormat:@"[%@]",jsonString];
            
        }
        
        [request setHTTPBody:postData];
        
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            
            NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            NSLog(@"Response: %@",dataString);
            
            double status = (long)[httpResponse statusCode];
            NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
            if (data.length > 0) {
                
                NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                if (error != nil) {
                    NSLog(@"Error parsing JSON.");
                }
                else {
                    
                    
                    if (status == 202)
                    {
                        
                        NSDictionary *jsonDict = [dataDict valueForKey:@"data"];
                        
                        NSLog(@"Dict: %@", jsonDict);
                        [[NSUserDefaults standardUserDefaults] setValue:_txtEmailID.text forKey:@"ownerEmail"];
                        
                        [self clearTextFileds];
                        
                        NSString *userName = [jsonDict valueForKey:@"username"];
                        NSLog(@"User:%@",userName);
                        [[NSUserDefaults standardUserDefaults] setValue:userName forKey:@"username"];
                        
                        NSString *token = [jsonDict valueForKey:@"token"];
                        NSLog(@"Token:%@",token);
                        [[NSUserDefaults standardUserDefaults] setValue:token forKey:@"token"];
                        
                        NSString *role = [jsonDict valueForKey:@"role"];
                        NSLog(@"Role:%@",role);

                        
                        RLMRealm *realm = [RLMRealm defaultRealm];
                        
                        
                        [realm beginWriteTransaction];
//                        [realm deleteObjects:[AppUser allObjectsInRealm:realm]];
                        AppUser *newUser = [[AppUser allObjects] firstObject];
                        
                        if (newUser == nil) {
                            newUser = [[AppUser alloc] init];
                        }

                        newUser.name = [jsonDict valueForKey:@"username"];
                        
                        newUser.token = [jsonDict valueForKey:@"token"];
                        
                        newUser.role = [jsonDict valueForKey:@"role"];
                        
                        newUser.profileID = [[jsonDict valueForKey:@"profileID"] integerValue];
                        newUser.referralCode = [jsonDict valueForKey:@"referalCode"];
                        
                        [[NSUserDefaults standardUserDefaults] setValue:userName forKey:@"username"];
                        
                        [[NSUserDefaults standardUserDefaults] setValue:token forKey:@"token"];
                        
                        newUser.rewards = [[jsonDict valueForKey:@"rewardsInDoller"] integerValue];
                        
                        newUser.isOwnerAccountUpdated = [[jsonDict valueForKey:@"isOwnerAccountUpdated"] integerValue];
                        newUser.isServerAccountUpdated = [[jsonDict valueForKey:@"isServerAccountUpdated"] integerValue];
                        newUser.isVerified = [[jsonDict valueForKey:@"isVerified"] integerValue];
                        
                        
                        
                        
                        
                        @try {
                            [realm addObject:newUser];
                            [realm commitWriteTransaction];
                        } @catch (NSException *exception) {
                            NSLog(@"Sign Up Exception:%@",exception.description);
                        }
                        
                        
                        [activityView stopAnimating];
                        
                        if([[[NSUserDefaults standardUserDefaults]valueForKey:@"Sender"] isEqualToString:@"Home"])
                        {
                            
                            
                        }
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"Registration Successful"
                                                           message:[jsonDict valueForKey:@"result"]
                                                           preferredStyle:UIAlertControllerStyleAlert];
                            [APP.hud setHidden:YES];
                            [self presentViewController:alert2 animated:NO completion:nil];
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                                
                               
                                if ([role isEqualToString:@"customer"]) {
                                    
                                    [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"isuserloggedin"];
                                    
                                    [[NSUserDefaults standardUserDefaults]setObject:@"customer" forKey:@"role"];
                                    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Sender"] isEqualToString:@"Home"]){
                                        
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [APP.hud setHidden:YES];
                                            
                                            UIAlertController * alert2=   [UIAlertController
                                                                           alertControllerWithTitle:@"Login Sucessful"
                                                                           message:@""
                                                                           preferredStyle:UIAlertControllerStyleAlert];
                                            
                                            [self presentViewController:alert2 animated:NO completion:nil];
                                            
                                            
                                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                                
                                             
                                                [self.view endEditing:YES];
                                                
                                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                                                [[LocalData sharedInstance] FetchAllRestaurants:^(BOOL result) {
                                                    if (result)
                                                    {
                                                        HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                                                        [self.navigationController pushViewController:home animated:NO];
                                                    }
                                                    else{
                                                        HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                                                        [self.navigationController pushViewController:home animated:NO];
                                                    }
                                                    
                                                    
                                                    
                                                }];

                                                //                                    [self.navigationController popViewControllerAnimated:NO];
                                                //                                    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
                                            });
                                            
                                        });
                                        
                                        
                                    }
                                    
                                    else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Sender"] isEqualToString:@"Comment"]) {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            
                                            UIAlertController * alert2=   [UIAlertController
                                                                           alertControllerWithTitle:@"Login Sucessful"
                                                                           message:@""
                                                                           preferredStyle:UIAlertControllerStyleAlert];
                                            
                                            [self presentViewController:alert2 animated:NO completion:nil];
                                            
                                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                                
                                              
                                                [self.view endEditing:YES];
                                                
                                                [APP.hud setHidden:YES];
                                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                                                
                                                [[NSUserDefaults standardUserDefaults] setValue:@"Login" forKey:@"Sender"];
                                                 [[NSNotificationCenter defaultCenter] postNotificationName:@"dismissToComment" object:self];
                                                [self dismissViewControllerAnimated:NO completion:nil];
                                                
                                            });
                                            
                                        });
                                    }
                                    else {
                                        
                                        [[LocalData sharedInstance] FetchAllRestaurants:^(BOOL result) {
                                            if (result)
                                            {
                                                
                                                HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                                                [self.navigationController pushViewController:home animated:NO];
                                            }
                                            else{
                                                
                                                
                                                HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                                                [self.navigationController pushViewController:home animated:NO];
                                            }
                                            
                                        }];
                                        
                                        
                                    }
                                    
                                }
                                else if ([role isEqualToString:@"server"])
                                {
                                    [[NSUserDefaults standardUserDefaults]setValue:@"server" forKey:@"role"];
                                    
                                    [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"isuserloggedin"];
                                    
                                    [self fetchDataFromServer];
                                    
                                }
                                else if ([role isEqualToString:@"owner"])
                                {
                                    [[NSUserDefaults standardUserDefaults]setValue:@"owner" forKey:@"role"];
                                    
                                    [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"isuserloggedin"];
                                    
                                    [self goToOwnerProfile];
                                    
                                }
                                
                                
                            });
                            
                        });
                        NSLog(@"Successfully Registered");
                        
                    }
                    else if (status == 400){
                        
                         [activityView stopAnimating];
                        NSLog(@"Login Failed");
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"Error"
                                                           message:[dataDict valueForKey:@"result"]                                                           preferredStyle:UIAlertControllerStyleAlert];
                            
                            [APP.hud setHidden:YES];
                            [self presentViewController:alert2 animated:NO completion:nil];
                            
                            
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                [alert2 dismissViewControllerAnimated:NO completion:nil];
                            });
                        });
                        
                    }
                    
                    
                }
            }
            else {
                 [activityView stopAnimating];
                NSLog(@"Login Failed");
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController * alert2=   [UIAlertController
                                                   alertControllerWithTitle:@"Error"
                                                   message:error.localizedDescription
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    [APP.hud setHidden:YES];
                    [self presentViewController:alert2 animated:NO completion:nil];
                    
                    
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [alert2 dismissViewControllerAnimated:NO completion:nil];
                    });
                });
            }
            
            
        }];
        
        [postDataTask resume];
        
        self.txtPassword.text = @"";
        self.txtReferralCode.text = @"";
        self.txtConfirmPassword.text = @"";
        [self.userSelectionSegment setSelectedSegmentIndex:UISegmentedControlNoSegment];
        userRole = @"";
    }
    
    
}

- (IBAction)selectRolePressed:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Customer" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        _txtUserRole.text = @"Customer";
        
        userRole = @"customer";
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Bartender/Server" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        _txtUserRole.text = @"Bartender/Server";
        
        userRole = @"server";
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Restaurant Owner" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        _txtUserRole.text = @"Restaurant Owner";
        userRole = @"owner";
        
    }]];
    
    [self.view endEditing:YES];
    [self presentViewController:actionSheet animated:NO completion:nil];
}

- (void)clearTextFileds{
    dispatch_async(dispatch_get_main_queue(), ^{
    _txtUsername.text = @"";
    _txtEmailID.text = @"";
    _txtPassword .text = @"";
    _txtConfirmPassword.text = @"";
    _txtReferralCode.text = @"";
    });
}

- (IBAction)rememberMePressed:(id)sender {
    
    if (toggle) {
        [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"rememberMe"];
        NSLog(@"%d",toggle);
    }
    else{
        [[NSUserDefaults standardUserDefaults] setValue:@"no" forKey:@"rememberMe"];
        NSLog(@"%d",toggle);
    }
    toggle = !toggle;
    
    [_btnRemeberMe setImage:[UIImage imageNamed:toggle ? @"un-selected.png" :@"selected.png"] forState:UIControlStateNormal];
    
}

- (BOOL)isPasswordMatching:(NSString*)password andConfirmPassword:(NSString*)confirmPassword {
    if ([password isEqualToString:confirmPassword])
        return true;
    else
        return false;
    
}

-(UIImage *)resizeImage:(UIImage *)image {
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 300.0;
    float maxWidth = 400.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
    
}

#pragma mark - Fetch Data From Server
- (void)fetchDataFromServer {
    
    if ([self isNetworkAvailable]) {
        
        AppUser *user = [[AppUser allObjects] firstObject];
        
        [[LocalData sharedInstance] FetchRatingsAndComments:[NSString stringWithFormat:@"%ld",(long)user.profileID] token:user.token completion:^(BOOL result) {
            
            if (result)
            {
                NSLog(@"Login: Ratings and Comments Downloaded");
                [APP.hud setHidden:YES];
                
            }
            else{
                NSLog(@"Login: Error while fetching Ratings and Comments");
                [APP.hud setHidden:YES];
            }
            
        }];
        
        [[LocalData sharedInstance] FetchServerProfile:user.token completion:^(BOOL result) {
            
            if (result)
            {
                NSLog(@"Login: Server Profile Fetched");
                [APP.hud setHidden:YES];
                
            }
            else{
                NSLog(@"Login: Error while fetching Server Profile");
                [APP.hud setHidden:YES];
            }
            
            
        }];
        
        [[LocalData sharedInstance] FetchRestaurantsList:^(BOOL result) {
            
            if (result)
            {
                NSLog(@"Login: Resto List Fetched");
                [APP.hud setHidden:YES];
                
            }
            else{
                NSLog(@"Login: Error while fetching Resto List");
                [APP.hud setHidden:YES];
            }
            
            
            
        }];
        
        [[LocalData sharedInstance] GetAllAvailableJobs:user.token];
        
        
        
        if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"role"]isEqualToString:@"server"]){
            [APP.hud setHidden:YES];
            ProfileStep1ViewController *serverProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileStep1ViewController"];
            serverProfile.aSender = @"Register";
            serverProfile.navigationItem.leftBarButtonItem = nil;
            serverProfile.navigationItem.hidesBackButton = YES;
            [self.navigationController pushViewController:serverProfile animated:NO];
        }
        
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [APP.hud setHidden:YES];
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Error while connecting to Server"
                                           message:@""
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:NO completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                
                [alert2 dismissViewControllerAnimated:NO completion:nil];
                return ;
                
            });
            
        });
    }
    
}
- (bool)isNetworkAvailable {
    SCNetworkReachabilityFlags flags;
    SCNetworkReachabilityRef address;
    address = SCNetworkReachabilityCreateWithName(NULL, "www.apple.com" );
    Boolean success = SCNetworkReachabilityGetFlags(address, &flags);
    CFRelease(address);
    
    bool canReach = success
    && !(flags & kSCNetworkReachabilityFlagsConnectionRequired)
    && (flags & kSCNetworkReachabilityFlagsReachable);
    
    return canReach;
}

- (IBAction)userSegmentPressed:(UISegmentedControl *)sender {
    
    NSInteger selectedSegment = sender.selectedSegmentIndex;
    if (selectedSegment == 0)
    {
        userRole = @"server";
    }
    else if (selectedSegment == 1)
    {
        userRole = @"customer";
    }
    else
    {
        userRole = @"owner";
    }
}

- (void)goToOwnerProfile {
    
    [APP.hud setHidden:YES];
   
//    OwnerProfileStep1ViewController *ownerProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerProfileStep1ViewController"];
//    
//    ownerProfile.navigationItem.leftBarButtonItem = nil;
//    ownerProfile.navigationItem.hidesBackButton = YES;
//    [self.navigationController pushViewController:ownerProfile animated:NO];
    
    OwnerVerifyOTPViewController *verifyOTPVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerVerifyOTPViewController"];
    verifyOTPVC.navigationItem.leftBarButtonItem = nil;
    verifyOTPVC.navigationItem.hidesBackButton = YES;
    [self.navigationController pushViewController:verifyOTPVC animated:NO];
    
    
}

@end
