//
//  StringUtils.h
//  Restaurant
//
//  Created by Parth Pandya on 16/02/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StringUtils : NSObject
+ (CGFloat)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font;

@end
