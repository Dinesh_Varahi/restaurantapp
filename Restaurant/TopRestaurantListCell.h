//
//  TopRestaurantListCell.h
//  
//
//  Created by Parth Pandya on 25/12/16.
//
//

#import <UIKit/UIKit.h>
#import "Restaurants.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface TopRestaurantListCell : UITableViewCell<UICollectionViewDelegate, UICollectionViewDataSource>
{
    RLMResults<Restaurants  *> *RestroList;
}

@property (weak, nonatomic) IBOutlet UICollectionView *topRestaurantListCollection;
@end
