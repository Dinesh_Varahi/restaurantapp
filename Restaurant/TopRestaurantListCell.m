//
//  TopRestaurantListCell.m
//  
//
//  Created by Parth Pandya on 25/12/16.
//
//

#import "TopRestaurantListCell.h"
#import "ServerListCell.h"
#import "RestaurantListCell.h"
#import "RestaurantDetailsViewController.h"

@implementation TopRestaurantListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.topRestaurantListCollection.delegate = self;
    self.topRestaurantListCollection.dataSource = self;
    RestroList = [Restaurants allObjects];
    
//    NSLog(@"Restro List:%@",RestroList);
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


#pragma CollectionView Delegates
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return RestroList.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //collectionCell *cell = [[collectionCell alloc]init];
    
    RestaurantListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ServerListCell" forIndexPath:indexPath];
    
    cell.RestroRatings.value = [RestroList objectAtIndex:indexPath.row].avgRating;
    //cell.starRatings.value = [RestroList objectAtIndex:indexPath.row].avgRating;
    cell.lblRestroName.text = [[[RestroList objectAtIndex:indexPath.row] valueForKey:@"name"] capitalizedString];
    cell.lblCity.text = [[RestroList objectAtIndex:indexPath.row] valueForKey:@"city"];
    cell.imageview.layer.cornerRadius = cell.imageview.frame.size.height/2;
    cell.imageview.layer.masksToBounds = YES;

    [cell.imageview sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,[RestroList objectAtIndex:indexPath.row].imageURL]]
                      placeholderImage:[UIImage imageNamed:@"Restaurant.png"]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    
    Restaurants *newRestaurant = RestroList[indexPath.row];
    NSLog(@"Restro:%@",newRestaurant);
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%ld",(long)newRestaurant.restroID] forKey:@"RestroID"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"restaurantSelected" object:self];
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

//- (CGSize)collectionView:(UICollectionView *)collectionView
//                  layout:(UICollectionViewLayout *)collectionViewLayout
//  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    // Adjust cell size for orientation
//    
//    return CGSizeMake(self.superview.frame.size.width/2.67, self.superview.frame.size.height/2.8);
//    
//    
//    
//}

@end
