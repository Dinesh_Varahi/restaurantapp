//
//  TopServerListCell.h
//  Restaurant
//
//  Created by Parth Pandya on 25/12/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "Servers.h"

@interface TopServerListCell : UITableViewCell<UICollectionViewDataSource,UICollectionViewDelegate>
{
     RLMResults<Servers  *> *serverList;
}

@property (weak, nonatomic) IBOutlet UICollectionView *topServerListCollection;
@end
