//
//  TopServerListCell.m
//  Restaurant
//
//  Created by Parth Pandya on 25/12/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "TopServerListCell.h"
#import "ServerListCell.h"

@implementation TopServerListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.topServerListCollection.delegate = self;
    self.topServerListCollection.dataSource = self;
    serverList = [[Servers allObjects] sortedResultsUsingProperty:@"toalRatings" ascending:NO];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


#pragma CollectionView Delegates
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return serverList.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //collectionCell *cell = [[collectionCell alloc]init];
    
    ServerListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ServerListCell" forIndexPath:indexPath];
    
    cell.starRatings.value = [serverList objectAtIndex:indexPath.row].avgRating;
    cell.serverName.text = [[[serverList objectAtIndex:indexPath.row] valueForKey:@"serverName"] capitalizedString];
    cell.ratings.text = [NSString stringWithFormat:@"%@ Ratings",[[serverList objectAtIndex:indexPath.row] valueForKey:@"toalRatings"]];
    [cell.imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,[serverList objectAtIndex:indexPath.row].imageURL]]
                      placeholderImage:[UIImage imageNamed:@"ic_user_b.png"]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    Servers *newServer = serverList[indexPath.row];
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%ld",(long)newServer.serverID] forKey:@"ServerID"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"serverSelected" object:self];

    
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

@end
