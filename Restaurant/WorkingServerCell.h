//
//  WorkingServerCell.h
//  Restaurant
//
//  Created by Parth Pandya on 13/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface WorkingServerCell : UITableViewCell<UICollectionViewDataSource,UICollectionViewDelegate>
{
    NSMutableArray *arrName;
    NSMutableArray *arrImage;
    NSMutableArray *arrRatings;
    NSMutableArray *arrIds;
    NSMutableArray *totalRatings;
    

}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
