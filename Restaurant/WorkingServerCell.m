//
//  WorkingServerCell.m
//  Restaurant
//
//  Created by Parth Pandya on 13/01/17.
//  Copyright © 2017 Varahi. All rights reserved.
//

#import "WorkingServerCell.h"
#import "ServerInfoCell.h"

@implementation WorkingServerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    arrRatings = [[NSMutableArray alloc]init];
    arrImage = [[NSMutableArray alloc] init];
    arrIds = [[NSMutableArray alloc] init];
    arrName = [[NSMutableArray alloc] init];
    totalRatings = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(arrAssign:)
                                                 name:@"Employees"
                                               object:nil];

    
}

-(void)arrAssign:(NSNotification *)notification
{
    if ([notification.name isEqualToString:@"Employees"])
    {
        NSDictionary *dict = notification.userInfo;
        arrImage = [dict valueForKey:@"image"];
        arrRatings = [dict valueForKey:@"avgRating"];
        arrName = [dict valueForKey:@"name"];
        arrIds = [dict valueForKey:@"_idUserProfile"];
        totalRatings = [dict valueForKey:@"totalRatings"];
        [self.collectionView reloadData];
    }
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
          return arrIds.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ServerInfoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ServerinfoCell" forIndexPath:indexPath];
    
    cell.lblServerName1.text = [NSString upperCase:[arrName objectAtIndex:indexPath.row]];
    cell.Ratings1.value = [[arrRatings objectAtIndex:indexPath.row] integerValue];
    cell.lblRestoName1.text = [NSString upperCase:[[NSUserDefaults standardUserDefaults] valueForKey:@"RestoName"]];
    cell.lblTotalRatings1.text = [NSString stringWithFormat:@"%ld Ratings",(long)[[totalRatings objectAtIndex:indexPath.row] integerValue]];
    //cell.lblRestoName1.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"RestoName"];
    cell.imgServer.layer.cornerRadius = cell.imgServer.frame.size.height/2;
    cell.imgServer.layer.masksToBounds = YES;
    
    cell.imgServer1.layer.cornerRadius = cell.imgServer1.frame.size.height/2;
    cell.imgServer1.layer.masksToBounds = YES;

    [cell.imgServer1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,[arrImage objectAtIndex:indexPath.row]]]
                      placeholderImage:[UIImage imageNamed:@"ic_user_b.png"]];

    
    return cell;
    
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    [[NSUserDefaults standardUserDefaults] setValue:[arrIds objectAtIndex:indexPath.row] forKey:@"ServerID"];
 [[NSNotificationCenter defaultCenter] postNotificationName:@"goToServerDetail1VC" object:nil];   
}

@end
