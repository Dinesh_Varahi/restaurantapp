//
//  step1InfoCell.h
//  Restaurant
//
//  Created by Parth Pandya on 06/12/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MVPlaceSearchTextField.h"
#import "AddressResolver.h"

@class step1InfoCell;

@protocol ProfileStep1Delegate <NSObject,MLPAutoCompleteTextFieldDelegate>
-(void)step1Cell:(step1InfoCell*)view didEndEditingFirstNameTextField:(UITextField*)firstName;
-(void)step1Cell:(step1InfoCell*)view didEndEditingMiddleNameTextField:(UITextField*)middleName;
-(void)step1Cell:(step1InfoCell*)view didEndEditingLastNameTextField:(UITextField*)lastName;
-(void)step1Cell:(step1InfoCell*)view didEndEditingNickNameTextField:(UITextField*)nickName;
-(void)step1Cell:(step1InfoCell*)view didEndEditingEmailTextField:(UITextField*)email;
-(void)step1Cell:(step1InfoCell*)view didEndEditingCityNameTextField:(UITextField*)city;
-(void)step1Cell:(step1InfoCell*)view didEndEditingZipTextField:(UITextField*)zip;
-(void)step1Cell:(step1InfoCell*)view didEndEditingMobileNumberTextField:(UITextField*)mobileNumber;
-(void)step1Cell:(step1InfoCell*)view didEndEditingQualificationTextField:(UITextField*)qualification;
-(void)step1Cell:(step1InfoCell*)view didEndEditingSSNumberTextField:(UITextField*)ssNumber;

@end


@interface step1InfoCell : UITableViewCell<UITextFieldDelegate,UIGestureRecognizerDelegate, PlaceSearchTextFieldDelegate>
{
    id <ProfileStep1Delegate> delegate;
}
@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtMiddleName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtNickName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *txtCity;
@property (weak, nonatomic) IBOutlet UITextField *txtZip;
@property (weak, nonatomic) IBOutlet UITextField *txtQualification;
@property (weak, nonatomic) IBOutlet UITextField *txtSSNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnSaveAndContinue;
@property (weak, nonatomic) IBOutlet UITextField *txtMobileNumber;



@property (weak, nonatomic) IBOutlet UIImageView *imgProfilePic;
- (IBAction)btnSaveContinuePressed:(id)sender;

@property (nonatomic,weak) id<ProfileStep1Delegate> delegate;
@property (strong, nonatomic)NSDictionary *locationInfo;
@property (strong, nonatomic)NSString *strCity;

@end
