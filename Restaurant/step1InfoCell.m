//
//  step1InfoCell.m
//  Restaurant
//
//  Created by Parth Pandya on 06/12/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "step1InfoCell.h"
#import "AppDelegate.h"

@implementation step1InfoCell
{
    CLLocationCoordinate2D locationCoord;
}
@synthesize delegate = _delegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UITapGestureRecognizer *tapSelectedImage = [[UITapGestureRecognizer alloc]initWithTarget:self
                                                                                      action:@selector(tappedToSelectImage:)];
    
    [_txtFirstName setDelegate:self];
    [_txtMiddleName setDelegate:self];
    [_txtLastName setDelegate:self];
    [_txtNickName setDelegate:self];
    [_txtEmail setDelegate:self];
    [_txtMobileNumber setDelegate:self];
    [_txtCity setDelegate:self];
    [_txtZip setDelegate:self];
    [_txtQualification setDelegate:self];
    [_txtSSNumber setDelegate:self];
    
    
    self.imgProfilePic.layer.cornerRadius = self.imgProfilePic.frame.size.height/2;
    self.imgProfilePic.layer.masksToBounds = YES;
    
    tapSelectedImage.delegate = self;
    [tapSelectedImage setNumberOfTapsRequired:1];
    [tapSelectedImage setNumberOfTouchesRequired:1];
    [self.imgProfilePic addGestureRecognizer:tapSelectedImage];
    [self.imgProfilePic setUserInteractionEnabled:YES];
    
    self.txtCity.borderStyle = UITextBorderStyleNone;
    
    [self setupAutoCompleteTextField];
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1.5;
    border.borderColor = [UIColor blackColor].CGColor;
    border.frame = CGRectMake(0,self.txtCity.frame.size.height - borderWidth , self.txtCity.frame.size.width, self.txtCity.frame.size.height);
    border.borderWidth = borderWidth;
    [self.txtCity.layer addSublayer:border];
    self.txtCity.layer.masksToBounds = YES;
}

- (IBAction)tappedToSelectImage:(UITapGestureRecognizer *)tapRecognizer {
    
    [self saveServerInfo];
    
    if (tapRecognizer.state == UIGestureRecognizerStateEnded)
    {
        CGFloat frameHeight = self.imgProfilePic.frame.size.height;
        CGRect imageViewFrame = CGRectInset(self.imgProfilePic.bounds, 0.0, (CGRectGetHeight(self.imgProfilePic.frame) - frameHeight) / 2.0 );
        BOOL userTappedOnimageView = (CGRectContainsPoint(imageViewFrame, [tapRecognizer locationInView:self.imgProfilePic]));
        if (userTappedOnimageView)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"CaptureImage" object:self];
            
        }
    }
    
}

-(void)setupAutoCompleteTextField {
    
    _txtCity.placeSearchDelegate = self;
    
    _txtCity.strApiKey = kGoogleAPIKey;
    
    _txtCity.superViewOfList = self.superview;
    
    _txtCity.autoCompleteShouldHideOnSelection = YES;
    
    _txtCity.maximumNumberOfAutoCompleteRows  = 2;
    
    _txtCity.partOfAutoCompleteRowHeightToCut = 0.2;
    
    
}
- (void)saveServerInfo
{
    
    
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    Servers *server = [[Servers allObjects]firstObject];
    
    [realm beginWriteTransaction];
    server.firstName = self.txtFirstName.text;
    server.middleName = self.txtMiddleName.text;
    server.lastName = self.txtLastName.text;
    server.city = self.txtCity.text;
    server.nickName = self.txtNickName.text;
    server.zipCode = self.txtZip.text;
    server.qualification = self.txtQualification.text;
    server.email = self.txtEmail.text;
    server.mobileNumber = self.txtMobileNumber.text;
    server.ssNumber = self.txtSSNumber.text;
    [realm addObject:server];
    [realm commitWriteTransaction];

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    Servers *newServer = [[Servers alloc]init];;
    if (textField == _txtFirstName) {
        NSLog(@"FirstName:%@",_txtFirstName.text);
        [realm beginWriteTransaction];
        newServer.firstName = textField.text;
        [realm addObject:newServer];
        [realm commitWriteTransaction];
        [self.delegate step1Cell:self didEndEditingFirstNameTextField:textField];
    }
    else if (textField == _txtMiddleName) {
        NSLog(@"FirstName:%@",_txtMiddleName.text);
        [realm beginWriteTransaction];
        newServer.middleName = textField.text;
        [realm addObject:newServer];
        [realm commitWriteTransaction];
        [self.delegate step1Cell:self didEndEditingMiddleNameTextField:textField];
    }
    else if (textField == _txtLastName) {
        NSLog(@"FirstName:%@",_txtLastName.text);
        [realm beginWriteTransaction];
        newServer.lastName = textField.text;
        [realm addObject:newServer];
        [realm commitWriteTransaction];
        [self.delegate step1Cell:self didEndEditingLastNameTextField:textField];
    }
    else if (textField == _txtNickName) {
        NSLog(@"FirstName:%@",_txtNickName.text);
        [realm beginWriteTransaction];
        newServer.nickName = textField.text;
        [realm addObject:newServer];
        [realm commitWriteTransaction];
        [self.delegate step1Cell:self didEndEditingNickNameTextField:textField];
    }
    else if (textField == _txtEmail) {
        NSLog(@"FirstName:%@",_txtEmail.text);
        [realm beginWriteTransaction];
        newServer.email = textField.text;
        [realm addObject:newServer];
        [realm commitWriteTransaction];
        [self.delegate step1Cell:self didEndEditingEmailTextField:textField];
    }
    else if (textField == _txtCity) {
        NSLog(@"FirstName:%@",_txtCity.text);
//        NSString *location;

//        location = [[AddressResolver sharedInstance] validateAddress:_txtCity.text];
//        
//        _txtCity.text = location;
//        

//        [self.delegate step1Cell:self didEndEditingCityNameTextField:textField];
//        

        
        [realm beginWriteTransaction];
        newServer.city = textField.text;
        [realm addObject:newServer];
        [realm commitWriteTransaction];
        
    }
    else if (textField == _txtZip) {
        NSLog(@"FirstName:%@",_txtZip.text);
        [realm beginWriteTransaction];
        newServer.zipCode = textField.text;
        [realm addObject:newServer];
        [realm commitWriteTransaction];
        [self.delegate step1Cell:self didEndEditingZipTextField:textField];
    }
    else if (textField == _txtQualification) {
        NSLog(@"FirstName:%@",_txtQualification.text);
        [realm beginWriteTransaction];
        newServer.qualification = textField.text;
        [realm addObject:newServer];
        [realm commitWriteTransaction];
        [self.delegate step1Cell:self didEndEditingQualificationTextField:textField];
    }
    else if (textField == _txtSSNumber) {
        NSLog(@"FirstName:%@",_txtSSNumber.text);
        [realm beginWriteTransaction];
        newServer.ssNumber = textField.text;
        [realm addObject:newServer];
        [realm commitWriteTransaction];
        [self.delegate step1Cell:self didEndEditingSSNumberTextField:textField];
    }
    else if (textField == _txtMobileNumber) {
        
        NSLog(@"Mobile Number:%@",_txtMobileNumber.text);
        [realm beginWriteTransaction];
        newServer.mobileNumber = textField.text;
        [realm addObject:newServer];
        [realm commitWriteTransaction];
//        [self.delegate step1Cell:self didEndEditingSSNumberTextField:textField];
        [self.delegate step1Cell:self didEndEditingMobileNumberTextField:textField];
        
    }
    
}
- (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString *numberRegEx = @"[0-9]{10}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    if ([numberTest evaluateWithObject:phoneNumber] == YES)
        return TRUE;
    else
        return FALSE;

}


- (IBAction)btnSaveContinuePressed:(id)sender {
    
    if((_txtFirstName.text.length <= 0) && (_txtLastName.text.length <= 0) &&
       (_txtNickName.text.length <= 0) && (_txtEmail.text.length <= 0) && (_txtCity.text.length <= 0) && (_txtZip.text.length <= 0) &&  (_txtMobileNumber.text.length <= 0))
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"validateData" object:nil];
    }
    
    if (![NSString validatePhone:_txtMobileNumber.text]) {
        
        NSLog(@"Please validate Mobile Number");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"validateMobile1" object:nil];
        return;
    }
    
    [self saveServerInfo];
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"btnSavePressed" object:nil];
    
    
}

-(void)placeSearch:(MVPlaceSearchTextField*)textField ResponseForSelectedPlace:(GMSPlace*)responseDict {
    [self.superview endEditing:YES];
    
    
    NSLog(@"SELECTED ADDRESS :%@",responseDict);
    NSLog(@"City Name:%@",responseDict.types);
    
    locationCoord.latitude = responseDict.coordinate.latitude;
    locationCoord.longitude = responseDict.coordinate.longitude;
    
    if (responseDict)
    {
        _locationInfo = [[NSDictionary alloc] init];
        _locationInfo = [[AddressResolver sharedInstance] getAddressDetails:locationCoord];
        _txtCity.text = [_locationInfo valueForKey:@"city"];
        _txtZip.text = [_locationInfo valueForKey:@"zipcode"];
        NSLog(@"Address Details:City %@, ZIP %@",_txtCity.text,_txtZip.text);
    }
    
}
-(void)placeSearchWillShowResult:(MVPlaceSearchTextField*)textField {
    
}
-(void)placeSearchWillHideResult:(MVPlaceSearchTextField*)textField {
    
}
-(void)placeSearch:(MVPlaceSearchTextField*)textField ResultCell:(UITableViewCell*)cell withPlaceObject:(PlaceObject*)placeObject atIndex:(NSInteger)index {
//    if(index%2==0){
//        cell.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
//    }else{
//        cell.contentView.backgroundColor = [UIColor whiteColor];
//    }
}

@end
