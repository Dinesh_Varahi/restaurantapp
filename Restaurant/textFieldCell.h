//
//  textFieldCell.h
//  Restaurant
//
//  Created by Parth Pandya on 06/12/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ServerWorkExperience.h"
#import "MVPlaceSearchTextField.h"
#import "AddressResolver.h"
#import "MLPAutoCompleteTextFieldDelegate.h"
#import "AutocompletionTableView.h"
#import "DEMODataSource.h"
#import "Restaurants.h"
#import "MLPAutoCompleteTextField.h"



@protocol AutocompletionTableViewDelegate;
@class DEMODataSource;
@class MLPAutoCompleteTextField;

@interface textFieldCell : UITableViewCell<UITextFieldDelegate,AutocompletionTableViewDelegate,MLPAutoCompleteTextFieldDelegate,MLPAutoCompleteTextFieldDataSource, UIPickerViewDelegate, UIPickerViewDataSource,PlaceSearchTextFieldDelegate>
{
    RLMResults<ServerWorkExperience  *> *arrServerWorkExperience;
    RLMResults<Restaurants  *> *restaurantsArray;
    NSString *restoID;
    NSArray *arr;

    NSDictionary *serverInfo;
    NSMutableArray *arrNames;
    NSMutableArray *arrCity;
    NSMutableArray *arrIDs;
    NSDate *FromDate;
    NSDate *ToDate;
}

- (IBAction)swchIsCurrentJob:(id)sender;
@property (strong, nonatomic) IBOutlet DEMODataSource *autocompleteDataSource;
@property (weak) IBOutlet MLPAutoCompleteTextField *autocompleteTextField;

@property (weak, nonatomic) IBOutlet MLPAutoCompleteTextField *txtRestaurentName;
@property (nonatomic, strong) AutocompletionTableView *autoCompleter;
@property (weak, nonatomic) IBOutlet UITextField *txtRestaurentType;
@property (weak, nonatomic) IBOutlet UITextField *txtWorkProfile;
@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *txtCity;
@property (weak, nonatomic) IBOutlet UITextField *txtZIPCode;
@property (weak, nonatomic) IBOutlet UITextField *txtFromDate;
@property (weak, nonatomic) IBOutlet UITextField *txtToDate;
@property (weak, nonatomic) IBOutlet UISwitch *swchCurrentJob;
@property (weak, nonatomic) IBOutlet UIButton *btnAddMoreJob;
@property (weak, nonatomic) IBOutlet UILabel *lblPreviousEmployer;
@property (nonatomic, strong) NSDate *selectedTime;
@property (nonatomic, strong) NSDate *fromDate;
@property (nonatomic, strong) NSDate *toDate;
@property (weak, nonatomic) IBOutlet UIButton *btnFromDate;
@property (weak, nonatomic) IBOutlet UIButton *btnToDate;


- (IBAction)btnAddMoreJobPressed:(id)sender;

- (IBAction)btnFromDatePressed:(id)sender;
- (IBAction)btnToDatePressed:(id)sender;

- (IBAction)selectDate:(id)sender;



@end
