//
//  textFieldCell.m
//  Restaurant
//
//  Created by Parth Pandya on 06/12/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "textFieldCell.h"
#import "ProfileStep2ViewController.h"
#import "MLPAutoCompleteTextField.h"
#import "DEMOCustomAutoCompleteCell.h"
#import "DEMOCustomAutoCompleteObject.h"
#import "DEMODataSource.h"
#import <QuartzCore/QuartzCore.h>
#import "ActionSheetDatePicker.h"

@implementation textFieldCell
{
    
    CLLocationCoordinate2D locationCoord;
    NSString *location;
    
}

@synthesize autoCompleter = _autoCompleter;


- (AutocompletionTableView *)autoCompleter
{
    if (!_autoCompleter)
    {
        NSMutableDictionary *options = [NSMutableDictionary dictionaryWithCapacity:2];
        [options setValue:[NSNumber numberWithBool:YES] forKey:ACOCaseSensitive];
        [options setValue:nil forKey:ACOUseSourceFont];
        
        ProfileStep2ViewController *PS2VC = [[ProfileStep2ViewController alloc]init];
        _autoCompleter = [[AutocompletionTableView alloc] initWithTextField:_txtRestaurentName inViewController:PS2VC withOptions:options];
        _autoCompleter.autoCompleteDelegate = self;
        _autoCompleter.suggestionsDictionary = [NSArray arrayWithObjects:@"hostel",@"caret",@"carrot",@"house",@"horse", nil];
    }
    _autoCompleter.layer.zPosition = 1;
    return _autoCompleter;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    _txtRestaurentName.delegate = self;
    _txtCity.delegate = self;
    _txtRestaurentType.delegate = self;
    _txtWorkProfile.delegate = self;
    _txtZIPCode.delegate = self;
    _txtFromDate.delegate = self;
    _txtToDate.delegate = self;
    
    arrNames = [[NSMutableArray alloc]init];
    arrServerWorkExperience = [ServerWorkExperience allObjects];
    restaurantsArray = [Restaurants allObjects];
    
    if (arrServerWorkExperience.count > 0)
    {
        self.lblPreviousEmployer.hidden = NO;
    }
    
    [_txtFromDate addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingDidBegin];
    [_txtToDate addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingDidBegin];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(EditPressed)
                                                 name:@"EditPressed"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setWorkProfile)
                                                 name:@"workProfile"
     
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setRestaurantType)
                                                 name:@"restaurantTypeSelected"
     
                                               object:nil];//restaurantTypeSelected
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(assignDict:)
                                                 name:@"serverDictFetched"
                                               object:nil];
    
    
    [_txtRestaurentName addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];

    self.txtRestaurentName.autoCompleteDelegate = self;
    self.txtRestaurentName.delegate = self;
    self.txtWorkProfile.delegate = self;
    
    self.txtCity.borderStyle = UITextBorderStyleNone;
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1.5;
    border.borderColor = [UIColor blackColor].CGColor;
    border.frame = CGRectMake(0,self.txtCity.frame.size.height - borderWidth , self.txtCity.frame.size.width + 50, self.txtCity.frame.size.height);
    border.borderWidth = borderWidth;
    [self.txtCity.layer addSublayer:border];
    self.txtCity.layer.masksToBounds = YES;
    
    self.txtRestaurentName.borderStyle = UITextBorderStyleNone;
    
    CALayer *border1 = [CALayer layer];
    CGFloat borderWidth1 = 1.5;
    border1.borderColor = [UIColor blackColor].CGColor;
    border1.frame = CGRectMake(0,self.txtRestaurentName.frame.size.height - borderWidth1 , self.txtRestaurentName.frame.size.width + 50, self.txtRestaurentName.frame.size.height);
    border1.borderWidth = borderWidth1;
    [self.txtRestaurentName.layer addSublayer:border1];
    self.txtRestaurentName.layer.masksToBounds = YES;
    
    
    self.txtRestaurentName.autoCompleteTableCellBackgroundColor = [UIColor whiteColor];
    self.txtRestaurentName.autoCompleteShouldHideOnSelection = YES;
    self.txtRestaurentName.autoCompleteShouldHideClosingKeyboard = YES;
    
    [self setupAutoCompleteTextField];
    
    
    [self.txtRestaurentName addTarget:self
                               action:@selector(typeDidChange:)
                     forControlEvents:UIControlEventValueChanged];
    
    [self.autocompleteTextField setBorderStyle:UITextBorderStyleRoundedRect];
    
    if ([[[UIDevice currentDevice] systemVersion] compare:@"6.0" options:NSNumericSearch] != NSOrderedAscending) {
        [self.txtRestaurentName registerAutoCompleteCellClass:[DEMOCustomAutoCompleteCell class]
                                       forCellReuseIdentifier:@"CustomCellId"];
    }
    else{
        //Turn off bold effects on iOS 5.0 as they are not supported and will result in an exception
        self.txtRestaurentName.applyBoldEffectToAutoCompleteSuggestions = NO;
    }
    
    
    /*
     _txtFromDate.rightViewMode = UITextFieldViewModeAlways;
     _txtFromDate.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"calendar.png"]];
     
     _txtToDate.rightViewMode = UITextFieldViewModeAlways;
     _txtToDate.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"calendar.png"]];
     
     UIToolbar *toolbar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,-44,[[UIScreen mainScreen] bounds].size.width,44)];
     toolbar.barStyle = UIBarStyleDefault;
     
     UIToolbar *toolbar1= [[UIToolbar alloc] initWithFrame:CGRectMake(0,-44,[[UIScreen mainScreen] bounds].size.width,44)];
     toolbar.layer.zPosition = 1;
     toolbar1.barStyle = UIBarStyleDefault;
     
     UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(closeDialouge)];
     
     UIBarButtonItem* doneButton1 = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(closeDialouge)];
     
     toolbar.items = @[doneButton];
     toolbar1.items = @[doneButton1];
     
     
     UIDatePicker *datePicker = [[UIDatePicker alloc]init];
     [datePicker setDate:[NSDate date]];
     datePicker.datePickerMode = UIDatePickerModeDate;
     [datePicker addTarget:self action:@selector(FromdateTextField:) forControlEvents:UIControlEventValueChanged];
     [self.txtFromDate setInputView:datePicker];
     
     UIDatePicker *datePicker1 = [[UIDatePicker alloc]init];
     [datePicker1 setDate:[NSDate date]];
     datePicker1.datePickerMode = UIDatePickerModeDate;
     [datePicker1 addTarget:self action:@selector(TodateTextField:) forControlEvents:UIControlEventValueChanged];
     [self.txtToDate setInputView:datePicker1];
     
     datePicker.layer.zPosition = 1;
     [datePicker addSubview:toolbar];
     
     
     
     [datePicker addSubview:toolbar];
     [datePicker1 addSubview:toolbar1];
     
     */
}

-(void)assignDict:(NSNotification *)notification
{
    if ([notification.name isEqualToString:@"serverDictFetched"])
    {
        serverInfo = notification.userInfo;
        arrNames = [serverInfo valueForKey:@"name"];
        arrCity = [serverInfo valueForKey:@"city"];
        arrIDs = [serverInfo valueForKey:@"_id"];
    }
    
}

-(void)setRestaurantType
{
    self.txtRestaurentType.text = [[NSUserDefaults standardUserDefaults]valueForKey:@"restaurantTypeSelected"];
}

-(void)setWorkProfile{
    
    self.txtWorkProfile.text = [[NSUserDefaults standardUserDefaults]valueForKey:@"workProfile"];
    
}

- (void)typeDidChange:(UISegmentedControl *)sender
{
    if(sender.selectedSegmentIndex == 0){
        [self.txtRestaurentName setAutoCompleteTableAppearsAsKeyboardAccessory:NO];
    } else {
        [self.txtRestaurentName setAutoCompleteTableAppearsAsKeyboardAccessory:YES];
    }
    
}

-(void)setupAutoCompleteTextField {
    
    _txtCity.placeSearchDelegate = self;
    
    _txtCity.strApiKey = kGoogleAPIKey;
    
    _txtCity.superViewOfList = self.superview;
    
    _txtCity.autoCompleteShouldHideOnSelection = YES;
    
    _txtCity.maximumNumberOfAutoCompleteRows  = 2;
    
    _txtCity.partOfAutoCompleteRowHeightToCut = 0.2;
    
    
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self closeDialouge];
}

-(void)closeDialouge
{
    //[self.superview endEditing:YES];
    if (_swchCurrentJob.on)
    {
        self.txtToDate.text = @"";
    }
    [self.txtFromDate resignFirstResponder];
    [self.txtToDate resignFirstResponder];
    [self.txtCity resignFirstResponder];
    [self.txtZIPCode resignFirstResponder];
    [self.txtRestaurentType resignFirstResponder];
}


-(void) FromdateTextField:(id)sender
{
}

-(void) TodateTextField:(id)sender
{
}

-(void)textDidChange:(id)sender
{
//    UITextField *textField = (UITextField*)sender;
//    
//    if (textField == _txtFromDate) {
//        
//        
//    }
    
    [[NSUserDefaults standardUserDefaults] setValue:_txtRestaurentName.text forKey:@"NameOfRestro"];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


-(void)EditPressed {
    
    int index = [[[NSUserDefaults standardUserDefaults]valueForKey:@"editTag"] intValue];
    arrServerWorkExperience = [ServerWorkExperience allObjects];
    
    self.txtRestaurentName.text = [[arrServerWorkExperience objectAtIndex:index] valueForKey:@"restoName"];
    restoID = [[arrServerWorkExperience objectAtIndex:index] valueForKey:@"workingRestroID"];
    self.txtRestaurentType.text = [[arrServerWorkExperience objectAtIndex:index] valueForKey:@"restoType"];
    self.txtCity.text = [[arrServerWorkExperience objectAtIndex:index] valueForKey:@"city"];
    self.txtZIPCode.text = [[arrServerWorkExperience objectAtIndex:index] valueForKey:@"zipCode"];
    self.txtWorkProfile.text = [[arrServerWorkExperience objectAtIndex:index] valueForKey:@"workProfile"];
    self.txtFromDate.text = [[arrServerWorkExperience objectAtIndex:index] valueForKey:@"fromDate"];
    self.txtToDate.text = [[arrServerWorkExperience objectAtIndex:index] valueForKey:@"toDate"];
    restoID = [[arrServerWorkExperience objectAtIndex:index] valueForKey:@"workingRestroID"];
    
    if ([[[arrServerWorkExperience objectAtIndex:index] valueForKey:@"isCurrentJob"] isEqualToString:@"YES"])
    {
        self.swchCurrentJob.on = YES;
        
        self.txtToDate.text = @"";
        self.txtToDate.userInteractionEnabled = NO;
    }
    else
    {
        self.swchCurrentJob.on = NO;
        
        self.txtToDate.userInteractionEnabled = YES;
    }
    [self.btnAddMoreJob setTitle:@"UPDATE THIS JOB" forState:UIControlStateNormal];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    //    if (textField == _txtWorkProfile)
    //    {
    //        [textField endEditing:YES];
    //        [textField resignFirstResponder];
    //        [[NSNotificationCenter defaultCenter] postNotificationName:@"actionSheet" object:self];
    //    }
    //
    
    
}



- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if (textField == _txtWorkProfile)
    {
        [textField endEditing:YES];
        [textField resignFirstResponder];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"actionSheet" object:self];
        return NO;
    }
    else if (textField == _txtRestaurentType)
    {
        [textField resignFirstResponder];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RestaurantTypes" object:self];
        return NO;
        
    }
    if (textField == _txtFromDate)
    {
        [textField resignFirstResponder];
        return NO;
    }
    if( textField == _txtToDate)
    {
        [textField resignFirstResponder];
        return NO;
    }
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason
{
    
    if (textField == _txtCity) {
        NSLog(@"City:%@",_txtCity.text);
       
        
        //        if (_txtCity.text.length > 0) {
        //            location = [[AddressResolver sharedInstance] validateAddress:_txtCity.text];
        //
        //            _txtCity.text = location;
        //        }
    }
    
    
    [textField resignFirstResponder];
}
- (BOOL)textFieldShouldReturn:(UITextField*)aTextField {
    [aTextField resignFirstResponder];
    return YES;
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    
//    if (textField == _txtRestaurentName)
//    {
//        [[NSUserDefaults standardUserDefaults] setValue:string forKey:@"NameOfRestro"];
//    }
//    {
//    }
//    return YES;
//}

-(BOOL)isValidPinCode:(NSString*)pincode
{
    NSString *pinRegex = @"^\\d{5}(-\\d{4})?$";
    NSPredicate *pinTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pinRegex];
    BOOL pinValidates = [pinTest evaluateWithObject:pincode];
    return pinValidates;
}


-(void)textFieldDidChange :(UITextField *)theTextField{
    
    if (theTextField == _txtRestaurentName)
    {
        
    }
    NSLog( @"text changed: %@", theTextField.text);
}

- (NSArray*) autoCompletion:(AutocompletionTableView*) completer suggestionsFor:(NSString*) string{
    // with the prodided string, build a new array with suggestions - from DB, from a service, etc.
    return [NSArray arrayWithObjects:@"Ethnic",@"Fast Food",@"Fast Casuals",@"house",@"horse", nil];
}

- (void) autoCompletion:(AutocompletionTableView*) completer didSelectAutoCompleteSuggestionWithIndex:(NSInteger) index{
    // invoked when an available suggestion is selected
    NSLog(@"%@ - Suggestion chosen: %ld", completer, (long)index);
}


#pragma mark - MLPAutoCompleteTextField Delegate

- (BOOL)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
shouldStyleAutoCompleteTableView:(UITableView *)autoCompleteTableView
               forBorderStyle:(UITextBorderStyle)borderStyle;
{
    
    
    
    return YES;
}


- (BOOL)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
          shouldConfigureCell:(UITableViewCell *)cell
       withAutoCompleteString:(NSString *)autocompleteString
         withAttributedString:(NSAttributedString *)boldedString
        forAutoCompleteObject:(id<MLPAutoCompletionObject>)autocompleteObject
            forRowAtIndexPath:(NSIndexPath *)indexPath;
{
    //This is your chance to customize an autocomplete tableview cell before it appears in the autocomplete tableview
    cell.backgroundColor = [UIColor lightGrayColor];
    
    return YES;
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
  didSelectAutoCompleteString:(NSString *)selectedString
       withAutoCompleteObject:(id<MLPAutoCompletionObject>)selectedObject
            forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(selectedObject){
        NSLog(@"selected object from autocomplete menu %@ with string %@", selectedObject, [selectedObject autocompleteString]);
    } else {
        NSLog(@"selected string '%@' from autocomplete menu", selectedString);
        
        NSInteger Integer = [arrNames indexOfObject:selectedString];
        restoID = [arrIDs objectAtIndex:Integer];
        
        
    }
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField willHideAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view will be removed from the view hierarchy");
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField willShowAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view will be added to the view hierarchy");
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField didHideAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view ws removed from the view hierarchy");
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField didShowAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view was added to the view hierarchy");
}

#pragma mark - Address Autocomplete Delegate Method

-(void)placeSearch:(MVPlaceSearchTextField*)textField ResponseForSelectedPlace:(GMSPlace*)responseDict {
    [self.superview endEditing:YES];
    
    NSDictionary *locationInfo = [[NSDictionary alloc] init];
    
    NSLog(@"SELECTED ADDRESS :%@",responseDict);
    NSLog(@"City Name:%@",responseDict.types);
    
    locationCoord.latitude = responseDict.coordinate.latitude;
    locationCoord.longitude = responseDict.coordinate.longitude;
    
    if (responseDict)
    {
        locationInfo = [[NSDictionary alloc] init];
        locationInfo = [[AddressResolver sharedInstance] getAddressDetails:locationCoord];
        _txtCity.text = [locationInfo valueForKey:@"city"];
        location = [locationInfo valueForKey:@"city"];
        _txtZIPCode.text = [locationInfo valueForKey:@"zipcode"];
        NSLog(@"Address Details:City %@, ZIP %@",_txtCity.text,_txtZIPCode.text);
    }
    
}

- (IBAction)btnFromDatePressed:(id)sender {
}

- (IBAction)btnToDatePressed:(id)sender {
}

- (IBAction)selectDate:(id)sender {
    
    UIButton *button = (UIButton*)sender;
    
    self.selectedTime = [NSDate date];
    
    ActionSheetDatePicker *aDatePicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select Date" datePickerMode:UIDatePickerModeDate  selectedDate:self.selectedTime target:self action:@selector(timeWasSelected:element:) origin:sender cancelAction:@selector(cancelTimeSelection:)];
    
    if (button.tag == 0)
    {
        aDatePicker.maximumDate = [NSDate date];
    }
    if (button.tag == 1)
    {
        aDatePicker.minimumDate = self.fromDate;
    }
    
    [aDatePicker showActionSheetPicker];
    [aDatePicker setLocale:[NSLocale systemLocale]];
    
    
}

- (void)timeWasSelected:(NSDate *)selectedTime element:(id)element {
    self.selectedTime = selectedTime;
    NSDate *today = [NSDate date];
    NSString *createdTime;
    NSComparisonResult result;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    [dateFormatter setDateFormat:@"MM/yyyy"];
    createdTime = [dateFormatter stringFromDate:selectedTime];
    
    UIButton *button = (UIButton*)element;
    
    if (button.tag == 0)
    {
        
        result = [today compare:selectedTime];
        
        if (result == NSOrderedAscending) {
            // Selected Date is less than today
        }
        else{
            
            self.fromDate = selectedTime;
            _txtFromDate.text = createdTime;
        }
        
    }
    else if(button.tag == 1){
        
        result = [self.fromDate compare:selectedTime];
        
        if (result == NSOrderedDescending) {
            // Selected Date is less than today
            
        }
        else{
            
            self.toDate = selectedTime;
            _txtToDate.text = createdTime;
        }
        
        
    }
    
    
}
- (void)cancelTimeSelection:(id)sender {
    NSLog(@"Time selection canceled..");
}

- (IBAction)btnAddMoreJobPressed:(id)sender {
    
    if (_swchCurrentJob.on)
    {
        
        {
            if (_txtRestaurentName.text.length > 0 && _txtRestaurentType.text.length > 0 && _txtCity.text.length > 0 && [self isValidPinCode:_txtZIPCode.text] && _txtWorkProfile.text.length > 0 && _txtFromDate.text.length > 0)  {
                if ([_btnAddMoreJob.titleLabel.text isEqualToString:@"ADD JOB"])
                {
                    
                    RLMRealm *realm = [RLMRealm defaultRealm];
                    
                    ServerWorkExperience *Experiance = [[ServerWorkExperience alloc]init];
                    
                    [realm beginWriteTransaction];
                    
                    Experiance.restoName = self.txtRestaurentName.text;
                    Experiance.restoType = self.txtRestaurentType.text;
                    Experiance.workProfile = self.txtWorkProfile.text;
                    Experiance.city = self.txtCity.text;
                    Experiance.zipCode = self.txtZIPCode.text;
                    Experiance.fromDate = self.txtFromDate.text;
                    Experiance.toDate = self.txtToDate.text;
                    Experiance.workingRestroID = [restoID integerValue];
                    if (_swchCurrentJob.on)
                    {
                        Experiance.isCurrentJob = @"YES";
                        _txtToDate.text = @"";
                    }
                    else
                    {
                        Experiance.isCurrentJob = @"NO";
                        
                    }
                    
                    
                    _swchCurrentJob.on = NO;
                    
                    [realm addObject:Experiance];
                    
                    [realm commitWriteTransaction];
                    
                    self.txtRestaurentName.text = @"";
                    self.txtRestaurentType.text = @"";
                    self.txtCity.text = @"";
                    self.txtWorkProfile.text = @"";
                    self.txtZIPCode.text = @"";
                    self.txtToDate.text = @"";
                    self.txtFromDate.text = @"";
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTable" object:self];
                }
                else if ([_btnAddMoreJob.titleLabel.text isEqualToString:@"UPDATE THIS JOB"])
                {
                    int index = [[[NSUserDefaults standardUserDefaults]valueForKey:@"editTag"] intValue];
                    arrServerWorkExperience = [ServerWorkExperience allObjects];
                    
                    RLMRealm *realm = [RLMRealm defaultRealm];
                    [realm beginWriteTransaction];
                    [realm deleteObject:[arrServerWorkExperience objectAtIndex:index]];
                    [realm commitWriteTransaction];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTable" object:self];
                    
                    
                    ServerWorkExperience *Experiance = [[ServerWorkExperience alloc]init];
                    
                    [realm beginWriteTransaction];
                    
                    Experiance.restoName = self.txtRestaurentName.text;
                    Experiance.restoType = self.txtRestaurentType.text;
                    Experiance.workProfile = self.txtWorkProfile.text;
                    Experiance.city = self.txtCity.text;
                    Experiance.zipCode = self.txtZIPCode.text;
                    Experiance.fromDate = self.txtFromDate.text;
                    Experiance.toDate = self.txtToDate.text;
                    Experiance.workingRestroID = [restoID integerValue];
                    if (_swchCurrentJob.on)
                    {
                        Experiance.isCurrentJob = @"YES";
                    }
                    else
                    {
                        Experiance.isCurrentJob = @"NO";
                        
                    }
                    
                    
                    
                    
                    [realm addObject:Experiance];
                    
                    [realm commitWriteTransaction];
                    
                    self.txtRestaurentName.text = @"";
                    self.txtRestaurentType.text = @"";
                    self.txtCity.text = @"";
                    self.txtWorkProfile.text = @"";
                    self.txtZIPCode.text = @"";
                    self.txtToDate.text = @"";
                    self.txtFromDate.text = @"";
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTable" object:self];
                    
                    [self.btnAddMoreJob setTitle:@"ADD JOB" forState:UIControlStateNormal];
                    
                    
                    
                }
                
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"fillAllTheData" object:self];
            }
            
        }
    }
    else
    {
        
        if ([FromDate timeIntervalSinceDate:ToDate] > 0)
            
        {
            if ([self isValidPinCode:_txtZIPCode.text])
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"DateNotify" object:self];
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ZIPNotify" object:self];
            }
            
        }
        else
        {
            
            
            if (_txtRestaurentName.text.length > 0 && _txtRestaurentType.text.length > 0 && _txtCity.text.length > 0 && _txtZIPCode.text.length > 0 && _txtWorkProfile.text.length > 0 && _txtFromDate.text.length > 0 && _txtToDate.text.length > 0)  {
                
                
                if ([_btnAddMoreJob.titleLabel.text isEqualToString:@"ADD JOB"])
                {
                    
                    RLMRealm *realm = [RLMRealm defaultRealm];
                    
                    ServerWorkExperience *Experiance = [[ServerWorkExperience alloc]init];
                    
                    [realm beginWriteTransaction];
                    
                    Experiance.restoName = self.txtRestaurentName.text;
                    Experiance.restoType = self.txtRestaurentType.text;
                    Experiance.workProfile = self.txtWorkProfile.text;
                    Experiance.city = self.txtCity.text;
                    Experiance.zipCode = self.txtZIPCode.text;
                    Experiance.fromDate = self.txtFromDate.text;
                    Experiance.toDate = self.txtToDate.text;
                    Experiance.workingRestroID = [restoID integerValue];
                    if (_swchCurrentJob.on)
                    {
                        Experiance.isCurrentJob = @"YES";
                        _txtToDate.text = @"";
                    }
                    else
                    {
                        Experiance.isCurrentJob = @"NO";
                        
                    }
                    
                    
                    _swchCurrentJob.on = NO;
                    
                    [realm addObject:Experiance];
                    
                    [realm commitWriteTransaction];
                    
                    self.txtRestaurentName.text = @"";
                    self.txtRestaurentType.text = @"";
                    self.txtCity.text = @"";
                    self.txtWorkProfile.text = @"";
                    self.txtZIPCode.text = @"";
                    self.txtToDate.text = @"";
                    self.txtFromDate.text = @"";
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTable" object:self];
                }
                else if ([_btnAddMoreJob.titleLabel.text isEqualToString:@"UPDATE THIS JOB"])
                {
                    int index = [[[NSUserDefaults standardUserDefaults]valueForKey:@"editTag"] intValue];
                    arrServerWorkExperience = [ServerWorkExperience allObjects];
                    
                    RLMRealm *realm = [RLMRealm defaultRealm];
                    [realm beginWriteTransaction];
                    [realm deleteObject:[arrServerWorkExperience objectAtIndex:index]];
                    [realm commitWriteTransaction];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTable" object:self];
                    
                    
                    ServerWorkExperience *Experiance = [[ServerWorkExperience alloc]init];
                    
                    [realm beginWriteTransaction];
                    
                    Experiance.restoName = self.txtRestaurentName.text;
                    Experiance.restoType = self.txtRestaurentType.text;
                    Experiance.workProfile = self.txtWorkProfile.text;
                    Experiance.city = self.txtCity.text;
                    Experiance.zipCode = self.txtZIPCode.text;
                    Experiance.fromDate = self.txtFromDate.text;
                    Experiance.toDate = self.txtToDate.text;
                    if (_swchCurrentJob.on)
                    {
                        Experiance.isCurrentJob = @"YES";
                    }
                    else
                    {
                        Experiance.isCurrentJob = @"NO";
                        
                    }
                    
                    
                    
                    
                    [realm addObject:Experiance];
                    
                    [realm commitWriteTransaction];
                    
                    self.txtRestaurentName.text = @"";
                    self.txtRestaurentType.text = @"";
                    self.txtCity.text = @"";
                    self.txtWorkProfile.text = @"";
                    self.txtZIPCode.text = @"";
                    self.txtToDate.text = @"";
                    self.txtFromDate.text = @"";
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTable" object:self];
                    
                    [self.btnAddMoreJob setTitle:@"ADD JOB" forState:UIControlStateNormal];
                    
                    
                    
                }
                
                
                
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"fillAllTheData" object:self];
            }
        }
        
        
    }
}
- (IBAction)swchIsCurrentJob:(id)sender {
    
    UISwitch *swch = (UISwitch *)sender;
    if (swch.on)
    {
        _txtToDate.userInteractionEnabled = NO;
        _btnToDate.userInteractionEnabled = NO;
        _txtToDate.text = @"";
    }
    else
    {
        _txtToDate.userInteractionEnabled = YES;
        _btnToDate.userInteractionEnabled = YES;
    }
}
@end
